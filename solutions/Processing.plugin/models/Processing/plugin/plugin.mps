<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:5b8e823a-2997-4792-afd6-fcdb962117ca(Processing.plugin.plugin)">
  <persistence version="9" />
  <languages>
    <use id="28f9e497-3b42-4291-aeba-0a1039153ab1" name="jetbrains.mps.lang.plugin" version="0" />
    <use id="ef7bf5ac-d06c-4342-b11d-e42104eb9343" name="jetbrains.mps.lang.plugin.standalone" version="0" />
    <use id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel" version="2" />
    <use id="63650c59-16c8-498a-99c8-005c7ee9515d" name="jetbrains.mps.lang.access" version="0" />
    <use id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage" version="4" />
    <use id="d4280a54-f6df-4383-aa41-d1b2bffa7eb1" name="com.mbeddr.core.base" version="3" />
  </languages>
  <imports>
    <import index="h7pl" ref="r:3842f4d7-6c76-41af-bee7-8a752efd1444(Processing.structure)" />
    <import index="df3e" ref="r:730c9fb4-e4ce-43d9-91d3-aba1e15e4166(Processing.runtime)" />
    <import index="z60i" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.awt(JDK/)" />
    <import index="lzb2" ref="498d89d2-c2e9-11e2-ad49-6cf049e62fe5/java:com.intellij.ui(MPS.IDEA/)" />
    <import index="qkt" ref="498d89d2-c2e9-11e2-ad49-6cf049e62fe5/java:com.intellij.openapi.actionSystem(MPS.IDEA/)" />
    <import index="dxuu" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:javax.swing(JDK/)" />
    <import index="kt01" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.awt.datatransfer(JDK/)" />
    <import index="wyt6" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.lang(JDK/)" />
    <import index="w1kc" ref="6ed54515-acc8-4d1e-a16c-9fd6cfe951ea/java:jetbrains.mps.smodel(MPS.Core/)" />
    <import index="mhbf" ref="8865b7a8-5271-43d3-884c-6fd1d9cfdd34/java:org.jetbrains.mps.openapi.model(MPS.OpenAPI/)" />
    <import index="dush" ref="8865b7a8-5271-43d3-884c-6fd1d9cfdd34/java:org.jetbrains.mps.openapi.persistence(MPS.OpenAPI/)" />
    <import index="mw71" ref="6ed54515-acc8-4d1e-a16c-9fd6cfe951ea/java:jetbrains.mps.smodel.persistence.def.v9(MPS.Core/)" />
    <import index="z1c3" ref="6ed54515-acc8-4d1e-a16c-9fd6cfe951ea/java:jetbrains.mps.project(MPS.Core/)" />
    <import index="hwgx" ref="r:fd2980c8-676c-4b19-b524-18c70e02f8b7(com.mbeddr.core.base.behavior)" implicit="true" />
    <import index="tprs" ref="r:00000000-0000-4000-0000-011c895904a4(jetbrains.mps.ide.actions)" implicit="true" />
  </imports>
  <registry>
    <language id="28f9e497-3b42-4291-aeba-0a1039153ab1" name="jetbrains.mps.lang.plugin">
      <concept id="1207145163717" name="jetbrains.mps.lang.plugin.structure.ElementListContents" flags="ng" index="ftmFs">
        <child id="1207145201301" name="reference" index="ftvYc" />
      </concept>
      <concept id="1203071646776" name="jetbrains.mps.lang.plugin.structure.ActionDeclaration" flags="ng" index="sE7Ow">
        <property id="1207149998849" name="isAlwaysVisible" index="fJN8o" />
        <property id="1205250923097" name="caption" index="2uzpH1" />
        <child id="1203083196627" name="updateBlock" index="tmbBb" />
        <child id="1203083461638" name="executeFunction" index="tncku" />
        <child id="1217413222820" name="parameter" index="1NuT2Z" />
      </concept>
      <concept id="1203083511112" name="jetbrains.mps.lang.plugin.structure.ExecuteBlock" flags="in" index="tnohg" />
      <concept id="1203087890642" name="jetbrains.mps.lang.plugin.structure.ActionGroupDeclaration" flags="ng" index="tC5Ba">
        <property id="1204991940915" name="caption" index="2f7twF" />
        <property id="1213283637680" name="isPopup" index="1XlLyE" />
        <child id="1204991552650" name="modifier" index="2f5YQi" />
        <child id="1207145245948" name="contents" index="ftER_" />
      </concept>
      <concept id="1203088046679" name="jetbrains.mps.lang.plugin.structure.ActionInstance" flags="ng" index="tCFHf">
        <reference id="1203088061055" name="action" index="tCJdB" />
      </concept>
      <concept id="1203092361741" name="jetbrains.mps.lang.plugin.structure.ModificationStatement" flags="lg" index="tT9cl">
        <reference id="1203092736097" name="modifiedGroup" index="tU$_T" />
      </concept>
      <concept id="1205679047295" name="jetbrains.mps.lang.plugin.structure.ActionParameterDeclaration" flags="ig" index="2S4$dB" />
      <concept id="1205681243813" name="jetbrains.mps.lang.plugin.structure.IsApplicableBlock" flags="in" index="2ScWuX" />
      <concept id="1206092561075" name="jetbrains.mps.lang.plugin.structure.ActionParameterReferenceOperation" flags="nn" index="3gHZIF" />
      <concept id="5538333046911348654" name="jetbrains.mps.lang.plugin.structure.RequiredCondition" flags="ng" index="1oajcY" />
      <concept id="1204478074808" name="jetbrains.mps.lang.plugin.structure.ConceptFunctionParameter_MPSProject" flags="nn" index="1KvdUw" />
      <concept id="1217413147516" name="jetbrains.mps.lang.plugin.structure.ActionParameter" flags="ng" index="1NuADB">
        <child id="5538333046911298738" name="condition" index="1oa70y" />
      </concept>
    </language>
    <language id="ef7bf5ac-d06c-4342-b11d-e42104eb9343" name="jetbrains.mps.lang.plugin.standalone">
      <concept id="481983775135178834" name="jetbrains.mps.lang.plugin.standalone.structure.ProjectPluginDeclaration" flags="ng" index="2uRRBy">
        <child id="481983775135178836" name="initBlock" index="2uRRB$" />
        <child id="481983775135178837" name="disposeBlock" index="2uRRB_" />
      </concept>
      <concept id="481983775135178819" name="jetbrains.mps.lang.plugin.standalone.structure.ProjectPluginDisposeBlock" flags="in" index="2uRRBN" />
      <concept id="481983775135178825" name="jetbrains.mps.lang.plugin.standalone.structure.ProjectPluginInitBlock" flags="in" index="2uRRBT" />
      <concept id="7520713872864775836" name="jetbrains.mps.lang.plugin.standalone.structure.StandalonePluginDescriptor" flags="ng" index="2DaZZR" />
    </language>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1215693861676" name="jetbrains.mps.baseLanguage.structure.BaseAssignmentExpression" flags="nn" index="d038R">
        <child id="1068498886297" name="rValue" index="37vLTx" />
        <child id="1068498886295" name="lValue" index="37vLTJ" />
      </concept>
      <concept id="4836112446988635817" name="jetbrains.mps.baseLanguage.structure.UndefinedType" flags="in" index="2jxLKc" />
      <concept id="1202948039474" name="jetbrains.mps.baseLanguage.structure.InstanceMethodCallOperation" flags="nn" index="liA8E" />
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1197029447546" name="jetbrains.mps.baseLanguage.structure.FieldReferenceOperation" flags="nn" index="2OwXpG">
        <reference id="1197029500499" name="fieldDeclaration" index="2Oxat5" />
      </concept>
      <concept id="1145552977093" name="jetbrains.mps.baseLanguage.structure.GenericNewExpression" flags="nn" index="2ShNRf">
        <child id="1145553007750" name="creator" index="2ShVmc" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1070475926800" name="jetbrains.mps.baseLanguage.structure.StringLiteral" flags="nn" index="Xl_RD">
        <property id="1070475926801" name="value" index="Xl_RC" />
      </concept>
      <concept id="1081236700937" name="jetbrains.mps.baseLanguage.structure.StaticMethodCall" flags="nn" index="2YIFZM">
        <reference id="1144433194310" name="classConcept" index="1Pybhc" />
      </concept>
      <concept id="1070533707846" name="jetbrains.mps.baseLanguage.structure.StaticFieldReference" flags="nn" index="10M0yZ">
        <reference id="1144433057691" name="classifier" index="1PxDUh" />
      </concept>
      <concept id="1070534058343" name="jetbrains.mps.baseLanguage.structure.NullLiteral" flags="nn" index="10Nm6u" />
      <concept id="1068431474542" name="jetbrains.mps.baseLanguage.structure.VariableDeclaration" flags="ng" index="33uBYm">
        <property id="1176718929932" name="isFinal" index="3TUv4t" />
        <child id="1068431790190" name="initializer" index="33vP2m" />
      </concept>
      <concept id="1068498886296" name="jetbrains.mps.baseLanguage.structure.VariableReference" flags="nn" index="37vLTw">
        <reference id="1068581517664" name="variableDeclaration" index="3cqZAo" />
      </concept>
      <concept id="1068498886294" name="jetbrains.mps.baseLanguage.structure.AssignmentExpression" flags="nn" index="37vLTI" />
      <concept id="4972933694980447171" name="jetbrains.mps.baseLanguage.structure.BaseVariableDeclaration" flags="ng" index="19Szcq">
        <child id="5680397130376446158" name="type" index="1tU5fm" />
      </concept>
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123159" name="jetbrains.mps.baseLanguage.structure.IfStatement" flags="nn" index="3clFbJ">
        <child id="1068580123160" name="condition" index="3clFbw" />
        <child id="1068580123161" name="ifTrue" index="3clFbx" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068580123137" name="jetbrains.mps.baseLanguage.structure.BooleanConstant" flags="nn" index="3clFbT">
        <property id="1068580123138" name="value" index="3clFbU" />
      </concept>
      <concept id="1068580320020" name="jetbrains.mps.baseLanguage.structure.IntegerConstant" flags="nn" index="3cmrfG">
        <property id="1068580320021" name="value" index="3cmrfH" />
      </concept>
      <concept id="1068581242875" name="jetbrains.mps.baseLanguage.structure.PlusExpression" flags="nn" index="3cpWs3" />
      <concept id="1068581242878" name="jetbrains.mps.baseLanguage.structure.ReturnStatement" flags="nn" index="3cpWs6">
        <child id="1068581517676" name="expression" index="3cqZAk" />
      </concept>
      <concept id="1068581242864" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclarationStatement" flags="nn" index="3cpWs8">
        <child id="1068581242865" name="localVariableDeclaration" index="3cpWs9" />
      </concept>
      <concept id="1068581242863" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclaration" flags="nr" index="3cpWsn" />
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
        <child id="1068499141038" name="actualArgument" index="37wK5m" />
      </concept>
      <concept id="1212685548494" name="jetbrains.mps.baseLanguage.structure.ClassCreator" flags="nn" index="1pGfFk" />
      <concept id="1107535904670" name="jetbrains.mps.baseLanguage.structure.ClassifierType" flags="in" index="3uibUv">
        <reference id="1107535924139" name="classifier" index="3uigEE" />
      </concept>
      <concept id="1081773326031" name="jetbrains.mps.baseLanguage.structure.BinaryOperation" flags="nn" index="3uHJSO">
        <child id="1081773367579" name="rightExpression" index="3uHU7w" />
        <child id="1081773367580" name="leftExpression" index="3uHU7B" />
      </concept>
      <concept id="1073239437375" name="jetbrains.mps.baseLanguage.structure.NotEqualsExpression" flags="nn" index="3y3z36" />
      <concept id="1178549954367" name="jetbrains.mps.baseLanguage.structure.IVisible" flags="ng" index="1B3ioH">
        <child id="1178549979242" name="visibility" index="1B3o_S" />
      </concept>
      <concept id="5497648299878491908" name="jetbrains.mps.baseLanguage.structure.BaseVariableReference" flags="nn" index="1M0zk4">
        <reference id="5497648299878491909" name="baseVariableDeclaration" index="1M0zk5" />
      </concept>
      <concept id="1146644623116" name="jetbrains.mps.baseLanguage.structure.PrivateVisibility" flags="nn" index="3Tm6S6" />
      <concept id="1080120340718" name="jetbrains.mps.baseLanguage.structure.AndExpression" flags="nn" index="1Wc70l" />
    </language>
    <language id="443f4c36-fcf5-4eb6-9500-8d06ed259e3e" name="jetbrains.mps.baseLanguage.classifiers">
      <concept id="1205752633985" name="jetbrains.mps.baseLanguage.classifiers.structure.ThisClassifierExpression" flags="nn" index="2WthIp" />
      <concept id="1205756064662" name="jetbrains.mps.baseLanguage.classifiers.structure.IMemberOperation" flags="ng" index="2WEnae">
        <reference id="1205756909548" name="member" index="2WH_rO" />
      </concept>
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1179409122411" name="jetbrains.mps.lang.smodel.structure.Node_ConceptMethodCall" flags="nn" index="2qgKlT" />
      <concept id="1883223317721008708" name="jetbrains.mps.lang.smodel.structure.IfInstanceOfStatement" flags="nn" index="Jncv_">
        <reference id="1883223317721008712" name="nodeConcept" index="JncvD" />
        <child id="1883223317721008709" name="body" index="Jncv$" />
        <child id="1883223317721008711" name="variable" index="JncvA" />
        <child id="1883223317721008710" name="nodeExpression" index="JncvB" />
      </concept>
      <concept id="1883223317721008713" name="jetbrains.mps.lang.smodel.structure.IfInstanceOfVariable" flags="ng" index="JncvC" />
      <concept id="1883223317721107059" name="jetbrains.mps.lang.smodel.structure.IfInstanceOfVarReference" flags="nn" index="Jnkvi" />
      <concept id="1172008320231" name="jetbrains.mps.lang.smodel.structure.Node_IsNotNullOperation" flags="nn" index="3x8VRR" />
      <concept id="1140137987495" name="jetbrains.mps.lang.smodel.structure.SNodeTypeCastExpression" flags="nn" index="1PxgMI">
        <reference id="1140138128738" name="concept" index="1PxNhF" />
        <child id="1140138123956" name="leftExpression" index="1PxMeX" />
      </concept>
      <concept id="1138055754698" name="jetbrains.mps.lang.smodel.structure.SNodeType" flags="in" index="3Tqbb2" />
      <concept id="1138056143562" name="jetbrains.mps.lang.smodel.structure.SLinkAccess" flags="nn" index="3TrEf2">
        <reference id="1138056516764" name="link" index="3Tt5mk" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="2DaZZR" id="2yiuz3qjhA9" />
  <node concept="sE7Ow" id="2yiuz3qjiiR">
    <property role="TrG5h" value="CreateFont" />
    <property role="2uzpH1" value="Create Font" />
    <property role="fJN8o" value="true" />
    <node concept="tnohg" id="2yiuz3qjiiS" role="tncku">
      <node concept="3clFbS" id="2yiuz3qjiiT" role="2VODD2">
        <node concept="3cpWs8" id="2yiuz3qkz8E" role="3cqZAp">
          <node concept="3cpWsn" id="2yiuz3qkz8F" role="3cpWs9">
            <property role="TrG5h" value="tool" />
            <node concept="3uibUv" id="2yiuz3qkz8G" role="1tU5fm">
              <ref role="3uigEE" to="df3e:2yiuz3qhGhA" resolve="CreateFont" />
            </node>
            <node concept="2ShNRf" id="2yiuz3qkNXq" role="33vP2m">
              <node concept="1pGfFk" id="2yiuz3qjEoz" role="2ShVmc">
                <ref role="37wK5l" to="df3e:2yiuz3qhGir" resolve="CreateFont" />
                <node concept="2OqwBi" id="2yiuz3qjWYq" role="37wK5m">
                  <node concept="2OqwBi" id="2yiuz3qjU4_" role="2Oq$k0">
                    <node concept="2OqwBi" id="2yiuz3qjO15" role="2Oq$k0">
                      <node concept="1PxgMI" id="2yiuz3qjEzp" role="2Oq$k0">
                        <ref role="1PxNhF" to="h7pl:2yiuz3qbfwd" resolve="Sketch" />
                        <node concept="2OqwBi" id="2yiuz3qjEpN" role="1PxMeX">
                          <node concept="2WthIp" id="2yiuz3qjEpQ" role="2Oq$k0" />
                          <node concept="3gHZIF" id="2yiuz3qjEpS" role="2OqNvi">
                            <ref role="2WH_rO" node="2yiuz3qjiPy" resolve="cnode" />
                          </node>
                        </node>
                      </node>
                      <node concept="3TrEf2" id="2yiuz3qjR2P" role="2OqNvi">
                        <ref role="3Tt5mk" to="h7pl:2yiuz3qgZd0" />
                      </node>
                    </node>
                    <node concept="3TrEf2" id="2yiuz3qjWSj" role="2OqNvi">
                      <ref role="3Tt5mk" to="h7pl:2yiuz3qgTBd" />
                    </node>
                  </node>
                  <node concept="2qgKlT" id="2yiuz3qjZNp" role="2OqNvi">
                    <ref role="37wK5l" to="hwgx:5lKnBeAuKov" resolve="getCanonicalPath" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qkV52" role="3cqZAp">
          <node concept="2OqwBi" id="2yiuz3qkV96" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qkV50" role="2Oq$k0">
              <ref role="3cqZAo" node="2yiuz3qkz8F" resolve="tool" />
            </node>
            <node concept="liA8E" id="2yiuz3qkZ4J" role="2OqNvi">
              <ref role="37wK5l" to="df3e:2yiuz3qhGiD" resolve="init" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3ql64B" role="3cqZAp">
          <node concept="2OqwBi" id="2yiuz3ql68U" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3ql64_" role="2Oq$k0">
              <ref role="3cqZAo" node="2yiuz3qkz8F" resolve="tool" />
            </node>
            <node concept="liA8E" id="2yiuz3qla5X" role="2OqNvi">
              <ref role="37wK5l" to="df3e:2yiuz3qhGq9" resolve="run" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="2S4$dB" id="2yiuz3qjiPy" role="1NuT2Z">
      <property role="TrG5h" value="cnode" />
      <node concept="3Tm6S6" id="2yiuz3qjiPz" role="1B3o_S" />
      <node concept="1oajcY" id="2yiuz3qjiP$" role="1oa70y" />
      <node concept="3Tqbb2" id="2yiuz3qjiJ$" role="1tU5fm" />
    </node>
    <node concept="2ScWuX" id="2yiuz3qjCpU" role="tmbBb">
      <node concept="3clFbS" id="2yiuz3qjCpV" role="2VODD2">
        <node concept="Jncv_" id="2yiuz3qjEMD" role="3cqZAp">
          <ref role="JncvD" to="h7pl:2yiuz3qbfwd" resolve="Sketch" />
          <node concept="2OqwBi" id="2yiuz3qjEQj" role="JncvB">
            <node concept="2WthIp" id="2yiuz3qjEQm" role="2Oq$k0" />
            <node concept="3gHZIF" id="2yiuz3qjEQo" role="2OqNvi">
              <ref role="2WH_rO" node="2yiuz3qjiPy" resolve="cnode" />
            </node>
          </node>
          <node concept="3clFbS" id="2yiuz3qjEMH" role="Jncv$">
            <node concept="3cpWs6" id="2yiuz3qjFet" role="3cqZAp">
              <node concept="1Wc70l" id="2yiuz3qjGMA" role="3cqZAk">
                <node concept="2OqwBi" id="2yiuz3qjI8o" role="3uHU7w">
                  <node concept="2OqwBi" id="2yiuz3qjHSY" role="2Oq$k0">
                    <node concept="2OqwBi" id="2yiuz3qjH0l" role="2Oq$k0">
                      <node concept="Jnkvi" id="2yiuz3qjGPc" role="2Oq$k0">
                        <ref role="1M0zk5" node="2yiuz3qjEMJ" resolve="sketch" />
                      </node>
                      <node concept="3TrEf2" id="2yiuz3qjHsY" role="2OqNvi">
                        <ref role="3Tt5mk" to="h7pl:2yiuz3qgZd0" />
                      </node>
                    </node>
                    <node concept="3TrEf2" id="2yiuz3qjI1w" role="2OqNvi">
                      <ref role="3Tt5mk" to="h7pl:2yiuz3qgTBd" />
                    </node>
                  </node>
                  <node concept="2qgKlT" id="2yiuz3qjIlX" role="2OqNvi">
                    <ref role="37wK5l" to="hwgx:5lKnBeAuiv7" resolve="isValidDirectory" />
                  </node>
                </node>
                <node concept="2OqwBi" id="2yiuz3qjGzL" role="3uHU7B">
                  <node concept="2OqwBi" id="2yiuz3qjFDO" role="2Oq$k0">
                    <node concept="Jnkvi" id="2yiuz3qjFho" role="2Oq$k0">
                      <ref role="1M0zk5" node="2yiuz3qjEMJ" resolve="sketch" />
                    </node>
                    <node concept="3TrEf2" id="2yiuz3qjG5H" role="2OqNvi">
                      <ref role="3Tt5mk" to="h7pl:2yiuz3qgZd0" />
                    </node>
                  </node>
                  <node concept="3x8VRR" id="2yiuz3qjGFE" role="2OqNvi" />
                </node>
              </node>
            </node>
          </node>
          <node concept="JncvC" id="2yiuz3qjEMJ" role="JncvA">
            <property role="TrG5h" value="sketch" />
            <node concept="2jxLKc" id="2yiuz3qjEMK" role="1tU5fm" />
          </node>
        </node>
        <node concept="3cpWs6" id="2yiuz3qjF64" role="3cqZAp">
          <node concept="3clFbT" id="2yiuz3qjF89" role="3cqZAk">
            <property role="3clFbU" value="false" />
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="tC5Ba" id="2yiuz3qmcq6">
    <property role="TrG5h" value="Processing" />
    <property role="2f7twF" value="Processing" />
    <property role="1XlLyE" value="true" />
    <node concept="ftmFs" id="2yiuz3qmcxK" role="ftER_">
      <node concept="tCFHf" id="2yiuz3qmcyD" role="ftvYc">
        <ref role="tCJdB" node="2yiuz3qjiiR" resolve="CreateFont" />
      </node>
      <node concept="tCFHf" id="2yiuz3qpQqM" role="ftvYc">
        <ref role="tCJdB" node="2yiuz3qnUOZ" resolve="ColorSelector" />
      </node>
    </node>
    <node concept="tT9cl" id="2yiuz3qmctw" role="2f5YQi">
      <ref role="tU$_T" to="tprs:hF$n$r$" resolve="IDEATools" />
    </node>
  </node>
  <node concept="sE7Ow" id="2yiuz3qnUOZ">
    <property role="TrG5h" value="ColorSelector" />
    <property role="2uzpH1" value="Color Selector" />
    <property role="fJN8o" value="true" />
    <node concept="tnohg" id="2yiuz3qnUP0" role="tncku">
      <node concept="3clFbS" id="2yiuz3qnUP1" role="2VODD2">
        <node concept="3cpWs8" id="2yiuz3qpG$t" role="3cqZAp">
          <node concept="3cpWsn" id="2yiuz3qpG$u" role="3cpWs9">
            <property role="TrG5h" value="color" />
            <node concept="3uibUv" id="2yiuz3qpG$v" role="1tU5fm">
              <ref role="3uigEE" to="z60i:~Color" resolve="Color" />
            </node>
            <node concept="2YIFZM" id="2yiuz3qpG8L" role="33vP2m">
              <ref role="37wK5l" to="dxuu:~JColorChooser.showDialog(java.awt.Component,java.lang.String,java.awt.Color):java.awt.Color" resolve="showDialog" />
              <ref role="1Pybhc" to="dxuu:~JColorChooser" resolve="JColorChooser" />
              <node concept="10Nm6u" id="2yiuz3qpGj8" role="37wK5m" />
              <node concept="Xl_RD" id="2yiuz3qpGT6" role="37wK5m">
                <property role="Xl_RC" value="Choose a color" />
              </node>
              <node concept="10M0yZ" id="2yiuz3qpHY7" role="37wK5m">
                <ref role="1PxDUh" to="z60i:~Color" resolve="Color" />
                <ref role="3cqZAo" to="z60i:~Color.BLACK" resolve="BLACK" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbJ" id="2yiuz3qpJBO" role="3cqZAp">
          <node concept="3clFbS" id="2yiuz3qpJBQ" role="3clFbx">
            <node concept="3cpWs8" id="2yiuz3qpKR1" role="3cqZAp">
              <node concept="3cpWsn" id="2yiuz3qpKR0" role="3cpWs9">
                <property role="3TUv4t" value="false" />
                <property role="TrG5h" value="hexColor" />
                <node concept="3uibUv" id="2yiuz3qpKR2" role="1tU5fm">
                  <ref role="3uigEE" to="wyt6:~String" resolve="String" />
                </node>
                <node concept="3cpWs3" id="2yiuz3qpPbl" role="33vP2m">
                  <node concept="Xl_RD" id="2yiuz3qpN5C" role="3uHU7B">
                    <property role="Xl_RC" value="#" />
                  </node>
                  <node concept="2OqwBi" id="2yiuz3qpN5D" role="3uHU7w">
                    <node concept="2YIFZM" id="2yiuz3qpN5I" role="2Oq$k0">
                      <ref role="37wK5l" to="wyt6:~Integer.toHexString(int):java.lang.String" resolve="toHexString" />
                      <ref role="1Pybhc" to="wyt6:~Integer" resolve="Integer" />
                      <node concept="2OqwBi" id="2yiuz3qpNzS" role="37wK5m">
                        <node concept="37vLTw" id="2yiuz3qpNwm" role="2Oq$k0">
                          <ref role="3cqZAo" node="2yiuz3qpG$u" resolve="color" />
                        </node>
                        <node concept="liA8E" id="2yiuz3qpNH$" role="2OqNvi">
                          <ref role="37wK5l" to="z60i:~Color.getRGB():int" resolve="getRGB" />
                        </node>
                      </node>
                    </node>
                    <node concept="liA8E" id="2yiuz3qpN5G" role="2OqNvi">
                      <ref role="37wK5l" to="wyt6:~String.substring(int):java.lang.String" resolve="substring" />
                      <node concept="3cmrfG" id="2yiuz3qpN5H" role="37wK5m">
                        <property role="3cmrfH" value="2" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3cpWs8" id="2yiuz3qpKR5" role="3cqZAp">
              <node concept="3cpWsn" id="2yiuz3qpKR4" role="3cpWs9">
                <property role="3TUv4t" value="false" />
                <property role="TrG5h" value="stringSelection" />
                <node concept="3uibUv" id="2yiuz3qpKR6" role="1tU5fm">
                  <ref role="3uigEE" to="kt01:~StringSelection" resolve="StringSelection" />
                </node>
                <node concept="2ShNRf" id="2yiuz3qpKRj" role="33vP2m">
                  <node concept="1pGfFk" id="2yiuz3qpKRk" role="2ShVmc">
                    <ref role="37wK5l" to="kt01:~StringSelection.&lt;init&gt;(java.lang.String)" resolve="StringSelection" />
                    <node concept="37vLTw" id="2yiuz3qpKR8" role="37wK5m">
                      <ref role="3cqZAo" node="2yiuz3qpKR0" resolve="hexColor" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3cpWs8" id="2yiuz3qpKRa" role="3cqZAp">
              <node concept="3cpWsn" id="2yiuz3qpKR9" role="3cpWs9">
                <property role="3TUv4t" value="false" />
                <property role="TrG5h" value="clpbrd" />
                <node concept="3uibUv" id="2yiuz3qpKRb" role="1tU5fm">
                  <ref role="3uigEE" to="kt01:~Clipboard" resolve="Clipboard" />
                </node>
                <node concept="2OqwBi" id="2yiuz3qpKRc" role="33vP2m">
                  <node concept="2YIFZM" id="2yiuz3qpKRl" role="2Oq$k0">
                    <ref role="1Pybhc" to="z60i:~Toolkit" resolve="Toolkit" />
                    <ref role="37wK5l" to="z60i:~Toolkit.getDefaultToolkit():java.awt.Toolkit" resolve="getDefaultToolkit" />
                  </node>
                  <node concept="liA8E" id="2yiuz3qpKRe" role="2OqNvi">
                    <ref role="37wK5l" to="z60i:~Toolkit.getSystemClipboard():java.awt.datatransfer.Clipboard" resolve="getSystemClipboard" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="2yiuz3qpKRf" role="3cqZAp">
              <node concept="2OqwBi" id="2yiuz3qpKRn" role="3clFbG">
                <node concept="37vLTw" id="2yiuz3qpKRm" role="2Oq$k0">
                  <ref role="3cqZAo" node="2yiuz3qpKR9" resolve="clpbrd" />
                </node>
                <node concept="liA8E" id="2yiuz3qpKRo" role="2OqNvi">
                  <ref role="37wK5l" to="kt01:~Clipboard.setContents(java.awt.datatransfer.Transferable,java.awt.datatransfer.ClipboardOwner):void" resolve="setContents" />
                  <node concept="37vLTw" id="2yiuz3qpKRh" role="37wK5m">
                    <ref role="3cqZAo" node="2yiuz3qpKR4" resolve="stringSelection" />
                  </node>
                  <node concept="10Nm6u" id="2yiuz3qpKRi" role="37wK5m" />
                </node>
              </node>
            </node>
          </node>
          <node concept="3y3z36" id="2yiuz3qpJO4" role="3clFbw">
            <node concept="10Nm6u" id="2yiuz3qpJOC" role="3uHU7w" />
            <node concept="37vLTw" id="2yiuz3qpJDy" role="3uHU7B">
              <ref role="3cqZAo" node="2yiuz3qpG$u" resolve="color" />
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="2uRRBy" id="1LhviqSkKMo">
    <property role="TrG5h" value="ProjectPugin" />
    <node concept="2uRRBT" id="1LhviqSkKP9" role="2uRRB$">
      <node concept="3clFbS" id="1LhviqSkKPa" role="2VODD2">
        <node concept="3clFbF" id="1LhviqSkSI6" role="3cqZAp">
          <node concept="37vLTI" id="1LhviqSkT_Q" role="3clFbG">
            <node concept="1KvdUw" id="1LhviqSkTBN" role="37vLTx" />
            <node concept="2OqwBi" id="1LhviqSkSKc" role="37vLTJ">
              <node concept="2YIFZM" id="1LhviqSkSJx" role="2Oq$k0">
                <ref role="1Pybhc" to="df3e:1LhviqSiNG4" resolve="IntegerManager" />
                <ref role="37wK5l" to="df3e:1LhviqSiZsM" resolve="getInstance" />
              </node>
              <node concept="2OwXpG" id="1LhviqSkTvE" role="2OqNvi">
                <ref role="2Oxat5" to="df3e:1LhviqSkSzZ" resolve="project" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="2uRRBN" id="1LhviqSkKQd" role="2uRRB_">
      <node concept="3clFbS" id="1LhviqSkKQe" role="2VODD2">
        <node concept="3clFbF" id="1LhviqSkTFp" role="3cqZAp">
          <node concept="37vLTI" id="1LhviqSkTFq" role="3clFbG">
            <node concept="10Nm6u" id="1LhviqSkTHv" role="37vLTx" />
            <node concept="2OqwBi" id="1LhviqSkTFs" role="37vLTJ">
              <node concept="2YIFZM" id="1LhviqSkTFt" role="2Oq$k0">
                <ref role="1Pybhc" to="df3e:1LhviqSiNG4" resolve="IntegerManager" />
                <ref role="37wK5l" to="df3e:1LhviqSiZsM" resolve="getInstance" />
              </node>
              <node concept="2OwXpG" id="1LhviqSkTFu" role="2OqNvi">
                <ref role="2Oxat5" to="df3e:1LhviqSkSzZ" resolve="project" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
</model>

