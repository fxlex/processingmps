<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:730c9fb4-e4ce-43d9-91d3-aba1e15e4166(Processing.runtime)">
  <persistence version="9" />
  <languages>
    <use id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage" version="4" />
    <use id="f2801650-65d5-424e-bb1b-463a8781b786" name="jetbrains.mps.baseLanguage.javadoc" version="2" />
    <use id="83888646-71ce-4f1c-9c53-c54016f6ad4f" name="jetbrains.mps.baseLanguage.collections" version="0" />
    <use id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel" version="2" />
    <use id="760a0a8c-eabb-4521-8bfd-65db761a9ba3" name="jetbrains.mps.baseLanguage.logging" version="0" />
    <use id="63650c59-16c8-498a-99c8-005c7ee9515d" name="jetbrains.mps.lang.access" version="0" />
    <use id="18bc6592-03a6-4e29-a83a-7ff23bde13ba" name="jetbrains.mps.lang.editor" version="3" />
  </languages>
  <imports>
    <import index="r7oa" ref="96f7ac91-a4c1-4f01-bffe-3b607acce953/java:processing.core(Processing/)" />
    <import index="z60i" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.awt(JDK/)" />
    <import index="dxuu" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:javax.swing(JDK/)" />
    <import index="9z78" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:javax.swing.border(JDK/)" />
    <import index="33ny" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.util(JDK/)" />
    <import index="wyt6" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.lang(JDK/)" />
    <import index="guwi" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.io(JDK/)" />
    <import index="hyam" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.awt.event(JDK/)" />
    <import index="gsia" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:javax.swing.event(JDK/)" />
    <import index="r791" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:javax.swing.text(JDK/)" />
    <import index="kt01" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.awt.datatransfer(JDK/)" />
    <import index="25x5" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.text(JDK/)" />
    <import index="eydd" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.util.zip(JDK/)" />
    <import index="mhbf" ref="8865b7a8-5271-43d3-884c-6fd1d9cfdd34/java:org.jetbrains.mps.openapi.model(MPS.OpenAPI/)" />
    <import index="dush" ref="8865b7a8-5271-43d3-884c-6fd1d9cfdd34/java:org.jetbrains.mps.openapi.persistence(MPS.OpenAPI/)" />
    <import index="9oh" ref="r:de82dfab-9448-49ba-813e-2b0579f7fb15(jetbrains.mps.ide.platform.actions)" />
    <import index="mw71" ref="6ed54515-acc8-4d1e-a16c-9fd6cfe951ea/java:jetbrains.mps.smodel.persistence.def.v9(MPS.Core/)" />
    <import index="w1kc" ref="6ed54515-acc8-4d1e-a16c-9fd6cfe951ea/java:jetbrains.mps.smodel(MPS.Core/)" />
    <import index="z1c3" ref="6ed54515-acc8-4d1e-a16c-9fd6cfe951ea/java:jetbrains.mps.project(MPS.Core/)" />
    <import index="tpee" ref="r:00000000-0000-4000-0000-011c895902ca(jetbrains.mps.baseLanguage.structure)" />
    <import index="4nm9" ref="498d89d2-c2e9-11e2-ad49-6cf049e62fe5/java:com.intellij.openapi.project(MPS.IDEA/)" />
    <import index="zf81" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.net(JDK/)" />
    <import index="tpek" ref="r:00000000-0000-4000-0000-011c895902c0(jetbrains.mps.baseLanguage.behavior)" />
  </imports>
  <registry>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1082485599095" name="jetbrains.mps.baseLanguage.structure.BlockStatement" flags="nn" index="9aQIb">
        <child id="1082485599096" name="statements" index="9aQI4" />
      </concept>
      <concept id="1215693861676" name="jetbrains.mps.baseLanguage.structure.BaseAssignmentExpression" flags="nn" index="d038R">
        <child id="1068498886297" name="rValue" index="37vLTx" />
        <child id="1068498886295" name="lValue" index="37vLTJ" />
      </concept>
      <concept id="1215695189714" name="jetbrains.mps.baseLanguage.structure.PlusAssignmentExpression" flags="nn" index="d57v9" />
      <concept id="1153422305557" name="jetbrains.mps.baseLanguage.structure.LessThanOrEqualsExpression" flags="nn" index="2dkUwp" />
      <concept id="1202948039474" name="jetbrains.mps.baseLanguage.structure.InstanceMethodCallOperation" flags="nn" index="liA8E" />
      <concept id="1179360813171" name="jetbrains.mps.baseLanguage.structure.HexIntegerLiteral" flags="nn" index="2nou5x">
        <property id="1179360856892" name="value" index="2noCCI" />
      </concept>
      <concept id="1465982738277781862" name="jetbrains.mps.baseLanguage.structure.PlaceholderMember" flags="ng" index="2tJIrI" />
      <concept id="5279705229678483897" name="jetbrains.mps.baseLanguage.structure.FloatingPointFloatConstant" flags="nn" index="2$xPTn">
        <property id="5279705229678483899" name="value" index="2$xPTl" />
      </concept>
      <concept id="1239714755177" name="jetbrains.mps.baseLanguage.structure.AbstractUnaryNumberOperation" flags="nn" index="2$Kvd9">
        <child id="1239714902950" name="expression" index="2$L3a6" />
      </concept>
      <concept id="1173175405605" name="jetbrains.mps.baseLanguage.structure.ArrayAccessExpression" flags="nn" index="AH0OO">
        <child id="1173175577737" name="index" index="AHEQo" />
        <child id="1173175590490" name="array" index="AHHXb" />
      </concept>
      <concept id="1188220165133" name="jetbrains.mps.baseLanguage.structure.ArrayLiteral" flags="nn" index="2BsdOp">
        <child id="1188220173759" name="item" index="2BsfMF" />
      </concept>
      <concept id="1095950406618" name="jetbrains.mps.baseLanguage.structure.DivExpression" flags="nn" index="FJ1c_" />
      <concept id="2820489544401957797" name="jetbrains.mps.baseLanguage.structure.DefaultClassCreator" flags="nn" index="HV5vD">
        <reference id="2820489544401957798" name="classifier" index="HV5vE" />
      </concept>
      <concept id="1154032098014" name="jetbrains.mps.baseLanguage.structure.AbstractLoopStatement" flags="nn" index="2LF5Ji">
        <child id="1154032183016" name="body" index="2LFqv$" />
      </concept>
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1197029447546" name="jetbrains.mps.baseLanguage.structure.FieldReferenceOperation" flags="nn" index="2OwXpG">
        <reference id="1197029500499" name="fieldDeclaration" index="2Oxat5" />
      </concept>
      <concept id="1164879751025" name="jetbrains.mps.baseLanguage.structure.TryCatchStatement" flags="nn" index="SfApY">
        <child id="1164879758292" name="body" index="SfCbr" />
        <child id="1164903496223" name="catchClause" index="TEbGg" />
      </concept>
      <concept id="1145552977093" name="jetbrains.mps.baseLanguage.structure.GenericNewExpression" flags="nn" index="2ShNRf">
        <child id="1145553007750" name="creator" index="2ShVmc" />
      </concept>
      <concept id="1164903280175" name="jetbrains.mps.baseLanguage.structure.CatchClause" flags="nn" index="TDmWw">
        <child id="1164903359218" name="catchBody" index="TDEfX" />
        <child id="1164903359217" name="throwable" index="TDEfY" />
      </concept>
      <concept id="1070462154015" name="jetbrains.mps.baseLanguage.structure.StaticFieldDeclaration" flags="ig" index="Wx3nA">
        <property id="6468716278899126575" name="isVolatile" index="2dlcS1" />
        <property id="6468716278899125786" name="isTransient" index="2dld4O" />
      </concept>
      <concept id="1070475354124" name="jetbrains.mps.baseLanguage.structure.ThisExpression" flags="nn" index="Xjq3P">
        <reference id="1182955020723" name="classConcept" index="1HBi2w" />
      </concept>
      <concept id="1070475587102" name="jetbrains.mps.baseLanguage.structure.SuperConstructorInvocation" flags="nn" index="XkiVB" />
      <concept id="1070475926800" name="jetbrains.mps.baseLanguage.structure.StringLiteral" flags="nn" index="Xl_RD">
        <property id="1070475926801" name="value" index="Xl_RC" />
      </concept>
      <concept id="1182160077978" name="jetbrains.mps.baseLanguage.structure.AnonymousClassCreator" flags="nn" index="YeOm9">
        <child id="1182160096073" name="cls" index="YeSDq" />
      </concept>
      <concept id="1081236700938" name="jetbrains.mps.baseLanguage.structure.StaticMethodDeclaration" flags="ig" index="2YIFZL" />
      <concept id="1081236700937" name="jetbrains.mps.baseLanguage.structure.StaticMethodCall" flags="nn" index="2YIFZM">
        <reference id="1144433194310" name="classConcept" index="1Pybhc" />
      </concept>
      <concept id="1070533707846" name="jetbrains.mps.baseLanguage.structure.StaticFieldReference" flags="nn" index="10M0yZ">
        <reference id="1144433057691" name="classifier" index="1PxDUh" />
      </concept>
      <concept id="1070534058343" name="jetbrains.mps.baseLanguage.structure.NullLiteral" flags="nn" index="10Nm6u" />
      <concept id="1070534370425" name="jetbrains.mps.baseLanguage.structure.IntegerType" flags="in" index="10Oyi0" />
      <concept id="1070534555686" name="jetbrains.mps.baseLanguage.structure.CharType" flags="in" index="10Pfzv" />
      <concept id="1070534644030" name="jetbrains.mps.baseLanguage.structure.BooleanType" flags="in" index="10P_77" />
      <concept id="1070534760951" name="jetbrains.mps.baseLanguage.structure.ArrayType" flags="in" index="10Q1$e">
        <child id="1070534760952" name="componentType" index="10Q1$1" />
      </concept>
      <concept id="1070534934090" name="jetbrains.mps.baseLanguage.structure.CastExpression" flags="nn" index="10QFUN">
        <child id="1070534934091" name="type" index="10QFUM" />
        <child id="1070534934092" name="expression" index="10QFUP" />
      </concept>
      <concept id="1068390468200" name="jetbrains.mps.baseLanguage.structure.FieldDeclaration" flags="ig" index="312cEg">
        <property id="8606350594693632173" name="isTransient" index="eg7rD" />
        <property id="1240249534625" name="isVolatile" index="34CwA1" />
      </concept>
      <concept id="1068390468198" name="jetbrains.mps.baseLanguage.structure.ClassConcept" flags="ig" index="312cEu">
        <property id="1075300953594" name="abstractClass" index="1sVAO0" />
        <property id="1221565133444" name="isFinal" index="1EXbeo" />
        <child id="1095933932569" name="implementedInterface" index="EKbjA" />
        <child id="1165602531693" name="superclass" index="1zkMxy" />
      </concept>
      <concept id="1068431474542" name="jetbrains.mps.baseLanguage.structure.VariableDeclaration" flags="ng" index="33uBYm">
        <property id="1176718929932" name="isFinal" index="3TUv4t" />
        <child id="1068431790190" name="initializer" index="33vP2m" />
      </concept>
      <concept id="1068498886296" name="jetbrains.mps.baseLanguage.structure.VariableReference" flags="nn" index="37vLTw">
        <reference id="1068581517664" name="variableDeclaration" index="3cqZAo" />
      </concept>
      <concept id="1068498886292" name="jetbrains.mps.baseLanguage.structure.ParameterDeclaration" flags="ir" index="37vLTG" />
      <concept id="1068498886294" name="jetbrains.mps.baseLanguage.structure.AssignmentExpression" flags="nn" index="37vLTI" />
      <concept id="1225271177708" name="jetbrains.mps.baseLanguage.structure.StringType" flags="in" index="17QB3L" />
      <concept id="4972933694980447171" name="jetbrains.mps.baseLanguage.structure.BaseVariableDeclaration" flags="ng" index="19Szcq">
        <child id="5680397130376446158" name="type" index="1tU5fm" />
      </concept>
      <concept id="1068580123132" name="jetbrains.mps.baseLanguage.structure.BaseMethodDeclaration" flags="ng" index="3clF44">
        <property id="4276006055363816570" name="isSynchronized" index="od$2w" />
        <property id="1181808852946" name="isFinal" index="DiZV1" />
        <child id="1068580123133" name="returnType" index="3clF45" />
        <child id="1068580123134" name="parameter" index="3clF46" />
        <child id="1068580123135" name="body" index="3clF47" />
      </concept>
      <concept id="1068580123165" name="jetbrains.mps.baseLanguage.structure.InstanceMethodDeclaration" flags="ig" index="3clFb_">
        <property id="1178608670077" name="isAbstract" index="1EzhhJ" />
      </concept>
      <concept id="1068580123152" name="jetbrains.mps.baseLanguage.structure.EqualsExpression" flags="nn" index="3clFbC" />
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123157" name="jetbrains.mps.baseLanguage.structure.Statement" flags="nn" index="3clFbH" />
      <concept id="1068580123159" name="jetbrains.mps.baseLanguage.structure.IfStatement" flags="nn" index="3clFbJ">
        <child id="1082485599094" name="ifFalseStatement" index="9aQIa" />
        <child id="1068580123160" name="condition" index="3clFbw" />
        <child id="1068580123161" name="ifTrue" index="3clFbx" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068580123137" name="jetbrains.mps.baseLanguage.structure.BooleanConstant" flags="nn" index="3clFbT">
        <property id="1068580123138" name="value" index="3clFbU" />
      </concept>
      <concept id="1068580123140" name="jetbrains.mps.baseLanguage.structure.ConstructorDeclaration" flags="ig" index="3clFbW" />
      <concept id="1068580320020" name="jetbrains.mps.baseLanguage.structure.IntegerConstant" flags="nn" index="3cmrfG">
        <property id="1068580320021" name="value" index="3cmrfH" />
      </concept>
      <concept id="1068581242875" name="jetbrains.mps.baseLanguage.structure.PlusExpression" flags="nn" index="3cpWs3" />
      <concept id="1068581242878" name="jetbrains.mps.baseLanguage.structure.ReturnStatement" flags="nn" index="3cpWs6">
        <child id="1068581517676" name="expression" index="3cqZAk" />
      </concept>
      <concept id="1068581242864" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclarationStatement" flags="nn" index="3cpWs8">
        <child id="1068581242865" name="localVariableDeclaration" index="3cpWs9" />
      </concept>
      <concept id="1068581242869" name="jetbrains.mps.baseLanguage.structure.MinusExpression" flags="nn" index="3cpWsd" />
      <concept id="1068581242863" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclaration" flags="nr" index="3cpWsn" />
      <concept id="1068581517677" name="jetbrains.mps.baseLanguage.structure.VoidType" flags="in" index="3cqZAl" />
      <concept id="1079359253375" name="jetbrains.mps.baseLanguage.structure.ParenthesizedExpression" flags="nn" index="1eOMI4">
        <child id="1079359253376" name="expression" index="1eOMHV" />
      </concept>
      <concept id="1081506762703" name="jetbrains.mps.baseLanguage.structure.GreaterThanExpression" flags="nn" index="3eOSWO" />
      <concept id="1081506773034" name="jetbrains.mps.baseLanguage.structure.LessThanExpression" flags="nn" index="3eOVzh" />
      <concept id="1081516740877" name="jetbrains.mps.baseLanguage.structure.NotExpression" flags="nn" index="3fqX7Q">
        <child id="1081516765348" name="expression" index="3fr31v" />
      </concept>
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
        <child id="1068499141038" name="actualArgument" index="37wK5m" />
      </concept>
      <concept id="1212685548494" name="jetbrains.mps.baseLanguage.structure.ClassCreator" flags="nn" index="1pGfFk">
        <child id="1212687122400" name="typeParameter" index="1pMfVU" />
      </concept>
      <concept id="1107461130800" name="jetbrains.mps.baseLanguage.structure.Classifier" flags="ng" index="3pOWGL">
        <property id="521412098689998745" name="nonStatic" index="2bfB8j" />
        <child id="5375687026011219971" name="member" index="jymVt" unordered="true" />
      </concept>
      <concept id="7812454656619025416" name="jetbrains.mps.baseLanguage.structure.MethodDeclaration" flags="ng" index="1rXfSm">
        <property id="8355037393041754995" name="isNative" index="2aFKle" />
      </concept>
      <concept id="7812454656619025412" name="jetbrains.mps.baseLanguage.structure.LocalMethodCall" flags="nn" index="1rXfSq" />
      <concept id="1107535904670" name="jetbrains.mps.baseLanguage.structure.ClassifierType" flags="in" index="3uibUv">
        <reference id="1107535924139" name="classifier" index="3uigEE" />
        <child id="1109201940907" name="parameter" index="11_B2D" />
      </concept>
      <concept id="1081773326031" name="jetbrains.mps.baseLanguage.structure.BinaryOperation" flags="nn" index="3uHJSO">
        <child id="1081773367579" name="rightExpression" index="3uHU7w" />
        <child id="1081773367580" name="leftExpression" index="3uHU7B" />
      </concept>
      <concept id="1214918800624" name="jetbrains.mps.baseLanguage.structure.PostfixIncrementExpression" flags="nn" index="3uNrnE" />
      <concept id="1073239437375" name="jetbrains.mps.baseLanguage.structure.NotEqualsExpression" flags="nn" index="3y3z36" />
      <concept id="1184950988562" name="jetbrains.mps.baseLanguage.structure.ArrayCreator" flags="nn" index="3$_iS1">
        <child id="1184951007469" name="componentType" index="3$_nBY" />
        <child id="1184952969026" name="dimensionExpression" index="3$GQph" />
      </concept>
      <concept id="1184952934362" name="jetbrains.mps.baseLanguage.structure.DimensionExpression" flags="nn" index="3$GHV9">
        <child id="1184953288404" name="expression" index="3$I4v7" />
      </concept>
      <concept id="1178549954367" name="jetbrains.mps.baseLanguage.structure.IVisible" flags="ng" index="1B3ioH">
        <child id="1178549979242" name="visibility" index="1B3o_S" />
      </concept>
      <concept id="1144226303539" name="jetbrains.mps.baseLanguage.structure.ForeachStatement" flags="nn" index="1DcWWT">
        <child id="1144226360166" name="iterable" index="1DdaDG" />
      </concept>
      <concept id="1144230876926" name="jetbrains.mps.baseLanguage.structure.AbstractForStatement" flags="nn" index="1DupvO">
        <child id="1144230900587" name="variable" index="1Duv9x" />
      </concept>
      <concept id="1144231330558" name="jetbrains.mps.baseLanguage.structure.ForStatement" flags="nn" index="1Dw8fO">
        <child id="1144231399730" name="condition" index="1Dwp0S" />
        <child id="1144231408325" name="iteration" index="1Dwrff" />
      </concept>
      <concept id="1163668896201" name="jetbrains.mps.baseLanguage.structure.TernaryOperatorExpression" flags="nn" index="3K4zz7">
        <child id="1163668914799" name="condition" index="3K4Cdx" />
        <child id="1163668922816" name="ifTrue" index="3K4E3e" />
        <child id="1163668934364" name="ifFalse" index="3K4GZi" />
      </concept>
      <concept id="1221737317277" name="jetbrains.mps.baseLanguage.structure.StaticInitializer" flags="lg" index="1Pe0a1">
        <child id="1221737317278" name="statementList" index="1Pe0a2" />
      </concept>
      <concept id="1208890769693" name="jetbrains.mps.baseLanguage.structure.ArrayLengthOperation" flags="nn" index="1Rwk04" />
      <concept id="6329021646629104957" name="jetbrains.mps.baseLanguage.structure.TextCommentPart" flags="nn" index="3SKdUq">
        <property id="6329021646629104958" name="text" index="3SKdUp" />
      </concept>
      <concept id="6329021646629104954" name="jetbrains.mps.baseLanguage.structure.SingleLineComment" flags="nn" index="3SKdUt">
        <child id="6329021646629175155" name="commentPart" index="3SKWNk" />
      </concept>
      <concept id="1146644602865" name="jetbrains.mps.baseLanguage.structure.PublicVisibility" flags="nn" index="3Tm1VV" />
      <concept id="1146644623116" name="jetbrains.mps.baseLanguage.structure.PrivateVisibility" flags="nn" index="3Tm6S6" />
      <concept id="1146644641414" name="jetbrains.mps.baseLanguage.structure.ProtectedVisibility" flags="nn" index="3Tmbuc" />
      <concept id="1080120340718" name="jetbrains.mps.baseLanguage.structure.AndExpression" flags="nn" index="1Wc70l" />
      <concept id="1200397529627" name="jetbrains.mps.baseLanguage.structure.CharConstant" flags="nn" index="1Xhbcc">
        <property id="1200397540847" name="charConstant" index="1XhdNS" />
      </concept>
      <concept id="1170345865475" name="jetbrains.mps.baseLanguage.structure.AnonymousClass" flags="ig" index="1Y3b0j">
        <reference id="1170346070688" name="classifier" index="1Y3XeK" />
      </concept>
      <concept id="8064396509828172209" name="jetbrains.mps.baseLanguage.structure.UnaryMinus" flags="nn" index="1ZRNhn" />
    </language>
    <language id="63650c59-16c8-498a-99c8-005c7ee9515d" name="jetbrains.mps.lang.access">
      <concept id="8974276187400348173" name="jetbrains.mps.lang.access.structure.CommandClosureLiteral" flags="nn" index="1QHqEC" />
      <concept id="8974276187400348170" name="jetbrains.mps.lang.access.structure.BaseExecuteCommandStatement" flags="nn" index="1QHqEJ">
        <child id="8974276187400348171" name="commandClosureLiteral" index="1QHqEI" />
      </concept>
      <concept id="8974276187400348181" name="jetbrains.mps.lang.access.structure.ExecuteLightweightCommandStatement" flags="nn" index="1QHqEK" />
    </language>
    <language id="fd392034-7849-419d-9071-12563d152375" name="jetbrains.mps.baseLanguage.closures">
      <concept id="1199569711397" name="jetbrains.mps.baseLanguage.closures.structure.ClosureLiteral" flags="nn" index="1bVj0M">
        <child id="1199569916463" name="body" index="1bW5cS" />
      </concept>
    </language>
    <language id="f2801650-65d5-424e-bb1b-463a8781b786" name="jetbrains.mps.baseLanguage.javadoc">
      <concept id="5349172909345501395" name="jetbrains.mps.baseLanguage.javadoc.structure.BaseDocComment" flags="ng" index="P$AiS">
        <child id="8465538089690331502" name="body" index="TZ5H$" />
      </concept>
      <concept id="8465538089690331500" name="jetbrains.mps.baseLanguage.javadoc.structure.CommentLine" flags="ng" index="TZ5HA">
        <child id="8970989240999019149" name="part" index="1dT_Ay" />
      </concept>
      <concept id="8970989240999019143" name="jetbrains.mps.baseLanguage.javadoc.structure.TextCommentLinePart" flags="ng" index="1dT_AC">
        <property id="8970989240999019144" name="text" index="1dT_AB" />
      </concept>
      <concept id="2068944020170241612" name="jetbrains.mps.baseLanguage.javadoc.structure.ClassifierDocComment" flags="ng" index="3UR2Jj" />
    </language>
    <language id="760a0a8c-eabb-4521-8bfd-65db761a9ba3" name="jetbrains.mps.baseLanguage.logging">
      <concept id="1167227138527" name="jetbrains.mps.baseLanguage.logging.structure.LogStatement" flags="nn" index="34ab3g">
        <property id="1167228628751" name="hasException" index="34fQS0" />
        <property id="1167245565795" name="severity" index="35gtTG" />
        <child id="1167227463056" name="logExpression" index="34bqiv" />
        <child id="1167227561449" name="exception" index="34bMjA" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
        <child id="5169995583184591170" name="smodelAttribute" index="lGtFl" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
      <concept id="4222318806802425298" name="jetbrains.mps.lang.core.structure.SuppressErrorsAnnotation" flags="ng" index="15s5l7" />
    </language>
    <language id="83888646-71ce-4f1c-9c53-c54016f6ad4f" name="jetbrains.mps.baseLanguage.collections">
      <concept id="1201306600024" name="jetbrains.mps.baseLanguage.collections.structure.ContainsKeyOperation" flags="nn" index="2Nt0df">
        <child id="1201654602639" name="key" index="38cxEo" />
      </concept>
      <concept id="1197683403723" name="jetbrains.mps.baseLanguage.collections.structure.MapType" flags="in" index="3rvAFt">
        <child id="1197683466920" name="keyType" index="3rvQeY" />
        <child id="1197683475734" name="valueType" index="3rvSg0" />
      </concept>
      <concept id="1197686869805" name="jetbrains.mps.baseLanguage.collections.structure.HashMapCreator" flags="nn" index="3rGOSV">
        <child id="1197687026896" name="keyType" index="3rHrn6" />
        <child id="1197687035757" name="valueType" index="3rHtpV" />
      </concept>
      <concept id="1197932370469" name="jetbrains.mps.baseLanguage.collections.structure.MapElement" flags="nn" index="3EllGN">
        <child id="1197932505799" name="map" index="3ElQJh" />
        <child id="1197932525128" name="key" index="3ElVtu" />
      </concept>
    </language>
  </registry>
  <node concept="312cEu" id="2yiuz3qhGhA">
    <property role="TrG5h" value="CreateFont" />
    <property role="2bfB8j" value="true" />
    <property role="1sVAO0" value="false" />
    <property role="1EXbeo" value="false" />
    <property role="3GE5qa" value="createFont" />
    <node concept="3Tm1VV" id="2yiuz3qhGhB" role="1B3o_S" />
    <node concept="3uibUv" id="2yiuz3qhGhC" role="1zkMxy">
      <ref role="3uigEE" to="dxuu:~JFrame" resolve="JFrame" />
    </node>
    <node concept="3UR2Jj" id="2yiuz3qhGtp" role="lGtFl">
      <node concept="TZ5HA" id="2yiuz3qhGHq" role="TZ5H$">
        <node concept="1dT_AC" id="2yiuz3qhGHr" role="1dT_Ay">
          <property role="1dT_AB" value="GUI tool for font creation heaven/hell." />
        </node>
      </node>
    </node>
    <node concept="312cEg" id="2yiuz3qhGhH" role="jymVt">
      <property role="34CwA1" value="false" />
      <property role="eg7rD" value="false" />
      <property role="TrG5h" value="fontSelector" />
      <property role="3TUv4t" value="false" />
      <node concept="3uibUv" id="2yiuz3qhGhJ" role="1tU5fm">
        <ref role="3uigEE" to="dxuu:~JList" resolve="JList" />
        <node concept="17QB3L" id="2yiuz3qlytt" role="11_B2D" />
      </node>
    </node>
    <node concept="312cEg" id="2yiuz3qhGhL" role="jymVt">
      <property role="34CwA1" value="false" />
      <property role="eg7rD" value="false" />
      <property role="TrG5h" value="sizeSelector" />
      <property role="3TUv4t" value="false" />
      <node concept="3uibUv" id="2yiuz3qhGhN" role="1tU5fm">
        <ref role="3uigEE" to="dxuu:~JTextField" resolve="JTextField" />
      </node>
    </node>
    <node concept="312cEg" id="2yiuz3qhGhO" role="jymVt">
      <property role="34CwA1" value="false" />
      <property role="eg7rD" value="false" />
      <property role="TrG5h" value="charsetButton" />
      <property role="3TUv4t" value="false" />
      <node concept="3uibUv" id="2yiuz3qhGhQ" role="1tU5fm">
        <ref role="3uigEE" to="dxuu:~JButton" resolve="JButton" />
      </node>
    </node>
    <node concept="312cEg" id="2yiuz3qhGhR" role="jymVt">
      <property role="34CwA1" value="false" />
      <property role="eg7rD" value="false" />
      <property role="TrG5h" value="smoothBox" />
      <property role="3TUv4t" value="false" />
      <node concept="3uibUv" id="2yiuz3qhGhT" role="1tU5fm">
        <ref role="3uigEE" to="dxuu:~JCheckBox" resolve="JCheckBox" />
      </node>
    </node>
    <node concept="312cEg" id="2yiuz3qhGhU" role="jymVt">
      <property role="34CwA1" value="false" />
      <property role="eg7rD" value="false" />
      <property role="TrG5h" value="sample" />
      <property role="3TUv4t" value="false" />
      <node concept="3uibUv" id="2yiuz3qhGhW" role="1tU5fm">
        <ref role="3uigEE" to="dxuu:~JComponent" resolve="JComponent" />
      </node>
    </node>
    <node concept="312cEg" id="2yiuz3qhGhX" role="jymVt">
      <property role="34CwA1" value="false" />
      <property role="eg7rD" value="false" />
      <property role="TrG5h" value="okButton" />
      <property role="3TUv4t" value="false" />
      <node concept="3uibUv" id="2yiuz3qhGhZ" role="1tU5fm">
        <ref role="3uigEE" to="dxuu:~JButton" resolve="JButton" />
      </node>
    </node>
    <node concept="312cEg" id="2yiuz3qhGi0" role="jymVt">
      <property role="34CwA1" value="false" />
      <property role="eg7rD" value="false" />
      <property role="TrG5h" value="filenameField" />
      <property role="3TUv4t" value="false" />
      <node concept="3uibUv" id="2yiuz3qhGi2" role="1tU5fm">
        <ref role="3uigEE" to="dxuu:~JTextField" resolve="JTextField" />
      </node>
    </node>
    <node concept="312cEg" id="2yiuz3qhGi3" role="jymVt">
      <property role="34CwA1" value="false" />
      <property role="eg7rD" value="false" />
      <property role="TrG5h" value="table" />
      <property role="3TUv4t" value="false" />
      <node concept="3uibUv" id="2yiuz3qhGi5" role="1tU5fm">
        <ref role="3uigEE" to="33ny:~Map" resolve="Map" />
        <node concept="17QB3L" id="2yiuz3qlytu" role="11_B2D" />
        <node concept="3uibUv" id="2yiuz3qhGi7" role="11_B2D">
          <ref role="3uigEE" to="z60i:~Font" resolve="Font" />
        </node>
      </node>
    </node>
    <node concept="312cEg" id="2yiuz3qhGi8" role="jymVt">
      <property role="34CwA1" value="false" />
      <property role="eg7rD" value="false" />
      <property role="TrG5h" value="smooth" />
      <property role="3TUv4t" value="false" />
      <node concept="10P_77" id="2yiuz3qhGia" role="1tU5fm" />
      <node concept="3clFbT" id="2yiuz3qhGib" role="33vP2m">
        <property role="3clFbU" value="true" />
      </node>
    </node>
    <node concept="312cEg" id="2yiuz3qhGic" role="jymVt">
      <property role="34CwA1" value="false" />
      <property role="eg7rD" value="false" />
      <property role="TrG5h" value="font" />
      <property role="3TUv4t" value="false" />
      <node concept="3uibUv" id="2yiuz3qhGie" role="1tU5fm">
        <ref role="3uigEE" to="z60i:~Font" resolve="Font" />
      </node>
    </node>
    <node concept="312cEg" id="2yiuz3qhGif" role="jymVt">
      <property role="34CwA1" value="false" />
      <property role="eg7rD" value="false" />
      <property role="TrG5h" value="list" />
      <property role="3TUv4t" value="false" />
      <node concept="10Q1$e" id="2yiuz3qhGii" role="1tU5fm">
        <node concept="17QB3L" id="2yiuz3qlytp" role="10Q1$1" />
      </node>
    </node>
    <node concept="312cEg" id="2yiuz3qhGij" role="jymVt">
      <property role="34CwA1" value="false" />
      <property role="eg7rD" value="false" />
      <property role="TrG5h" value="selection" />
      <property role="3TUv4t" value="false" />
      <node concept="10Oyi0" id="2yiuz3qhGil" role="1tU5fm" />
      <node concept="1ZRNhn" id="2yiuz3qhGim" role="33vP2m">
        <node concept="3cmrfG" id="2yiuz3qhGin" role="2$L3a6">
          <property role="3cmrfH" value="1" />
        </node>
      </node>
    </node>
    <node concept="312cEg" id="2yiuz3qhGio" role="jymVt">
      <property role="34CwA1" value="false" />
      <property role="eg7rD" value="false" />
      <property role="TrG5h" value="charSelector" />
      <property role="3TUv4t" value="false" />
      <node concept="3uibUv" id="2yiuz3qhGiq" role="1tU5fm">
        <ref role="3uigEE" node="2yiuz3qhGwv" resolve="CharacterSelector" />
      </node>
    </node>
    <node concept="312cEg" id="2yiuz3qjaJN" role="jymVt">
      <property role="34CwA1" value="false" />
      <property role="eg7rD" value="false" />
      <property role="TrG5h" value="dataPath" />
      <property role="3TUv4t" value="false" />
      <node concept="17QB3L" id="2yiuz3qj9ui" role="1tU5fm" />
    </node>
    <node concept="3clFbW" id="2yiuz3qhGir" role="jymVt">
      <property role="DiZV1" value="false" />
      <property role="od$2w" value="false" />
      <node concept="3cqZAl" id="2yiuz3qhGis" role="3clF45" />
      <node concept="3clFbS" id="2yiuz3qhGit" role="3clF47">
        <node concept="XkiVB" id="2yiuz3qhGKu" role="3cqZAp">
          <ref role="37wK5l" to="dxuu:~JFrame.&lt;init&gt;(java.lang.String)" resolve="JFrame" />
          <node concept="Xl_RD" id="2yiuz3qiQgE" role="37wK5m">
            <property role="Xl_RC" value="Create Font" />
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qjdpn" role="3cqZAp">
          <node concept="37vLTI" id="2yiuz3qjepr" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qjfbT" role="37vLTx">
              <ref role="3cqZAo" node="2yiuz3qjca9" resolve="dataPath" />
            </node>
            <node concept="2OqwBi" id="2yiuz3qjdqx" role="37vLTJ">
              <node concept="Xjq3P" id="2yiuz3qjdpl" role="2Oq$k0" />
              <node concept="2OwXpG" id="2yiuz3qjdY8" role="2OqNvi">
                <ref role="2Oxat5" node="2yiuz3qjaJN" resolve="dataPath" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="3Tm1VV" id="2yiuz3qhGix" role="1B3o_S" />
      <node concept="37vLTG" id="2yiuz3qjca9" role="3clF46">
        <property role="TrG5h" value="dataPath" />
        <node concept="17QB3L" id="2yiuz3qjca8" role="1tU5fm" />
      </node>
    </node>
    <node concept="3clFb_" id="2yiuz3qhGiy" role="jymVt">
      <property role="TrG5h" value="getMenuTitle" />
      <property role="DiZV1" value="false" />
      <property role="od$2w" value="false" />
      <node concept="3clFbS" id="2yiuz3qhGiz" role="3clF47">
        <node concept="3cpWs6" id="2yiuz3qhGi$" role="3cqZAp">
          <node concept="Xl_RD" id="2yiuz3qiHRG" role="3cqZAk">
            <property role="Xl_RC" value="Create Font..." />
          </node>
        </node>
      </node>
      <node concept="3Tm1VV" id="2yiuz3qhGiB" role="1B3o_S" />
      <node concept="17QB3L" id="2yiuz3qlyto" role="3clF45" />
    </node>
    <node concept="3clFb_" id="2yiuz3qhGiD" role="jymVt">
      <property role="TrG5h" value="init" />
      <property role="DiZV1" value="false" />
      <property role="od$2w" value="false" />
      <node concept="3clFbS" id="2yiuz3qhGiG" role="3clF47">
        <node concept="3cpWs8" id="2yiuz3qhGiO" role="3cqZAp">
          <node concept="3cpWsn" id="2yiuz3qhGiN" role="3cpWs9">
            <property role="3TUv4t" value="false" />
            <property role="TrG5h" value="paine" />
            <node concept="3uibUv" id="2yiuz3qhGiP" role="1tU5fm">
              <ref role="3uigEE" to="z60i:~Container" resolve="Container" />
            </node>
            <node concept="1rXfSq" id="2yiuz3qhGiQ" role="33vP2m">
              <ref role="37wK5l" to="dxuu:~JFrame.getContentPane():java.awt.Container" resolve="getContentPane" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhGiR" role="3cqZAp">
          <node concept="2OqwBi" id="2yiuz3qhGKz" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qhGKy" role="2Oq$k0">
              <ref role="3cqZAo" node="2yiuz3qhGiN" resolve="paine" />
            </node>
            <node concept="liA8E" id="2yiuz3qhGK$" role="2OqNvi">
              <ref role="37wK5l" to="z60i:~Container.setLayout(java.awt.LayoutManager):void" resolve="setLayout" />
              <node concept="2ShNRf" id="2yiuz3qhGK_" role="37wK5m">
                <node concept="1pGfFk" id="2yiuz3qhGKA" role="2ShVmc">
                  <ref role="37wK5l" to="z60i:~BorderLayout.&lt;init&gt;()" resolve="BorderLayout" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3SKdUt" id="2yiuz3qhGHt" role="3cqZAp">
          <node concept="3SKdUq" id="2yiuz3qhGHs" role="3SKWNk">
            <property role="3SKdUp" value="10, 10));" />
          </node>
        </node>
        <node concept="3cpWs8" id="2yiuz3qhGiV" role="3cqZAp">
          <node concept="3cpWsn" id="2yiuz3qhGiU" role="3cpWs9">
            <property role="3TUv4t" value="false" />
            <property role="TrG5h" value="pain" />
            <node concept="3uibUv" id="2yiuz3qhGiW" role="1tU5fm">
              <ref role="3uigEE" to="dxuu:~JPanel" resolve="JPanel" />
            </node>
            <node concept="2ShNRf" id="2yiuz3qhGKB" role="33vP2m">
              <node concept="1pGfFk" id="2yiuz3qhGKC" role="2ShVmc">
                <ref role="37wK5l" to="dxuu:~JPanel.&lt;init&gt;()" resolve="JPanel" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhGiY" role="3cqZAp">
          <node concept="2OqwBi" id="2yiuz3qhGKF" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qhGKE" role="2Oq$k0">
              <ref role="3cqZAo" node="2yiuz3qhGiU" resolve="pain" />
            </node>
            <node concept="liA8E" id="2yiuz3qhGKG" role="2OqNvi">
              <ref role="37wK5l" to="dxuu:~JComponent.setBorder(javax.swing.border.Border):void" resolve="setBorder" />
              <node concept="2ShNRf" id="2yiuz3qhGKH" role="37wK5m">
                <node concept="1pGfFk" id="2yiuz3qhGKI" role="2ShVmc">
                  <ref role="37wK5l" to="9z78:~EmptyBorder.&lt;init&gt;(int,int,int,int)" resolve="EmptyBorder" />
                  <node concept="3cmrfG" id="2yiuz3qhGj1" role="37wK5m">
                    <property role="3cmrfH" value="13" />
                  </node>
                  <node concept="3cmrfG" id="2yiuz3qhGj2" role="37wK5m">
                    <property role="3cmrfH" value="13" />
                  </node>
                  <node concept="3cmrfG" id="2yiuz3qhGj3" role="37wK5m">
                    <property role="3cmrfH" value="13" />
                  </node>
                  <node concept="3cmrfG" id="2yiuz3qhGj4" role="37wK5m">
                    <property role="3cmrfH" value="13" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhGj5" role="3cqZAp">
          <node concept="2OqwBi" id="2yiuz3qhGKL" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qhGKK" role="2Oq$k0">
              <ref role="3cqZAo" node="2yiuz3qhGiN" resolve="paine" />
            </node>
            <node concept="liA8E" id="2yiuz3qhGKM" role="2OqNvi">
              <ref role="37wK5l" to="z60i:~Container.add(java.awt.Component,java.lang.Object):void" resolve="add" />
              <node concept="37vLTw" id="2yiuz3qhGj7" role="37wK5m">
                <ref role="3cqZAo" node="2yiuz3qhGiU" resolve="pain" />
              </node>
              <node concept="10M0yZ" id="2yiuz3qhXsN" role="37wK5m">
                <ref role="1PxDUh" to="z60i:~BorderLayout" resolve="BorderLayout" />
                <ref role="3cqZAo" to="z60i:~BorderLayout.CENTER" resolve="CENTER" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhGj9" role="3cqZAp">
          <node concept="2OqwBi" id="2yiuz3qhGKR" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qhGKQ" role="2Oq$k0">
              <ref role="3cqZAo" node="2yiuz3qhGiU" resolve="pain" />
            </node>
            <node concept="liA8E" id="2yiuz3qhGKS" role="2OqNvi">
              <ref role="37wK5l" to="z60i:~Container.setLayout(java.awt.LayoutManager):void" resolve="setLayout" />
              <node concept="2ShNRf" id="2yiuz3qhGKT" role="37wK5m">
                <node concept="1pGfFk" id="2yiuz3qhGKU" role="2ShVmc">
                  <ref role="37wK5l" to="dxuu:~BoxLayout.&lt;init&gt;(java.awt.Container,int)" resolve="BoxLayout" />
                  <node concept="37vLTw" id="2yiuz3qhGjc" role="37wK5m">
                    <ref role="3cqZAo" node="2yiuz3qhGiU" resolve="pain" />
                  </node>
                  <node concept="10M0yZ" id="2yiuz3qhXsO" role="37wK5m">
                    <ref role="1PxDUh" to="dxuu:~BoxLayout" resolve="BoxLayout" />
                    <ref role="3cqZAo" to="dxuu:~BoxLayout.Y_AXIS" resolve="Y_AXIS" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="2yiuz3qhGjf" role="3cqZAp">
          <node concept="3cpWsn" id="2yiuz3qhGje" role="3cpWs9">
            <property role="3TUv4t" value="false" />
            <property role="TrG5h" value="labelText" />
            <node concept="17QB3L" id="2yiuz3qlyty" role="1tU5fm" />
            <node concept="Xl_RD" id="2yiuz3qiS6w" role="33vP2m">
              <property role="Xl_RC" value="Use this tool to create bitmap fonts for your program.\nSelect a font and size, and click 'OK' to generate the font.\nIt will be added to the data folder of the current sketch." />
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="2yiuz3qhGjk" role="3cqZAp">
          <node concept="3cpWsn" id="2yiuz3qhGjj" role="3cpWs9">
            <property role="3TUv4t" value="false" />
            <property role="TrG5h" value="textarea" />
            <node concept="3uibUv" id="2yiuz3qhGjl" role="1tU5fm">
              <ref role="3uigEE" to="dxuu:~JTextArea" resolve="JTextArea" />
            </node>
            <node concept="2ShNRf" id="2yiuz3qhGKY" role="33vP2m">
              <node concept="1pGfFk" id="2yiuz3qhGLa" role="2ShVmc">
                <ref role="37wK5l" to="dxuu:~JTextArea.&lt;init&gt;(java.lang.String)" resolve="JTextArea" />
                <node concept="37vLTw" id="2yiuz3qhGjn" role="37wK5m">
                  <ref role="3cqZAo" node="2yiuz3qhGje" resolve="labelText" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhGjo" role="3cqZAp">
          <node concept="2OqwBi" id="2yiuz3qhGLd" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qhGLc" role="2Oq$k0">
              <ref role="3cqZAo" node="2yiuz3qhGjj" resolve="textarea" />
            </node>
            <node concept="liA8E" id="2yiuz3qhGLe" role="2OqNvi">
              <ref role="37wK5l" to="dxuu:~JComponent.setBorder(javax.swing.border.Border):void" resolve="setBorder" />
              <node concept="2ShNRf" id="2yiuz3qhGLf" role="37wK5m">
                <node concept="1pGfFk" id="2yiuz3qhGLg" role="2ShVmc">
                  <ref role="37wK5l" to="9z78:~EmptyBorder.&lt;init&gt;(int,int,int,int)" resolve="EmptyBorder" />
                  <node concept="3cmrfG" id="2yiuz3qhGjr" role="37wK5m">
                    <property role="3cmrfH" value="10" />
                  </node>
                  <node concept="3cmrfG" id="2yiuz3qhGjs" role="37wK5m">
                    <property role="3cmrfH" value="10" />
                  </node>
                  <node concept="3cmrfG" id="2yiuz3qhGjt" role="37wK5m">
                    <property role="3cmrfH" value="20" />
                  </node>
                  <node concept="3cmrfG" id="2yiuz3qhGju" role="37wK5m">
                    <property role="3cmrfH" value="10" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhGjv" role="3cqZAp">
          <node concept="2OqwBi" id="2yiuz3qhGLj" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qhGLi" role="2Oq$k0">
              <ref role="3cqZAo" node="2yiuz3qhGjj" resolve="textarea" />
            </node>
            <node concept="liA8E" id="2yiuz3qhGLk" role="2OqNvi">
              <ref role="37wK5l" to="dxuu:~JComponent.setBackground(java.awt.Color):void" resolve="setBackground" />
              <node concept="10Nm6u" id="2yiuz3qhGjx" role="37wK5m" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhGjy" role="3cqZAp">
          <node concept="2OqwBi" id="2yiuz3qhGLn" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qhGLm" role="2Oq$k0">
              <ref role="3cqZAo" node="2yiuz3qhGjj" resolve="textarea" />
            </node>
            <node concept="liA8E" id="2yiuz3qhGLo" role="2OqNvi">
              <ref role="37wK5l" to="r791:~JTextComponent.setEditable(boolean):void" resolve="setEditable" />
              <node concept="3clFbT" id="2yiuz3qhGj$" role="37wK5m">
                <property role="3clFbU" value="false" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhGj_" role="3cqZAp">
          <node concept="2OqwBi" id="2yiuz3qhGLr" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qhGLq" role="2Oq$k0">
              <ref role="3cqZAo" node="2yiuz3qhGjj" resolve="textarea" />
            </node>
            <node concept="liA8E" id="2yiuz3qhGLs" role="2OqNvi">
              <ref role="37wK5l" to="r791:~JTextComponent.setHighlighter(javax.swing.text.Highlighter):void" resolve="setHighlighter" />
              <node concept="10Nm6u" id="2yiuz3qhGjB" role="37wK5m" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhGjC" role="3cqZAp">
          <node concept="2OqwBi" id="2yiuz3qhGLv" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qhGLu" role="2Oq$k0">
              <ref role="3cqZAo" node="2yiuz3qhGjj" resolve="textarea" />
            </node>
            <node concept="liA8E" id="2yiuz3qhGLw" role="2OqNvi">
              <ref role="37wK5l" to="dxuu:~JTextArea.setFont(java.awt.Font):void" resolve="setFont" />
              <node concept="2ShNRf" id="2yiuz3qhGLx" role="37wK5m">
                <node concept="1pGfFk" id="2yiuz3qhGLy" role="2ShVmc">
                  <ref role="37wK5l" to="z60i:~Font.&lt;init&gt;(java.lang.String,int,int)" resolve="Font" />
                  <node concept="Xl_RD" id="2yiuz3qhGjF" role="37wK5m">
                    <property role="Xl_RC" value="Dialog" />
                  </node>
                  <node concept="10M0yZ" id="2yiuz3qhXsP" role="37wK5m">
                    <ref role="1PxDUh" to="z60i:~Font" resolve="Font" />
                    <ref role="3cqZAo" to="z60i:~Font.PLAIN" resolve="PLAIN" />
                  </node>
                  <node concept="3cmrfG" id="2yiuz3qhGjH" role="37wK5m">
                    <property role="3cmrfH" value="12" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhGjI" role="3cqZAp">
          <node concept="2OqwBi" id="2yiuz3qhGLB" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qhGLA" role="2Oq$k0">
              <ref role="3cqZAo" node="2yiuz3qhGiU" resolve="pain" />
            </node>
            <node concept="liA8E" id="2yiuz3qhGLC" role="2OqNvi">
              <ref role="37wK5l" to="z60i:~Container.add(java.awt.Component):java.awt.Component" resolve="add" />
              <node concept="37vLTw" id="2yiuz3qhGjK" role="37wK5m">
                <ref role="3cqZAo" node="2yiuz3qhGjj" resolve="textarea" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3SKdUt" id="2yiuz3qhGHv" role="3cqZAp">
          <node concept="3SKdUq" id="2yiuz3qhGHu" role="3SKWNk">
            <property role="3SKdUp" value="don't care about families starting with . or #" />
          </node>
        </node>
        <node concept="3SKdUt" id="2yiuz3qhGHx" role="3cqZAp">
          <node concept="3SKdUq" id="2yiuz3qhGHw" role="3SKWNk">
            <property role="3SKdUp" value="also ignore dialog, dialoginput, monospaced, serif, sansserif" />
          </node>
        </node>
        <node concept="3SKdUt" id="2yiuz3qhGHz" role="3cqZAp">
          <node concept="3SKdUq" id="2yiuz3qhGHy" role="3SKWNk">
            <property role="3SKdUp" value="getFontList is deprecated in 1.4, so this has to be used" />
          </node>
        </node>
        <node concept="3SKdUt" id="2yiuz3qhGH_" role="3cqZAp">
          <node concept="3SKdUq" id="2yiuz3qhGH$" role="3SKWNk">
            <property role="3SKdUp" value="long t = System.currentTimeMillis();" />
          </node>
        </node>
        <node concept="3cpWs8" id="2yiuz3qhGjM" role="3cqZAp">
          <node concept="3cpWsn" id="2yiuz3qhGjL" role="3cpWs9">
            <property role="3TUv4t" value="false" />
            <property role="TrG5h" value="ge" />
            <node concept="3uibUv" id="2yiuz3qhGjN" role="1tU5fm">
              <ref role="3uigEE" to="z60i:~GraphicsEnvironment" resolve="GraphicsEnvironment" />
            </node>
            <node concept="2YIFZM" id="2yiuz3qhGLE" role="33vP2m">
              <ref role="1Pybhc" to="z60i:~GraphicsEnvironment" resolve="GraphicsEnvironment" />
              <ref role="37wK5l" to="z60i:~GraphicsEnvironment.getLocalGraphicsEnvironment():java.awt.GraphicsEnvironment" resolve="getLocalGraphicsEnvironment" />
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="2yiuz3qhGjQ" role="3cqZAp">
          <node concept="3cpWsn" id="2yiuz3qhGjP" role="3cpWs9">
            <property role="3TUv4t" value="false" />
            <property role="TrG5h" value="fonts" />
            <node concept="10Q1$e" id="2yiuz3qhGjS" role="1tU5fm">
              <node concept="3uibUv" id="2yiuz3qhGjR" role="10Q1$1">
                <ref role="3uigEE" to="z60i:~Font" resolve="Font" />
              </node>
            </node>
            <node concept="2OqwBi" id="2yiuz3qhGLH" role="33vP2m">
              <node concept="37vLTw" id="2yiuz3qhGLG" role="2Oq$k0">
                <ref role="3cqZAo" node="2yiuz3qhGjL" resolve="ge" />
              </node>
              <node concept="liA8E" id="2yiuz3qhGLI" role="2OqNvi">
                <ref role="37wK5l" to="z60i:~GraphicsEnvironment.getAllFonts():java.awt.Font[]" resolve="getAllFonts" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3SKdUt" id="2yiuz3qhGHB" role="3cqZAp">
          <node concept="3SKdUq" id="2yiuz3qhGHA" role="3SKWNk">
            <property role="3SKdUp" value="System.out.println(&quot;font startup took &quot; + (System.currentTimeMillis() - t) + &quot; ms&quot;);" />
          </node>
        </node>
        <node concept="3SKdUt" id="2yiuz3qhGHD" role="3cqZAp">
          <node concept="3SKdUq" id="2yiuz3qhGHC" role="3SKWNk">
            <property role="3SKdUp" value="   if (false) {" />
          </node>
        </node>
        <node concept="3SKdUt" id="2yiuz3qhGHF" role="3cqZAp">
          <node concept="3SKdUq" id="2yiuz3qhGHE" role="3SKWNk">
            <property role="3SKdUp" value="     ArrayList&lt;Font&gt; fontList = new ArrayList&lt;Font&gt;();" />
          </node>
        </node>
        <node concept="3SKdUt" id="2yiuz3qhGHH" role="3cqZAp">
          <node concept="3SKdUq" id="2yiuz3qhGHG" role="3SKWNk">
            <property role="3SKdUp" value="     File folderList = new File(&quot;/Users/fry/coconut/sys/fonts/web&quot;);" />
          </node>
        </node>
        <node concept="3SKdUt" id="2yiuz3qhGHJ" role="3cqZAp">
          <node concept="3SKdUq" id="2yiuz3qhGHI" role="3SKWNk">
            <property role="3SKdUp" value="     for (File folder : folderList.listFiles()) {" />
          </node>
        </node>
        <node concept="3SKdUt" id="2yiuz3qhGHL" role="3cqZAp">
          <node concept="3SKdUq" id="2yiuz3qhGHK" role="3SKWNk">
            <property role="3SKdUp" value="       if (folder.isDirectory()) {" />
          </node>
        </node>
        <node concept="3SKdUt" id="2yiuz3qhGHN" role="3cqZAp">
          <node concept="3SKdUq" id="2yiuz3qhGHM" role="3SKWNk">
            <property role="3SKdUp" value="         File[] items = folder.listFiles(new FilenameFilter() {" />
          </node>
        </node>
        <node concept="3SKdUt" id="2yiuz3qhGHP" role="3cqZAp">
          <node concept="3SKdUq" id="2yiuz3qhGHO" role="3SKWNk">
            <property role="3SKdUp" value="           public boolean accept(File dir, String name) {" />
          </node>
        </node>
        <node concept="3SKdUt" id="2yiuz3qhGHR" role="3cqZAp">
          <node concept="3SKdUq" id="2yiuz3qhGHQ" role="3SKWNk">
            <property role="3SKdUp" value="             if (name.charAt(0) == '.') return false;" />
          </node>
        </node>
        <node concept="3SKdUt" id="2yiuz3qhGHT" role="3cqZAp">
          <node concept="3SKdUq" id="2yiuz3qhGHS" role="3SKWNk">
            <property role="3SKdUp" value="             return (name.toLowerCase().endsWith(&quot;.ttf&quot;) ||" />
          </node>
        </node>
        <node concept="3SKdUt" id="2yiuz3qhGHV" role="3cqZAp">
          <node concept="3SKdUq" id="2yiuz3qhGHU" role="3SKWNk">
            <property role="3SKdUp" value="                     name.toLowerCase().endsWith(&quot;.otf&quot;));" />
          </node>
        </node>
        <node concept="3SKdUt" id="2yiuz3qhGHX" role="3cqZAp">
          <node concept="3SKdUq" id="2yiuz3qhGHW" role="3SKWNk">
            <property role="3SKdUp" value="           }" />
          </node>
        </node>
        <node concept="3SKdUt" id="2yiuz3qhGHZ" role="3cqZAp">
          <node concept="3SKdUq" id="2yiuz3qhGHY" role="3SKWNk">
            <property role="3SKdUp" value="         });" />
          </node>
        </node>
        <node concept="3SKdUt" id="2yiuz3qhGI1" role="3cqZAp">
          <node concept="3SKdUq" id="2yiuz3qhGI0" role="3SKWNk">
            <property role="3SKdUp" value="         for (File fontFile : items) {" />
          </node>
        </node>
        <node concept="3SKdUt" id="2yiuz3qhGI3" role="3cqZAp">
          <node concept="3SKdUq" id="2yiuz3qhGI2" role="3SKWNk">
            <property role="3SKdUp" value="           try {" />
          </node>
        </node>
        <node concept="3SKdUt" id="2yiuz3qhGI5" role="3cqZAp">
          <node concept="3SKdUq" id="2yiuz3qhGI4" role="3SKWNk">
            <property role="3SKdUp" value="             FileInputStream fis = new FileInputStream(fontFile);" />
          </node>
        </node>
        <node concept="3SKdUt" id="2yiuz3qhGI7" role="3cqZAp">
          <node concept="3SKdUq" id="2yiuz3qhGI6" role="3SKWNk">
            <property role="3SKdUp" value="             BufferedInputStream input = new BufferedInputStream(fis);" />
          </node>
        </node>
        <node concept="3SKdUt" id="2yiuz3qhGI9" role="3cqZAp">
          <node concept="3SKdUq" id="2yiuz3qhGI8" role="3SKWNk">
            <property role="3SKdUp" value="             Font font = Font.createFont(Font.TRUETYPE_FONT, input);" />
          </node>
        </node>
        <node concept="3SKdUt" id="2yiuz3qhGIb" role="3cqZAp">
          <node concept="3SKdUq" id="2yiuz3qhGIa" role="3SKWNk">
            <property role="3SKdUp" value="             input.close();" />
          </node>
        </node>
        <node concept="3SKdUt" id="2yiuz3qhGId" role="3cqZAp">
          <node concept="3SKdUq" id="2yiuz3qhGIc" role="3SKWNk">
            <property role="3SKdUp" value="             fontList.add(font);" />
          </node>
        </node>
        <node concept="3SKdUt" id="2yiuz3qhGIf" role="3cqZAp">
          <node concept="3SKdUq" id="2yiuz3qhGIe" role="3SKWNk">
            <property role="3SKdUp" value="" />
          </node>
        </node>
        <node concept="3SKdUt" id="2yiuz3qhGIh" role="3cqZAp">
          <node concept="3SKdUq" id="2yiuz3qhGIg" role="3SKWNk">
            <property role="3SKdUp" value="           } catch (Exception e) {" />
          </node>
        </node>
        <node concept="3SKdUt" id="2yiuz3qhGIj" role="3cqZAp">
          <node concept="3SKdUq" id="2yiuz3qhGIi" role="3SKWNk">
            <property role="3SKdUp" value="             System.out.println(&quot;Ignoring &quot; + fontFile.getName());" />
          </node>
        </node>
        <node concept="3SKdUt" id="2yiuz3qhGIl" role="3cqZAp">
          <node concept="3SKdUq" id="2yiuz3qhGIk" role="3SKWNk">
            <property role="3SKdUp" value="           }" />
          </node>
        </node>
        <node concept="3SKdUt" id="2yiuz3qhGIn" role="3cqZAp">
          <node concept="3SKdUq" id="2yiuz3qhGIm" role="3SKWNk">
            <property role="3SKdUp" value="         }" />
          </node>
        </node>
        <node concept="3SKdUt" id="2yiuz3qhGIp" role="3cqZAp">
          <node concept="3SKdUq" id="2yiuz3qhGIo" role="3SKWNk">
            <property role="3SKdUp" value="       }" />
          </node>
        </node>
        <node concept="3SKdUt" id="2yiuz3qhGIr" role="3cqZAp">
          <node concept="3SKdUq" id="2yiuz3qhGIq" role="3SKWNk">
            <property role="3SKdUp" value="     }" />
          </node>
        </node>
        <node concept="3SKdUt" id="2yiuz3qhGIt" role="3cqZAp">
          <node concept="3SKdUq" id="2yiuz3qhGIs" role="3SKWNk">
            <property role="3SKdUp" value="//      fonts = new Font[fontList.size()];" />
          </node>
        </node>
        <node concept="3SKdUt" id="2yiuz3qhGIv" role="3cqZAp">
          <node concept="3SKdUq" id="2yiuz3qhGIu" role="3SKWNk">
            <property role="3SKdUp" value="//      fontList.toArray(fonts);" />
          </node>
        </node>
        <node concept="3SKdUt" id="2yiuz3qhGIx" role="3cqZAp">
          <node concept="3SKdUq" id="2yiuz3qhGIw" role="3SKWNk">
            <property role="3SKdUp" value="     fonts = fontList.toArray(new Font[fontList.size()]);" />
          </node>
        </node>
        <node concept="3SKdUt" id="2yiuz3qhGIz" role="3cqZAp">
          <node concept="3SKdUq" id="2yiuz3qhGIy" role="3SKWNk">
            <property role="3SKdUp" value="   }" />
          </node>
        </node>
        <node concept="3cpWs8" id="2yiuz3qhGjV" role="3cqZAp">
          <node concept="3cpWsn" id="2yiuz3qhGjU" role="3cpWs9">
            <property role="3TUv4t" value="false" />
            <property role="TrG5h" value="fontList" />
            <node concept="10Q1$e" id="2yiuz3qhGjX" role="1tU5fm">
              <node concept="17QB3L" id="2yiuz3qlyts" role="10Q1$1" />
            </node>
            <node concept="2ShNRf" id="2yiuz3qhGk2" role="33vP2m">
              <node concept="3$_iS1" id="2yiuz3qhGk0" role="2ShVmc">
                <node concept="3$GHV9" id="2yiuz3qhGk1" role="3$GQph">
                  <node concept="2OqwBi" id="2yiuz3qhGLL" role="3$I4v7">
                    <node concept="37vLTw" id="2yiuz3qhGLK" role="2Oq$k0">
                      <ref role="3cqZAo" node="2yiuz3qhGjP" resolve="fonts" />
                    </node>
                    <node concept="1Rwk04" id="2yiuz3qhXUg" role="2OqNvi" />
                  </node>
                </node>
                <node concept="17QB3L" id="2yiuz3qlytz" role="3$_nBY" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhGk3" role="3cqZAp">
          <node concept="37vLTI" id="2yiuz3qhGk4" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qhGk5" role="37vLTJ">
              <ref role="3cqZAo" node="2yiuz3qhGi3" resolve="table" />
            </node>
            <node concept="2ShNRf" id="2yiuz3qhGLN" role="37vLTx">
              <node concept="1pGfFk" id="2yiuz3qhGLO" role="2ShVmc">
                <ref role="37wK5l" to="33ny:~HashMap.&lt;init&gt;()" resolve="HashMap" />
                <node concept="17QB3L" id="2yiuz3qlytx" role="1pMfVU" />
                <node concept="3uibUv" id="2yiuz3qhGk8" role="1pMfVU">
                  <ref role="3uigEE" to="z60i:~Font" resolve="Font" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="2yiuz3qhGka" role="3cqZAp">
          <node concept="3cpWsn" id="2yiuz3qhGk9" role="3cpWs9">
            <property role="3TUv4t" value="false" />
            <property role="TrG5h" value="index" />
            <node concept="10Oyi0" id="2yiuz3qhGkb" role="1tU5fm" />
            <node concept="3cmrfG" id="2yiuz3qhGkc" role="33vP2m">
              <property role="3cmrfH" value="0" />
            </node>
          </node>
        </node>
        <node concept="1Dw8fO" id="2yiuz3qhGkd" role="3cqZAp">
          <node concept="3cpWsn" id="2yiuz3qhGke" role="1Duv9x">
            <property role="3TUv4t" value="false" />
            <property role="TrG5h" value="i" />
            <node concept="10Oyi0" id="2yiuz3qhGkg" role="1tU5fm" />
            <node concept="3cmrfG" id="2yiuz3qhGkh" role="33vP2m">
              <property role="3cmrfH" value="0" />
            </node>
          </node>
          <node concept="3eOVzh" id="2yiuz3qhGki" role="1Dwp0S">
            <node concept="37vLTw" id="2yiuz3qhGkj" role="3uHU7B">
              <ref role="3cqZAo" node="2yiuz3qhGke" resolve="i" />
            </node>
            <node concept="2OqwBi" id="2yiuz3qhGLR" role="3uHU7w">
              <node concept="37vLTw" id="2yiuz3qhGLQ" role="2Oq$k0">
                <ref role="3cqZAo" node="2yiuz3qhGjP" resolve="fonts" />
              </node>
              <node concept="1Rwk04" id="2yiuz3qhXUh" role="2OqNvi" />
            </node>
          </node>
          <node concept="3uNrnE" id="2yiuz3qhGkm" role="1Dwrff">
            <node concept="37vLTw" id="2yiuz3qhGkn" role="2$L3a6">
              <ref role="3cqZAo" node="2yiuz3qhGke" resolve="i" />
            </node>
          </node>
          <node concept="3clFbS" id="2yiuz3qhGkp" role="2LFqv$">
            <node concept="3SKdUt" id="2yiuz3qhGI_" role="3cqZAp">
              <node concept="3SKdUq" id="2yiuz3qhGI$" role="3SKWNk">
                <property role="3SKdUp" value="String psname = fonts[i].getPSName();" />
              </node>
            </node>
            <node concept="3SKdUt" id="2yiuz3qhGIB" role="3cqZAp">
              <node concept="3SKdUq" id="2yiuz3qhGIA" role="3SKWNk">
                <property role="3SKdUp" value="if (psname == null) System.err.println(&quot;ps name is null&quot;);" />
              </node>
            </node>
            <node concept="SfApY" id="2yiuz3qhGkS" role="3cqZAp">
              <node concept="TDmWw" id="2yiuz3qhGkT" role="TEbGg">
                <node concept="3clFbS" id="2yiuz3qhGkP" role="TDEfX">
                  <node concept="3SKdUt" id="2yiuz3qhGIN" role="3cqZAp">
                    <node concept="3SKdUq" id="2yiuz3qhGIM" role="3SKWNk">
                      <property role="3SKdUp" value="Sometimes fonts cause lots of trouble." />
                    </node>
                  </node>
                  <node concept="3SKdUt" id="2yiuz3qhGIP" role="3cqZAp">
                    <node concept="3SKdUq" id="2yiuz3qhGIO" role="3SKWNk">
                      <property role="3SKdUp" value="http://code.google.com/p/processing/issues/detail?id=442" />
                    </node>
                  </node>
                  <node concept="3clFbF" id="2yiuz3qhGkQ" role="3cqZAp">
                    <node concept="2OqwBi" id="2yiuz3qhGLV" role="3clFbG">
                      <node concept="37vLTw" id="2yiuz3qhGLU" role="2Oq$k0">
                        <ref role="3cqZAo" node="2yiuz3qhGkL" resolve="e" />
                      </node>
                      <node concept="liA8E" id="2yiuz3qhGLW" role="2OqNvi">
                        <ref role="37wK5l" to="wyt6:~Throwable.printStackTrace():void" resolve="printStackTrace" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3cpWsn" id="2yiuz3qhGkL" role="TDEfY">
                  <property role="3TUv4t" value="false" />
                  <property role="TrG5h" value="e" />
                  <node concept="3uibUv" id="2yiuz3qhGkN" role="1tU5fm">
                    <ref role="3uigEE" to="wyt6:~Exception" resolve="Exception" />
                  </node>
                </node>
              </node>
              <node concept="3clFbS" id="2yiuz3qhGkr" role="SfCbr">
                <node concept="3clFbF" id="2yiuz3qhGks" role="3cqZAp">
                  <node concept="37vLTI" id="2yiuz3qhGkt" role="3clFbG">
                    <node concept="AH0OO" id="2yiuz3qhGku" role="37vLTJ">
                      <node concept="37vLTw" id="2yiuz3qhGkv" role="AHHXb">
                        <ref role="3cqZAo" node="2yiuz3qhGjU" resolve="fontList" />
                      </node>
                      <node concept="3uNrnE" id="2yiuz3qhGkw" role="AHEQo">
                        <node concept="37vLTw" id="2yiuz3qhGkx" role="2$L3a6">
                          <ref role="3cqZAo" node="2yiuz3qhGk9" resolve="index" />
                        </node>
                      </node>
                    </node>
                    <node concept="2OqwBi" id="2yiuz3qhGky" role="37vLTx">
                      <node concept="AH0OO" id="2yiuz3qhGkz" role="2Oq$k0">
                        <node concept="37vLTw" id="2yiuz3qhGk$" role="AHHXb">
                          <ref role="3cqZAo" node="2yiuz3qhGjP" resolve="fonts" />
                        </node>
                        <node concept="37vLTw" id="2yiuz3qhGk_" role="AHEQo">
                          <ref role="3cqZAo" node="2yiuz3qhGke" resolve="i" />
                        </node>
                      </node>
                      <node concept="liA8E" id="2yiuz3qhGkA" role="2OqNvi">
                        <ref role="37wK5l" to="z60i:~Font.getPSName():java.lang.String" resolve="getPSName" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbF" id="2yiuz3qhGkB" role="3cqZAp">
                  <node concept="2OqwBi" id="2yiuz3qhGLZ" role="3clFbG">
                    <node concept="37vLTw" id="2yiuz3qhGLY" role="2Oq$k0">
                      <ref role="3cqZAo" node="2yiuz3qhGi3" resolve="table" />
                    </node>
                    <node concept="liA8E" id="2yiuz3qhGM0" role="2OqNvi">
                      <ref role="37wK5l" to="33ny:~Map.put(java.lang.Object,java.lang.Object):java.lang.Object" resolve="put" />
                      <node concept="2OqwBi" id="2yiuz3qhGkD" role="37wK5m">
                        <node concept="AH0OO" id="2yiuz3qhGkE" role="2Oq$k0">
                          <node concept="37vLTw" id="2yiuz3qhGkF" role="AHHXb">
                            <ref role="3cqZAo" node="2yiuz3qhGjP" resolve="fonts" />
                          </node>
                          <node concept="37vLTw" id="2yiuz3qhGkG" role="AHEQo">
                            <ref role="3cqZAo" node="2yiuz3qhGke" resolve="i" />
                          </node>
                        </node>
                        <node concept="liA8E" id="2yiuz3qhGkH" role="2OqNvi">
                          <ref role="37wK5l" to="z60i:~Font.getPSName():java.lang.String" resolve="getPSName" />
                        </node>
                      </node>
                      <node concept="AH0OO" id="2yiuz3qhGkI" role="37wK5m">
                        <node concept="37vLTw" id="2yiuz3qhGkJ" role="AHHXb">
                          <ref role="3cqZAo" node="2yiuz3qhGjP" resolve="fonts" />
                        </node>
                        <node concept="37vLTw" id="2yiuz3qhGkK" role="AHEQo">
                          <ref role="3cqZAo" node="2yiuz3qhGke" resolve="i" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3SKdUt" id="2yiuz3qhGID" role="3cqZAp">
                  <node concept="3SKdUq" id="2yiuz3qhGIC" role="3SKWNk">
                    <property role="3SKdUp" value="Checking into http://processing.org/bugs/bugzilla/407.html" />
                  </node>
                </node>
                <node concept="3SKdUt" id="2yiuz3qhGIF" role="3cqZAp">
                  <node concept="3SKdUq" id="2yiuz3qhGIE" role="3SKWNk">
                    <property role="3SKdUp" value="and https://github.com/processing/processing/issues/1727" />
                  </node>
                </node>
                <node concept="3SKdUt" id="2yiuz3qhGIH" role="3cqZAp">
                  <node concept="3SKdUq" id="2yiuz3qhGIG" role="3SKWNk">
                    <property role="3SKdUp" value="if (fonts[i].getPSName().contains(&quot;Helv&quot;)) {" />
                  </node>
                </node>
                <node concept="3SKdUt" id="2yiuz3qhGIJ" role="3cqZAp">
                  <node concept="3SKdUq" id="2yiuz3qhGII" role="3SKWNk">
                    <property role="3SKdUp" value="System.out.println(fonts[i].getPSName() + &quot; -&gt; &quot; + fonts[i]);" />
                  </node>
                </node>
                <node concept="3SKdUt" id="2yiuz3qhGIL" role="3cqZAp">
                  <node concept="3SKdUq" id="2yiuz3qhGIK" role="3SKWNk">
                    <property role="3SKdUp" value="}" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhGkU" role="3cqZAp">
          <node concept="37vLTI" id="2yiuz3qhGkV" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qhGkW" role="37vLTJ">
              <ref role="3cqZAo" node="2yiuz3qhGif" resolve="list" />
            </node>
            <node concept="2ShNRf" id="2yiuz3qhGl1" role="37vLTx">
              <node concept="3$_iS1" id="2yiuz3qhGkZ" role="2ShVmc">
                <node concept="3$GHV9" id="2yiuz3qhGl0" role="3$GQph">
                  <node concept="37vLTw" id="2yiuz3qhGkY" role="3$I4v7">
                    <ref role="3cqZAo" node="2yiuz3qhGk9" resolve="index" />
                  </node>
                </node>
                <node concept="17QB3L" id="2yiuz3qlytr" role="3$_nBY" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhGl2" role="3cqZAp">
          <node concept="2YIFZM" id="2yiuz3qhGM2" role="3clFbG">
            <ref role="1Pybhc" to="wyt6:~System" resolve="System" />
            <ref role="37wK5l" to="wyt6:~System.arraycopy(java.lang.Object,int,java.lang.Object,int,int):void" resolve="arraycopy" />
            <node concept="37vLTw" id="2yiuz3qhGl4" role="37wK5m">
              <ref role="3cqZAo" node="2yiuz3qhGjU" resolve="fontList" />
            </node>
            <node concept="3cmrfG" id="2yiuz3qhGl5" role="37wK5m">
              <property role="3cmrfH" value="0" />
            </node>
            <node concept="37vLTw" id="2yiuz3qhGl6" role="37wK5m">
              <ref role="3cqZAo" node="2yiuz3qhGif" resolve="list" />
            </node>
            <node concept="3cmrfG" id="2yiuz3qhGl7" role="37wK5m">
              <property role="3cmrfH" value="0" />
            </node>
            <node concept="37vLTw" id="2yiuz3qhGl8" role="37wK5m">
              <ref role="3cqZAo" node="2yiuz3qhGk9" resolve="index" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhGl9" role="3cqZAp">
          <node concept="37vLTI" id="2yiuz3qhGla" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qhGlb" role="37vLTJ">
              <ref role="3cqZAo" node="2yiuz3qhGhH" resolve="fontSelector" />
            </node>
            <node concept="2ShNRf" id="2yiuz3qhGM3" role="37vLTx">
              <node concept="1pGfFk" id="2yiuz3qhGQi" role="2ShVmc">
                <ref role="37wK5l" to="dxuu:~JList.&lt;init&gt;(java.lang.Object[])" resolve="JList" />
                <node concept="37vLTw" id="2yiuz3qhGld" role="37wK5m">
                  <ref role="3cqZAo" node="2yiuz3qhGif" resolve="list" />
                </node>
                <node concept="17QB3L" id="2yiuz3qlytw" role="1pMfVU" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhGlf" role="3cqZAp">
          <node concept="2OqwBi" id="2yiuz3qhGQl" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qhGQk" role="2Oq$k0">
              <ref role="3cqZAo" node="2yiuz3qhGhH" resolve="fontSelector" />
            </node>
            <node concept="liA8E" id="2yiuz3qhGQm" role="2OqNvi">
              <ref role="37wK5l" to="dxuu:~JList.addListSelectionListener(javax.swing.event.ListSelectionListener):void" resolve="addListSelectionListener" />
              <node concept="2ShNRf" id="2yiuz3qhGlh" role="37wK5m">
                <node concept="YeOm9" id="2yiuz3qhGli" role="2ShVmc">
                  <node concept="1Y3b0j" id="2yiuz3qhGlj" role="YeSDq">
                    <property role="1sVAO0" value="false" />
                    <property role="1EXbeo" value="false" />
                    <ref role="1Y3XeK" to="gsia:~ListSelectionListener" resolve="ListSelectionListener" />
                    <ref role="37wK5l" to="wyt6:~Object.&lt;init&gt;()" resolve="Object" />
                    <node concept="3clFb_" id="2yiuz3qhGlk" role="jymVt">
                      <property role="TrG5h" value="valueChanged" />
                      <property role="DiZV1" value="false" />
                      <property role="od$2w" value="false" />
                      <node concept="37vLTG" id="2yiuz3qhGll" role="3clF46">
                        <property role="TrG5h" value="e" />
                        <property role="3TUv4t" value="false" />
                        <node concept="3uibUv" id="2yiuz3qhGlm" role="1tU5fm">
                          <ref role="3uigEE" to="gsia:~ListSelectionEvent" resolve="ListSelectionEvent" />
                        </node>
                      </node>
                      <node concept="3clFbS" id="2yiuz3qhGln" role="3clF47">
                        <node concept="3clFbJ" id="2yiuz3qhGlo" role="3cqZAp">
                          <node concept="3clFbC" id="2yiuz3qhGlp" role="3clFbw">
                            <node concept="2OqwBi" id="2yiuz3qhGQs" role="3uHU7B">
                              <node concept="37vLTw" id="2yiuz3qhGQr" role="2Oq$k0">
                                <ref role="3cqZAo" node="2yiuz3qhGll" resolve="e" />
                              </node>
                              <node concept="liA8E" id="2yiuz3qhGQt" role="2OqNvi">
                                <ref role="37wK5l" to="gsia:~ListSelectionEvent.getValueIsAdjusting():boolean" resolve="getValueIsAdjusting" />
                              </node>
                            </node>
                            <node concept="3clFbT" id="2yiuz3qhGlr" role="3uHU7w">
                              <property role="3clFbU" value="false" />
                            </node>
                          </node>
                          <node concept="3clFbS" id="2yiuz3qhGlt" role="3clFbx">
                            <node concept="3clFbF" id="2yiuz3qhGlu" role="3cqZAp">
                              <node concept="37vLTI" id="2yiuz3qhGlv" role="3clFbG">
                                <node concept="37vLTw" id="2yiuz3qhGlw" role="37vLTJ">
                                  <ref role="3cqZAo" node="2yiuz3qhGij" resolve="selection" />
                                </node>
                                <node concept="2OqwBi" id="2yiuz3qhGQz" role="37vLTx">
                                  <node concept="37vLTw" id="2yiuz3qhGQy" role="2Oq$k0">
                                    <ref role="3cqZAo" node="2yiuz3qhGhH" resolve="fontSelector" />
                                  </node>
                                  <node concept="liA8E" id="2yiuz3qhGQ$" role="2OqNvi">
                                    <ref role="37wK5l" to="dxuu:~JList.getSelectedIndex():int" resolve="getSelectedIndex" />
                                  </node>
                                </node>
                              </node>
                            </node>
                            <node concept="3clFbF" id="2yiuz3qhGly" role="3cqZAp">
                              <node concept="2OqwBi" id="2yiuz3qhGQE" role="3clFbG">
                                <node concept="37vLTw" id="2yiuz3qhGQD" role="2Oq$k0">
                                  <ref role="3cqZAo" node="2yiuz3qhGhX" resolve="okButton" />
                                </node>
                                <node concept="liA8E" id="2yiuz3qhGQF" role="2OqNvi">
                                  <ref role="37wK5l" to="dxuu:~AbstractButton.setEnabled(boolean):void" resolve="setEnabled" />
                                  <node concept="3clFbT" id="2yiuz3qhGl$" role="37wK5m">
                                    <property role="3clFbU" value="true" />
                                  </node>
                                </node>
                              </node>
                            </node>
                            <node concept="3clFbF" id="2yiuz3qhGl_" role="3cqZAp">
                              <node concept="1rXfSq" id="2yiuz3qhGlA" role="3clFbG">
                                <ref role="37wK5l" node="2yiuz3qhGqg" resolve="update" />
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                      <node concept="3Tm1VV" id="2yiuz3qhGlB" role="1B3o_S" />
                      <node concept="3cqZAl" id="2yiuz3qhGlC" role="3clF45" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhGlD" role="3cqZAp">
          <node concept="2OqwBi" id="2yiuz3qhGQI" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qhGQH" role="2Oq$k0">
              <ref role="3cqZAo" node="2yiuz3qhGhH" resolve="fontSelector" />
            </node>
            <node concept="liA8E" id="2yiuz3qhGQJ" role="2OqNvi">
              <ref role="37wK5l" to="dxuu:~JList.setSelectionMode(int):void" resolve="setSelectionMode" />
              <node concept="10M0yZ" id="2yiuz3qhXsQ" role="37wK5m">
                <ref role="1PxDUh" to="dxuu:~ListSelectionModel" resolve="ListSelectionModel" />
                <ref role="3cqZAo" to="dxuu:~ListSelectionModel.SINGLE_SELECTION" resolve="SINGLE_SELECTION" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhGlG" role="3cqZAp">
          <node concept="2OqwBi" id="2yiuz3qhGQO" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qhGQN" role="2Oq$k0">
              <ref role="3cqZAo" node="2yiuz3qhGhH" resolve="fontSelector" />
            </node>
            <node concept="liA8E" id="2yiuz3qhGQP" role="2OqNvi">
              <ref role="37wK5l" to="dxuu:~JList.setVisibleRowCount(int):void" resolve="setVisibleRowCount" />
              <node concept="3cmrfG" id="2yiuz3qhGlI" role="37wK5m">
                <property role="3cmrfH" value="12" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="2yiuz3qhGlK" role="3cqZAp">
          <node concept="3cpWsn" id="2yiuz3qhGlJ" role="3cpWs9">
            <property role="3TUv4t" value="false" />
            <property role="TrG5h" value="fontScroller" />
            <node concept="3uibUv" id="2yiuz3qhGlL" role="1tU5fm">
              <ref role="3uigEE" to="dxuu:~JScrollPane" resolve="JScrollPane" />
            </node>
            <node concept="2ShNRf" id="2yiuz3qhGQQ" role="33vP2m">
              <node concept="1pGfFk" id="2yiuz3qhGQR" role="2ShVmc">
                <ref role="37wK5l" to="dxuu:~JScrollPane.&lt;init&gt;(java.awt.Component)" resolve="JScrollPane" />
                <node concept="37vLTw" id="2yiuz3qhGlN" role="37wK5m">
                  <ref role="3cqZAo" node="2yiuz3qhGhH" resolve="fontSelector" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhGlO" role="3cqZAp">
          <node concept="2OqwBi" id="2yiuz3qhGQU" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qhGQT" role="2Oq$k0">
              <ref role="3cqZAo" node="2yiuz3qhGiU" resolve="pain" />
            </node>
            <node concept="liA8E" id="2yiuz3qhGQV" role="2OqNvi">
              <ref role="37wK5l" to="z60i:~Container.add(java.awt.Component):java.awt.Component" resolve="add" />
              <node concept="37vLTw" id="2yiuz3qhGlQ" role="37wK5m">
                <ref role="3cqZAo" node="2yiuz3qhGlJ" resolve="fontScroller" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="2yiuz3qhGlS" role="3cqZAp">
          <node concept="3cpWsn" id="2yiuz3qhGlR" role="3cpWs9">
            <property role="3TUv4t" value="false" />
            <property role="TrG5h" value="d1" />
            <node concept="3uibUv" id="2yiuz3qhGlT" role="1tU5fm">
              <ref role="3uigEE" to="z60i:~Dimension" resolve="Dimension" />
            </node>
            <node concept="2ShNRf" id="2yiuz3qhGQW" role="33vP2m">
              <node concept="1pGfFk" id="2yiuz3qhGQX" role="2ShVmc">
                <ref role="37wK5l" to="z60i:~Dimension.&lt;init&gt;(int,int)" resolve="Dimension" />
                <node concept="3cmrfG" id="2yiuz3qhGlV" role="37wK5m">
                  <property role="3cmrfH" value="13" />
                </node>
                <node concept="3cmrfG" id="2yiuz3qhGlW" role="37wK5m">
                  <property role="3cmrfH" value="13" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhGlX" role="3cqZAp">
          <node concept="2OqwBi" id="2yiuz3qhGR0" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qhGQZ" role="2Oq$k0">
              <ref role="3cqZAo" node="2yiuz3qhGiU" resolve="pain" />
            </node>
            <node concept="liA8E" id="2yiuz3qhGR1" role="2OqNvi">
              <ref role="37wK5l" to="z60i:~Container.add(java.awt.Component):java.awt.Component" resolve="add" />
              <node concept="2ShNRf" id="2yiuz3qhGR2" role="37wK5m">
                <node concept="1pGfFk" id="2yiuz3qhGR3" role="2ShVmc">
                  <ref role="37wK5l" to="dxuu:~Box$Filler.&lt;init&gt;(java.awt.Dimension,java.awt.Dimension,java.awt.Dimension)" resolve="Box.Filler" />
                  <node concept="37vLTw" id="2yiuz3qhGm0" role="37wK5m">
                    <ref role="3cqZAo" node="2yiuz3qhGlR" resolve="d1" />
                  </node>
                  <node concept="37vLTw" id="2yiuz3qhGm1" role="37wK5m">
                    <ref role="3cqZAo" node="2yiuz3qhGlR" resolve="d1" />
                  </node>
                  <node concept="37vLTw" id="2yiuz3qhGm2" role="37wK5m">
                    <ref role="3cqZAo" node="2yiuz3qhGlR" resolve="d1" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhGm3" role="3cqZAp">
          <node concept="37vLTI" id="2yiuz3qhGm4" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qhGm5" role="37vLTJ">
              <ref role="3cqZAo" node="2yiuz3qhGhU" resolve="sample" />
            </node>
            <node concept="2ShNRf" id="2yiuz3qhGR4" role="37vLTx">
              <node concept="1pGfFk" id="2yiuz3qhGR5" role="2ShVmc">
                <ref role="37wK5l" node="2yiuz3qhGtX" resolve="SampleComponent" />
                <node concept="Xjq3P" id="2yiuz3qhGm7" role="37wK5m" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3SKdUt" id="2yiuz3qhGIR" role="3cqZAp">
          <node concept="3SKdUq" id="2yiuz3qhGIQ" role="3SKWNk">
            <property role="3SKdUp" value="Seems that in some instances, no default font is set" />
          </node>
        </node>
        <node concept="3SKdUt" id="2yiuz3qhGIT" role="3cqZAp">
          <node concept="3SKdUq" id="2yiuz3qhGIS" role="3SKWNk">
            <property role="3SKdUp" value="http://dev.processing.org/bugs/show_bug.cgi?id=777" />
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhGm8" role="3cqZAp">
          <node concept="2OqwBi" id="2yiuz3qhGR8" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qhGR7" role="2Oq$k0">
              <ref role="3cqZAo" node="2yiuz3qhGhU" resolve="sample" />
            </node>
            <node concept="liA8E" id="2yiuz3qhGR9" role="2OqNvi">
              <ref role="37wK5l" to="dxuu:~JComponent.setFont(java.awt.Font):void" resolve="setFont" />
              <node concept="2ShNRf" id="2yiuz3qhGRa" role="37wK5m">
                <node concept="1pGfFk" id="2yiuz3qhGRb" role="2ShVmc">
                  <ref role="37wK5l" to="z60i:~Font.&lt;init&gt;(java.lang.String,int,int)" resolve="Font" />
                  <node concept="Xl_RD" id="2yiuz3qhGmb" role="37wK5m">
                    <property role="Xl_RC" value="Dialog" />
                  </node>
                  <node concept="10M0yZ" id="2yiuz3qhXsR" role="37wK5m">
                    <ref role="1PxDUh" to="z60i:~Font" resolve="Font" />
                    <ref role="3cqZAo" to="z60i:~Font.PLAIN" resolve="PLAIN" />
                  </node>
                  <node concept="3cmrfG" id="2yiuz3qhGmd" role="37wK5m">
                    <property role="3cmrfH" value="12" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhGme" role="3cqZAp">
          <node concept="2OqwBi" id="2yiuz3qhGRg" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qhGRf" role="2Oq$k0">
              <ref role="3cqZAo" node="2yiuz3qhGiU" resolve="pain" />
            </node>
            <node concept="liA8E" id="2yiuz3qhGRh" role="2OqNvi">
              <ref role="37wK5l" to="z60i:~Container.add(java.awt.Component):java.awt.Component" resolve="add" />
              <node concept="37vLTw" id="2yiuz3qhGmg" role="37wK5m">
                <ref role="3cqZAo" node="2yiuz3qhGhU" resolve="sample" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="2yiuz3qhGmi" role="3cqZAp">
          <node concept="3cpWsn" id="2yiuz3qhGmh" role="3cpWs9">
            <property role="3TUv4t" value="false" />
            <property role="TrG5h" value="d2" />
            <node concept="3uibUv" id="2yiuz3qhGmj" role="1tU5fm">
              <ref role="3uigEE" to="z60i:~Dimension" resolve="Dimension" />
            </node>
            <node concept="2ShNRf" id="2yiuz3qhGRi" role="33vP2m">
              <node concept="1pGfFk" id="2yiuz3qhGRj" role="2ShVmc">
                <ref role="37wK5l" to="z60i:~Dimension.&lt;init&gt;(int,int)" resolve="Dimension" />
                <node concept="3cmrfG" id="2yiuz3qhGml" role="37wK5m">
                  <property role="3cmrfH" value="6" />
                </node>
                <node concept="3cmrfG" id="2yiuz3qhGmm" role="37wK5m">
                  <property role="3cmrfH" value="6" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhGmn" role="3cqZAp">
          <node concept="2OqwBi" id="2yiuz3qhGRm" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qhGRl" role="2Oq$k0">
              <ref role="3cqZAo" node="2yiuz3qhGiU" resolve="pain" />
            </node>
            <node concept="liA8E" id="2yiuz3qhGRn" role="2OqNvi">
              <ref role="37wK5l" to="z60i:~Container.add(java.awt.Component):java.awt.Component" resolve="add" />
              <node concept="2ShNRf" id="2yiuz3qhGRo" role="37wK5m">
                <node concept="1pGfFk" id="2yiuz3qhGRp" role="2ShVmc">
                  <ref role="37wK5l" to="dxuu:~Box$Filler.&lt;init&gt;(java.awt.Dimension,java.awt.Dimension,java.awt.Dimension)" resolve="Box.Filler" />
                  <node concept="37vLTw" id="2yiuz3qhGmq" role="37wK5m">
                    <ref role="3cqZAo" node="2yiuz3qhGmh" resolve="d2" />
                  </node>
                  <node concept="37vLTw" id="2yiuz3qhGmr" role="37wK5m">
                    <ref role="3cqZAo" node="2yiuz3qhGmh" resolve="d2" />
                  </node>
                  <node concept="37vLTw" id="2yiuz3qhGms" role="37wK5m">
                    <ref role="3cqZAo" node="2yiuz3qhGmh" resolve="d2" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="2yiuz3qhGmu" role="3cqZAp">
          <node concept="3cpWsn" id="2yiuz3qhGmt" role="3cpWs9">
            <property role="3TUv4t" value="false" />
            <property role="TrG5h" value="panel" />
            <node concept="3uibUv" id="2yiuz3qhGmv" role="1tU5fm">
              <ref role="3uigEE" to="dxuu:~JPanel" resolve="JPanel" />
            </node>
            <node concept="2ShNRf" id="2yiuz3qhGRq" role="33vP2m">
              <node concept="1pGfFk" id="2yiuz3qhGRr" role="2ShVmc">
                <ref role="37wK5l" to="dxuu:~JPanel.&lt;init&gt;()" resolve="JPanel" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhGmx" role="3cqZAp">
          <node concept="2OqwBi" id="2yiuz3qhGRu" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qhGRt" role="2Oq$k0">
              <ref role="3cqZAo" node="2yiuz3qhGmt" resolve="panel" />
            </node>
            <node concept="liA8E" id="2yiuz3qhGRv" role="2OqNvi">
              <ref role="37wK5l" to="z60i:~Container.add(java.awt.Component):java.awt.Component" resolve="add" />
              <node concept="2ShNRf" id="2yiuz3qhGRw" role="37wK5m">
                <node concept="1pGfFk" id="2yiuz3qhGS4" role="2ShVmc">
                  <ref role="37wK5l" to="dxuu:~JLabel.&lt;init&gt;(java.lang.String)" resolve="JLabel" />
                  <node concept="3cpWs3" id="2yiuz3qhGm$" role="37wK5m">
                    <node concept="Xl_RD" id="2yiuz3qhGmB" role="3uHU7w">
                      <property role="Xl_RC" value=":" />
                    </node>
                    <node concept="Xl_RD" id="2yiuz3qiVe7" role="3uHU7B">
                      <property role="Xl_RC" value="Size" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhGmC" role="3cqZAp">
          <node concept="37vLTI" id="2yiuz3qhGmD" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qhGmE" role="37vLTJ">
              <ref role="3cqZAo" node="2yiuz3qhGhL" resolve="sizeSelector" />
            </node>
            <node concept="2ShNRf" id="2yiuz3qhGS6" role="37vLTx">
              <node concept="1pGfFk" id="2yiuz3qhGSw" role="2ShVmc">
                <ref role="37wK5l" to="dxuu:~JTextField.&lt;init&gt;(java.lang.String)" resolve="JTextField" />
                <node concept="Xl_RD" id="2yiuz3qhGmG" role="37wK5m">
                  <property role="Xl_RC" value=" 48 " />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhGmH" role="3cqZAp">
          <node concept="2OqwBi" id="2yiuz3qhGmI" role="3clFbG">
            <node concept="2OqwBi" id="2yiuz3qhGSz" role="2Oq$k0">
              <node concept="37vLTw" id="2yiuz3qhGSy" role="2Oq$k0">
                <ref role="3cqZAo" node="2yiuz3qhGhL" resolve="sizeSelector" />
              </node>
              <node concept="liA8E" id="2yiuz3qhGS$" role="2OqNvi">
                <ref role="37wK5l" to="r791:~JTextComponent.getDocument():javax.swing.text.Document" resolve="getDocument" />
              </node>
            </node>
            <node concept="liA8E" id="2yiuz3qhGmK" role="2OqNvi">
              <ref role="37wK5l" to="r791:~Document.addDocumentListener(javax.swing.event.DocumentListener):void" resolve="addDocumentListener" />
              <node concept="2ShNRf" id="2yiuz3qhGmL" role="37wK5m">
                <node concept="YeOm9" id="2yiuz3qlQI$" role="2ShVmc">
                  <node concept="1Y3b0j" id="2yiuz3qlQIB" role="YeSDq">
                    <property role="2bfB8j" value="true" />
                    <ref role="1Y3XeK" to="gsia:~DocumentListener" resolve="DocumentListener" />
                    <ref role="37wK5l" to="wyt6:~Object.&lt;init&gt;()" resolve="Object" />
                    <node concept="3Tm1VV" id="2yiuz3qlQIC" role="1B3o_S" />
                    <node concept="3clFb_" id="2yiuz3qlQID" role="jymVt">
                      <property role="1EzhhJ" value="false" />
                      <property role="TrG5h" value="insertUpdate" />
                      <property role="DiZV1" value="false" />
                      <property role="od$2w" value="false" />
                      <node concept="3Tm1VV" id="2yiuz3qlQIE" role="1B3o_S" />
                      <node concept="3cqZAl" id="2yiuz3qlQIG" role="3clF45" />
                      <node concept="37vLTG" id="2yiuz3qlQIH" role="3clF46">
                        <property role="TrG5h" value="p0" />
                        <node concept="3uibUv" id="2yiuz3qlQII" role="1tU5fm">
                          <ref role="3uigEE" to="gsia:~DocumentEvent" resolve="DocumentEvent" />
                        </node>
                      </node>
                      <node concept="3clFbS" id="2yiuz3qlQIJ" role="3clF47">
                        <node concept="3clFbF" id="2yiuz3qlRqc" role="3cqZAp">
                          <node concept="1rXfSq" id="2yiuz3qlRqb" role="3clFbG">
                            <ref role="37wK5l" node="2yiuz3qhGqg" resolve="update" />
                          </node>
                        </node>
                      </node>
                    </node>
                    <node concept="3clFb_" id="2yiuz3qlQIL" role="jymVt">
                      <property role="1EzhhJ" value="false" />
                      <property role="TrG5h" value="removeUpdate" />
                      <property role="DiZV1" value="false" />
                      <property role="od$2w" value="false" />
                      <node concept="3Tm1VV" id="2yiuz3qlQIM" role="1B3o_S" />
                      <node concept="3cqZAl" id="2yiuz3qlQIO" role="3clF45" />
                      <node concept="37vLTG" id="2yiuz3qlQIP" role="3clF46">
                        <property role="TrG5h" value="p0" />
                        <node concept="3uibUv" id="2yiuz3qlQIQ" role="1tU5fm">
                          <ref role="3uigEE" to="gsia:~DocumentEvent" resolve="DocumentEvent" />
                        </node>
                      </node>
                      <node concept="3clFbS" id="2yiuz3qlQIR" role="3clF47">
                        <node concept="3clFbF" id="2yiuz3qlRZ9" role="3cqZAp">
                          <node concept="1rXfSq" id="2yiuz3qlRZ8" role="3clFbG">
                            <ref role="37wK5l" node="2yiuz3qhGqg" resolve="update" />
                          </node>
                        </node>
                      </node>
                    </node>
                    <node concept="3clFb_" id="2yiuz3qlQIT" role="jymVt">
                      <property role="1EzhhJ" value="false" />
                      <property role="TrG5h" value="changedUpdate" />
                      <property role="DiZV1" value="false" />
                      <property role="od$2w" value="false" />
                      <node concept="3Tm1VV" id="2yiuz3qlQIU" role="1B3o_S" />
                      <node concept="3cqZAl" id="2yiuz3qlQIW" role="3clF45" />
                      <node concept="37vLTG" id="2yiuz3qlQIX" role="3clF46">
                        <property role="TrG5h" value="p0" />
                        <node concept="3uibUv" id="2yiuz3qlQIY" role="1tU5fm">
                          <ref role="3uigEE" to="gsia:~DocumentEvent" resolve="DocumentEvent" />
                        </node>
                      </node>
                      <node concept="3clFbS" id="2yiuz3qlQIZ" role="3clF47" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhGna" role="3cqZAp">
          <node concept="2OqwBi" id="2yiuz3qhGSB" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qhGSA" role="2Oq$k0">
              <ref role="3cqZAo" node="2yiuz3qhGmt" resolve="panel" />
            </node>
            <node concept="liA8E" id="2yiuz3qhGSC" role="2OqNvi">
              <ref role="37wK5l" to="z60i:~Container.add(java.awt.Component):java.awt.Component" resolve="add" />
              <node concept="37vLTw" id="2yiuz3qhGnc" role="37wK5m">
                <ref role="3cqZAo" node="2yiuz3qhGhL" resolve="sizeSelector" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhGnd" role="3cqZAp">
          <node concept="37vLTI" id="2yiuz3qhGne" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qhGnf" role="37vLTJ">
              <ref role="3cqZAo" node="2yiuz3qhGhR" resolve="smoothBox" />
            </node>
            <node concept="2ShNRf" id="2yiuz3qhGSD" role="37vLTx">
              <node concept="1pGfFk" id="2yiuz3qhGSY" role="2ShVmc">
                <ref role="37wK5l" to="dxuu:~JCheckBox.&lt;init&gt;(java.lang.String)" resolve="JCheckBox" />
                <node concept="Xl_RD" id="2yiuz3qiW3t" role="37wK5m">
                  <property role="Xl_RC" value="Smooth" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhGnj" role="3cqZAp">
          <node concept="2OqwBi" id="2yiuz3qhGT2" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qhGT1" role="2Oq$k0">
              <ref role="3cqZAo" node="2yiuz3qhGhR" resolve="smoothBox" />
            </node>
            <node concept="liA8E" id="2yiuz3qhGT3" role="2OqNvi">
              <ref role="37wK5l" to="dxuu:~AbstractButton.addActionListener(java.awt.event.ActionListener):void" resolve="addActionListener" />
              <node concept="2ShNRf" id="2yiuz3qhGnl" role="37wK5m">
                <node concept="YeOm9" id="2yiuz3qm0rM" role="2ShVmc">
                  <node concept="1Y3b0j" id="2yiuz3qm0rP" role="YeSDq">
                    <property role="2bfB8j" value="true" />
                    <ref role="1Y3XeK" to="hyam:~ActionListener" resolve="ActionListener" />
                    <ref role="37wK5l" to="wyt6:~Object.&lt;init&gt;()" resolve="Object" />
                    <node concept="3Tm1VV" id="2yiuz3qm0rQ" role="1B3o_S" />
                    <node concept="3clFb_" id="2yiuz3qm0rR" role="jymVt">
                      <property role="1EzhhJ" value="false" />
                      <property role="TrG5h" value="actionPerformed" />
                      <property role="DiZV1" value="false" />
                      <property role="od$2w" value="false" />
                      <node concept="3Tm1VV" id="2yiuz3qm0rS" role="1B3o_S" />
                      <node concept="3cqZAl" id="2yiuz3qm0rU" role="3clF45" />
                      <node concept="37vLTG" id="2yiuz3qm0rV" role="3clF46">
                        <property role="TrG5h" value="p0" />
                        <node concept="3uibUv" id="2yiuz3qm0rW" role="1tU5fm">
                          <ref role="3uigEE" to="hyam:~ActionEvent" resolve="ActionEvent" />
                        </node>
                      </node>
                      <node concept="3clFbS" id="2yiuz3qm0rX" role="3clF47">
                        <node concept="3clFbF" id="2yiuz3qm18v" role="3cqZAp">
                          <node concept="37vLTI" id="2yiuz3qm1gA" role="3clFbG">
                            <node concept="2OqwBi" id="2yiuz3qm2u9" role="37vLTx">
                              <node concept="37vLTw" id="2yiuz3qm2a4" role="2Oq$k0">
                                <ref role="3cqZAo" node="2yiuz3qhGhR" resolve="smoothBox" />
                              </node>
                              <node concept="liA8E" id="2yiuz3qm3ab" role="2OqNvi">
                                <ref role="37wK5l" to="dxuu:~AbstractButton.isSelected():boolean" resolve="isSelected" />
                              </node>
                            </node>
                            <node concept="37vLTw" id="2yiuz3qm18u" role="37vLTJ">
                              <ref role="3cqZAo" node="2yiuz3qhGi8" resolve="smooth" />
                            </node>
                          </node>
                        </node>
                        <node concept="3clFbF" id="2yiuz3qm3Ms" role="3cqZAp">
                          <node concept="1rXfSq" id="2yiuz3qm3Mq" role="3clFbG">
                            <ref role="37wK5l" node="2yiuz3qhGqg" resolve="update" />
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhGn$" role="3cqZAp">
          <node concept="2OqwBi" id="2yiuz3qhGTd" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qhGTc" role="2Oq$k0">
              <ref role="3cqZAo" node="2yiuz3qhGhR" resolve="smoothBox" />
            </node>
            <node concept="liA8E" id="2yiuz3qhGTe" role="2OqNvi">
              <ref role="37wK5l" to="dxuu:~AbstractButton.setSelected(boolean):void" resolve="setSelected" />
              <node concept="37vLTw" id="2yiuz3qhGnA" role="37wK5m">
                <ref role="3cqZAo" node="2yiuz3qhGi8" resolve="smooth" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhGnB" role="3cqZAp">
          <node concept="2OqwBi" id="2yiuz3qhGTh" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qhGTg" role="2Oq$k0">
              <ref role="3cqZAo" node="2yiuz3qhGmt" resolve="panel" />
            </node>
            <node concept="liA8E" id="2yiuz3qhGTi" role="2OqNvi">
              <ref role="37wK5l" to="z60i:~Container.add(java.awt.Component):java.awt.Component" resolve="add" />
              <node concept="37vLTw" id="2yiuz3qhGnD" role="37wK5m">
                <ref role="3cqZAo" node="2yiuz3qhGhR" resolve="smoothBox" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3SKdUt" id="2yiuz3qhGIV" role="3cqZAp">
          <node concept="3SKdUq" id="2yiuz3qhGIU" role="3SKWNk">
            <property role="3SKdUp" value="allBox = new JCheckBox(&quot;All Characters&quot;);" />
          </node>
        </node>
        <node concept="3SKdUt" id="2yiuz3qhGIX" role="3cqZAp">
          <node concept="3SKdUq" id="2yiuz3qhGIW" role="3SKWNk">
            <property role="3SKdUp" value="allBox.addActionListener(new ActionListener() {" />
          </node>
        </node>
        <node concept="3SKdUt" id="2yiuz3qhGIZ" role="3cqZAp">
          <node concept="3SKdUq" id="2yiuz3qhGIY" role="3SKWNk">
            <property role="3SKdUp" value="public void actionPerformed(ActionEvent e) {" />
          </node>
        </node>
        <node concept="3SKdUt" id="2yiuz3qhGJ1" role="3cqZAp">
          <node concept="3SKdUq" id="2yiuz3qhGJ0" role="3SKWNk">
            <property role="3SKdUp" value="all = allBox.isSelected();" />
          </node>
        </node>
        <node concept="3SKdUt" id="2yiuz3qhGJ3" role="3cqZAp">
          <node concept="3SKdUq" id="2yiuz3qhGJ2" role="3SKWNk">
            <property role="3SKdUp" value="}" />
          </node>
        </node>
        <node concept="3SKdUt" id="2yiuz3qhGJ5" role="3cqZAp">
          <node concept="3SKdUq" id="2yiuz3qhGJ4" role="3SKWNk">
            <property role="3SKdUp" value="});" />
          </node>
        </node>
        <node concept="3SKdUt" id="2yiuz3qhGJ7" role="3cqZAp">
          <node concept="3SKdUq" id="2yiuz3qhGJ6" role="3SKWNk">
            <property role="3SKdUp" value="allBox.setSelected(all);" />
          </node>
        </node>
        <node concept="3SKdUt" id="2yiuz3qhGJ9" role="3cqZAp">
          <node concept="3SKdUq" id="2yiuz3qhGJ8" role="3SKWNk">
            <property role="3SKdUp" value="panel.add(allBox);" />
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhGnE" role="3cqZAp">
          <node concept="37vLTI" id="2yiuz3qhGnF" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qhGnG" role="37vLTJ">
              <ref role="3cqZAo" node="2yiuz3qhGhO" resolve="charsetButton" />
            </node>
            <node concept="2ShNRf" id="2yiuz3qhGTj" role="37vLTx">
              <node concept="1pGfFk" id="2yiuz3qhGTC" role="2ShVmc">
                <ref role="37wK5l" to="dxuu:~JButton.&lt;init&gt;(java.lang.String)" resolve="JButton" />
                <node concept="Xl_RD" id="2yiuz3qiXqo" role="37wK5m">
                  <property role="Xl_RC" value="Characters..." />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhGnK" role="3cqZAp">
          <node concept="2OqwBi" id="2yiuz3qhGTG" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qhGTF" role="2Oq$k0">
              <ref role="3cqZAo" node="2yiuz3qhGhO" resolve="charsetButton" />
            </node>
            <node concept="liA8E" id="2yiuz3qhGTH" role="2OqNvi">
              <ref role="37wK5l" to="dxuu:~AbstractButton.addActionListener(java.awt.event.ActionListener):void" resolve="addActionListener" />
              <node concept="2ShNRf" id="2yiuz3qhGnM" role="37wK5m">
                <node concept="YeOm9" id="2yiuz3qlTbd" role="2ShVmc">
                  <node concept="1Y3b0j" id="2yiuz3qlTbg" role="YeSDq">
                    <property role="2bfB8j" value="true" />
                    <ref role="1Y3XeK" to="hyam:~ActionListener" resolve="ActionListener" />
                    <ref role="37wK5l" to="wyt6:~Object.&lt;init&gt;()" resolve="Object" />
                    <node concept="3Tm1VV" id="2yiuz3qlTbh" role="1B3o_S" />
                    <node concept="3clFb_" id="2yiuz3qlTbi" role="jymVt">
                      <property role="1EzhhJ" value="false" />
                      <property role="TrG5h" value="actionPerformed" />
                      <property role="DiZV1" value="false" />
                      <property role="od$2w" value="false" />
                      <node concept="3Tm1VV" id="2yiuz3qlTbj" role="1B3o_S" />
                      <node concept="3cqZAl" id="2yiuz3qlTbl" role="3clF45" />
                      <node concept="37vLTG" id="2yiuz3qlTbm" role="3clF46">
                        <property role="TrG5h" value="p0" />
                        <node concept="3uibUv" id="2yiuz3qlTbn" role="1tU5fm">
                          <ref role="3uigEE" to="hyam:~ActionEvent" resolve="ActionEvent" />
                        </node>
                      </node>
                      <node concept="3clFbS" id="2yiuz3qlTbo" role="3clF47">
                        <node concept="3clFbF" id="2yiuz3qlTOR" role="3cqZAp">
                          <node concept="2OqwBi" id="2yiuz3qlUaG" role="3clFbG">
                            <node concept="37vLTw" id="2yiuz3qlTOQ" role="2Oq$k0">
                              <ref role="3cqZAo" node="2yiuz3qhGio" resolve="charSelector" />
                            </node>
                            <node concept="liA8E" id="2yiuz3qlUDu" role="2OqNvi">
                              <ref role="37wK5l" to="z60i:~Window.setVisible(boolean):void" resolve="setVisible" />
                              <node concept="3clFbT" id="2yiuz3qlUXd" role="37wK5m">
                                <property role="3clFbU" value="true" />
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhGnY" role="3cqZAp">
          <node concept="2OqwBi" id="2yiuz3qhGTR" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qhGTQ" role="2Oq$k0">
              <ref role="3cqZAo" node="2yiuz3qhGmt" resolve="panel" />
            </node>
            <node concept="liA8E" id="2yiuz3qhGTS" role="2OqNvi">
              <ref role="37wK5l" to="z60i:~Container.add(java.awt.Component):java.awt.Component" resolve="add" />
              <node concept="37vLTw" id="2yiuz3qhGo0" role="37wK5m">
                <ref role="3cqZAo" node="2yiuz3qhGhO" resolve="charsetButton" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhGo1" role="3cqZAp">
          <node concept="2OqwBi" id="2yiuz3qhGTV" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qhGTU" role="2Oq$k0">
              <ref role="3cqZAo" node="2yiuz3qhGiU" resolve="pain" />
            </node>
            <node concept="liA8E" id="2yiuz3qhGTW" role="2OqNvi">
              <ref role="37wK5l" to="z60i:~Container.add(java.awt.Component):java.awt.Component" resolve="add" />
              <node concept="37vLTw" id="2yiuz3qhGo3" role="37wK5m">
                <ref role="3cqZAo" node="2yiuz3qhGmt" resolve="panel" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="2yiuz3qhGo5" role="3cqZAp">
          <node concept="3cpWsn" id="2yiuz3qhGo4" role="3cpWs9">
            <property role="3TUv4t" value="false" />
            <property role="TrG5h" value="filestuff" />
            <node concept="3uibUv" id="2yiuz3qhGo6" role="1tU5fm">
              <ref role="3uigEE" to="dxuu:~JPanel" resolve="JPanel" />
            </node>
            <node concept="2ShNRf" id="2yiuz3qhGTX" role="33vP2m">
              <node concept="1pGfFk" id="2yiuz3qhGTY" role="2ShVmc">
                <ref role="37wK5l" to="dxuu:~JPanel.&lt;init&gt;()" resolve="JPanel" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhGo8" role="3cqZAp">
          <node concept="2OqwBi" id="2yiuz3qhGU1" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qhGU0" role="2Oq$k0">
              <ref role="3cqZAo" node="2yiuz3qhGo4" resolve="filestuff" />
            </node>
            <node concept="liA8E" id="2yiuz3qhGU2" role="2OqNvi">
              <ref role="37wK5l" to="z60i:~Container.add(java.awt.Component):java.awt.Component" resolve="add" />
              <node concept="2ShNRf" id="2yiuz3qhGU3" role="37wK5m">
                <node concept="1pGfFk" id="2yiuz3qhGUm" role="2ShVmc">
                  <ref role="37wK5l" to="dxuu:~JLabel.&lt;init&gt;(java.lang.String)" resolve="JLabel" />
                  <node concept="3cpWs3" id="2yiuz3qhGob" role="37wK5m">
                    <node concept="Xl_RD" id="2yiuz3qiYNr" role="3uHU7B">
                      <property role="Xl_RC" value="Filename" />
                    </node>
                    <node concept="Xl_RD" id="2yiuz3qhGoe" role="3uHU7w">
                      <property role="Xl_RC" value=":" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhGof" role="3cqZAp">
          <node concept="2OqwBi" id="2yiuz3qhGUq" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qhGUp" role="2Oq$k0">
              <ref role="3cqZAo" node="2yiuz3qhGo4" resolve="filestuff" />
            </node>
            <node concept="liA8E" id="2yiuz3qhGUr" role="2OqNvi">
              <ref role="37wK5l" to="z60i:~Container.add(java.awt.Component):java.awt.Component" resolve="add" />
              <node concept="37vLTI" id="2yiuz3qhGoh" role="37wK5m">
                <node concept="37vLTw" id="2yiuz3qhGoi" role="37vLTJ">
                  <ref role="3cqZAo" node="2yiuz3qhGi0" resolve="filenameField" />
                </node>
                <node concept="2ShNRf" id="2yiuz3qhGUs" role="37vLTx">
                  <node concept="1pGfFk" id="2yiuz3qhGUF" role="2ShVmc">
                    <ref role="37wK5l" to="dxuu:~JTextField.&lt;init&gt;(int)" resolve="JTextField" />
                    <node concept="3cmrfG" id="2yiuz3qhGok" role="37wK5m">
                      <property role="3cmrfH" value="20" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhGol" role="3cqZAp">
          <node concept="2OqwBi" id="2yiuz3qhGUI" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qhGUH" role="2Oq$k0">
              <ref role="3cqZAo" node="2yiuz3qhGo4" resolve="filestuff" />
            </node>
            <node concept="liA8E" id="2yiuz3qhGUJ" role="2OqNvi">
              <ref role="37wK5l" to="z60i:~Container.add(java.awt.Component):java.awt.Component" resolve="add" />
              <node concept="2ShNRf" id="2yiuz3qhGUK" role="37wK5m">
                <node concept="1pGfFk" id="2yiuz3qhGUV" role="2ShVmc">
                  <ref role="37wK5l" to="dxuu:~JLabel.&lt;init&gt;(java.lang.String)" resolve="JLabel" />
                  <node concept="Xl_RD" id="2yiuz3qhGoo" role="37wK5m">
                    <property role="Xl_RC" value=".vlw" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhGop" role="3cqZAp">
          <node concept="2OqwBi" id="2yiuz3qhGUY" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qhGUX" role="2Oq$k0">
              <ref role="3cqZAo" node="2yiuz3qhGiU" resolve="pain" />
            </node>
            <node concept="liA8E" id="2yiuz3qhGUZ" role="2OqNvi">
              <ref role="37wK5l" to="z60i:~Container.add(java.awt.Component):java.awt.Component" resolve="add" />
              <node concept="37vLTw" id="2yiuz3qhGor" role="37wK5m">
                <ref role="3cqZAo" node="2yiuz3qhGo4" resolve="filestuff" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="2yiuz3qhGot" role="3cqZAp">
          <node concept="3cpWsn" id="2yiuz3qhGos" role="3cpWs9">
            <property role="3TUv4t" value="false" />
            <property role="TrG5h" value="buttons" />
            <node concept="3uibUv" id="2yiuz3qhGou" role="1tU5fm">
              <ref role="3uigEE" to="dxuu:~JPanel" resolve="JPanel" />
            </node>
            <node concept="2ShNRf" id="2yiuz3qhGV0" role="33vP2m">
              <node concept="1pGfFk" id="2yiuz3qhGV1" role="2ShVmc">
                <ref role="37wK5l" to="dxuu:~JPanel.&lt;init&gt;()" resolve="JPanel" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="2yiuz3qhGox" role="3cqZAp">
          <node concept="3cpWsn" id="2yiuz3qhGow" role="3cpWs9">
            <property role="3TUv4t" value="false" />
            <property role="TrG5h" value="cancelButton" />
            <node concept="3uibUv" id="2yiuz3qhGoy" role="1tU5fm">
              <ref role="3uigEE" to="dxuu:~JButton" resolve="JButton" />
            </node>
            <node concept="2ShNRf" id="2yiuz3qhGV2" role="33vP2m">
              <node concept="1pGfFk" id="2yiuz3qhGVn" role="2ShVmc">
                <ref role="37wK5l" to="dxuu:~JButton.&lt;init&gt;(java.lang.String)" resolve="JButton" />
                <node concept="Xl_RD" id="2yiuz3qiZCK" role="37wK5m">
                  <property role="Xl_RC" value="Cancel" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhGoA" role="3cqZAp">
          <node concept="2OqwBi" id="2yiuz3qhGVr" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qhGVq" role="2Oq$k0">
              <ref role="3cqZAo" node="2yiuz3qhGow" resolve="cancelButton" />
            </node>
            <node concept="liA8E" id="2yiuz3qhGVs" role="2OqNvi">
              <ref role="37wK5l" to="dxuu:~AbstractButton.addActionListener(java.awt.event.ActionListener):void" resolve="addActionListener" />
              <node concept="2ShNRf" id="2yiuz3qhGoC" role="37wK5m">
                <node concept="YeOm9" id="2yiuz3qlXOE" role="2ShVmc">
                  <node concept="1Y3b0j" id="2yiuz3qlXOH" role="YeSDq">
                    <property role="2bfB8j" value="true" />
                    <ref role="1Y3XeK" to="hyam:~ActionListener" resolve="ActionListener" />
                    <ref role="37wK5l" to="wyt6:~Object.&lt;init&gt;()" resolve="Object" />
                    <node concept="3Tm1VV" id="2yiuz3qlXOI" role="1B3o_S" />
                    <node concept="3clFb_" id="2yiuz3qlXOJ" role="jymVt">
                      <property role="1EzhhJ" value="false" />
                      <property role="TrG5h" value="actionPerformed" />
                      <property role="DiZV1" value="false" />
                      <property role="od$2w" value="false" />
                      <node concept="3Tm1VV" id="2yiuz3qlXOK" role="1B3o_S" />
                      <node concept="3cqZAl" id="2yiuz3qlXOM" role="3clF45" />
                      <node concept="37vLTG" id="2yiuz3qlXON" role="3clF46">
                        <property role="TrG5h" value="p0" />
                        <node concept="3uibUv" id="2yiuz3qlXOO" role="1tU5fm">
                          <ref role="3uigEE" to="hyam:~ActionEvent" resolve="ActionEvent" />
                        </node>
                      </node>
                      <node concept="3clFbS" id="2yiuz3qlXOP" role="3clF47">
                        <node concept="3clFbF" id="2yiuz3qlYYQ" role="3cqZAp">
                          <node concept="1rXfSq" id="2yiuz3qlYYP" role="3clFbG">
                            <ref role="37wK5l" to="z60i:~Window.setVisible(boolean):void" resolve="setVisible" />
                            <node concept="3clFbT" id="2yiuz3qlZj0" role="37wK5m">
                              <property role="3clFbU" value="false" />
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhGoO" role="3cqZAp">
          <node concept="37vLTI" id="2yiuz3qhGoP" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qhGoQ" role="37vLTJ">
              <ref role="3cqZAo" node="2yiuz3qhGhX" resolve="okButton" />
            </node>
            <node concept="2ShNRf" id="2yiuz3qhGVt" role="37vLTx">
              <node concept="1pGfFk" id="2yiuz3qhGVM" role="2ShVmc">
                <ref role="37wK5l" to="dxuu:~JButton.&lt;init&gt;(java.lang.String)" resolve="JButton" />
                <node concept="Xl_RD" id="2yiuz3qj12G" role="37wK5m">
                  <property role="Xl_RC" value="OK" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhGoU" role="3cqZAp">
          <node concept="2OqwBi" id="2yiuz3qhGVQ" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qhGVP" role="2Oq$k0">
              <ref role="3cqZAo" node="2yiuz3qhGhX" resolve="okButton" />
            </node>
            <node concept="liA8E" id="2yiuz3qhGVR" role="2OqNvi">
              <ref role="37wK5l" to="dxuu:~AbstractButton.addActionListener(java.awt.event.ActionListener):void" resolve="addActionListener" />
              <node concept="2ShNRf" id="2yiuz3qhGoW" role="37wK5m">
                <node concept="YeOm9" id="2yiuz3qlW4t" role="2ShVmc">
                  <node concept="1Y3b0j" id="2yiuz3qlW4w" role="YeSDq">
                    <property role="2bfB8j" value="true" />
                    <ref role="1Y3XeK" to="hyam:~ActionListener" resolve="ActionListener" />
                    <ref role="37wK5l" to="wyt6:~Object.&lt;init&gt;()" resolve="Object" />
                    <node concept="3Tm1VV" id="2yiuz3qlW4x" role="1B3o_S" />
                    <node concept="3clFb_" id="2yiuz3qlW4y" role="jymVt">
                      <property role="1EzhhJ" value="false" />
                      <property role="TrG5h" value="actionPerformed" />
                      <property role="DiZV1" value="false" />
                      <property role="od$2w" value="false" />
                      <node concept="3Tm1VV" id="2yiuz3qlW4z" role="1B3o_S" />
                      <node concept="3cqZAl" id="2yiuz3qlW4_" role="3clF45" />
                      <node concept="37vLTG" id="2yiuz3qlW4A" role="3clF46">
                        <property role="TrG5h" value="p0" />
                        <node concept="3uibUv" id="2yiuz3qlW4B" role="1tU5fm">
                          <ref role="3uigEE" to="hyam:~ActionEvent" resolve="ActionEvent" />
                        </node>
                      </node>
                      <node concept="3clFbS" id="2yiuz3qlW4C" role="3clF47">
                        <node concept="3clFbF" id="2yiuz3qlWHU" role="3cqZAp">
                          <node concept="1rXfSq" id="2yiuz3qlWHT" role="3clFbG">
                            <ref role="37wK5l" node="2yiuz3qhGru" resolve="build" />
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhGp7" role="3cqZAp">
          <node concept="2OqwBi" id="2yiuz3qhGVU" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qhGVT" role="2Oq$k0">
              <ref role="3cqZAo" node="2yiuz3qhGhX" resolve="okButton" />
            </node>
            <node concept="liA8E" id="2yiuz3qhGVV" role="2OqNvi">
              <ref role="37wK5l" to="dxuu:~AbstractButton.setEnabled(boolean):void" resolve="setEnabled" />
              <node concept="3clFbT" id="2yiuz3qhGp9" role="37wK5m">
                <property role="3clFbU" value="false" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhGpa" role="3cqZAp">
          <node concept="2OqwBi" id="2yiuz3qhGVY" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qhGVX" role="2Oq$k0">
              <ref role="3cqZAo" node="2yiuz3qhGos" resolve="buttons" />
            </node>
            <node concept="liA8E" id="2yiuz3qhGVZ" role="2OqNvi">
              <ref role="37wK5l" to="z60i:~Container.add(java.awt.Component):java.awt.Component" resolve="add" />
              <node concept="37vLTw" id="2yiuz3qhGpc" role="37wK5m">
                <ref role="3cqZAo" node="2yiuz3qhGow" resolve="cancelButton" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhGpd" role="3cqZAp">
          <node concept="2OqwBi" id="2yiuz3qhGW2" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qhGW1" role="2Oq$k0">
              <ref role="3cqZAo" node="2yiuz3qhGos" resolve="buttons" />
            </node>
            <node concept="liA8E" id="2yiuz3qhGW3" role="2OqNvi">
              <ref role="37wK5l" to="z60i:~Container.add(java.awt.Component):java.awt.Component" resolve="add" />
              <node concept="37vLTw" id="2yiuz3qhGpf" role="37wK5m">
                <ref role="3cqZAo" node="2yiuz3qhGhX" resolve="okButton" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhGpg" role="3cqZAp">
          <node concept="2OqwBi" id="2yiuz3qhGW6" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qhGW5" role="2Oq$k0">
              <ref role="3cqZAo" node="2yiuz3qhGiU" resolve="pain" />
            </node>
            <node concept="liA8E" id="2yiuz3qhGW7" role="2OqNvi">
              <ref role="37wK5l" to="z60i:~Container.add(java.awt.Component):java.awt.Component" resolve="add" />
              <node concept="37vLTw" id="2yiuz3qhGpi" role="37wK5m">
                <ref role="3cqZAo" node="2yiuz3qhGos" resolve="buttons" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="2yiuz3qhGpk" role="3cqZAp">
          <node concept="3cpWsn" id="2yiuz3qhGpj" role="3cpWs9">
            <property role="3TUv4t" value="false" />
            <property role="TrG5h" value="root" />
            <node concept="3uibUv" id="2yiuz3qhGpl" role="1tU5fm">
              <ref role="3uigEE" to="dxuu:~JRootPane" resolve="JRootPane" />
            </node>
            <node concept="1rXfSq" id="2yiuz3qhGpm" role="33vP2m">
              <ref role="37wK5l" to="dxuu:~JFrame.getRootPane():javax.swing.JRootPane" resolve="getRootPane" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhGpn" role="3cqZAp">
          <node concept="2OqwBi" id="2yiuz3qhGWa" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qhGW9" role="2Oq$k0">
              <ref role="3cqZAo" node="2yiuz3qhGpj" resolve="root" />
            </node>
            <node concept="liA8E" id="2yiuz3qhGWb" role="2OqNvi">
              <ref role="37wK5l" to="dxuu:~JRootPane.setDefaultButton(javax.swing.JButton):void" resolve="setDefaultButton" />
              <node concept="37vLTw" id="2yiuz3qhGpp" role="37wK5m">
                <ref role="3cqZAo" node="2yiuz3qhGhX" resolve="okButton" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="2yiuz3qhGpr" role="3cqZAp">
          <node concept="3cpWsn" id="2yiuz3qhGpq" role="3cpWs9">
            <property role="3TUv4t" value="false" />
            <property role="TrG5h" value="disposer" />
            <node concept="3uibUv" id="2yiuz3qhGps" role="1tU5fm">
              <ref role="3uigEE" to="hyam:~ActionListener" resolve="ActionListener" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhGpK" role="3cqZAp">
          <node concept="1rXfSq" id="2yiuz3qhGpL" role="3clFbG">
            <ref role="37wK5l" to="z60i:~Window.pack():void" resolve="pack" />
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhGpM" role="3cqZAp">
          <node concept="1rXfSq" id="2yiuz3qhGpN" role="3clFbG">
            <ref role="37wK5l" to="z60i:~Frame.setResizable(boolean):void" resolve="setResizable" />
            <node concept="3clFbT" id="2yiuz3qhGpO" role="37wK5m">
              <property role="3clFbU" value="false" />
            </node>
          </node>
        </node>
        <node concept="3SKdUt" id="2yiuz3qhGJd" role="3cqZAp">
          <node concept="3SKdUq" id="2yiuz3qhGJc" role="3SKWNk">
            <property role="3SKdUp" value="System.out.println(getPreferredSize());" />
          </node>
        </node>
        <node concept="3SKdUt" id="2yiuz3qhGJf" role="3cqZAp">
          <node concept="3SKdUq" id="2yiuz3qhGJe" role="3SKWNk">
            <property role="3SKdUp" value="do this after pack so it doesn't affect layout" />
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhGpP" role="3cqZAp">
          <node concept="2OqwBi" id="2yiuz3qhGWg" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qhGWf" role="2Oq$k0">
              <ref role="3cqZAo" node="2yiuz3qhGhU" resolve="sample" />
            </node>
            <node concept="liA8E" id="2yiuz3qhGWh" role="2OqNvi">
              <ref role="37wK5l" to="dxuu:~JComponent.setFont(java.awt.Font):void" resolve="setFont" />
              <node concept="2ShNRf" id="2yiuz3qhGWi" role="37wK5m">
                <node concept="1pGfFk" id="2yiuz3qhGWj" role="2ShVmc">
                  <ref role="37wK5l" to="z60i:~Font.&lt;init&gt;(java.lang.String,int,int)" resolve="Font" />
                  <node concept="AH0OO" id="2yiuz3qhGpS" role="37wK5m">
                    <node concept="37vLTw" id="2yiuz3qhGpT" role="AHHXb">
                      <ref role="3cqZAo" node="2yiuz3qhGif" resolve="list" />
                    </node>
                    <node concept="3cmrfG" id="2yiuz3qhGpU" role="AHEQo">
                      <property role="3cmrfH" value="0" />
                    </node>
                  </node>
                  <node concept="10M0yZ" id="2yiuz3qhXsS" role="37wK5m">
                    <ref role="1PxDUh" to="z60i:~Font" resolve="Font" />
                    <ref role="3cqZAo" to="z60i:~Font.PLAIN" resolve="PLAIN" />
                  </node>
                  <node concept="3cmrfG" id="2yiuz3qhGpW" role="37wK5m">
                    <property role="3cmrfH" value="48" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhGpX" role="3cqZAp">
          <node concept="2OqwBi" id="2yiuz3qhGWo" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qhGWn" role="2Oq$k0">
              <ref role="3cqZAo" node="2yiuz3qhGhH" resolve="fontSelector" />
            </node>
            <node concept="liA8E" id="2yiuz3qhGWp" role="2OqNvi">
              <ref role="37wK5l" to="dxuu:~JList.setSelectedIndex(int):void" resolve="setSelectedIndex" />
              <node concept="3cmrfG" id="2yiuz3qhGpZ" role="37wK5m">
                <property role="3cmrfH" value="0" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3SKdUt" id="2yiuz3qhGJh" role="3cqZAp">
          <node concept="3SKdUq" id="2yiuz3qhGJg" role="3SKWNk">
            <property role="3SKdUp" value="Dimension screen = Toolkit.getScreenSize();" />
          </node>
        </node>
        <node concept="3SKdUt" id="2yiuz3qhGJj" role="3cqZAp">
          <node concept="3SKdUq" id="2yiuz3qhGJi" role="3SKWNk">
            <property role="3SKdUp" value="windowSize = getSize();" />
          </node>
        </node>
        <node concept="3SKdUt" id="2yiuz3qhGJl" role="3cqZAp">
          <node concept="3SKdUq" id="2yiuz3qhGJk" role="3SKWNk">
            <property role="3SKdUp" value="setLocation((screen.width - windowSize.width) / 2," />
          </node>
        </node>
        <node concept="3SKdUt" id="2yiuz3qhGJn" role="3cqZAp">
          <node concept="3SKdUq" id="2yiuz3qhGJm" role="3SKWNk">
            <property role="3SKdUp" value="(screen.height - windowSize.height) / 2);" />
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhGq0" role="3cqZAp">
          <node concept="1rXfSq" id="2yiuz3qhGq1" role="3clFbG">
            <ref role="37wK5l" to="z60i:~Window.setLocationRelativeTo(java.awt.Component):void" resolve="setLocationRelativeTo" />
            <node concept="10Nm6u" id="2yiuz3qhGq2" role="37wK5m" />
          </node>
        </node>
        <node concept="3SKdUt" id="2yiuz3qhGJp" role="3cqZAp">
          <node concept="3SKdUq" id="2yiuz3qhGJo" role="3SKWNk">
            <property role="3SKdUp" value="create this behind the scenes" />
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhGq3" role="3cqZAp">
          <node concept="37vLTI" id="2yiuz3qhGq4" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qhGq5" role="37vLTJ">
              <ref role="3cqZAo" node="2yiuz3qhGio" resolve="charSelector" />
            </node>
            <node concept="2ShNRf" id="2yiuz3qhGWq" role="37vLTx">
              <node concept="1pGfFk" id="2yiuz3qhGWr" role="2ShVmc">
                <ref role="37wK5l" node="2yiuz3qhG$v" resolve="CharacterSelector" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="3Tm1VV" id="2yiuz3qhGq7" role="1B3o_S" />
      <node concept="3cqZAl" id="2yiuz3qhGq8" role="3clF45" />
    </node>
    <node concept="3clFb_" id="2yiuz3qhGq9" role="jymVt">
      <property role="TrG5h" value="run" />
      <property role="DiZV1" value="false" />
      <property role="od$2w" value="false" />
      <node concept="3clFbS" id="2yiuz3qhGqa" role="3clF47">
        <node concept="3clFbF" id="2yiuz3qhGqb" role="3cqZAp">
          <node concept="1rXfSq" id="2yiuz3qhGqc" role="3clFbG">
            <ref role="37wK5l" to="z60i:~Window.setVisible(boolean):void" resolve="setVisible" />
            <node concept="3clFbT" id="2yiuz3qhGqd" role="37wK5m">
              <property role="3clFbU" value="true" />
            </node>
          </node>
        </node>
      </node>
      <node concept="3Tm1VV" id="2yiuz3qhGqe" role="1B3o_S" />
      <node concept="3cqZAl" id="2yiuz3qhGqf" role="3clF45" />
    </node>
    <node concept="3clFb_" id="2yiuz3qhGqg" role="jymVt">
      <property role="TrG5h" value="update" />
      <property role="DiZV1" value="false" />
      <property role="od$2w" value="false" />
      <node concept="3clFbS" id="2yiuz3qhGqh" role="3clF47">
        <node concept="3cpWs8" id="2yiuz3qhGqj" role="3cqZAp">
          <node concept="3cpWsn" id="2yiuz3qhGqi" role="3cpWs9">
            <property role="3TUv4t" value="false" />
            <property role="TrG5h" value="fontsize" />
            <node concept="10Oyi0" id="2yiuz3qhGqk" role="1tU5fm" />
            <node concept="3cmrfG" id="2yiuz3qhGql" role="33vP2m">
              <property role="3cmrfH" value="0" />
            </node>
          </node>
        </node>
        <node concept="SfApY" id="2yiuz3qhGq$" role="3cqZAp">
          <node concept="TDmWw" id="2yiuz3qhGq_" role="TEbGg">
            <node concept="3clFbS" id="2yiuz3qhGqz" role="TDEfX" />
            <node concept="3cpWsn" id="2yiuz3qhGqv" role="TDEfY">
              <property role="3TUv4t" value="false" />
              <property role="TrG5h" value="e2" />
              <node concept="3uibUv" id="2yiuz3qhGqx" role="1tU5fm">
                <ref role="3uigEE" to="wyt6:~NumberFormatException" resolve="NumberFormatException" />
              </node>
            </node>
          </node>
          <node concept="3clFbS" id="2yiuz3qhGqn" role="SfCbr">
            <node concept="3clFbF" id="2yiuz3qhGqo" role="3cqZAp">
              <node concept="37vLTI" id="2yiuz3qhGqp" role="3clFbG">
                <node concept="37vLTw" id="2yiuz3qhGqq" role="37vLTJ">
                  <ref role="3cqZAo" node="2yiuz3qhGqi" resolve="fontsize" />
                </node>
                <node concept="2YIFZM" id="2yiuz3qhGWt" role="37vLTx">
                  <ref role="1Pybhc" to="wyt6:~Integer" resolve="Integer" />
                  <ref role="37wK5l" to="wyt6:~Integer.parseInt(java.lang.String):int" resolve="parseInt" />
                  <node concept="2OqwBi" id="2yiuz3qhGqs" role="37wK5m">
                    <node concept="2OqwBi" id="2yiuz3qhGWw" role="2Oq$k0">
                      <node concept="37vLTw" id="2yiuz3qhGWv" role="2Oq$k0">
                        <ref role="3cqZAo" node="2yiuz3qhGhL" resolve="sizeSelector" />
                      </node>
                      <node concept="liA8E" id="2yiuz3qhGWx" role="2OqNvi">
                        <ref role="37wK5l" to="r791:~JTextComponent.getText():java.lang.String" resolve="getText" />
                      </node>
                    </node>
                    <node concept="liA8E" id="2yiuz3qhGqu" role="2OqNvi">
                      <ref role="37wK5l" to="wyt6:~String.trim():java.lang.String" resolve="trim" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3SKdUt" id="2yiuz3qhGJr" role="3cqZAp">
              <node concept="3SKdUq" id="2yiuz3qhGJq" role="3SKWNk">
                <property role="3SKdUp" value="System.out.println(&quot;'&quot; + sizeSelector.getText() + &quot;'&quot;);" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3SKdUt" id="2yiuz3qhGJt" role="3cqZAp">
          <node concept="3SKdUq" id="2yiuz3qhGJs" role="3SKWNk">
            <property role="3SKdUp" value="if a deselect occurred, selection will be -1" />
          </node>
        </node>
        <node concept="3clFbJ" id="2yiuz3qhGqA" role="3cqZAp">
          <node concept="1Wc70l" id="2yiuz3qhGqB" role="3clFbw">
            <node concept="1Wc70l" id="2yiuz3qhGqC" role="3uHU7B">
              <node concept="1eOMI4" id="2yiuz3qhGqG" role="3uHU7B">
                <node concept="3eOSWO" id="2yiuz3qhGqD" role="1eOMHV">
                  <node concept="37vLTw" id="2yiuz3qhGqE" role="3uHU7B">
                    <ref role="3cqZAo" node="2yiuz3qhGqi" resolve="fontsize" />
                  </node>
                  <node concept="3cmrfG" id="2yiuz3qhGqF" role="3uHU7w">
                    <property role="3cmrfH" value="0" />
                  </node>
                </node>
              </node>
              <node concept="1eOMI4" id="2yiuz3qhGqK" role="3uHU7w">
                <node concept="3eOVzh" id="2yiuz3qhGqH" role="1eOMHV">
                  <node concept="37vLTw" id="2yiuz3qhGqI" role="3uHU7B">
                    <ref role="3cqZAo" node="2yiuz3qhGqi" resolve="fontsize" />
                  </node>
                  <node concept="3cmrfG" id="2yiuz3qhGqJ" role="3uHU7w">
                    <property role="3cmrfH" value="256" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="1eOMI4" id="2yiuz3qhGqP" role="3uHU7w">
              <node concept="3y3z36" id="2yiuz3qhGqL" role="1eOMHV">
                <node concept="37vLTw" id="2yiuz3qhGqM" role="3uHU7B">
                  <ref role="3cqZAo" node="2yiuz3qhGij" resolve="selection" />
                </node>
                <node concept="1ZRNhn" id="2yiuz3qhGqN" role="3uHU7w">
                  <node concept="3cmrfG" id="2yiuz3qhGqO" role="2$L3a6">
                    <property role="3cmrfH" value="1" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="3clFbS" id="2yiuz3qhGqR" role="3clFbx">
            <node concept="3SKdUt" id="2yiuz3qhGJv" role="3cqZAp">
              <node concept="3SKdUq" id="2yiuz3qhGJu" role="3SKWNk">
                <property role="3SKdUp" value="font = new Font(list[selection], Font.PLAIN, fontsize);" />
              </node>
            </node>
            <node concept="3cpWs8" id="2yiuz3qhGqT" role="3cqZAp">
              <node concept="3cpWsn" id="2yiuz3qhGqS" role="3cpWs9">
                <property role="3TUv4t" value="false" />
                <property role="TrG5h" value="instance" />
                <node concept="3uibUv" id="2yiuz3qhGqU" role="1tU5fm">
                  <ref role="3uigEE" to="z60i:~Font" resolve="Font" />
                </node>
                <node concept="2OqwBi" id="2yiuz3qhGW$" role="33vP2m">
                  <node concept="37vLTw" id="2yiuz3qhGWz" role="2Oq$k0">
                    <ref role="3cqZAo" node="2yiuz3qhGi3" resolve="table" />
                  </node>
                  <node concept="liA8E" id="2yiuz3qhGW_" role="2OqNvi">
                    <ref role="37wK5l" to="33ny:~Map.get(java.lang.Object):java.lang.Object" resolve="get" />
                    <node concept="AH0OO" id="2yiuz3qhGqW" role="37wK5m">
                      <node concept="37vLTw" id="2yiuz3qhGqX" role="AHHXb">
                        <ref role="3cqZAo" node="2yiuz3qhGif" resolve="list" />
                      </node>
                      <node concept="37vLTw" id="2yiuz3qhGqY" role="AHEQo">
                        <ref role="3cqZAo" node="2yiuz3qhGij" resolve="selection" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3SKdUt" id="2yiuz3qhGJx" role="3cqZAp">
              <node concept="3SKdUq" id="2yiuz3qhGJw" role="3SKWNk">
                <property role="3SKdUp" value="font = instance.deriveFont(Font.PLAIN, fontsize);" />
              </node>
            </node>
            <node concept="3clFbF" id="2yiuz3qhGqZ" role="3cqZAp">
              <node concept="37vLTI" id="2yiuz3qhGr0" role="3clFbG">
                <node concept="37vLTw" id="2yiuz3qhGr1" role="37vLTJ">
                  <ref role="3cqZAo" node="2yiuz3qhGic" resolve="font" />
                </node>
                <node concept="2OqwBi" id="2yiuz3qhGWC" role="37vLTx">
                  <node concept="37vLTw" id="2yiuz3qhGWB" role="2Oq$k0">
                    <ref role="3cqZAo" node="2yiuz3qhGqS" resolve="instance" />
                  </node>
                  <node concept="liA8E" id="2yiuz3qhGWD" role="2OqNvi">
                    <ref role="37wK5l" to="z60i:~Font.deriveFont(int):java.awt.Font" resolve="deriveFont" />
                    <node concept="37vLTw" id="2yiuz3qhGr4" role="37wK5m">
                      <ref role="3cqZAo" node="2yiuz3qhGqi" resolve="fontsize" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3SKdUt" id="2yiuz3qhGJz" role="3cqZAp">
              <node concept="3SKdUq" id="2yiuz3qhGJy" role="3SKWNk">
                <property role="3SKdUp" value="System.out.println(&quot;setting font to &quot; + font);" />
              </node>
            </node>
            <node concept="3clFbF" id="2yiuz3qhGr6" role="3cqZAp">
              <node concept="2OqwBi" id="2yiuz3qhGWG" role="3clFbG">
                <node concept="37vLTw" id="2yiuz3qhGWF" role="2Oq$k0">
                  <ref role="3cqZAo" node="2yiuz3qhGhU" resolve="sample" />
                </node>
                <node concept="liA8E" id="2yiuz3qhGWH" role="2OqNvi">
                  <ref role="37wK5l" to="dxuu:~JComponent.setFont(java.awt.Font):void" resolve="setFont" />
                  <node concept="37vLTw" id="2yiuz3qhGr8" role="37wK5m">
                    <ref role="3cqZAo" node="2yiuz3qhGic" resolve="font" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3cpWs8" id="2yiuz3qhGra" role="3cqZAp">
              <node concept="3cpWsn" id="2yiuz3qhGr9" role="3cpWs9">
                <property role="3TUv4t" value="false" />
                <property role="TrG5h" value="filenameSuggestion" />
                <node concept="17QB3L" id="2yiuz3qlytv" role="1tU5fm" />
                <node concept="2OqwBi" id="2yiuz3qhGrc" role="33vP2m">
                  <node concept="AH0OO" id="2yiuz3qhGrd" role="2Oq$k0">
                    <node concept="37vLTw" id="2yiuz3qhGre" role="AHHXb">
                      <ref role="3cqZAo" node="2yiuz3qhGif" resolve="list" />
                    </node>
                    <node concept="37vLTw" id="2yiuz3qhGrf" role="AHEQo">
                      <ref role="3cqZAo" node="2yiuz3qhGij" resolve="selection" />
                    </node>
                  </node>
                  <node concept="liA8E" id="2yiuz3qhGrg" role="2OqNvi">
                    <ref role="37wK5l" to="wyt6:~String.replace(char,char):java.lang.String" resolve="replace" />
                    <node concept="1Xhbcc" id="2yiuz3qhGrh" role="37wK5m">
                      <property role="1XhdNS" value=" " />
                    </node>
                    <node concept="1Xhbcc" id="2yiuz3qhGri" role="37wK5m">
                      <property role="1XhdNS" value="_" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="2yiuz3qhGrj" role="3cqZAp">
              <node concept="d57v9" id="2yiuz3qhGrk" role="3clFbG">
                <node concept="37vLTw" id="2yiuz3qhGrl" role="37vLTJ">
                  <ref role="3cqZAo" node="2yiuz3qhGr9" resolve="filenameSuggestion" />
                </node>
                <node concept="3cpWs3" id="2yiuz3qhGrm" role="37vLTx">
                  <node concept="Xl_RD" id="2yiuz3qhGrn" role="3uHU7B">
                    <property role="Xl_RC" value="-" />
                  </node>
                  <node concept="37vLTw" id="2yiuz3qhGro" role="3uHU7w">
                    <ref role="3cqZAo" node="2yiuz3qhGqi" resolve="fontsize" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="2yiuz3qhGrp" role="3cqZAp">
              <node concept="2OqwBi" id="2yiuz3qhGWK" role="3clFbG">
                <node concept="37vLTw" id="2yiuz3qhGWJ" role="2Oq$k0">
                  <ref role="3cqZAo" node="2yiuz3qhGi0" resolve="filenameField" />
                </node>
                <node concept="liA8E" id="2yiuz3qhGWL" role="2OqNvi">
                  <ref role="37wK5l" to="r791:~JTextComponent.setText(java.lang.String):void" resolve="setText" />
                  <node concept="37vLTw" id="2yiuz3qhGrr" role="37wK5m">
                    <ref role="3cqZAo" node="2yiuz3qhGr9" resolve="filenameSuggestion" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="3Tm1VV" id="2yiuz3qhGrs" role="1B3o_S" />
      <node concept="3cqZAl" id="2yiuz3qhGrt" role="3clF45" />
    </node>
    <node concept="3clFb_" id="2yiuz3qhGru" role="jymVt">
      <property role="TrG5h" value="build" />
      <property role="DiZV1" value="false" />
      <property role="od$2w" value="false" />
      <node concept="3clFbS" id="2yiuz3qhGrv" role="3clF47">
        <node concept="3cpWs8" id="2yiuz3qhGrx" role="3cqZAp">
          <node concept="3cpWsn" id="2yiuz3qhGrw" role="3cpWs9">
            <property role="3TUv4t" value="false" />
            <property role="TrG5h" value="fontsize" />
            <node concept="10Oyi0" id="2yiuz3qhGry" role="1tU5fm" />
            <node concept="3cmrfG" id="2yiuz3qhGrz" role="33vP2m">
              <property role="3cmrfH" value="0" />
            </node>
          </node>
        </node>
        <node concept="SfApY" id="2yiuz3qhGrM" role="3cqZAp">
          <node concept="TDmWw" id="2yiuz3qhGrN" role="TEbGg">
            <node concept="3clFbS" id="2yiuz3qhGrL" role="TDEfX" />
            <node concept="3cpWsn" id="2yiuz3qhGrH" role="TDEfY">
              <property role="3TUv4t" value="false" />
              <property role="TrG5h" value="e" />
              <node concept="3uibUv" id="2yiuz3qhGrJ" role="1tU5fm">
                <ref role="3uigEE" to="wyt6:~NumberFormatException" resolve="NumberFormatException" />
              </node>
            </node>
          </node>
          <node concept="3clFbS" id="2yiuz3qhGr_" role="SfCbr">
            <node concept="3clFbF" id="2yiuz3qhGrA" role="3cqZAp">
              <node concept="37vLTI" id="2yiuz3qhGrB" role="3clFbG">
                <node concept="37vLTw" id="2yiuz3qhGrC" role="37vLTJ">
                  <ref role="3cqZAo" node="2yiuz3qhGrw" resolve="fontsize" />
                </node>
                <node concept="2YIFZM" id="2yiuz3qhGWN" role="37vLTx">
                  <ref role="1Pybhc" to="wyt6:~Integer" resolve="Integer" />
                  <ref role="37wK5l" to="wyt6:~Integer.parseInt(java.lang.String):int" resolve="parseInt" />
                  <node concept="2OqwBi" id="2yiuz3qhGrE" role="37wK5m">
                    <node concept="2OqwBi" id="2yiuz3qhGWQ" role="2Oq$k0">
                      <node concept="37vLTw" id="2yiuz3qhGWP" role="2Oq$k0">
                        <ref role="3cqZAo" node="2yiuz3qhGhL" resolve="sizeSelector" />
                      </node>
                      <node concept="liA8E" id="2yiuz3qhGWR" role="2OqNvi">
                        <ref role="37wK5l" to="r791:~JTextComponent.getText():java.lang.String" resolve="getText" />
                      </node>
                    </node>
                    <node concept="liA8E" id="2yiuz3qhGrG" role="2OqNvi">
                      <ref role="37wK5l" to="wyt6:~String.trim():java.lang.String" resolve="trim" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbJ" id="2yiuz3qhGrO" role="3cqZAp">
          <node concept="2dkUwp" id="2yiuz3qhGrP" role="3clFbw">
            <node concept="37vLTw" id="2yiuz3qhGrQ" role="3uHU7B">
              <ref role="3cqZAo" node="2yiuz3qhGrw" resolve="fontsize" />
            </node>
            <node concept="3cmrfG" id="2yiuz3qhGrR" role="3uHU7w">
              <property role="3cmrfH" value="0" />
            </node>
          </node>
          <node concept="3clFbS" id="2yiuz3qhGrT" role="3clFbx">
            <node concept="3clFbF" id="2yiuz3qhGrU" role="3cqZAp">
              <node concept="2YIFZM" id="2yiuz3qhGWT" role="3clFbG">
                <ref role="1Pybhc" to="dxuu:~JOptionPane" resolve="JOptionPane" />
                <ref role="37wK5l" to="dxuu:~JOptionPane.showMessageDialog(java.awt.Component,java.lang.Object,java.lang.String,int):void" resolve="showMessageDialog" />
                <node concept="Xjq3P" id="2yiuz3qhGrW" role="37wK5m" />
                <node concept="Xl_RD" id="2yiuz3qhGrX" role="37wK5m">
                  <property role="Xl_RC" value="Bad font size, try again." />
                </node>
                <node concept="Xl_RD" id="2yiuz3qhGrY" role="37wK5m">
                  <property role="Xl_RC" value="Badness" />
                </node>
                <node concept="10M0yZ" id="2yiuz3qhXsT" role="37wK5m">
                  <ref role="1PxDUh" to="dxuu:~JOptionPane" resolve="JOptionPane" />
                  <ref role="3cqZAo" to="dxuu:~JOptionPane.WARNING_MESSAGE" resolve="WARNING_MESSAGE" />
                </node>
              </node>
            </node>
            <node concept="3cpWs6" id="2yiuz3qhGs0" role="3cqZAp" />
          </node>
        </node>
        <node concept="3cpWs8" id="2yiuz3qhGs2" role="3cqZAp">
          <node concept="3cpWsn" id="2yiuz3qhGs1" role="3cpWs9">
            <property role="3TUv4t" value="false" />
            <property role="TrG5h" value="filename" />
            <node concept="17QB3L" id="2yiuz3qlytq" role="1tU5fm" />
            <node concept="2OqwBi" id="2yiuz3qhGs4" role="33vP2m">
              <node concept="2OqwBi" id="2yiuz3qhGWY" role="2Oq$k0">
                <node concept="37vLTw" id="2yiuz3qhGWX" role="2Oq$k0">
                  <ref role="3cqZAo" node="2yiuz3qhGi0" resolve="filenameField" />
                </node>
                <node concept="liA8E" id="2yiuz3qhGWZ" role="2OqNvi">
                  <ref role="37wK5l" to="r791:~JTextComponent.getText():java.lang.String" resolve="getText" />
                </node>
              </node>
              <node concept="liA8E" id="2yiuz3qhGs6" role="2OqNvi">
                <ref role="37wK5l" to="wyt6:~String.trim():java.lang.String" resolve="trim" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbJ" id="2yiuz3qhGs7" role="3cqZAp">
          <node concept="3clFbC" id="2yiuz3qhGs8" role="3clFbw">
            <node concept="2OqwBi" id="2yiuz3qhGX2" role="3uHU7B">
              <node concept="37vLTw" id="2yiuz3qhGX1" role="2Oq$k0">
                <ref role="3cqZAo" node="2yiuz3qhGs1" resolve="filename" />
              </node>
              <node concept="liA8E" id="2yiuz3qhGX3" role="2OqNvi">
                <ref role="37wK5l" to="wyt6:~String.length():int" resolve="length" />
              </node>
            </node>
            <node concept="3cmrfG" id="2yiuz3qhGsa" role="3uHU7w">
              <property role="3cmrfH" value="0" />
            </node>
          </node>
          <node concept="3clFbS" id="2yiuz3qhGsc" role="3clFbx">
            <node concept="3clFbF" id="2yiuz3qhGsd" role="3cqZAp">
              <node concept="2YIFZM" id="2yiuz3qhGX5" role="3clFbG">
                <ref role="1Pybhc" to="dxuu:~JOptionPane" resolve="JOptionPane" />
                <ref role="37wK5l" to="dxuu:~JOptionPane.showMessageDialog(java.awt.Component,java.lang.Object,java.lang.String,int):void" resolve="showMessageDialog" />
                <node concept="Xjq3P" id="2yiuz3qhGsf" role="37wK5m" />
                <node concept="Xl_RD" id="2yiuz3qhGsg" role="37wK5m">
                  <property role="Xl_RC" value="Enter a file name for the font." />
                </node>
                <node concept="Xl_RD" id="2yiuz3qhGsh" role="37wK5m">
                  <property role="Xl_RC" value="Lameness" />
                </node>
                <node concept="10M0yZ" id="2yiuz3qhXsU" role="37wK5m">
                  <ref role="1PxDUh" to="dxuu:~JOptionPane" resolve="JOptionPane" />
                  <ref role="3cqZAo" to="dxuu:~JOptionPane.WARNING_MESSAGE" resolve="WARNING_MESSAGE" />
                </node>
              </node>
            </node>
            <node concept="3cpWs6" id="2yiuz3qhGsj" role="3cqZAp" />
          </node>
        </node>
        <node concept="3clFbJ" id="2yiuz3qhGsk" role="3cqZAp">
          <node concept="3fqX7Q" id="2yiuz3qhGsl" role="3clFbw">
            <node concept="2OqwBi" id="2yiuz3qhGXa" role="3fr31v">
              <node concept="37vLTw" id="2yiuz3qhGX9" role="2Oq$k0">
                <ref role="3cqZAo" node="2yiuz3qhGs1" resolve="filename" />
              </node>
              <node concept="liA8E" id="2yiuz3qhGXb" role="2OqNvi">
                <ref role="37wK5l" to="wyt6:~String.endsWith(java.lang.String):boolean" resolve="endsWith" />
                <node concept="Xl_RD" id="2yiuz3qhGsn" role="37wK5m">
                  <property role="Xl_RC" value=".vlw" />
                </node>
              </node>
            </node>
          </node>
          <node concept="3clFbS" id="2yiuz3qhGsp" role="3clFbx">
            <node concept="3clFbF" id="2yiuz3qhGsq" role="3cqZAp">
              <node concept="d57v9" id="2yiuz3qhGsr" role="3clFbG">
                <node concept="37vLTw" id="2yiuz3qhGss" role="37vLTJ">
                  <ref role="3cqZAo" node="2yiuz3qhGs1" resolve="filename" />
                </node>
                <node concept="Xl_RD" id="2yiuz3qhGst" role="37vLTx">
                  <property role="Xl_RC" value=".vlw" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="SfApY" id="2yiuz3qhGti" role="3cqZAp">
          <node concept="TDmWw" id="2yiuz3qhGtj" role="TEbGg">
            <node concept="3clFbS" id="2yiuz3qhGt8" role="TDEfX">
              <node concept="3clFbF" id="2yiuz3qhGt9" role="3cqZAp">
                <node concept="2YIFZM" id="2yiuz3qhGXd" role="3clFbG">
                  <ref role="1Pybhc" to="dxuu:~JOptionPane" resolve="JOptionPane" />
                  <ref role="37wK5l" to="dxuu:~JOptionPane.showMessageDialog(java.awt.Component,java.lang.Object,java.lang.String,int):void" resolve="showMessageDialog" />
                  <node concept="Xjq3P" id="2yiuz3qhGtb" role="37wK5m">
                    <ref role="1HBi2w" node="2yiuz3qhGhA" resolve="CreateFont" />
                  </node>
                  <node concept="Xl_RD" id="2yiuz3qhGtd" role="37wK5m">
                    <property role="Xl_RC" value="An error occurred while creating font." />
                  </node>
                  <node concept="Xl_RD" id="2yiuz3qhGte" role="37wK5m">
                    <property role="Xl_RC" value="No font for you" />
                  </node>
                  <node concept="10M0yZ" id="2yiuz3qhXsV" role="37wK5m">
                    <ref role="1PxDUh" to="dxuu:~JOptionPane" resolve="JOptionPane" />
                    <ref role="3cqZAo" to="dxuu:~JOptionPane.WARNING_MESSAGE" resolve="WARNING_MESSAGE" />
                  </node>
                </node>
              </node>
              <node concept="3clFbF" id="2yiuz3qhGtg" role="3cqZAp">
                <node concept="2OqwBi" id="2yiuz3qhGXi" role="3clFbG">
                  <node concept="37vLTw" id="2yiuz3qhGXh" role="2Oq$k0">
                    <ref role="3cqZAo" node="2yiuz3qhGt4" resolve="e" />
                  </node>
                  <node concept="liA8E" id="2yiuz3qhGXj" role="2OqNvi">
                    <ref role="37wK5l" to="wyt6:~Throwable.printStackTrace():void" resolve="printStackTrace" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3cpWsn" id="2yiuz3qhGt4" role="TDEfY">
              <property role="3TUv4t" value="false" />
              <property role="TrG5h" value="e" />
              <node concept="3uibUv" id="2yiuz3qhGt6" role="1tU5fm">
                <ref role="3uigEE" to="guwi:~IOException" resolve="IOException" />
              </node>
            </node>
          </node>
          <node concept="3clFbS" id="2yiuz3qhGsv" role="SfCbr">
            <node concept="3cpWs8" id="2yiuz3qhGsx" role="3cqZAp">
              <node concept="3cpWsn" id="2yiuz3qhGsw" role="3cpWs9">
                <property role="3TUv4t" value="false" />
                <property role="TrG5h" value="instance" />
                <node concept="3uibUv" id="2yiuz3qhGsy" role="1tU5fm">
                  <ref role="3uigEE" to="z60i:~Font" resolve="Font" />
                </node>
                <node concept="2OqwBi" id="2yiuz3qhGXm" role="33vP2m">
                  <node concept="37vLTw" id="2yiuz3qhGXl" role="2Oq$k0">
                    <ref role="3cqZAo" node="2yiuz3qhGi3" resolve="table" />
                  </node>
                  <node concept="liA8E" id="2yiuz3qhGXn" role="2OqNvi">
                    <ref role="37wK5l" to="33ny:~Map.get(java.lang.Object):java.lang.Object" resolve="get" />
                    <node concept="AH0OO" id="2yiuz3qhGs$" role="37wK5m">
                      <node concept="37vLTw" id="2yiuz3qhGs_" role="AHHXb">
                        <ref role="3cqZAo" node="2yiuz3qhGif" resolve="list" />
                      </node>
                      <node concept="37vLTw" id="2yiuz3qhGsA" role="AHEQo">
                        <ref role="3cqZAo" node="2yiuz3qhGij" resolve="selection" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="2yiuz3qhGsB" role="3cqZAp">
              <node concept="37vLTI" id="2yiuz3qhGsC" role="3clFbG">
                <node concept="37vLTw" id="2yiuz3qhGsD" role="37vLTJ">
                  <ref role="3cqZAo" node="2yiuz3qhGic" resolve="font" />
                </node>
                <node concept="2OqwBi" id="2yiuz3qhGXq" role="37vLTx">
                  <node concept="37vLTw" id="2yiuz3qhGXp" role="2Oq$k0">
                    <ref role="3cqZAo" node="2yiuz3qhGsw" resolve="instance" />
                  </node>
                  <node concept="liA8E" id="2yiuz3qhGXr" role="2OqNvi">
                    <ref role="37wK5l" to="z60i:~Font.deriveFont(int,float):java.awt.Font" resolve="deriveFont" />
                    <node concept="10M0yZ" id="2yiuz3qhXsW" role="37wK5m">
                      <ref role="1PxDUh" to="z60i:~Font" resolve="Font" />
                      <ref role="3cqZAo" to="z60i:~Font.PLAIN" resolve="PLAIN" />
                    </node>
                    <node concept="37vLTw" id="2yiuz3qhGsG" role="37wK5m">
                      <ref role="3cqZAo" node="2yiuz3qhGrw" resolve="fontsize" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3SKdUt" id="2yiuz3qhGJ_" role="3cqZAp">
              <node concept="3SKdUq" id="2yiuz3qhGJ$" role="3SKWNk">
                <property role="3SKdUp" value="PFont f = new PFont(font, smooth, all ? null : PFont.CHARSET);" />
              </node>
            </node>
            <node concept="3cpWs8" id="2yiuz3qhGsI" role="3cqZAp">
              <node concept="3cpWsn" id="2yiuz3qhGsH" role="3cpWs9">
                <property role="3TUv4t" value="false" />
                <property role="TrG5h" value="f" />
                <node concept="3uibUv" id="2yiuz3qhGsJ" role="1tU5fm">
                  <ref role="3uigEE" to="r7oa:~PFont" resolve="PFont" />
                </node>
                <node concept="2ShNRf" id="2yiuz3qhGXu" role="33vP2m">
                  <node concept="1pGfFk" id="2yiuz3qhGXv" role="2ShVmc">
                    <ref role="37wK5l" to="r7oa:~PFont.&lt;init&gt;(java.awt.Font,boolean,char[])" resolve="PFont" />
                    <node concept="37vLTw" id="2yiuz3qhGsL" role="37wK5m">
                      <ref role="3cqZAo" node="2yiuz3qhGic" resolve="font" />
                    </node>
                    <node concept="37vLTw" id="2yiuz3qhGsM" role="37wK5m">
                      <ref role="3cqZAo" node="2yiuz3qhGi8" resolve="smooth" />
                    </node>
                    <node concept="2OqwBi" id="2yiuz3qhGXy" role="37wK5m">
                      <node concept="37vLTw" id="2yiuz3qhGXx" role="2Oq$k0">
                        <ref role="3cqZAo" node="2yiuz3qhGio" resolve="charSelector" />
                      </node>
                      <node concept="liA8E" id="2yiuz3qhGXz" role="2OqNvi">
                        <ref role="37wK5l" node="2yiuz3qhGD5" resolve="getCharacters" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3SKdUt" id="2yiuz3qhGJD" role="3cqZAp">
              <node concept="3SKdUq" id="2yiuz3qhGJC" role="3SKWNk">
                <property role="3SKdUp" value="make sure the 'data' folder exists" />
              </node>
            </node>
            <node concept="3cpWs8" id="2yiuz3qhGsT" role="3cqZAp">
              <node concept="3cpWsn" id="2yiuz3qhGsS" role="3cpWs9">
                <property role="3TUv4t" value="false" />
                <property role="TrG5h" value="folder" />
                <node concept="3uibUv" id="2yiuz3qhGsU" role="1tU5fm">
                  <ref role="3uigEE" to="guwi:~File" resolve="File" />
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="2yiuz3qhGsY" role="3cqZAp">
              <node concept="2OqwBi" id="2yiuz3qhGXI" role="3clFbG">
                <node concept="37vLTw" id="2yiuz3qhGXH" role="2Oq$k0">
                  <ref role="3cqZAo" node="2yiuz3qhGsH" resolve="f" />
                </node>
                <node concept="liA8E" id="2yiuz3qhGXJ" role="2OqNvi">
                  <ref role="37wK5l" to="r7oa:~PFont.save(java.io.OutputStream):void" resolve="save" />
                  <node concept="2ShNRf" id="2yiuz3qhGXK" role="37wK5m">
                    <node concept="1pGfFk" id="2yiuz3qhGY5" role="2ShVmc">
                      <ref role="37wK5l" to="guwi:~FileOutputStream.&lt;init&gt;(java.io.File)" resolve="FileOutputStream" />
                      <node concept="2ShNRf" id="2yiuz3qhGY6" role="37wK5m">
                        <node concept="1pGfFk" id="2yiuz3qhGYi" role="2ShVmc">
                          <ref role="37wK5l" to="guwi:~File.&lt;init&gt;(java.lang.String,java.lang.String)" resolve="File" />
                          <node concept="37vLTw" id="2yiuz3qjfy3" role="37wK5m">
                            <ref role="3cqZAo" node="2yiuz3qjaJN" resolve="dataPath" />
                          </node>
                          <node concept="37vLTw" id="2yiuz3qhGt3" role="37wK5m">
                            <ref role="3cqZAo" node="2yiuz3qhGs1" resolve="filename" />
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhGtk" role="3cqZAp">
          <node concept="1rXfSq" id="2yiuz3qhGtl" role="3clFbG">
            <ref role="37wK5l" to="z60i:~Window.setVisible(boolean):void" resolve="setVisible" />
            <node concept="3clFbT" id="2yiuz3qhGtm" role="37wK5m">
              <property role="3clFbU" value="false" />
            </node>
          </node>
        </node>
      </node>
      <node concept="3Tm1VV" id="2yiuz3qhGtn" role="1B3o_S" />
      <node concept="3cqZAl" id="2yiuz3qhGto" role="3clF45" />
    </node>
  </node>
  <node concept="312cEu" id="2yiuz3qhGtK">
    <property role="TrG5h" value="SampleComponent" />
    <property role="2bfB8j" value="true" />
    <property role="1sVAO0" value="false" />
    <property role="1EXbeo" value="false" />
    <property role="3GE5qa" value="createFont" />
    <node concept="3uibUv" id="2yiuz3qhGtL" role="1zkMxy">
      <ref role="3uigEE" to="dxuu:~JComponent" resolve="JComponent" />
    </node>
    <node concept="3UR2Jj" id="2yiuz3qhGw8" role="lGtFl">
      <node concept="TZ5HA" id="2yiuz3qhGJE" role="TZ5H$">
        <node concept="1dT_AC" id="2yiuz3qhGJF" role="1dT_Ay">
          <property role="1dT_AB" value="Component that draws the sample text. This is its own subclassed component" />
        </node>
      </node>
      <node concept="TZ5HA" id="2yiuz3qhGJG" role="TZ5H$">
        <node concept="1dT_AC" id="2yiuz3qhGJH" role="1dT_Ay">
          <property role="1dT_AB" value="because Mac OS X controls seem to reset the RenderingHints for smoothing" />
        </node>
      </node>
      <node concept="TZ5HA" id="2yiuz3qhGJI" role="TZ5H$">
        <node concept="1dT_AC" id="2yiuz3qhGJJ" role="1dT_Ay">
          <property role="1dT_AB" value="so that they cannot be overridden properly for JLabel or JTextArea." />
        </node>
      </node>
      <node concept="TZ5HA" id="2yiuz3qhGJK" role="TZ5H$">
        <node concept="1dT_AC" id="2yiuz3qhGJL" role="1dT_Ay">
          <property role="1dT_AB" value="@author fry" />
        </node>
      </node>
    </node>
    <node concept="312cEg" id="2yiuz3qhGtM" role="jymVt">
      <property role="34CwA1" value="false" />
      <property role="eg7rD" value="false" />
      <property role="TrG5h" value="text" />
      <property role="3TUv4t" value="false" />
      <node concept="17QB3L" id="2yiuz3qlyt_" role="1tU5fm" />
      <node concept="Xl_RD" id="2yiuz3qhGtP" role="33vP2m">
        <property role="Xl_RC" value="Forsaking monastic tradition, twelve jovial friars gave up their vocation for a questionable existence on the flying trapeze." />
      </node>
    </node>
    <node concept="312cEg" id="2yiuz3qhGtQ" role="jymVt">
      <property role="34CwA1" value="false" />
      <property role="eg7rD" value="false" />
      <property role="TrG5h" value="high" />
      <property role="3TUv4t" value="false" />
      <node concept="10Oyi0" id="2yiuz3qhGtS" role="1tU5fm" />
      <node concept="3cmrfG" id="2yiuz3qhGtT" role="33vP2m">
        <property role="3cmrfH" value="80" />
      </node>
    </node>
    <node concept="312cEg" id="2yiuz3qhGtU" role="jymVt">
      <property role="34CwA1" value="false" />
      <property role="eg7rD" value="false" />
      <property role="TrG5h" value="parent" />
      <property role="3TUv4t" value="false" />
      <node concept="3uibUv" id="2yiuz3qhGtW" role="1tU5fm">
        <ref role="3uigEE" node="2yiuz3qhGhA" resolve="CreateFont" />
      </node>
    </node>
    <node concept="3clFbW" id="2yiuz3qhGtX" role="jymVt">
      <property role="DiZV1" value="false" />
      <property role="od$2w" value="false" />
      <node concept="3cqZAl" id="2yiuz3qhGtY" role="3clF45" />
      <node concept="37vLTG" id="2yiuz3qhGtZ" role="3clF46">
        <property role="TrG5h" value="p" />
        <property role="3TUv4t" value="false" />
        <node concept="3uibUv" id="2yiuz3qhGu0" role="1tU5fm">
          <ref role="3uigEE" node="2yiuz3qhGhA" resolve="CreateFont" />
        </node>
      </node>
      <node concept="3clFbS" id="2yiuz3qhGu1" role="3clF47">
        <node concept="3clFbF" id="2yiuz3qhGu2" role="3cqZAp">
          <node concept="37vLTI" id="2yiuz3qhGu3" role="3clFbG">
            <node concept="2OqwBi" id="2yiuz3qhGu4" role="37vLTJ">
              <node concept="Xjq3P" id="2yiuz3qhGu5" role="2Oq$k0" />
              <node concept="2OwXpG" id="2yiuz3qhGu6" role="2OqNvi">
                <ref role="2Oxat5" node="2yiuz3qhGtU" resolve="parent" />
              </node>
            </node>
            <node concept="37vLTw" id="2yiuz3qhGu7" role="37vLTx">
              <ref role="3cqZAo" node="2yiuz3qhGtZ" resolve="p" />
            </node>
          </node>
        </node>
        <node concept="3SKdUt" id="2yiuz3qhGJN" role="3cqZAp">
          <node concept="3SKdUq" id="2yiuz3qhGJM" role="3SKWNk">
            <property role="3SKdUp" value="and yet, we still need an inner class to handle the basics." />
          </node>
        </node>
        <node concept="3SKdUt" id="2yiuz3qhGJP" role="3cqZAp">
          <node concept="3SKdUq" id="2yiuz3qhGJO" role="3SKWNk">
            <property role="3SKdUp" value="or no, maybe i'll refactor this as a separate class!" />
          </node>
        </node>
        <node concept="3SKdUt" id="2yiuz3qhGJR" role="3cqZAp">
          <node concept="3SKdUq" id="2yiuz3qhGJQ" role="3SKWNk">
            <property role="3SKdUp" value="maybe a few getters and setters? mmm?" />
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhGu8" role="3cqZAp">
          <node concept="1rXfSq" id="2yiuz3qhGu9" role="3clFbG">
            <ref role="37wK5l" to="z60i:~Component.addMouseListener(java.awt.event.MouseListener):void" resolve="addMouseListener" />
            <node concept="2ShNRf" id="2yiuz3qhGua" role="37wK5m">
              <node concept="YeOm9" id="2yiuz3qhGub" role="2ShVmc">
                <node concept="1Y3b0j" id="2yiuz3qhGuc" role="YeSDq">
                  <property role="1sVAO0" value="false" />
                  <property role="1EXbeo" value="false" />
                  <ref role="1Y3XeK" to="hyam:~MouseAdapter" resolve="MouseAdapter" />
                  <ref role="37wK5l" to="hyam:~MouseAdapter.&lt;init&gt;()" resolve="MouseAdapter" />
                  <node concept="3clFb_" id="2yiuz3qhGud" role="jymVt">
                    <property role="TrG5h" value="mousePressed" />
                    <property role="DiZV1" value="false" />
                    <property role="od$2w" value="false" />
                    <node concept="37vLTG" id="2yiuz3qhGue" role="3clF46">
                      <property role="TrG5h" value="e" />
                      <property role="3TUv4t" value="false" />
                      <node concept="3uibUv" id="2yiuz3qhGuf" role="1tU5fm">
                        <ref role="3uigEE" to="hyam:~MouseEvent" resolve="MouseEvent" />
                      </node>
                    </node>
                    <node concept="3clFbS" id="2yiuz3qhGug" role="3clF47">
                      <node concept="3cpWs8" id="2yiuz3qhGui" role="3cqZAp">
                        <node concept="3cpWsn" id="2yiuz3qhGuh" role="3cpWs9">
                          <property role="3TUv4t" value="false" />
                          <property role="TrG5h" value="input" />
                          <node concept="17QB3L" id="2yiuz3qlyt$" role="1tU5fm" />
                          <node concept="10QFUN" id="2yiuz3qhGuk" role="33vP2m">
                            <node concept="2YIFZM" id="2yiuz3qhGYm" role="10QFUP">
                              <ref role="1Pybhc" to="dxuu:~JOptionPane" resolve="JOptionPane" />
                              <ref role="37wK5l" to="dxuu:~JOptionPane.showInputDialog(java.awt.Component,java.lang.Object,java.lang.String,int,javax.swing.Icon,java.lang.Object[],java.lang.Object):java.lang.Object" resolve="showInputDialog" />
                              <node concept="37vLTw" id="2yiuz3qhGum" role="37wK5m">
                                <ref role="3cqZAo" node="2yiuz3qhGtU" resolve="parent" />
                              </node>
                              <node concept="Xl_RD" id="2yiuz3qhGun" role="37wK5m">
                                <property role="Xl_RC" value="Enter new sample text:" />
                              </node>
                              <node concept="Xl_RD" id="2yiuz3qhGuo" role="37wK5m">
                                <property role="Xl_RC" value="Sample Text" />
                              </node>
                              <node concept="10M0yZ" id="2yiuz3qhXsX" role="37wK5m">
                                <ref role="1PxDUh" to="dxuu:~JOptionPane" resolve="JOptionPane" />
                                <ref role="3cqZAo" to="dxuu:~JOptionPane.PLAIN_MESSAGE" resolve="PLAIN_MESSAGE" />
                              </node>
                              <node concept="10Nm6u" id="2yiuz3qhGuq" role="37wK5m" />
                              <node concept="10Nm6u" id="2yiuz3qhGur" role="37wK5m" />
                              <node concept="37vLTw" id="2yiuz3qhGus" role="37wK5m">
                                <ref role="3cqZAo" node="2yiuz3qhGtM" resolve="text" />
                              </node>
                            </node>
                            <node concept="17QB3L" id="2yiuz3qlytA" role="10QFUM" />
                          </node>
                        </node>
                      </node>
                      <node concept="3SKdUt" id="2yiuz3qhGJT" role="3cqZAp">
                        <node concept="3SKdUq" id="2yiuz3qhGJS" role="3SKWNk">
                          <property role="3SKdUp" value="icon" />
                        </node>
                      </node>
                      <node concept="3SKdUt" id="2yiuz3qhGJV" role="3cqZAp">
                        <node concept="3SKdUq" id="2yiuz3qhGJU" role="3SKWNk">
                          <property role="3SKdUp" value="choices" />
                        </node>
                      </node>
                      <node concept="3clFbJ" id="2yiuz3qhGuu" role="3cqZAp">
                        <node concept="3y3z36" id="2yiuz3qhGuv" role="3clFbw">
                          <node concept="37vLTw" id="2yiuz3qhGuw" role="3uHU7B">
                            <ref role="3cqZAo" node="2yiuz3qhGuh" resolve="input" />
                          </node>
                          <node concept="10Nm6u" id="2yiuz3qhGux" role="3uHU7w" />
                        </node>
                        <node concept="3clFbS" id="2yiuz3qhGuz" role="3clFbx">
                          <node concept="3clFbF" id="2yiuz3qhGu$" role="3cqZAp">
                            <node concept="37vLTI" id="2yiuz3qhGu_" role="3clFbG">
                              <node concept="37vLTw" id="2yiuz3qhGuA" role="37vLTJ">
                                <ref role="3cqZAo" node="2yiuz3qhGtM" resolve="text" />
                              </node>
                              <node concept="37vLTw" id="2yiuz3qhGuB" role="37vLTx">
                                <ref role="3cqZAo" node="2yiuz3qhGuh" resolve="input" />
                              </node>
                            </node>
                          </node>
                          <node concept="3clFbF" id="2yiuz3qhGuC" role="3cqZAp">
                            <node concept="2OqwBi" id="2yiuz3qhGYv" role="3clFbG">
                              <node concept="37vLTw" id="2yiuz3qhGYu" role="2Oq$k0">
                                <ref role="3cqZAo" node="2yiuz3qhGtU" resolve="parent" />
                              </node>
                              <node concept="liA8E" id="2yiuz3qhGYw" role="2OqNvi">
                                <ref role="37wK5l" to="z60i:~Component.repaint():void" resolve="repaint" />
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                    <node concept="3Tm1VV" id="2yiuz3qhGuE" role="1B3o_S" />
                    <node concept="3cqZAl" id="2yiuz3qhGuF" role="3clF45" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="3Tm1VV" id="2yiuz3qhGuG" role="1B3o_S" />
    </node>
    <node concept="3clFb_" id="2yiuz3qhGuH" role="jymVt">
      <property role="TrG5h" value="paintComponent" />
      <property role="DiZV1" value="false" />
      <property role="od$2w" value="false" />
      <node concept="37vLTG" id="2yiuz3qhGuI" role="3clF46">
        <property role="TrG5h" value="g" />
        <property role="3TUv4t" value="false" />
        <node concept="3uibUv" id="2yiuz3qhGuJ" role="1tU5fm">
          <ref role="3uigEE" to="z60i:~Graphics" resolve="Graphics" />
        </node>
      </node>
      <node concept="3clFbS" id="2yiuz3qhGuK" role="3clF47">
        <node concept="3SKdUt" id="2yiuz3qhGJX" role="3cqZAp">
          <node concept="3SKdUq" id="2yiuz3qhGJW" role="3SKWNk">
            <property role="3SKdUp" value="System.out.println(&quot;smoothing set to &quot; + smooth);" />
          </node>
        </node>
        <node concept="3cpWs8" id="2yiuz3qhGuM" role="3cqZAp">
          <node concept="3cpWsn" id="2yiuz3qhGuL" role="3cpWs9">
            <property role="3TUv4t" value="false" />
            <property role="TrG5h" value="g2" />
            <node concept="3uibUv" id="2yiuz3qhGuN" role="1tU5fm">
              <ref role="3uigEE" to="z60i:~Graphics2D" resolve="Graphics2D" />
            </node>
            <node concept="10QFUN" id="2yiuz3qhGuO" role="33vP2m">
              <node concept="37vLTw" id="2yiuz3qhGuP" role="10QFUP">
                <ref role="3cqZAo" node="2yiuz3qhGuI" resolve="g" />
              </node>
              <node concept="3uibUv" id="2yiuz3qhGuQ" role="10QFUM">
                <ref role="3uigEE" to="z60i:~Graphics2D" resolve="Graphics2D" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhGuR" role="3cqZAp">
          <node concept="2OqwBi" id="2yiuz3qhGYz" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qhGYy" role="2Oq$k0">
              <ref role="3cqZAo" node="2yiuz3qhGuL" resolve="g2" />
            </node>
            <node concept="liA8E" id="2yiuz3qhGY$" role="2OqNvi">
              <ref role="37wK5l" to="z60i:~Graphics.setColor(java.awt.Color):void" resolve="setColor" />
              <node concept="10M0yZ" id="2yiuz3qhXsY" role="37wK5m">
                <ref role="1PxDUh" to="z60i:~Color" resolve="Color" />
                <ref role="3cqZAo" to="z60i:~Color.WHITE" resolve="WHITE" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="2yiuz3qhGuV" role="3cqZAp">
          <node concept="3cpWsn" id="2yiuz3qhGuU" role="3cpWs9">
            <property role="3TUv4t" value="false" />
            <property role="TrG5h" value="dim" />
            <node concept="3uibUv" id="2yiuz3qhGuW" role="1tU5fm">
              <ref role="3uigEE" to="z60i:~Dimension" resolve="Dimension" />
            </node>
            <node concept="1rXfSq" id="2yiuz3qhGuX" role="33vP2m">
              <ref role="37wK5l" to="z60i:~Component.getSize():java.awt.Dimension" resolve="getSize" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhGuY" role="3cqZAp">
          <node concept="2OqwBi" id="2yiuz3qhGYD" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qhGYC" role="2Oq$k0">
              <ref role="3cqZAo" node="2yiuz3qhGuL" resolve="g2" />
            </node>
            <node concept="liA8E" id="2yiuz3qhGYE" role="2OqNvi">
              <ref role="37wK5l" to="z60i:~Graphics.fillRect(int,int,int,int):void" resolve="fillRect" />
              <node concept="3cmrfG" id="2yiuz3qhGv0" role="37wK5m">
                <property role="3cmrfH" value="0" />
              </node>
              <node concept="3cmrfG" id="2yiuz3qhGv1" role="37wK5m">
                <property role="3cmrfH" value="0" />
              </node>
              <node concept="2OqwBi" id="2yiuz3qhGYH" role="37wK5m">
                <node concept="37vLTw" id="2yiuz3qhGYG" role="2Oq$k0">
                  <ref role="3cqZAo" node="2yiuz3qhGuU" resolve="dim" />
                </node>
                <node concept="2OwXpG" id="2yiuz3qhGYI" role="2OqNvi">
                  <ref role="2Oxat5" to="z60i:~Dimension.width" resolve="width" />
                </node>
              </node>
              <node concept="2OqwBi" id="2yiuz3qhGYL" role="37wK5m">
                <node concept="37vLTw" id="2yiuz3qhGYK" role="2Oq$k0">
                  <ref role="3cqZAo" node="2yiuz3qhGuU" resolve="dim" />
                </node>
                <node concept="2OwXpG" id="2yiuz3qhGYM" role="2OqNvi">
                  <ref role="2Oxat5" to="z60i:~Dimension.height" resolve="height" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhGv4" role="3cqZAp">
          <node concept="2OqwBi" id="2yiuz3qhGYP" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qhGYO" role="2Oq$k0">
              <ref role="3cqZAo" node="2yiuz3qhGuL" resolve="g2" />
            </node>
            <node concept="liA8E" id="2yiuz3qhGYQ" role="2OqNvi">
              <ref role="37wK5l" to="z60i:~Graphics.setColor(java.awt.Color):void" resolve="setColor" />
              <node concept="10M0yZ" id="2yiuz3qhXsZ" role="37wK5m">
                <ref role="1PxDUh" to="z60i:~Color" resolve="Color" />
                <ref role="3cqZAo" to="z60i:~Color.BLACK" resolve="BLACK" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhGv7" role="3cqZAp">
          <node concept="2OqwBi" id="2yiuz3qhGYV" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qhGYU" role="2Oq$k0">
              <ref role="3cqZAo" node="2yiuz3qhGuL" resolve="g2" />
            </node>
            <node concept="liA8E" id="2yiuz3qhGYW" role="2OqNvi">
              <ref role="37wK5l" to="z60i:~Graphics2D.setRenderingHint(java.awt.RenderingHints$Key,java.lang.Object):void" resolve="setRenderingHint" />
              <node concept="10M0yZ" id="2yiuz3qhXt0" role="37wK5m">
                <ref role="1PxDUh" to="z60i:~RenderingHints" resolve="RenderingHints" />
                <ref role="3cqZAo" to="z60i:~RenderingHints.KEY_TEXT_ANTIALIASING" resolve="KEY_TEXT_ANTIALIASING" />
              </node>
              <node concept="3K4zz7" id="2yiuz3qhGvd" role="37wK5m">
                <node concept="2OqwBi" id="2yiuz3qhGZ1" role="3K4Cdx">
                  <node concept="37vLTw" id="2yiuz3qhGZ0" role="2Oq$k0">
                    <ref role="3cqZAo" node="2yiuz3qhGtU" resolve="parent" />
                  </node>
                  <node concept="2OwXpG" id="2yiuz3qhGZ2" role="2OqNvi">
                    <ref role="2Oxat5" node="2yiuz3qhGi8" resolve="smooth" />
                  </node>
                </node>
                <node concept="10M0yZ" id="2yiuz3qhXt1" role="3K4E3e">
                  <ref role="1PxDUh" to="z60i:~RenderingHints" resolve="RenderingHints" />
                  <ref role="3cqZAo" to="z60i:~RenderingHints.VALUE_TEXT_ANTIALIAS_ON" resolve="VALUE_TEXT_ANTIALIAS_ON" />
                </node>
                <node concept="10M0yZ" id="2yiuz3qhXt2" role="3K4GZi">
                  <ref role="1PxDUh" to="z60i:~RenderingHints" resolve="RenderingHints" />
                  <ref role="3cqZAo" to="z60i:~RenderingHints.VALUE_TEXT_ANTIALIAS_OFF" resolve="VALUE_TEXT_ANTIALIAS_OFF" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3SKdUt" id="2yiuz3qhGJZ" role="3cqZAp">
          <node concept="3SKdUq" id="2yiuz3qhGJY" role="3SKWNk">
            <property role="3SKdUp" value="add this one as well (after 1.0.9)" />
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhGve" role="3cqZAp">
          <node concept="2OqwBi" id="2yiuz3qhGZ9" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qhGZ8" role="2Oq$k0">
              <ref role="3cqZAo" node="2yiuz3qhGuL" resolve="g2" />
            </node>
            <node concept="liA8E" id="2yiuz3qhGZa" role="2OqNvi">
              <ref role="37wK5l" to="z60i:~Graphics2D.setRenderingHint(java.awt.RenderingHints$Key,java.lang.Object):void" resolve="setRenderingHint" />
              <node concept="10M0yZ" id="2yiuz3qhXt3" role="37wK5m">
                <ref role="1PxDUh" to="z60i:~RenderingHints" resolve="RenderingHints" />
                <ref role="3cqZAo" to="z60i:~RenderingHints.KEY_ANTIALIASING" resolve="KEY_ANTIALIASING" />
              </node>
              <node concept="3K4zz7" id="2yiuz3qhGvk" role="37wK5m">
                <node concept="2OqwBi" id="2yiuz3qhGZf" role="3K4Cdx">
                  <node concept="37vLTw" id="2yiuz3qhGZe" role="2Oq$k0">
                    <ref role="3cqZAo" node="2yiuz3qhGtU" resolve="parent" />
                  </node>
                  <node concept="2OwXpG" id="2yiuz3qhGZg" role="2OqNvi">
                    <ref role="2Oxat5" node="2yiuz3qhGi8" resolve="smooth" />
                  </node>
                </node>
                <node concept="10M0yZ" id="2yiuz3qhXt4" role="3K4E3e">
                  <ref role="1PxDUh" to="z60i:~RenderingHints" resolve="RenderingHints" />
                  <ref role="3cqZAo" to="z60i:~RenderingHints.VALUE_ANTIALIAS_ON" resolve="VALUE_ANTIALIAS_ON" />
                </node>
                <node concept="10M0yZ" id="2yiuz3qhXt5" role="3K4GZi">
                  <ref role="1PxDUh" to="z60i:~RenderingHints" resolve="RenderingHints" />
                  <ref role="3cqZAo" to="z60i:~RenderingHints.VALUE_ANTIALIAS_OFF" resolve="VALUE_ANTIALIAS_OFF" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3SKdUt" id="2yiuz3qhGK1" role="3cqZAp">
          <node concept="3SKdUq" id="2yiuz3qhGK0" role="3SKWNk">
            <property role="3SKdUp" value="super.paintComponent(g2);" />
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qmZ$J" role="3cqZAp">
          <node concept="1rXfSq" id="2yiuz3qmZ$H" role="3clFbG">
            <ref role="37wK5l" to="dxuu:~JComponent.setFont(java.awt.Font):void" resolve="setFont" />
            <node concept="2OqwBi" id="2yiuz3qmZVK" role="37wK5m">
              <node concept="1rXfSq" id="2yiuz3qmZU7" role="2Oq$k0">
                <ref role="37wK5l" to="z60i:~Component.getFont():java.awt.Font" resolve="getFont" />
              </node>
              <node concept="liA8E" id="2yiuz3qn00b" role="2OqNvi">
                <ref role="37wK5l" to="z60i:~Font.deriveFont(float):java.awt.Font" resolve="deriveFont" />
                <node concept="2$xPTn" id="2yiuz3qn84Y" role="37wK5m">
                  <property role="2$xPTl" value="50.f" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="2yiuz3qhGvm" role="3cqZAp">
          <node concept="3cpWsn" id="2yiuz3qhGvl" role="3cpWs9">
            <property role="3TUv4t" value="false" />
            <property role="TrG5h" value="font" />
            <node concept="3uibUv" id="2yiuz3qhGvn" role="1tU5fm">
              <ref role="3uigEE" to="z60i:~Font" resolve="Font" />
            </node>
            <node concept="1rXfSq" id="2yiuz3qmZ9o" role="33vP2m">
              <ref role="37wK5l" to="z60i:~Component.getFont():java.awt.Font" resolve="getFont" />
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="2yiuz3qhGvq" role="3cqZAp">
          <node concept="3cpWsn" id="2yiuz3qhGvp" role="3cpWs9">
            <property role="3TUv4t" value="false" />
            <property role="TrG5h" value="ascent" />
            <node concept="10Oyi0" id="2yiuz3qhGvr" role="1tU5fm" />
            <node concept="2OqwBi" id="2yiuz3qhGvs" role="33vP2m">
              <node concept="2OqwBi" id="2yiuz3qhGZn" role="2Oq$k0">
                <node concept="37vLTw" id="2yiuz3qhGZm" role="2Oq$k0">
                  <ref role="3cqZAo" node="2yiuz3qhGuL" resolve="g2" />
                </node>
                <node concept="liA8E" id="2yiuz3qhGZo" role="2OqNvi">
                  <ref role="37wK5l" to="z60i:~Graphics.getFontMetrics():java.awt.FontMetrics" resolve="getFontMetrics" />
                </node>
              </node>
              <node concept="liA8E" id="2yiuz3qhGvu" role="2OqNvi">
                <ref role="37wK5l" to="z60i:~FontMetrics.getAscent():int" resolve="getAscent" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3SKdUt" id="2yiuz3qhGK3" role="3cqZAp">
          <node concept="3SKdUq" id="2yiuz3qhGK2" role="3SKWNk">
            <property role="3SKdUp" value="System.out.println(f.getName());" />
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhGvv" role="3cqZAp">
          <node concept="2OqwBi" id="2yiuz3qhGZr" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qhGZq" role="2Oq$k0">
              <ref role="3cqZAo" node="2yiuz3qhGuL" resolve="g2" />
            </node>
            <node concept="liA8E" id="2yiuz3qhGZs" role="2OqNvi">
              <ref role="37wK5l" to="z60i:~Graphics.setFont(java.awt.Font):void" resolve="setFont" />
              <node concept="37vLTw" id="2yiuz3qhGvx" role="37wK5m">
                <ref role="3cqZAo" node="2yiuz3qhGvl" resolve="font" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhGvy" role="3cqZAp">
          <node concept="2OqwBi" id="2yiuz3qhGZv" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qhGZu" role="2Oq$k0">
              <ref role="3cqZAo" node="2yiuz3qhGuL" resolve="g2" />
            </node>
            <node concept="liA8E" id="2yiuz3qhGZw" role="2OqNvi">
              <ref role="37wK5l" to="z60i:~Graphics2D.drawString(java.lang.String,int,int):void" resolve="drawString" />
              <node concept="37vLTw" id="2yiuz3qhGv$" role="37wK5m">
                <ref role="3cqZAo" node="2yiuz3qhGtM" resolve="text" />
              </node>
              <node concept="3cmrfG" id="2yiuz3qhGv_" role="37wK5m">
                <property role="3cmrfH" value="5" />
              </node>
              <node concept="3cpWsd" id="2yiuz3qhGvA" role="37wK5m">
                <node concept="2OqwBi" id="2yiuz3qhGZz" role="3uHU7B">
                  <node concept="37vLTw" id="2yiuz3qhGZy" role="2Oq$k0">
                    <ref role="3cqZAo" node="2yiuz3qhGuU" resolve="dim" />
                  </node>
                  <node concept="2OwXpG" id="2yiuz3qhGZ$" role="2OqNvi">
                    <ref role="2Oxat5" to="z60i:~Dimension.height" resolve="height" />
                  </node>
                </node>
                <node concept="FJ1c_" id="2yiuz3qhGvC" role="3uHU7w">
                  <node concept="1eOMI4" id="2yiuz3qhGvG" role="3uHU7B">
                    <node concept="3cpWsd" id="2yiuz3qhGvD" role="1eOMHV">
                      <node concept="2OqwBi" id="2yiuz3qhGZB" role="3uHU7B">
                        <node concept="37vLTw" id="2yiuz3qhGZA" role="2Oq$k0">
                          <ref role="3cqZAo" node="2yiuz3qhGuU" resolve="dim" />
                        </node>
                        <node concept="2OwXpG" id="2yiuz3qhGZC" role="2OqNvi">
                          <ref role="2Oxat5" to="z60i:~Dimension.height" resolve="height" />
                        </node>
                      </node>
                      <node concept="37vLTw" id="2yiuz3qhGvF" role="3uHU7w">
                        <ref role="3cqZAo" node="2yiuz3qhGvp" resolve="ascent" />
                      </node>
                    </node>
                  </node>
                  <node concept="3cmrfG" id="2yiuz3qhGvH" role="3uHU7w">
                    <property role="3cmrfH" value="2" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="2yiuz3qnfBl" role="3cqZAp" />
      </node>
      <node concept="3Tm1VV" id="2yiuz3qhGvI" role="1B3o_S" />
      <node concept="3cqZAl" id="2yiuz3qhGvJ" role="3clF45" />
    </node>
    <node concept="3clFb_" id="2yiuz3qhGvK" role="jymVt">
      <property role="TrG5h" value="getPreferredSize" />
      <property role="DiZV1" value="false" />
      <property role="od$2w" value="false" />
      <node concept="3clFbS" id="2yiuz3qhGvL" role="3clF47">
        <node concept="3cpWs6" id="2yiuz3qhGvM" role="3cqZAp">
          <node concept="2ShNRf" id="2yiuz3qhGZD" role="3cqZAk">
            <node concept="1pGfFk" id="2yiuz3qhGZE" role="2ShVmc">
              <ref role="37wK5l" to="z60i:~Dimension.&lt;init&gt;(int,int)" resolve="Dimension" />
              <node concept="3cmrfG" id="2yiuz3qhGvO" role="37wK5m">
                <property role="3cmrfH" value="400" />
              </node>
              <node concept="37vLTw" id="2yiuz3qhGvP" role="37wK5m">
                <ref role="3cqZAo" node="2yiuz3qhGtQ" resolve="high" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="3Tm1VV" id="2yiuz3qhGvQ" role="1B3o_S" />
      <node concept="3uibUv" id="2yiuz3qhGvR" role="3clF45">
        <ref role="3uigEE" to="z60i:~Dimension" resolve="Dimension" />
      </node>
    </node>
    <node concept="3clFb_" id="2yiuz3qhGvS" role="jymVt">
      <property role="TrG5h" value="getMaximumSize" />
      <property role="DiZV1" value="false" />
      <property role="od$2w" value="false" />
      <node concept="3clFbS" id="2yiuz3qhGvT" role="3clF47">
        <node concept="3cpWs6" id="2yiuz3qhGvU" role="3cqZAp">
          <node concept="2ShNRf" id="2yiuz3qhGZF" role="3cqZAk">
            <node concept="1pGfFk" id="2yiuz3qhGZG" role="2ShVmc">
              <ref role="37wK5l" to="z60i:~Dimension.&lt;init&gt;(int,int)" resolve="Dimension" />
              <node concept="3cmrfG" id="2yiuz3qhGvW" role="37wK5m">
                <property role="3cmrfH" value="10000" />
              </node>
              <node concept="37vLTw" id="2yiuz3qhGvX" role="37wK5m">
                <ref role="3cqZAo" node="2yiuz3qhGtQ" resolve="high" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="3Tm1VV" id="2yiuz3qhGvY" role="1B3o_S" />
      <node concept="3uibUv" id="2yiuz3qhGvZ" role="3clF45">
        <ref role="3uigEE" to="z60i:~Dimension" resolve="Dimension" />
      </node>
    </node>
    <node concept="3clFb_" id="2yiuz3qhGw0" role="jymVt">
      <property role="TrG5h" value="getMinimumSize" />
      <property role="DiZV1" value="false" />
      <property role="od$2w" value="false" />
      <node concept="3clFbS" id="2yiuz3qhGw1" role="3clF47">
        <node concept="3cpWs6" id="2yiuz3qhGw2" role="3cqZAp">
          <node concept="2ShNRf" id="2yiuz3qhGZH" role="3cqZAk">
            <node concept="1pGfFk" id="2yiuz3qhGZI" role="2ShVmc">
              <ref role="37wK5l" to="z60i:~Dimension.&lt;init&gt;(int,int)" resolve="Dimension" />
              <node concept="3cmrfG" id="2yiuz3qhGw4" role="37wK5m">
                <property role="3cmrfH" value="100" />
              </node>
              <node concept="37vLTw" id="2yiuz3qhGw5" role="37wK5m">
                <ref role="3cqZAo" node="2yiuz3qhGtQ" resolve="high" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="3Tm1VV" id="2yiuz3qhGw6" role="1B3o_S" />
      <node concept="3uibUv" id="2yiuz3qhGw7" role="3clF45">
        <ref role="3uigEE" to="z60i:~Dimension" resolve="Dimension" />
      </node>
    </node>
  </node>
  <node concept="312cEu" id="2yiuz3qhGwv">
    <property role="TrG5h" value="CharacterSelector" />
    <property role="2bfB8j" value="true" />
    <property role="1sVAO0" value="false" />
    <property role="1EXbeo" value="false" />
    <property role="3GE5qa" value="createFont" />
    <node concept="3uibUv" id="2yiuz3qhGww" role="1zkMxy">
      <ref role="3uigEE" to="dxuu:~JFrame" resolve="JFrame" />
    </node>
    <node concept="3UR2Jj" id="2yiuz3qhGES" role="lGtFl">
      <node concept="TZ5HA" id="2yiuz3qhGK4" role="TZ5H$">
        <node concept="1dT_AC" id="2yiuz3qhGK5" role="1dT_Ay">
          <property role="1dT_AB" value="Frame for selecting which characters will be included with the font." />
        </node>
      </node>
    </node>
    <node concept="312cEg" id="2yiuz3qhGwx" role="jymVt">
      <property role="34CwA1" value="false" />
      <property role="eg7rD" value="false" />
      <property role="TrG5h" value="defaultCharsButton" />
      <property role="3TUv4t" value="false" />
      <node concept="3uibUv" id="2yiuz3qhGwz" role="1tU5fm">
        <ref role="3uigEE" to="dxuu:~JRadioButton" resolve="JRadioButton" />
      </node>
    </node>
    <node concept="312cEg" id="2yiuz3qhGw$" role="jymVt">
      <property role="34CwA1" value="false" />
      <property role="eg7rD" value="false" />
      <property role="TrG5h" value="allCharsButton" />
      <property role="3TUv4t" value="false" />
      <node concept="3uibUv" id="2yiuz3qhGwA" role="1tU5fm">
        <ref role="3uigEE" to="dxuu:~JRadioButton" resolve="JRadioButton" />
      </node>
    </node>
    <node concept="312cEg" id="2yiuz3qhGwB" role="jymVt">
      <property role="34CwA1" value="false" />
      <property role="eg7rD" value="false" />
      <property role="TrG5h" value="unicodeCharsButton" />
      <property role="3TUv4t" value="false" />
      <node concept="3uibUv" id="2yiuz3qhGwD" role="1tU5fm">
        <ref role="3uigEE" to="dxuu:~JRadioButton" resolve="JRadioButton" />
      </node>
    </node>
    <node concept="312cEg" id="2yiuz3qhGwE" role="jymVt">
      <property role="34CwA1" value="false" />
      <property role="eg7rD" value="false" />
      <property role="TrG5h" value="unicodeBlockScroller" />
      <property role="3TUv4t" value="false" />
      <node concept="3uibUv" id="2yiuz3qhGwG" role="1tU5fm">
        <ref role="3uigEE" to="dxuu:~JScrollPane" resolve="JScrollPane" />
      </node>
    </node>
    <node concept="312cEg" id="2yiuz3qhGwH" role="jymVt">
      <property role="34CwA1" value="false" />
      <property role="eg7rD" value="false" />
      <property role="TrG5h" value="charsetList" />
      <property role="3TUv4t" value="false" />
      <node concept="3uibUv" id="2yiuz3qhGwJ" role="1tU5fm">
        <ref role="3uigEE" to="dxuu:~JList" resolve="JList" />
        <node concept="3uibUv" id="2yiuz3qhGwK" role="11_B2D">
          <ref role="3uigEE" to="dxuu:~JCheckBox" resolve="JCheckBox" />
        </node>
      </node>
    </node>
    <node concept="3clFbW" id="2yiuz3qhG$v" role="jymVt">
      <property role="DiZV1" value="false" />
      <property role="od$2w" value="false" />
      <node concept="3cqZAl" id="2yiuz3qhG$w" role="3clF45" />
      <node concept="3clFbS" id="2yiuz3qhG$x" role="3clF47">
        <node concept="XkiVB" id="2yiuz3qhGZJ" role="3cqZAp">
          <ref role="37wK5l" to="dxuu:~JFrame.&lt;init&gt;(java.lang.String)" resolve="JFrame" />
          <node concept="Xl_RD" id="2yiuz3qiaUW" role="37wK5m">
            <property role="Xl_RC" value="Character Selector" />
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhG$y" role="3cqZAp">
          <node concept="37vLTI" id="2yiuz3qhG$z" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qhG$$" role="37vLTJ">
              <ref role="3cqZAo" node="2yiuz3qhGwH" resolve="charsetList" />
            </node>
            <node concept="2ShNRf" id="2yiuz3qhGZL" role="37vLTx">
              <node concept="1pGfFk" id="2yiuz3qhGZM" role="2ShVmc">
                <ref role="37wK5l" node="2yiuz3qhGGi" resolve="CheckBoxList" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="2yiuz3qhG$B" role="3cqZAp">
          <node concept="3cpWsn" id="2yiuz3qhG$A" role="3cpWs9">
            <property role="3TUv4t" value="false" />
            <property role="TrG5h" value="model" />
            <node concept="3uibUv" id="2yiuz3qhG$C" role="1tU5fm">
              <ref role="3uigEE" to="dxuu:~DefaultListModel" resolve="DefaultListModel" />
              <node concept="3uibUv" id="2yiuz3qhG$D" role="11_B2D">
                <ref role="3uigEE" to="dxuu:~JCheckBox" resolve="JCheckBox" />
              </node>
            </node>
            <node concept="2ShNRf" id="2yiuz3qhGZN" role="33vP2m">
              <node concept="1pGfFk" id="2yiuz3qhGZO" role="2ShVmc">
                <ref role="37wK5l" to="dxuu:~DefaultListModel.&lt;init&gt;()" resolve="DefaultListModel" />
                <node concept="3uibUv" id="2yiuz3qhG$F" role="1pMfVU">
                  <ref role="3uigEE" to="dxuu:~JCheckBox" resolve="JCheckBox" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhG$G" role="3cqZAp">
          <node concept="2OqwBi" id="2yiuz3qhGZR" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qhGZQ" role="2Oq$k0">
              <ref role="3cqZAo" node="2yiuz3qhGwH" resolve="charsetList" />
            </node>
            <node concept="liA8E" id="2yiuz3qhGZS" role="2OqNvi">
              <ref role="37wK5l" to="dxuu:~JList.setModel(javax.swing.ListModel):void" resolve="setModel" />
              <node concept="37vLTw" id="2yiuz3qhG$I" role="37wK5m">
                <ref role="3cqZAo" node="2yiuz3qhG$A" resolve="model" />
              </node>
            </node>
          </node>
        </node>
        <node concept="1DcWWT" id="2yiuz3qhG$J" role="3cqZAp">
          <node concept="37vLTw" id="2yiuz3qhG$T" role="1DdaDG">
            <ref role="3cqZAo" node="2yiuz3qhGzb" resolve="blockNames" />
          </node>
          <node concept="3cpWsn" id="2yiuz3qhG$Q" role="1Duv9x">
            <property role="3TUv4t" value="false" />
            <property role="TrG5h" value="item" />
            <node concept="17QB3L" id="2yiuz3qlytE" role="1tU5fm" />
          </node>
          <node concept="3clFbS" id="2yiuz3qhG$L" role="2LFqv$">
            <node concept="3clFbF" id="2yiuz3qhG$M" role="3cqZAp">
              <node concept="2OqwBi" id="2yiuz3qhGZV" role="3clFbG">
                <node concept="37vLTw" id="2yiuz3qhGZU" role="2Oq$k0">
                  <ref role="3cqZAo" node="2yiuz3qhG$A" resolve="model" />
                </node>
                <node concept="liA8E" id="2yiuz3qhGZW" role="2OqNvi">
                  <ref role="37wK5l" to="dxuu:~DefaultListModel.addElement(java.lang.Object):void" resolve="addElement" />
                  <node concept="2ShNRf" id="2yiuz3qhGZX" role="37wK5m">
                    <node concept="1pGfFk" id="2yiuz3qhH09" role="2ShVmc">
                      <ref role="37wK5l" to="dxuu:~JCheckBox.&lt;init&gt;(java.lang.String)" resolve="JCheckBox" />
                      <node concept="37vLTw" id="2yiuz3qhG$P" role="37wK5m">
                        <ref role="3cqZAo" node="2yiuz3qhG$Q" resolve="item" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhG$U" role="3cqZAp">
          <node concept="37vLTI" id="2yiuz3qhG$V" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qhG$W" role="37vLTJ">
              <ref role="3cqZAo" node="2yiuz3qhGwE" resolve="unicodeBlockScroller" />
            </node>
            <node concept="2ShNRf" id="2yiuz3qhH0a" role="37vLTx">
              <node concept="1pGfFk" id="2yiuz3qhH0b" role="2ShVmc">
                <ref role="37wK5l" to="dxuu:~JScrollPane.&lt;init&gt;(java.awt.Component,int,int)" resolve="JScrollPane" />
                <node concept="37vLTw" id="2yiuz3qhG$Y" role="37wK5m">
                  <ref role="3cqZAo" node="2yiuz3qhGwH" resolve="charsetList" />
                </node>
                <node concept="10M0yZ" id="2yiuz3qhXU6" role="37wK5m">
                  <ref role="1PxDUh" to="dxuu:~ScrollPaneConstants" resolve="ScrollPaneConstants" />
                  <ref role="3cqZAo" to="dxuu:~ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS" resolve="VERTICAL_SCROLLBAR_ALWAYS" />
                </node>
                <node concept="10M0yZ" id="2yiuz3qhXU7" role="37wK5m">
                  <ref role="1PxDUh" to="dxuu:~ScrollPaneConstants" resolve="ScrollPaneConstants" />
                  <ref role="3cqZAo" to="dxuu:~ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER" resolve="HORIZONTAL_SCROLLBAR_NEVER" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="2yiuz3qhG_2" role="3cqZAp">
          <node concept="3cpWsn" id="2yiuz3qhG_1" role="3cpWs9">
            <property role="3TUv4t" value="false" />
            <property role="TrG5h" value="outer" />
            <node concept="3uibUv" id="2yiuz3qhG_3" role="1tU5fm">
              <ref role="3uigEE" to="z60i:~Container" resolve="Container" />
            </node>
            <node concept="1rXfSq" id="2yiuz3qhG_4" role="33vP2m">
              <ref role="37wK5l" to="dxuu:~JFrame.getContentPane():java.awt.Container" resolve="getContentPane" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhG_5" role="3cqZAp">
          <node concept="2OqwBi" id="2yiuz3qhH0i" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qhH0h" role="2Oq$k0">
              <ref role="3cqZAo" node="2yiuz3qhG_1" resolve="outer" />
            </node>
            <node concept="liA8E" id="2yiuz3qhH0j" role="2OqNvi">
              <ref role="37wK5l" to="z60i:~Container.setLayout(java.awt.LayoutManager):void" resolve="setLayout" />
              <node concept="2ShNRf" id="2yiuz3qhH0k" role="37wK5m">
                <node concept="1pGfFk" id="2yiuz3qhH0l" role="2ShVmc">
                  <ref role="37wK5l" to="z60i:~BorderLayout.&lt;init&gt;()" resolve="BorderLayout" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="2yiuz3qhG_9" role="3cqZAp">
          <node concept="3cpWsn" id="2yiuz3qhG_8" role="3cpWs9">
            <property role="3TUv4t" value="false" />
            <property role="TrG5h" value="pain" />
            <node concept="3uibUv" id="2yiuz3qhG_a" role="1tU5fm">
              <ref role="3uigEE" to="dxuu:~JPanel" resolve="JPanel" />
            </node>
            <node concept="2ShNRf" id="2yiuz3qhH0m" role="33vP2m">
              <node concept="1pGfFk" id="2yiuz3qhH0n" role="2ShVmc">
                <ref role="37wK5l" to="dxuu:~JPanel.&lt;init&gt;()" resolve="JPanel" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhG_c" role="3cqZAp">
          <node concept="2OqwBi" id="2yiuz3qhH0q" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qhH0p" role="2Oq$k0">
              <ref role="3cqZAo" node="2yiuz3qhG_8" resolve="pain" />
            </node>
            <node concept="liA8E" id="2yiuz3qhH0r" role="2OqNvi">
              <ref role="37wK5l" to="dxuu:~JComponent.setBorder(javax.swing.border.Border):void" resolve="setBorder" />
              <node concept="2ShNRf" id="2yiuz3qhH0s" role="37wK5m">
                <node concept="1pGfFk" id="2yiuz3qhH0t" role="2ShVmc">
                  <ref role="37wK5l" to="9z78:~EmptyBorder.&lt;init&gt;(int,int,int,int)" resolve="EmptyBorder" />
                  <node concept="3cmrfG" id="2yiuz3qhG_f" role="37wK5m">
                    <property role="3cmrfH" value="13" />
                  </node>
                  <node concept="3cmrfG" id="2yiuz3qhG_g" role="37wK5m">
                    <property role="3cmrfH" value="13" />
                  </node>
                  <node concept="3cmrfG" id="2yiuz3qhG_h" role="37wK5m">
                    <property role="3cmrfH" value="13" />
                  </node>
                  <node concept="3cmrfG" id="2yiuz3qhG_i" role="37wK5m">
                    <property role="3cmrfH" value="13" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhG_j" role="3cqZAp">
          <node concept="2OqwBi" id="2yiuz3qhH0w" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qhH0v" role="2Oq$k0">
              <ref role="3cqZAo" node="2yiuz3qhG_1" resolve="outer" />
            </node>
            <node concept="liA8E" id="2yiuz3qhH0x" role="2OqNvi">
              <ref role="37wK5l" to="z60i:~Container.add(java.awt.Component,java.lang.Object):void" resolve="add" />
              <node concept="37vLTw" id="2yiuz3qhG_l" role="37wK5m">
                <ref role="3cqZAo" node="2yiuz3qhG_8" resolve="pain" />
              </node>
              <node concept="10M0yZ" id="2yiuz3qhXU8" role="37wK5m">
                <ref role="1PxDUh" to="z60i:~BorderLayout" resolve="BorderLayout" />
                <ref role="3cqZAo" to="z60i:~BorderLayout.CENTER" resolve="CENTER" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhG_n" role="3cqZAp">
          <node concept="2OqwBi" id="2yiuz3qhH0A" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qhH0_" role="2Oq$k0">
              <ref role="3cqZAo" node="2yiuz3qhG_8" resolve="pain" />
            </node>
            <node concept="liA8E" id="2yiuz3qhH0B" role="2OqNvi">
              <ref role="37wK5l" to="z60i:~Container.setLayout(java.awt.LayoutManager):void" resolve="setLayout" />
              <node concept="2ShNRf" id="2yiuz3qhH0C" role="37wK5m">
                <node concept="1pGfFk" id="2yiuz3qhH0D" role="2ShVmc">
                  <ref role="37wK5l" to="dxuu:~BoxLayout.&lt;init&gt;(java.awt.Container,int)" resolve="BoxLayout" />
                  <node concept="37vLTw" id="2yiuz3qhG_q" role="37wK5m">
                    <ref role="3cqZAo" node="2yiuz3qhG_8" resolve="pain" />
                  </node>
                  <node concept="10M0yZ" id="2yiuz3qhXU9" role="37wK5m">
                    <ref role="1PxDUh" to="dxuu:~BoxLayout" resolve="BoxLayout" />
                    <ref role="3cqZAo" to="dxuu:~BoxLayout.Y_AXIS" resolve="Y_AXIS" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="2yiuz3qhG_t" role="3cqZAp">
          <node concept="3cpWsn" id="2yiuz3qhG_s" role="3cpWs9">
            <property role="3TUv4t" value="false" />
            <property role="TrG5h" value="labelText" />
            <node concept="17QB3L" id="2yiuz3qlytF" role="1tU5fm" />
            <node concept="Xl_RD" id="2yiuz3qijTw" role="33vP2m">
              <property role="Xl_RC" value="Default characters will include most bitmaps for Mac OS\nand Windows Latin scripts. Including all characters may\nrequire large amounts of memory for all of the bitmaps.\nFor greater control, you can select specific Unicode blocks." />
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="2yiuz3qhG_y" role="3cqZAp">
          <node concept="3cpWsn" id="2yiuz3qhG_x" role="3cpWs9">
            <property role="3TUv4t" value="false" />
            <property role="TrG5h" value="textarea" />
            <node concept="3uibUv" id="2yiuz3qhG_z" role="1tU5fm">
              <ref role="3uigEE" to="dxuu:~JTextArea" resolve="JTextArea" />
            </node>
            <node concept="2ShNRf" id="2yiuz3qhH0H" role="33vP2m">
              <node concept="1pGfFk" id="2yiuz3qhH0T" role="2ShVmc">
                <ref role="37wK5l" to="dxuu:~JTextArea.&lt;init&gt;(java.lang.String)" resolve="JTextArea" />
                <node concept="37vLTw" id="2yiuz3qhG__" role="37wK5m">
                  <ref role="3cqZAo" node="2yiuz3qhG_s" resolve="labelText" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhG_A" role="3cqZAp">
          <node concept="2OqwBi" id="2yiuz3qhH0W" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qhH0V" role="2Oq$k0">
              <ref role="3cqZAo" node="2yiuz3qhG_x" resolve="textarea" />
            </node>
            <node concept="liA8E" id="2yiuz3qhH0X" role="2OqNvi">
              <ref role="37wK5l" to="dxuu:~JComponent.setBorder(javax.swing.border.Border):void" resolve="setBorder" />
              <node concept="2ShNRf" id="2yiuz3qhH0Y" role="37wK5m">
                <node concept="1pGfFk" id="2yiuz3qhH0Z" role="2ShVmc">
                  <ref role="37wK5l" to="9z78:~EmptyBorder.&lt;init&gt;(int,int,int,int)" resolve="EmptyBorder" />
                  <node concept="3cmrfG" id="2yiuz3qhG_D" role="37wK5m">
                    <property role="3cmrfH" value="13" />
                  </node>
                  <node concept="3cmrfG" id="2yiuz3qhG_E" role="37wK5m">
                    <property role="3cmrfH" value="8" />
                  </node>
                  <node concept="3cmrfG" id="2yiuz3qhG_F" role="37wK5m">
                    <property role="3cmrfH" value="13" />
                  </node>
                  <node concept="3cmrfG" id="2yiuz3qhG_G" role="37wK5m">
                    <property role="3cmrfH" value="8" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhG_H" role="3cqZAp">
          <node concept="2OqwBi" id="2yiuz3qhH12" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qhH11" role="2Oq$k0">
              <ref role="3cqZAo" node="2yiuz3qhG_x" resolve="textarea" />
            </node>
            <node concept="liA8E" id="2yiuz3qhH13" role="2OqNvi">
              <ref role="37wK5l" to="dxuu:~JComponent.setBackground(java.awt.Color):void" resolve="setBackground" />
              <node concept="10Nm6u" id="2yiuz3qhG_J" role="37wK5m" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhG_K" role="3cqZAp">
          <node concept="2OqwBi" id="2yiuz3qhH16" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qhH15" role="2Oq$k0">
              <ref role="3cqZAo" node="2yiuz3qhG_x" resolve="textarea" />
            </node>
            <node concept="liA8E" id="2yiuz3qhH17" role="2OqNvi">
              <ref role="37wK5l" to="r791:~JTextComponent.setEditable(boolean):void" resolve="setEditable" />
              <node concept="3clFbT" id="2yiuz3qhG_M" role="37wK5m">
                <property role="3clFbU" value="false" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhG_N" role="3cqZAp">
          <node concept="2OqwBi" id="2yiuz3qhH1a" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qhH19" role="2Oq$k0">
              <ref role="3cqZAo" node="2yiuz3qhG_x" resolve="textarea" />
            </node>
            <node concept="liA8E" id="2yiuz3qhH1b" role="2OqNvi">
              <ref role="37wK5l" to="r791:~JTextComponent.setHighlighter(javax.swing.text.Highlighter):void" resolve="setHighlighter" />
              <node concept="10Nm6u" id="2yiuz3qhG_P" role="37wK5m" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhG_Q" role="3cqZAp">
          <node concept="2OqwBi" id="2yiuz3qhH1e" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qhH1d" role="2Oq$k0">
              <ref role="3cqZAo" node="2yiuz3qhG_x" resolve="textarea" />
            </node>
            <node concept="liA8E" id="2yiuz3qhH1f" role="2OqNvi">
              <ref role="37wK5l" to="dxuu:~JTextArea.setFont(java.awt.Font):void" resolve="setFont" />
              <node concept="2ShNRf" id="2yiuz3qhH1g" role="37wK5m">
                <node concept="1pGfFk" id="2yiuz3qhH1h" role="2ShVmc">
                  <ref role="37wK5l" to="z60i:~Font.&lt;init&gt;(java.lang.String,int,int)" resolve="Font" />
                  <node concept="Xl_RD" id="2yiuz3qhG_T" role="37wK5m">
                    <property role="Xl_RC" value="Dialog" />
                  </node>
                  <node concept="10M0yZ" id="2yiuz3qhXUa" role="37wK5m">
                    <ref role="1PxDUh" to="z60i:~Font" resolve="Font" />
                    <ref role="3cqZAo" to="z60i:~Font.PLAIN" resolve="PLAIN" />
                  </node>
                  <node concept="3cmrfG" id="2yiuz3qhG_V" role="37wK5m">
                    <property role="3cmrfH" value="12" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhG_W" role="3cqZAp">
          <node concept="2OqwBi" id="2yiuz3qhH1m" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qhH1l" role="2Oq$k0">
              <ref role="3cqZAo" node="2yiuz3qhG_8" resolve="pain" />
            </node>
            <node concept="liA8E" id="2yiuz3qhH1n" role="2OqNvi">
              <ref role="37wK5l" to="z60i:~Container.add(java.awt.Component):java.awt.Component" resolve="add" />
              <node concept="37vLTw" id="2yiuz3qhG_Y" role="37wK5m">
                <ref role="3cqZAo" node="2yiuz3qhG_x" resolve="textarea" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="2yiuz3qhGA0" role="3cqZAp">
          <node concept="3cpWsn" id="2yiuz3qhG_Z" role="3cpWs9">
            <property role="3TUv4t" value="false" />
            <property role="TrG5h" value="listener" />
            <node concept="3uibUv" id="2yiuz3qhGA1" role="1tU5fm">
              <ref role="3uigEE" to="hyam:~ActionListener" resolve="ActionListener" />
            </node>
            <node concept="2ShNRf" id="2yiuz3qhGA2" role="33vP2m">
              <node concept="YeOm9" id="2yiuz3qhGA3" role="2ShVmc">
                <node concept="1Y3b0j" id="2yiuz3qlHp_" role="YeSDq">
                  <property role="2bfB8j" value="true" />
                  <ref role="37wK5l" to="wyt6:~Object.&lt;init&gt;()" resolve="Object" />
                  <ref role="1Y3XeK" to="hyam:~ActionListener" resolve="ActionListener" />
                  <node concept="3Tm1VV" id="2yiuz3qlHpA" role="1B3o_S" />
                  <node concept="3clFb_" id="2yiuz3qlHK$" role="jymVt">
                    <property role="1EzhhJ" value="false" />
                    <property role="TrG5h" value="actionPerformed" />
                    <property role="DiZV1" value="false" />
                    <property role="od$2w" value="false" />
                    <node concept="3Tm1VV" id="2yiuz3qlHK_" role="1B3o_S" />
                    <node concept="3cqZAl" id="2yiuz3qlHKB" role="3clF45" />
                    <node concept="37vLTG" id="2yiuz3qlHKC" role="3clF46">
                      <property role="TrG5h" value="event" />
                      <node concept="3uibUv" id="2yiuz3qlHKD" role="1tU5fm">
                        <ref role="3uigEE" to="hyam:~ActionEvent" resolve="ActionEvent" />
                      </node>
                    </node>
                    <node concept="3clFbS" id="2yiuz3qlHKF" role="3clF47">
                      <node concept="3clFbF" id="2yiuz3qlISs" role="3cqZAp">
                        <node concept="2OqwBi" id="2yiuz3qlJcB" role="3clFbG">
                          <node concept="37vLTw" id="2yiuz3qlISr" role="2Oq$k0">
                            <ref role="3cqZAo" node="2yiuz3qhGwH" resolve="charsetList" />
                          </node>
                          <node concept="liA8E" id="2yiuz3qlJQs" role="2OqNvi">
                            <ref role="37wK5l" to="dxuu:~JComponent.setEnabled(boolean):void" resolve="setEnabled" />
                            <node concept="2OqwBi" id="2yiuz3qlKMa" role="37wK5m">
                              <node concept="37vLTw" id="2yiuz3qlKnU" role="2Oq$k0">
                                <ref role="3cqZAo" node="2yiuz3qhGwB" resolve="unicodeCharsButton" />
                              </node>
                              <node concept="liA8E" id="2yiuz3qlLy1" role="2OqNvi">
                                <ref role="37wK5l" to="dxuu:~AbstractButton.isSelected():boolean" resolve="isSelected" />
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhGAe" role="3cqZAp">
          <node concept="37vLTI" id="2yiuz3qhGAf" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qhGAg" role="37vLTJ">
              <ref role="3cqZAo" node="2yiuz3qhGwx" resolve="defaultCharsButton" />
            </node>
            <node concept="2ShNRf" id="2yiuz3qhH1A" role="37vLTx">
              <node concept="1pGfFk" id="2yiuz3qhH1V" role="2ShVmc">
                <ref role="37wK5l" to="dxuu:~JRadioButton.&lt;init&gt;(java.lang.String)" resolve="JRadioButton" />
                <node concept="Xl_RD" id="2yiuz3qi84Z" role="37wK5m">
                  <property role="Xl_RC" value="Default Characters" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhGAk" role="3cqZAp">
          <node concept="37vLTI" id="2yiuz3qhGAl" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qhGAm" role="37vLTJ">
              <ref role="3cqZAo" node="2yiuz3qhGw$" resolve="allCharsButton" />
            </node>
            <node concept="2ShNRf" id="2yiuz3qhH1X" role="37vLTx">
              <node concept="1pGfFk" id="2yiuz3qhH2i" role="2ShVmc">
                <ref role="37wK5l" to="dxuu:~JRadioButton.&lt;init&gt;(java.lang.String)" resolve="JRadioButton" />
                <node concept="Xl_RD" id="2yiuz3qi8s4" role="37wK5m">
                  <property role="Xl_RC" value="All Characters" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhGAq" role="3cqZAp">
          <node concept="37vLTI" id="2yiuz3qhGAr" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qhGAs" role="37vLTJ">
              <ref role="3cqZAo" node="2yiuz3qhGwB" resolve="unicodeCharsButton" />
            </node>
            <node concept="2ShNRf" id="2yiuz3qhH2k" role="37vLTx">
              <node concept="1pGfFk" id="2yiuz3qhH2D" role="2ShVmc">
                <ref role="37wK5l" to="dxuu:~JRadioButton.&lt;init&gt;(java.lang.String)" resolve="JRadioButton" />
                <node concept="Xl_RD" id="2yiuz3qi8Mk" role="37wK5m">
                  <property role="Xl_RC" value="Specific Unicode Blocks" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhGAw" role="3cqZAp">
          <node concept="2OqwBi" id="2yiuz3qhH2H" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qhH2G" role="2Oq$k0">
              <ref role="3cqZAo" node="2yiuz3qhGwx" resolve="defaultCharsButton" />
            </node>
            <node concept="liA8E" id="2yiuz3qhH2I" role="2OqNvi">
              <ref role="37wK5l" to="dxuu:~AbstractButton.addActionListener(java.awt.event.ActionListener):void" resolve="addActionListener" />
              <node concept="37vLTw" id="2yiuz3qhGAy" role="37wK5m">
                <ref role="3cqZAo" node="2yiuz3qhG_Z" resolve="listener" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhGAz" role="3cqZAp">
          <node concept="2OqwBi" id="2yiuz3qhH2L" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qhH2K" role="2Oq$k0">
              <ref role="3cqZAo" node="2yiuz3qhGw$" resolve="allCharsButton" />
            </node>
            <node concept="liA8E" id="2yiuz3qhH2M" role="2OqNvi">
              <ref role="37wK5l" to="dxuu:~AbstractButton.addActionListener(java.awt.event.ActionListener):void" resolve="addActionListener" />
              <node concept="37vLTw" id="2yiuz3qhGA_" role="37wK5m">
                <ref role="3cqZAo" node="2yiuz3qhG_Z" resolve="listener" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhGAA" role="3cqZAp">
          <node concept="2OqwBi" id="2yiuz3qhH2P" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qhH2O" role="2Oq$k0">
              <ref role="3cqZAo" node="2yiuz3qhGwB" resolve="unicodeCharsButton" />
            </node>
            <node concept="liA8E" id="2yiuz3qhH2Q" role="2OqNvi">
              <ref role="37wK5l" to="dxuu:~AbstractButton.addActionListener(java.awt.event.ActionListener):void" resolve="addActionListener" />
              <node concept="37vLTw" id="2yiuz3qhGAC" role="37wK5m">
                <ref role="3cqZAo" node="2yiuz3qhG_Z" resolve="listener" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="2yiuz3qhGAE" role="3cqZAp">
          <node concept="3cpWsn" id="2yiuz3qhGAD" role="3cpWs9">
            <property role="3TUv4t" value="false" />
            <property role="TrG5h" value="group" />
            <node concept="3uibUv" id="2yiuz3qhGAF" role="1tU5fm">
              <ref role="3uigEE" to="dxuu:~ButtonGroup" resolve="ButtonGroup" />
            </node>
            <node concept="2ShNRf" id="2yiuz3qhH2R" role="33vP2m">
              <node concept="1pGfFk" id="2yiuz3qhH2S" role="2ShVmc">
                <ref role="37wK5l" to="dxuu:~ButtonGroup.&lt;init&gt;()" resolve="ButtonGroup" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhGAH" role="3cqZAp">
          <node concept="2OqwBi" id="2yiuz3qhH2V" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qhH2U" role="2Oq$k0">
              <ref role="3cqZAo" node="2yiuz3qhGAD" resolve="group" />
            </node>
            <node concept="liA8E" id="2yiuz3qhH2W" role="2OqNvi">
              <ref role="37wK5l" to="dxuu:~ButtonGroup.add(javax.swing.AbstractButton):void" resolve="add" />
              <node concept="37vLTw" id="2yiuz3qhGAJ" role="37wK5m">
                <ref role="3cqZAo" node="2yiuz3qhGwx" resolve="defaultCharsButton" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhGAK" role="3cqZAp">
          <node concept="2OqwBi" id="2yiuz3qhH2Z" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qhH2Y" role="2Oq$k0">
              <ref role="3cqZAo" node="2yiuz3qhGAD" resolve="group" />
            </node>
            <node concept="liA8E" id="2yiuz3qhH30" role="2OqNvi">
              <ref role="37wK5l" to="dxuu:~ButtonGroup.add(javax.swing.AbstractButton):void" resolve="add" />
              <node concept="37vLTw" id="2yiuz3qhGAM" role="37wK5m">
                <ref role="3cqZAo" node="2yiuz3qhGw$" resolve="allCharsButton" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhGAN" role="3cqZAp">
          <node concept="2OqwBi" id="2yiuz3qhH33" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qhH32" role="2Oq$k0">
              <ref role="3cqZAo" node="2yiuz3qhGAD" resolve="group" />
            </node>
            <node concept="liA8E" id="2yiuz3qhH34" role="2OqNvi">
              <ref role="37wK5l" to="dxuu:~ButtonGroup.add(javax.swing.AbstractButton):void" resolve="add" />
              <node concept="37vLTw" id="2yiuz3qhGAP" role="37wK5m">
                <ref role="3cqZAo" node="2yiuz3qhGwB" resolve="unicodeCharsButton" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="2yiuz3qhGAR" role="3cqZAp">
          <node concept="3cpWsn" id="2yiuz3qhGAQ" role="3cpWs9">
            <property role="3TUv4t" value="false" />
            <property role="TrG5h" value="radioPanel" />
            <node concept="3uibUv" id="2yiuz3qhGAS" role="1tU5fm">
              <ref role="3uigEE" to="dxuu:~JPanel" resolve="JPanel" />
            </node>
            <node concept="2ShNRf" id="2yiuz3qhH35" role="33vP2m">
              <node concept="1pGfFk" id="2yiuz3qhH36" role="2ShVmc">
                <ref role="37wK5l" to="dxuu:~JPanel.&lt;init&gt;()" resolve="JPanel" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3SKdUt" id="2yiuz3qhGKb" role="3cqZAp">
          <node concept="3SKdUq" id="2yiuz3qhGKa" role="3SKWNk">
            <property role="3SKdUp" value="radioPanel.setBackground(Color.red);" />
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhGAU" role="3cqZAp">
          <node concept="2OqwBi" id="2yiuz3qhH39" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qhH38" role="2Oq$k0">
              <ref role="3cqZAo" node="2yiuz3qhGAQ" resolve="radioPanel" />
            </node>
            <node concept="liA8E" id="2yiuz3qhH3a" role="2OqNvi">
              <ref role="37wK5l" to="z60i:~Container.setLayout(java.awt.LayoutManager):void" resolve="setLayout" />
              <node concept="2ShNRf" id="2yiuz3qhH3b" role="37wK5m">
                <node concept="1pGfFk" id="2yiuz3qhH3c" role="2ShVmc">
                  <ref role="37wK5l" to="dxuu:~BoxLayout.&lt;init&gt;(java.awt.Container,int)" resolve="BoxLayout" />
                  <node concept="37vLTw" id="2yiuz3qhGAX" role="37wK5m">
                    <ref role="3cqZAo" node="2yiuz3qhGAQ" resolve="radioPanel" />
                  </node>
                  <node concept="10M0yZ" id="2yiuz3qhXUb" role="37wK5m">
                    <ref role="1PxDUh" to="dxuu:~BoxLayout" resolve="BoxLayout" />
                    <ref role="3cqZAo" to="dxuu:~BoxLayout.Y_AXIS" resolve="Y_AXIS" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhGAZ" role="3cqZAp">
          <node concept="2OqwBi" id="2yiuz3qhH3h" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qhH3g" role="2Oq$k0">
              <ref role="3cqZAo" node="2yiuz3qhGAQ" resolve="radioPanel" />
            </node>
            <node concept="liA8E" id="2yiuz3qhH3i" role="2OqNvi">
              <ref role="37wK5l" to="z60i:~Container.add(java.awt.Component):java.awt.Component" resolve="add" />
              <node concept="37vLTw" id="2yiuz3qhGB1" role="37wK5m">
                <ref role="3cqZAo" node="2yiuz3qhGwx" resolve="defaultCharsButton" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhGB2" role="3cqZAp">
          <node concept="2OqwBi" id="2yiuz3qhH3l" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qhH3k" role="2Oq$k0">
              <ref role="3cqZAo" node="2yiuz3qhGAQ" resolve="radioPanel" />
            </node>
            <node concept="liA8E" id="2yiuz3qhH3m" role="2OqNvi">
              <ref role="37wK5l" to="z60i:~Container.add(java.awt.Component):java.awt.Component" resolve="add" />
              <node concept="37vLTw" id="2yiuz3qhGB4" role="37wK5m">
                <ref role="3cqZAo" node="2yiuz3qhGw$" resolve="allCharsButton" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhGB5" role="3cqZAp">
          <node concept="2OqwBi" id="2yiuz3qhH3p" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qhH3o" role="2Oq$k0">
              <ref role="3cqZAo" node="2yiuz3qhGAQ" resolve="radioPanel" />
            </node>
            <node concept="liA8E" id="2yiuz3qhH3q" role="2OqNvi">
              <ref role="37wK5l" to="z60i:~Container.add(java.awt.Component):java.awt.Component" resolve="add" />
              <node concept="37vLTw" id="2yiuz3qhGB7" role="37wK5m">
                <ref role="3cqZAo" node="2yiuz3qhGwB" resolve="unicodeCharsButton" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="2yiuz3qhGB9" role="3cqZAp">
          <node concept="3cpWsn" id="2yiuz3qhGB8" role="3cpWs9">
            <property role="3TUv4t" value="false" />
            <property role="TrG5h" value="rightStuff" />
            <node concept="3uibUv" id="2yiuz3qhGBa" role="1tU5fm">
              <ref role="3uigEE" to="dxuu:~JPanel" resolve="JPanel" />
            </node>
            <node concept="2ShNRf" id="2yiuz3qhH3r" role="33vP2m">
              <node concept="1pGfFk" id="2yiuz3qhH3s" role="2ShVmc">
                <ref role="37wK5l" to="dxuu:~JPanel.&lt;init&gt;()" resolve="JPanel" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhGBc" role="3cqZAp">
          <node concept="2OqwBi" id="2yiuz3qhH3v" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qhH3u" role="2Oq$k0">
              <ref role="3cqZAo" node="2yiuz3qhGB8" resolve="rightStuff" />
            </node>
            <node concept="liA8E" id="2yiuz3qhH3w" role="2OqNvi">
              <ref role="37wK5l" to="z60i:~Container.setLayout(java.awt.LayoutManager):void" resolve="setLayout" />
              <node concept="2ShNRf" id="2yiuz3qhH3x" role="37wK5m">
                <node concept="1pGfFk" id="2yiuz3qhH3y" role="2ShVmc">
                  <ref role="37wK5l" to="dxuu:~BoxLayout.&lt;init&gt;(java.awt.Container,int)" resolve="BoxLayout" />
                  <node concept="37vLTw" id="2yiuz3qhGBf" role="37wK5m">
                    <ref role="3cqZAo" node="2yiuz3qhGB8" resolve="rightStuff" />
                  </node>
                  <node concept="10M0yZ" id="2yiuz3qhXUc" role="37wK5m">
                    <ref role="1PxDUh" to="dxuu:~BoxLayout" resolve="BoxLayout" />
                    <ref role="3cqZAo" to="dxuu:~BoxLayout.X_AXIS" resolve="X_AXIS" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhGBh" role="3cqZAp">
          <node concept="2OqwBi" id="2yiuz3qhH3B" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qhH3A" role="2Oq$k0">
              <ref role="3cqZAo" node="2yiuz3qhGB8" resolve="rightStuff" />
            </node>
            <node concept="liA8E" id="2yiuz3qhH3C" role="2OqNvi">
              <ref role="37wK5l" to="z60i:~Container.add(java.awt.Component):java.awt.Component" resolve="add" />
              <node concept="37vLTw" id="2yiuz3qhGBj" role="37wK5m">
                <ref role="3cqZAo" node="2yiuz3qhGAQ" resolve="radioPanel" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhGBk" role="3cqZAp">
          <node concept="2OqwBi" id="2yiuz3qhH3F" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qhH3E" role="2Oq$k0">
              <ref role="3cqZAo" node="2yiuz3qhGB8" resolve="rightStuff" />
            </node>
            <node concept="liA8E" id="2yiuz3qhH3G" role="2OqNvi">
              <ref role="37wK5l" to="z60i:~Container.add(java.awt.Component):java.awt.Component" resolve="add" />
              <node concept="2YIFZM" id="2yiuz3qhH3I" role="37wK5m">
                <ref role="1Pybhc" to="dxuu:~Box" resolve="Box" />
                <ref role="37wK5l" to="dxuu:~Box.createHorizontalGlue():java.awt.Component" resolve="createHorizontalGlue" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhGBn" role="3cqZAp">
          <node concept="2OqwBi" id="2yiuz3qhH3L" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qhH3K" role="2Oq$k0">
              <ref role="3cqZAo" node="2yiuz3qhG_8" resolve="pain" />
            </node>
            <node concept="liA8E" id="2yiuz3qhH3M" role="2OqNvi">
              <ref role="37wK5l" to="z60i:~Container.add(java.awt.Component):java.awt.Component" resolve="add" />
              <node concept="37vLTw" id="2yiuz3qhGBp" role="37wK5m">
                <ref role="3cqZAo" node="2yiuz3qhGB8" resolve="rightStuff" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhGBq" role="3cqZAp">
          <node concept="2OqwBi" id="2yiuz3qhH3P" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qhH3O" role="2Oq$k0">
              <ref role="3cqZAo" node="2yiuz3qhG_8" resolve="pain" />
            </node>
            <node concept="liA8E" id="2yiuz3qhH3Q" role="2OqNvi">
              <ref role="37wK5l" to="z60i:~Container.add(java.awt.Component):java.awt.Component" resolve="add" />
              <node concept="2YIFZM" id="2yiuz3qhH3S" role="37wK5m">
                <ref role="1Pybhc" to="dxuu:~Box" resolve="Box" />
                <ref role="37wK5l" to="dxuu:~Box.createVerticalStrut(int):java.awt.Component" resolve="createVerticalStrut" />
                <node concept="3cmrfG" id="2yiuz3qhGBt" role="37wK5m">
                  <property role="3cmrfH" value="13" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3SKdUt" id="2yiuz3qhGKd" role="3cqZAp">
          <node concept="3SKdUq" id="2yiuz3qhGKc" role="3SKWNk">
            <property role="3SKdUp" value="pain.add(radioPanel);" />
          </node>
        </node>
        <node concept="3SKdUt" id="2yiuz3qhGKf" role="3cqZAp">
          <node concept="3SKdUq" id="2yiuz3qhGKe" role="3SKWNk">
            <property role="3SKdUp" value="pain.add(defaultCharsButton);" />
          </node>
        </node>
        <node concept="3SKdUt" id="2yiuz3qhGKh" role="3cqZAp">
          <node concept="3SKdUq" id="2yiuz3qhGKg" role="3SKWNk">
            <property role="3SKdUp" value="pain.add(allCharsButton);" />
          </node>
        </node>
        <node concept="3SKdUt" id="2yiuz3qhGKj" role="3cqZAp">
          <node concept="3SKdUq" id="2yiuz3qhGKi" role="3SKWNk">
            <property role="3SKdUp" value="pain.add(unicodeCharsButton);" />
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhGBu" role="3cqZAp">
          <node concept="2OqwBi" id="2yiuz3qhH3V" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qhH3U" role="2Oq$k0">
              <ref role="3cqZAo" node="2yiuz3qhGwx" resolve="defaultCharsButton" />
            </node>
            <node concept="liA8E" id="2yiuz3qhH3W" role="2OqNvi">
              <ref role="37wK5l" to="dxuu:~AbstractButton.setSelected(boolean):void" resolve="setSelected" />
              <node concept="3clFbT" id="2yiuz3qhGBw" role="37wK5m">
                <property role="3clFbU" value="true" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhGBx" role="3cqZAp">
          <node concept="2OqwBi" id="2yiuz3qhH3Z" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qhH3Y" role="2Oq$k0">
              <ref role="3cqZAo" node="2yiuz3qhGwH" resolve="charsetList" />
            </node>
            <node concept="liA8E" id="2yiuz3qhH40" role="2OqNvi">
              <ref role="37wK5l" to="dxuu:~JComponent.setEnabled(boolean):void" resolve="setEnabled" />
              <node concept="3clFbT" id="2yiuz3qhGBz" role="37wK5m">
                <property role="3clFbU" value="false" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3SKdUt" id="2yiuz3qhGKl" role="3cqZAp">
          <node concept="3SKdUq" id="2yiuz3qhGKk" role="3SKWNk">
            <property role="3SKdUp" value="frame.getContentPane().add(scroller);" />
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhGB$" role="3cqZAp">
          <node concept="2OqwBi" id="2yiuz3qhH43" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qhH42" role="2Oq$k0">
              <ref role="3cqZAo" node="2yiuz3qhG_8" resolve="pain" />
            </node>
            <node concept="liA8E" id="2yiuz3qhH44" role="2OqNvi">
              <ref role="37wK5l" to="z60i:~Container.add(java.awt.Component):java.awt.Component" resolve="add" />
              <node concept="37vLTw" id="2yiuz3qhGBA" role="37wK5m">
                <ref role="3cqZAo" node="2yiuz3qhGwE" resolve="unicodeBlockScroller" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhGBB" role="3cqZAp">
          <node concept="2OqwBi" id="2yiuz3qhH47" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qhH46" role="2Oq$k0">
              <ref role="3cqZAo" node="2yiuz3qhG_8" resolve="pain" />
            </node>
            <node concept="liA8E" id="2yiuz3qhH48" role="2OqNvi">
              <ref role="37wK5l" to="z60i:~Container.add(java.awt.Component):java.awt.Component" resolve="add" />
              <node concept="2YIFZM" id="2yiuz3qhH4a" role="37wK5m">
                <ref role="1Pybhc" to="dxuu:~Box" resolve="Box" />
                <ref role="37wK5l" to="dxuu:~Box.createVerticalStrut(int):java.awt.Component" resolve="createVerticalStrut" />
                <node concept="3cmrfG" id="2yiuz3qhGBE" role="37wK5m">
                  <property role="3cmrfH" value="8" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="2yiuz3qhGBG" role="3cqZAp">
          <node concept="3cpWsn" id="2yiuz3qhGBF" role="3cpWs9">
            <property role="3TUv4t" value="false" />
            <property role="TrG5h" value="buttons" />
            <node concept="3uibUv" id="2yiuz3qhGBH" role="1tU5fm">
              <ref role="3uigEE" to="dxuu:~JPanel" resolve="JPanel" />
            </node>
            <node concept="2ShNRf" id="2yiuz3qhH4b" role="33vP2m">
              <node concept="1pGfFk" id="2yiuz3qhH4c" role="2ShVmc">
                <ref role="37wK5l" to="dxuu:~JPanel.&lt;init&gt;()" resolve="JPanel" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="2yiuz3qhGBK" role="3cqZAp">
          <node concept="3cpWsn" id="2yiuz3qhGBJ" role="3cpWs9">
            <property role="3TUv4t" value="false" />
            <property role="TrG5h" value="okButton" />
            <node concept="3uibUv" id="2yiuz3qhGBL" role="1tU5fm">
              <ref role="3uigEE" to="dxuu:~JButton" resolve="JButton" />
            </node>
            <node concept="2ShNRf" id="2yiuz3qhH4d" role="33vP2m">
              <node concept="1pGfFk" id="2yiuz3qhH4y" role="2ShVmc">
                <ref role="37wK5l" to="dxuu:~JButton.&lt;init&gt;(java.lang.String)" resolve="JButton" />
                <node concept="Xl_RD" id="2yiuz3qi98z" role="37wK5m">
                  <property role="Xl_RC" value="OK" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhGBP" role="3cqZAp">
          <node concept="2OqwBi" id="2yiuz3qhH4A" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qhH4_" role="2Oq$k0">
              <ref role="3cqZAo" node="2yiuz3qhGBJ" resolve="okButton" />
            </node>
            <node concept="liA8E" id="2yiuz3qhH4B" role="2OqNvi">
              <ref role="37wK5l" to="dxuu:~AbstractButton.addActionListener(java.awt.event.ActionListener):void" resolve="addActionListener" />
              <node concept="2ShNRf" id="2yiuz3qhGBR" role="37wK5m">
                <node concept="YeOm9" id="2yiuz3qlMHr" role="2ShVmc">
                  <node concept="1Y3b0j" id="2yiuz3qlMHu" role="YeSDq">
                    <property role="2bfB8j" value="true" />
                    <ref role="1Y3XeK" to="hyam:~ActionListener" resolve="ActionListener" />
                    <ref role="37wK5l" to="wyt6:~Object.&lt;init&gt;()" resolve="Object" />
                    <node concept="3Tm1VV" id="2yiuz3qlMHv" role="1B3o_S" />
                    <node concept="3clFb_" id="2yiuz3qlMHw" role="jymVt">
                      <property role="1EzhhJ" value="false" />
                      <property role="TrG5h" value="actionPerformed" />
                      <property role="DiZV1" value="false" />
                      <property role="od$2w" value="false" />
                      <node concept="3Tm1VV" id="2yiuz3qlMHx" role="1B3o_S" />
                      <node concept="3cqZAl" id="2yiuz3qlMHz" role="3clF45" />
                      <node concept="37vLTG" id="2yiuz3qlMH$" role="3clF46">
                        <property role="TrG5h" value="p0" />
                        <node concept="3uibUv" id="2yiuz3qlMH_" role="1tU5fm">
                          <ref role="3uigEE" to="hyam:~ActionEvent" resolve="ActionEvent" />
                        </node>
                      </node>
                      <node concept="3clFbS" id="2yiuz3qlMHA" role="3clF47">
                        <node concept="3clFbF" id="2yiuz3qlNkS" role="3cqZAp">
                          <node concept="1rXfSq" id="2yiuz3qlNkR" role="3clFbG">
                            <ref role="37wK5l" to="z60i:~Window.setVisible(boolean):void" resolve="setVisible" />
                            <node concept="3clFbT" id="2yiuz3qlNBL" role="37wK5m">
                              <property role="3clFbU" value="false" />
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhGC3" role="3cqZAp">
          <node concept="2OqwBi" id="2yiuz3qhH4E" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qhH4D" role="2Oq$k0">
              <ref role="3cqZAo" node="2yiuz3qhGBJ" resolve="okButton" />
            </node>
            <node concept="liA8E" id="2yiuz3qhH4F" role="2OqNvi">
              <ref role="37wK5l" to="dxuu:~AbstractButton.setEnabled(boolean):void" resolve="setEnabled" />
              <node concept="3clFbT" id="2yiuz3qhGC5" role="37wK5m">
                <property role="3clFbU" value="true" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhGC6" role="3cqZAp">
          <node concept="2OqwBi" id="2yiuz3qhH4I" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qhH4H" role="2Oq$k0">
              <ref role="3cqZAo" node="2yiuz3qhGBF" resolve="buttons" />
            </node>
            <node concept="liA8E" id="2yiuz3qhH4J" role="2OqNvi">
              <ref role="37wK5l" to="z60i:~Container.add(java.awt.Component):java.awt.Component" resolve="add" />
              <node concept="37vLTw" id="2yiuz3qhGC8" role="37wK5m">
                <ref role="3cqZAo" node="2yiuz3qhGBJ" resolve="okButton" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhGC9" role="3cqZAp">
          <node concept="2OqwBi" id="2yiuz3qhH4M" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qhH4L" role="2Oq$k0">
              <ref role="3cqZAo" node="2yiuz3qhG_8" resolve="pain" />
            </node>
            <node concept="liA8E" id="2yiuz3qhH4N" role="2OqNvi">
              <ref role="37wK5l" to="z60i:~Container.add(java.awt.Component):java.awt.Component" resolve="add" />
              <node concept="37vLTw" id="2yiuz3qhGCb" role="37wK5m">
                <ref role="3cqZAo" node="2yiuz3qhGBF" resolve="buttons" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="2yiuz3qhGCd" role="3cqZAp">
          <node concept="3cpWsn" id="2yiuz3qhGCc" role="3cpWs9">
            <property role="3TUv4t" value="false" />
            <property role="TrG5h" value="root" />
            <node concept="3uibUv" id="2yiuz3qhGCe" role="1tU5fm">
              <ref role="3uigEE" to="dxuu:~JRootPane" resolve="JRootPane" />
            </node>
            <node concept="1rXfSq" id="2yiuz3qhGCf" role="33vP2m">
              <ref role="37wK5l" to="dxuu:~JFrame.getRootPane():javax.swing.JRootPane" resolve="getRootPane" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhGCg" role="3cqZAp">
          <node concept="2OqwBi" id="2yiuz3qhH4Q" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qhH4P" role="2Oq$k0">
              <ref role="3cqZAo" node="2yiuz3qhGCc" resolve="root" />
            </node>
            <node concept="liA8E" id="2yiuz3qhH4R" role="2OqNvi">
              <ref role="37wK5l" to="dxuu:~JRootPane.setDefaultButton(javax.swing.JButton):void" resolve="setDefaultButton" />
              <node concept="37vLTw" id="2yiuz3qhGCi" role="37wK5m">
                <ref role="3cqZAo" node="2yiuz3qhGBJ" resolve="okButton" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="2yiuz3qhGCk" role="3cqZAp">
          <node concept="3cpWsn" id="2yiuz3qhGCj" role="3cpWs9">
            <property role="3TUv4t" value="false" />
            <property role="TrG5h" value="disposer" />
            <node concept="3uibUv" id="2yiuz3qhGCl" role="1tU5fm">
              <ref role="3uigEE" to="hyam:~ActionListener" resolve="ActionListener" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhGCD" role="3cqZAp">
          <node concept="1rXfSq" id="2yiuz3qhGCE" role="3clFbG">
            <ref role="37wK5l" to="z60i:~Window.pack():void" resolve="pack" />
          </node>
        </node>
        <node concept="3cpWs8" id="2yiuz3qipad" role="3cqZAp">
          <node concept="3cpWsn" id="2yiuz3qipac" role="3cpWs9">
            <property role="3TUv4t" value="false" />
            <property role="TrG5h" value="gd" />
            <node concept="3uibUv" id="2yiuz3qipae" role="1tU5fm">
              <ref role="3uigEE" to="z60i:~GraphicsDevice" resolve="GraphicsDevice" />
            </node>
            <node concept="2OqwBi" id="2yiuz3qipaf" role="33vP2m">
              <node concept="2YIFZM" id="2yiuz3qipaj" role="2Oq$k0">
                <ref role="1Pybhc" to="z60i:~GraphicsEnvironment" resolve="GraphicsEnvironment" />
                <ref role="37wK5l" to="z60i:~GraphicsEnvironment.getLocalGraphicsEnvironment():java.awt.GraphicsEnvironment" resolve="getLocalGraphicsEnvironment" />
              </node>
              <node concept="liA8E" id="2yiuz3qipah" role="2OqNvi">
                <ref role="37wK5l" to="z60i:~GraphicsEnvironment.getDefaultScreenDevice():java.awt.GraphicsDevice" resolve="getDefaultScreenDevice" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="2yiuz3qhGCG" role="3cqZAp">
          <node concept="3cpWsn" id="2yiuz3qhGCF" role="3cpWs9">
            <property role="3TUv4t" value="false" />
            <property role="TrG5h" value="screen" />
            <node concept="3uibUv" id="2yiuz3qhGCH" role="1tU5fm">
              <ref role="3uigEE" to="z60i:~Dimension" resolve="Dimension" />
            </node>
            <node concept="2ShNRf" id="2yiuz3qisx1" role="33vP2m">
              <node concept="1pGfFk" id="2yiuz3qitIB" role="2ShVmc">
                <ref role="37wK5l" to="z60i:~Dimension.&lt;init&gt;(int,int)" resolve="Dimension" />
                <node concept="2OqwBi" id="2yiuz3qitSr" role="37wK5m">
                  <node concept="2OqwBi" id="2yiuz3qiqYb" role="2Oq$k0">
                    <node concept="37vLTw" id="2yiuz3qiqEs" role="2Oq$k0">
                      <ref role="3cqZAo" node="2yiuz3qipac" resolve="gd" />
                    </node>
                    <node concept="liA8E" id="2yiuz3qirSe" role="2OqNvi">
                      <ref role="37wK5l" to="z60i:~GraphicsDevice.getDisplayMode():java.awt.DisplayMode" resolve="getDisplayMode" />
                    </node>
                  </node>
                  <node concept="liA8E" id="2yiuz3qitXz" role="2OqNvi">
                    <ref role="37wK5l" to="z60i:~DisplayMode.getWidth():int" resolve="getWidth" />
                  </node>
                </node>
                <node concept="2OqwBi" id="2yiuz3qiue9" role="37wK5m">
                  <node concept="2OqwBi" id="2yiuz3qiuai" role="2Oq$k0">
                    <node concept="37vLTw" id="2yiuz3qiuaj" role="2Oq$k0">
                      <ref role="3cqZAo" node="2yiuz3qipac" resolve="gd" />
                    </node>
                    <node concept="liA8E" id="2yiuz3qiuak" role="2OqNvi">
                      <ref role="37wK5l" to="z60i:~GraphicsDevice.getDisplayMode():java.awt.DisplayMode" resolve="getDisplayMode" />
                    </node>
                  </node>
                  <node concept="liA8E" id="2yiuz3qiujH" role="2OqNvi">
                    <ref role="37wK5l" to="z60i:~DisplayMode.getHeight():int" resolve="getHeight" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="2yiuz3qhGCK" role="3cqZAp">
          <node concept="3cpWsn" id="2yiuz3qhGCJ" role="3cpWs9">
            <property role="3TUv4t" value="false" />
            <property role="TrG5h" value="windowSize" />
            <node concept="3uibUv" id="2yiuz3qhGCL" role="1tU5fm">
              <ref role="3uigEE" to="z60i:~Dimension" resolve="Dimension" />
            </node>
            <node concept="1rXfSq" id="2yiuz3qhGCM" role="33vP2m">
              <ref role="37wK5l" to="z60i:~Component.getSize():java.awt.Dimension" resolve="getSize" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhGCN" role="3cqZAp">
          <node concept="1rXfSq" id="2yiuz3qhGCO" role="3clFbG">
            <ref role="37wK5l" to="z60i:~Window.setLocation(int,int):void" resolve="setLocation" />
            <node concept="FJ1c_" id="2yiuz3qhGCP" role="37wK5m">
              <node concept="1eOMI4" id="2yiuz3qhGCT" role="3uHU7B">
                <node concept="3cpWsd" id="2yiuz3qhGCQ" role="1eOMHV">
                  <node concept="2OqwBi" id="2yiuz3qhH4X" role="3uHU7B">
                    <node concept="37vLTw" id="2yiuz3qhH4W" role="2Oq$k0">
                      <ref role="3cqZAo" node="2yiuz3qhGCF" resolve="screen" />
                    </node>
                    <node concept="2OwXpG" id="2yiuz3qhH4Y" role="2OqNvi">
                      <ref role="2Oxat5" to="z60i:~Dimension.width" resolve="width" />
                    </node>
                  </node>
                  <node concept="2OqwBi" id="2yiuz3qhH51" role="3uHU7w">
                    <node concept="37vLTw" id="2yiuz3qhH50" role="2Oq$k0">
                      <ref role="3cqZAo" node="2yiuz3qhGCJ" resolve="windowSize" />
                    </node>
                    <node concept="2OwXpG" id="2yiuz3qhH52" role="2OqNvi">
                      <ref role="2Oxat5" to="z60i:~Dimension.width" resolve="width" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3cmrfG" id="2yiuz3qhGCU" role="3uHU7w">
                <property role="3cmrfH" value="2" />
              </node>
            </node>
            <node concept="FJ1c_" id="2yiuz3qhGCV" role="37wK5m">
              <node concept="1eOMI4" id="2yiuz3qhGCZ" role="3uHU7B">
                <node concept="3cpWsd" id="2yiuz3qhGCW" role="1eOMHV">
                  <node concept="2OqwBi" id="2yiuz3qhH55" role="3uHU7B">
                    <node concept="37vLTw" id="2yiuz3qhH54" role="2Oq$k0">
                      <ref role="3cqZAo" node="2yiuz3qhGCF" resolve="screen" />
                    </node>
                    <node concept="2OwXpG" id="2yiuz3qhH56" role="2OqNvi">
                      <ref role="2Oxat5" to="z60i:~Dimension.height" resolve="height" />
                    </node>
                  </node>
                  <node concept="2OqwBi" id="2yiuz3qhH59" role="3uHU7w">
                    <node concept="37vLTw" id="2yiuz3qhH58" role="2Oq$k0">
                      <ref role="3cqZAo" node="2yiuz3qhGCJ" resolve="windowSize" />
                    </node>
                    <node concept="2OwXpG" id="2yiuz3qhH5a" role="2OqNvi">
                      <ref role="2Oxat5" to="z60i:~Dimension.height" resolve="height" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3cmrfG" id="2yiuz3qhGD0" role="3uHU7w">
                <property role="3cmrfH" value="2" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="3Tm1VV" id="2yiuz3qhGD4" role="1B3o_S" />
    </node>
    <node concept="3clFb_" id="2yiuz3qhGD5" role="jymVt">
      <property role="TrG5h" value="getCharacters" />
      <property role="DiZV1" value="false" />
      <property role="od$2w" value="false" />
      <node concept="3clFbS" id="2yiuz3qhGD6" role="3clF47">
        <node concept="3clFbJ" id="2yiuz3qhGD7" role="3cqZAp">
          <node concept="2OqwBi" id="2yiuz3qhH5d" role="3clFbw">
            <node concept="37vLTw" id="2yiuz3qhH5c" role="2Oq$k0">
              <ref role="3cqZAo" node="2yiuz3qhGwx" resolve="defaultCharsButton" />
            </node>
            <node concept="liA8E" id="2yiuz3qhH5e" role="2OqNvi">
              <ref role="37wK5l" to="dxuu:~AbstractButton.isSelected():boolean" resolve="isSelected" />
            </node>
          </node>
          <node concept="3clFbS" id="2yiuz3qhGDa" role="3clFbx">
            <node concept="3cpWs6" id="2yiuz3qhGDb" role="3cqZAp">
              <node concept="10M0yZ" id="2yiuz3qhXUd" role="3cqZAk">
                <ref role="1PxDUh" to="r7oa:~PFont" resolve="PFont" />
                <ref role="3cqZAo" to="r7oa:~PFont.CHARSET" resolve="CHARSET" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="2yiuz3qhGDe" role="3cqZAp">
          <node concept="3cpWsn" id="2yiuz3qhGDd" role="3cpWs9">
            <property role="3TUv4t" value="false" />
            <property role="TrG5h" value="charset" />
            <node concept="10Q1$e" id="2yiuz3qhGDg" role="1tU5fm">
              <node concept="10Pfzv" id="2yiuz3qhGDf" role="10Q1$1" />
            </node>
            <node concept="2ShNRf" id="2yiuz3qhGDl" role="33vP2m">
              <node concept="3$_iS1" id="2yiuz3qhGDj" role="2ShVmc">
                <node concept="3$GHV9" id="2yiuz3qhGDk" role="3$GQph">
                  <node concept="3cmrfG" id="2yiuz3qhGDi" role="3$I4v7">
                    <property role="3cmrfH" value="65536" />
                  </node>
                </node>
                <node concept="10Pfzv" id="2yiuz3qhGDh" role="3$_nBY" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbJ" id="2yiuz3qhGDm" role="3cqZAp">
          <node concept="2OqwBi" id="2yiuz3qhH5j" role="3clFbw">
            <node concept="37vLTw" id="2yiuz3qhH5i" role="2Oq$k0">
              <ref role="3cqZAo" node="2yiuz3qhGw$" resolve="allCharsButton" />
            </node>
            <node concept="liA8E" id="2yiuz3qhH5k" role="2OqNvi">
              <ref role="37wK5l" to="dxuu:~AbstractButton.isSelected():boolean" resolve="isSelected" />
            </node>
          </node>
          <node concept="9aQIb" id="2yiuz3qhGDJ" role="9aQIa">
            <node concept="3clFbS" id="2yiuz3qhGDK" role="9aQI4">
              <node concept="3cpWs8" id="2yiuz3qhGDM" role="3cqZAp">
                <node concept="3cpWsn" id="2yiuz3qhGDL" role="3cpWs9">
                  <property role="3TUv4t" value="false" />
                  <property role="TrG5h" value="model" />
                  <node concept="3uibUv" id="2yiuz3qhGDN" role="1tU5fm">
                    <ref role="3uigEE" to="dxuu:~DefaultListModel" resolve="DefaultListModel" />
                  </node>
                  <node concept="10QFUN" id="2yiuz3qhGDO" role="33vP2m">
                    <node concept="2OqwBi" id="2yiuz3qhH5n" role="10QFUP">
                      <node concept="37vLTw" id="2yiuz3qhH5m" role="2Oq$k0">
                        <ref role="3cqZAo" node="2yiuz3qhGwH" resolve="charsetList" />
                      </node>
                      <node concept="liA8E" id="2yiuz3qhH5o" role="2OqNvi">
                        <ref role="37wK5l" to="dxuu:~JList.getModel():javax.swing.ListModel" resolve="getModel" />
                      </node>
                    </node>
                    <node concept="3uibUv" id="2yiuz3qhGDQ" role="10QFUM">
                      <ref role="3uigEE" to="dxuu:~DefaultListModel" resolve="DefaultListModel" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3cpWs8" id="2yiuz3qhGDS" role="3cqZAp">
                <node concept="3cpWsn" id="2yiuz3qhGDR" role="3cpWs9">
                  <property role="3TUv4t" value="false" />
                  <property role="TrG5h" value="index" />
                  <node concept="10Oyi0" id="2yiuz3qhGDT" role="1tU5fm" />
                  <node concept="3cmrfG" id="2yiuz3qhGDU" role="33vP2m">
                    <property role="3cmrfH" value="0" />
                  </node>
                </node>
              </node>
              <node concept="1Dw8fO" id="2yiuz3qhGDV" role="3cqZAp">
                <node concept="3cpWsn" id="2yiuz3qhGDW" role="1Duv9x">
                  <property role="3TUv4t" value="false" />
                  <property role="TrG5h" value="i" />
                  <node concept="10Oyi0" id="2yiuz3qhGDY" role="1tU5fm" />
                  <node concept="3cmrfG" id="2yiuz3qhGDZ" role="33vP2m">
                    <property role="3cmrfH" value="0" />
                  </node>
                </node>
                <node concept="3eOVzh" id="2yiuz3qhGE0" role="1Dwp0S">
                  <node concept="37vLTw" id="2yiuz3qhGE1" role="3uHU7B">
                    <ref role="3cqZAo" node="2yiuz3qhGDW" resolve="i" />
                  </node>
                  <node concept="2OqwBi" id="2yiuz3qhH5r" role="3uHU7w">
                    <node concept="37vLTw" id="2yiuz3qhH5q" role="2Oq$k0">
                      <ref role="3cqZAo" node="2yiuz3qhGwL" resolve="BLOCKS" />
                    </node>
                    <node concept="1Rwk04" id="2yiuz3qhXUi" role="2OqNvi" />
                  </node>
                </node>
                <node concept="3uNrnE" id="2yiuz3qhGE4" role="1Dwrff">
                  <node concept="37vLTw" id="2yiuz3qhGE5" role="2$L3a6">
                    <ref role="3cqZAo" node="2yiuz3qhGDW" resolve="i" />
                  </node>
                </node>
                <node concept="3clFbS" id="2yiuz3qhGE7" role="2LFqv$">
                  <node concept="3clFbJ" id="2yiuz3qhGE8" role="3cqZAp">
                    <node concept="2OqwBi" id="2yiuz3qhGE9" role="3clFbw">
                      <node concept="1eOMI4" id="2yiuz3qhGEe" role="2Oq$k0">
                        <node concept="10QFUN" id="2yiuz3qhGEa" role="1eOMHV">
                          <node concept="2OqwBi" id="2yiuz3qhH5v" role="10QFUP">
                            <node concept="37vLTw" id="2yiuz3qhH5u" role="2Oq$k0">
                              <ref role="3cqZAo" node="2yiuz3qhGDL" resolve="model" />
                            </node>
                            <node concept="liA8E" id="2yiuz3qhH5w" role="2OqNvi">
                              <ref role="37wK5l" to="dxuu:~DefaultListModel.get(int):java.lang.Object" resolve="get" />
                              <node concept="37vLTw" id="2yiuz3qhGEc" role="37wK5m">
                                <ref role="3cqZAo" node="2yiuz3qhGDW" resolve="i" />
                              </node>
                            </node>
                          </node>
                          <node concept="3uibUv" id="2yiuz3qhGEd" role="10QFUM">
                            <ref role="3uigEE" to="dxuu:~JCheckBox" resolve="JCheckBox" />
                          </node>
                        </node>
                      </node>
                      <node concept="liA8E" id="2yiuz3qhGEf" role="2OqNvi">
                        <ref role="37wK5l" to="dxuu:~AbstractButton.isSelected():boolean" resolve="isSelected" />
                      </node>
                    </node>
                    <node concept="3clFbS" id="2yiuz3qhGEh" role="3clFbx">
                      <node concept="1Dw8fO" id="2yiuz3qhGEi" role="3cqZAp">
                        <node concept="3cpWsn" id="2yiuz3qhGEj" role="1Duv9x">
                          <property role="3TUv4t" value="false" />
                          <property role="TrG5h" value="j" />
                          <node concept="10Oyi0" id="2yiuz3qhGEl" role="1tU5fm" />
                          <node concept="AH0OO" id="2yiuz3qhGEm" role="33vP2m">
                            <node concept="37vLTw" id="2yiuz3qhGEn" role="AHHXb">
                              <ref role="3cqZAo" node="2yiuz3qhGze" resolve="blockStart" />
                            </node>
                            <node concept="37vLTw" id="2yiuz3qhGEo" role="AHEQo">
                              <ref role="3cqZAo" node="2yiuz3qhGDW" resolve="i" />
                            </node>
                          </node>
                        </node>
                        <node concept="2dkUwp" id="2yiuz3qhGEp" role="1Dwp0S">
                          <node concept="37vLTw" id="2yiuz3qhGEq" role="3uHU7B">
                            <ref role="3cqZAo" node="2yiuz3qhGEj" resolve="j" />
                          </node>
                          <node concept="AH0OO" id="2yiuz3qhGEr" role="3uHU7w">
                            <node concept="37vLTw" id="2yiuz3qhGEs" role="AHHXb">
                              <ref role="3cqZAo" node="2yiuz3qhGzh" resolve="blockStop" />
                            </node>
                            <node concept="37vLTw" id="2yiuz3qhGEt" role="AHEQo">
                              <ref role="3cqZAo" node="2yiuz3qhGDW" resolve="i" />
                            </node>
                          </node>
                        </node>
                        <node concept="3uNrnE" id="2yiuz3qhGEv" role="1Dwrff">
                          <node concept="37vLTw" id="2yiuz3qhGEw" role="2$L3a6">
                            <ref role="3cqZAo" node="2yiuz3qhGEj" resolve="j" />
                          </node>
                        </node>
                        <node concept="3clFbS" id="2yiuz3qhGEy" role="2LFqv$">
                          <node concept="3clFbF" id="2yiuz3qhGEz" role="3cqZAp">
                            <node concept="37vLTI" id="2yiuz3qhGE$" role="3clFbG">
                              <node concept="AH0OO" id="2yiuz3qhGE_" role="37vLTJ">
                                <node concept="37vLTw" id="2yiuz3qhGEA" role="AHHXb">
                                  <ref role="3cqZAo" node="2yiuz3qhGDd" resolve="charset" />
                                </node>
                                <node concept="3uNrnE" id="2yiuz3qhGEB" role="AHEQo">
                                  <node concept="37vLTw" id="2yiuz3qhGEC" role="2$L3a6">
                                    <ref role="3cqZAo" node="2yiuz3qhGDR" resolve="index" />
                                  </node>
                                </node>
                              </node>
                              <node concept="10QFUN" id="2yiuz3qhGED" role="37vLTx">
                                <node concept="37vLTw" id="2yiuz3qhGEE" role="10QFUP">
                                  <ref role="3cqZAo" node="2yiuz3qhGEj" resolve="j" />
                                </node>
                                <node concept="10Pfzv" id="2yiuz3qhGEF" role="10QFUM" />
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3clFbF" id="2yiuz3qhGEG" role="3cqZAp">
                <node concept="37vLTI" id="2yiuz3qhGEH" role="3clFbG">
                  <node concept="37vLTw" id="2yiuz3qhGEI" role="37vLTJ">
                    <ref role="3cqZAo" node="2yiuz3qhGDd" resolve="charset" />
                  </node>
                  <node concept="2YIFZM" id="2yiuz3qhH5y" role="37vLTx">
                    <ref role="1Pybhc" to="r7oa:~PApplet" resolve="PApplet" />
                    <ref role="37wK5l" to="r7oa:~PApplet.subset(char[],int,int):char[]" resolve="subset" />
                    <node concept="37vLTw" id="2yiuz3qhGEK" role="37wK5m">
                      <ref role="3cqZAo" node="2yiuz3qhGDd" resolve="charset" />
                    </node>
                    <node concept="3cmrfG" id="2yiuz3qhGEL" role="37wK5m">
                      <property role="3cmrfH" value="0" />
                    </node>
                    <node concept="37vLTw" id="2yiuz3qhGEM" role="37wK5m">
                      <ref role="3cqZAo" node="2yiuz3qhGDR" resolve="index" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="3clFbS" id="2yiuz3qhGDp" role="3clFbx">
            <node concept="1Dw8fO" id="2yiuz3qhGDq" role="3cqZAp">
              <node concept="3cpWsn" id="2yiuz3qhGDr" role="1Duv9x">
                <property role="3TUv4t" value="false" />
                <property role="TrG5h" value="i" />
                <node concept="10Oyi0" id="2yiuz3qhGDt" role="1tU5fm" />
                <node concept="3cmrfG" id="2yiuz3qhGDu" role="33vP2m">
                  <property role="3cmrfH" value="0" />
                </node>
              </node>
              <node concept="3eOVzh" id="2yiuz3qhGDv" role="1Dwp0S">
                <node concept="37vLTw" id="2yiuz3qhGDw" role="3uHU7B">
                  <ref role="3cqZAo" node="2yiuz3qhGDr" resolve="i" />
                </node>
                <node concept="2nou5x" id="2yiuz3qhGDx" role="3uHU7w">
                  <property role="2noCCI" value="FFFF" />
                </node>
              </node>
              <node concept="3uNrnE" id="2yiuz3qhGDz" role="1Dwrff">
                <node concept="37vLTw" id="2yiuz3qhGD$" role="2$L3a6">
                  <ref role="3cqZAo" node="2yiuz3qhGDr" resolve="i" />
                </node>
              </node>
              <node concept="3clFbS" id="2yiuz3qhGDA" role="2LFqv$">
                <node concept="3clFbF" id="2yiuz3qhGDB" role="3cqZAp">
                  <node concept="37vLTI" id="2yiuz3qhGDC" role="3clFbG">
                    <node concept="AH0OO" id="2yiuz3qhGDD" role="37vLTJ">
                      <node concept="37vLTw" id="2yiuz3qhGDE" role="AHHXb">
                        <ref role="3cqZAo" node="2yiuz3qhGDd" resolve="charset" />
                      </node>
                      <node concept="37vLTw" id="2yiuz3qhGDF" role="AHEQo">
                        <ref role="3cqZAo" node="2yiuz3qhGDr" resolve="i" />
                      </node>
                    </node>
                    <node concept="10QFUN" id="2yiuz3qhGDG" role="37vLTx">
                      <node concept="37vLTw" id="2yiuz3qhGDH" role="10QFUP">
                        <ref role="3cqZAo" node="2yiuz3qhGDr" resolve="i" />
                      </node>
                      <node concept="10Pfzv" id="2yiuz3qhGDI" role="10QFUM" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3SKdUt" id="2yiuz3qhGKn" role="3cqZAp">
          <node concept="3SKdUq" id="2yiuz3qhGKm" role="3SKWNk">
            <property role="3SKdUp" value="System.out.println(&quot;Creating font with &quot; + charset.length + &quot; characters.&quot;);" />
          </node>
        </node>
        <node concept="3cpWs6" id="2yiuz3qhGEN" role="3cqZAp">
          <node concept="37vLTw" id="2yiuz3qhGEO" role="3cqZAk">
            <ref role="3cqZAo" node="2yiuz3qhGDd" resolve="charset" />
          </node>
        </node>
      </node>
      <node concept="3Tmbuc" id="2yiuz3qhGEP" role="1B3o_S" />
      <node concept="10Q1$e" id="2yiuz3qhGER" role="3clF45">
        <node concept="10Pfzv" id="2yiuz3qhGEQ" role="10Q1$1" />
      </node>
    </node>
    <node concept="Wx3nA" id="2yiuz3qhGwL" role="jymVt">
      <property role="TrG5h" value="BLOCKS" />
      <property role="3TUv4t" value="true" />
      <node concept="10Q1$e" id="2yiuz3qhGwN" role="1tU5fm">
        <node concept="17QB3L" id="2yiuz3qlytC" role="10Q1$1" />
      </node>
      <node concept="2BsdOp" id="2yiuz3qhGza" role="33vP2m">
        <node concept="Xl_RD" id="2yiuz3qhGwO" role="2BsfMF">
          <property role="Xl_RC" value="0000..007F; Basic Latin" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGwP" role="2BsfMF">
          <property role="Xl_RC" value="0080..00FF; Latin-1 Supplement" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGwQ" role="2BsfMF">
          <property role="Xl_RC" value="0100..017F; Latin Extended-A" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGwR" role="2BsfMF">
          <property role="Xl_RC" value="0180..024F; Latin Extended-B" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGwS" role="2BsfMF">
          <property role="Xl_RC" value="0250..02AF; IPA Extensions" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGwT" role="2BsfMF">
          <property role="Xl_RC" value="02B0..02FF; Spacing Modifier Letters" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGwU" role="2BsfMF">
          <property role="Xl_RC" value="0300..036F; Combining Diacritical Marks" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGwV" role="2BsfMF">
          <property role="Xl_RC" value="0370..03FF; Greek and Coptic" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGwW" role="2BsfMF">
          <property role="Xl_RC" value="0400..04FF; Cyrillic" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGwX" role="2BsfMF">
          <property role="Xl_RC" value="0500..052F; Cyrillic Supplement" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGwY" role="2BsfMF">
          <property role="Xl_RC" value="0530..058F; Armenian" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGwZ" role="2BsfMF">
          <property role="Xl_RC" value="0590..05FF; Hebrew" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGx0" role="2BsfMF">
          <property role="Xl_RC" value="0600..06FF; Arabic" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGx1" role="2BsfMF">
          <property role="Xl_RC" value="0700..074F; Syriac" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGx2" role="2BsfMF">
          <property role="Xl_RC" value="0750..077F; Arabic Supplement" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGx3" role="2BsfMF">
          <property role="Xl_RC" value="0780..07BF; Thaana" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGx4" role="2BsfMF">
          <property role="Xl_RC" value="07C0..07FF; NKo" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGx5" role="2BsfMF">
          <property role="Xl_RC" value="0800..083F; Samaritan" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGx6" role="2BsfMF">
          <property role="Xl_RC" value="0900..097F; Devanagari" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGx7" role="2BsfMF">
          <property role="Xl_RC" value="0980..09FF; Bengali" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGx8" role="2BsfMF">
          <property role="Xl_RC" value="0A00..0A7F; Gurmukhi" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGx9" role="2BsfMF">
          <property role="Xl_RC" value="0A80..0AFF; Gujarati" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGxa" role="2BsfMF">
          <property role="Xl_RC" value="0B00..0B7F; Oriya" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGxb" role="2BsfMF">
          <property role="Xl_RC" value="0B80..0BFF; Tamil" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGxc" role="2BsfMF">
          <property role="Xl_RC" value="0C00..0C7F; Telugu" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGxd" role="2BsfMF">
          <property role="Xl_RC" value="0C80..0CFF; Kannada" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGxe" role="2BsfMF">
          <property role="Xl_RC" value="0D00..0D7F; Malayalam" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGxf" role="2BsfMF">
          <property role="Xl_RC" value="0D80..0DFF; Sinhala" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGxg" role="2BsfMF">
          <property role="Xl_RC" value="0E00..0E7F; Thai" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGxh" role="2BsfMF">
          <property role="Xl_RC" value="0E80..0EFF; Lao" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGxi" role="2BsfMF">
          <property role="Xl_RC" value="0F00..0FFF; Tibetan" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGxj" role="2BsfMF">
          <property role="Xl_RC" value="1000..109F; Myanmar" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGxk" role="2BsfMF">
          <property role="Xl_RC" value="10A0..10FF; Georgian" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGxl" role="2BsfMF">
          <property role="Xl_RC" value="1100..11FF; Hangul Jamo" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGxm" role="2BsfMF">
          <property role="Xl_RC" value="1200..137F; Ethiopic" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGxn" role="2BsfMF">
          <property role="Xl_RC" value="1380..139F; Ethiopic Supplement" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGxo" role="2BsfMF">
          <property role="Xl_RC" value="13A0..13FF; Cherokee" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGxp" role="2BsfMF">
          <property role="Xl_RC" value="1400..167F; Unified Canadian Aboriginal Syllabics" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGxq" role="2BsfMF">
          <property role="Xl_RC" value="1680..169F; Ogham" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGxr" role="2BsfMF">
          <property role="Xl_RC" value="16A0..16FF; Runic" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGxs" role="2BsfMF">
          <property role="Xl_RC" value="1700..171F; Tagalog" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGxt" role="2BsfMF">
          <property role="Xl_RC" value="1720..173F; Hanunoo" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGxu" role="2BsfMF">
          <property role="Xl_RC" value="1740..175F; Buhid" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGxv" role="2BsfMF">
          <property role="Xl_RC" value="1760..177F; Tagbanwa" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGxw" role="2BsfMF">
          <property role="Xl_RC" value="1780..17FF; Khmer" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGxx" role="2BsfMF">
          <property role="Xl_RC" value="1800..18AF; Mongolian" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGxy" role="2BsfMF">
          <property role="Xl_RC" value="18B0..18FF; Unified Canadian Aboriginal Syllabics Extended" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGxz" role="2BsfMF">
          <property role="Xl_RC" value="1900..194F; Limbu" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGx$" role="2BsfMF">
          <property role="Xl_RC" value="1950..197F; Tai Le" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGx_" role="2BsfMF">
          <property role="Xl_RC" value="1980..19DF; New Tai Lue" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGxA" role="2BsfMF">
          <property role="Xl_RC" value="19E0..19FF; Khmer Symbols" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGxB" role="2BsfMF">
          <property role="Xl_RC" value="1A00..1A1F; Buginese" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGxC" role="2BsfMF">
          <property role="Xl_RC" value="1A20..1AAF; Tai Tham" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGxD" role="2BsfMF">
          <property role="Xl_RC" value="1B00..1B7F; Balinese" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGxE" role="2BsfMF">
          <property role="Xl_RC" value="1B80..1BBF; Sundanese" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGxF" role="2BsfMF">
          <property role="Xl_RC" value="1C00..1C4F; Lepcha" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGxG" role="2BsfMF">
          <property role="Xl_RC" value="1C50..1C7F; Ol Chiki" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGxH" role="2BsfMF">
          <property role="Xl_RC" value="1CD0..1CFF; Vedic Extensions" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGxI" role="2BsfMF">
          <property role="Xl_RC" value="1D00..1D7F; Phonetic Extensions" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGxJ" role="2BsfMF">
          <property role="Xl_RC" value="1D80..1DBF; Phonetic Extensions Supplement" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGxK" role="2BsfMF">
          <property role="Xl_RC" value="1DC0..1DFF; Combining Diacritical Marks Supplement" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGxL" role="2BsfMF">
          <property role="Xl_RC" value="1E00..1EFF; Latin Extended Additional" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGxM" role="2BsfMF">
          <property role="Xl_RC" value="1F00..1FFF; Greek Extended" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGxN" role="2BsfMF">
          <property role="Xl_RC" value="2000..206F; General Punctuation" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGxO" role="2BsfMF">
          <property role="Xl_RC" value="2070..209F; Superscripts and Subscripts" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGxP" role="2BsfMF">
          <property role="Xl_RC" value="20A0..20CF; Currency Symbols" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGxQ" role="2BsfMF">
          <property role="Xl_RC" value="20D0..20FF; Combining Diacritical Marks for Symbols" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGxR" role="2BsfMF">
          <property role="Xl_RC" value="2100..214F; Letterlike Symbols" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGxS" role="2BsfMF">
          <property role="Xl_RC" value="2150..218F; Number Forms" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGxT" role="2BsfMF">
          <property role="Xl_RC" value="2190..21FF; Arrows" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGxU" role="2BsfMF">
          <property role="Xl_RC" value="2200..22FF; Mathematical Operators" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGxV" role="2BsfMF">
          <property role="Xl_RC" value="2300..23FF; Miscellaneous Technical" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGxW" role="2BsfMF">
          <property role="Xl_RC" value="2400..243F; Control Pictures" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGxX" role="2BsfMF">
          <property role="Xl_RC" value="2440..245F; Optical Character Recognition" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGxY" role="2BsfMF">
          <property role="Xl_RC" value="2460..24FF; Enclosed Alphanumerics" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGxZ" role="2BsfMF">
          <property role="Xl_RC" value="2500..257F; Box Drawing" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGy0" role="2BsfMF">
          <property role="Xl_RC" value="2580..259F; Block Elements" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGy1" role="2BsfMF">
          <property role="Xl_RC" value="25A0..25FF; Geometric Shapes" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGy2" role="2BsfMF">
          <property role="Xl_RC" value="2600..26FF; Miscellaneous Symbols" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGy3" role="2BsfMF">
          <property role="Xl_RC" value="2700..27BF; Dingbats" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGy4" role="2BsfMF">
          <property role="Xl_RC" value="27C0..27EF; Miscellaneous Mathematical Symbols-A" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGy5" role="2BsfMF">
          <property role="Xl_RC" value="27F0..27FF; Supplemental Arrows-A" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGy6" role="2BsfMF">
          <property role="Xl_RC" value="2800..28FF; Braille Patterns" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGy7" role="2BsfMF">
          <property role="Xl_RC" value="2900..297F; Supplemental Arrows-B" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGy8" role="2BsfMF">
          <property role="Xl_RC" value="2980..29FF; Miscellaneous Mathematical Symbols-B" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGy9" role="2BsfMF">
          <property role="Xl_RC" value="2A00..2AFF; Supplemental Mathematical Operators" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGya" role="2BsfMF">
          <property role="Xl_RC" value="2B00..2BFF; Miscellaneous Symbols and Arrows" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGyb" role="2BsfMF">
          <property role="Xl_RC" value="2C00..2C5F; Glagolitic" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGyc" role="2BsfMF">
          <property role="Xl_RC" value="2C60..2C7F; Latin Extended-C" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGyd" role="2BsfMF">
          <property role="Xl_RC" value="2C80..2CFF; Coptic" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGye" role="2BsfMF">
          <property role="Xl_RC" value="2D00..2D2F; Georgian Supplement" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGyf" role="2BsfMF">
          <property role="Xl_RC" value="2D30..2D7F; Tifinagh" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGyg" role="2BsfMF">
          <property role="Xl_RC" value="2D80..2DDF; Ethiopic Extended" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGyh" role="2BsfMF">
          <property role="Xl_RC" value="2DE0..2DFF; Cyrillic Extended-A" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGyi" role="2BsfMF">
          <property role="Xl_RC" value="2E00..2E7F; Supplemental Punctuation" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGyj" role="2BsfMF">
          <property role="Xl_RC" value="2E80..2EFF; CJK Radicals Supplement" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGyk" role="2BsfMF">
          <property role="Xl_RC" value="2F00..2FDF; Kangxi Radicals" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGyl" role="2BsfMF">
          <property role="Xl_RC" value="2FF0..2FFF; Ideographic Description Characters" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGym" role="2BsfMF">
          <property role="Xl_RC" value="3000..303F; CJK Symbols and Punctuation" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGyn" role="2BsfMF">
          <property role="Xl_RC" value="3040..309F; Hiragana" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGyo" role="2BsfMF">
          <property role="Xl_RC" value="30A0..30FF; Katakana" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGyp" role="2BsfMF">
          <property role="Xl_RC" value="3100..312F; Bopomofo" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGyq" role="2BsfMF">
          <property role="Xl_RC" value="3130..318F; Hangul Compatibility Jamo" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGyr" role="2BsfMF">
          <property role="Xl_RC" value="3190..319F; Kanbun" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGys" role="2BsfMF">
          <property role="Xl_RC" value="31A0..31BF; Bopomofo Extended" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGyt" role="2BsfMF">
          <property role="Xl_RC" value="31C0..31EF; CJK Strokes" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGyu" role="2BsfMF">
          <property role="Xl_RC" value="31F0..31FF; Katakana Phonetic Extensions" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGyv" role="2BsfMF">
          <property role="Xl_RC" value="3200..32FF; Enclosed CJK Letters and Months" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGyw" role="2BsfMF">
          <property role="Xl_RC" value="3300..33FF; CJK Compatibility" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGyx" role="2BsfMF">
          <property role="Xl_RC" value="3400..4DBF; CJK Unified Ideographs Extension A" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGyy" role="2BsfMF">
          <property role="Xl_RC" value="4DC0..4DFF; Yijing Hexagram Symbols" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGyz" role="2BsfMF">
          <property role="Xl_RC" value="4E00..9FFF; CJK Unified Ideographs" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGy$" role="2BsfMF">
          <property role="Xl_RC" value="A000..A48F; Yi Syllables" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGy_" role="2BsfMF">
          <property role="Xl_RC" value="A490..A4CF; Yi Radicals" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGyA" role="2BsfMF">
          <property role="Xl_RC" value="A4D0..A4FF; Lisu" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGyB" role="2BsfMF">
          <property role="Xl_RC" value="A500..A63F; Vai" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGyC" role="2BsfMF">
          <property role="Xl_RC" value="A640..A69F; Cyrillic Extended-B" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGyD" role="2BsfMF">
          <property role="Xl_RC" value="A6A0..A6FF; Bamum" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGyE" role="2BsfMF">
          <property role="Xl_RC" value="A700..A71F; Modifier Tone Letters" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGyF" role="2BsfMF">
          <property role="Xl_RC" value="A720..A7FF; Latin Extended-D" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGyG" role="2BsfMF">
          <property role="Xl_RC" value="A800..A82F; Syloti Nagri" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGyH" role="2BsfMF">
          <property role="Xl_RC" value="A830..A83F; Common Indic Number Forms" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGyI" role="2BsfMF">
          <property role="Xl_RC" value="A840..A87F; Phags-pa" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGyJ" role="2BsfMF">
          <property role="Xl_RC" value="A880..A8DF; Saurashtra" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGyK" role="2BsfMF">
          <property role="Xl_RC" value="A8E0..A8FF; Devanagari Extended" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGyL" role="2BsfMF">
          <property role="Xl_RC" value="A900..A92F; Kayah Li" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGyM" role="2BsfMF">
          <property role="Xl_RC" value="A930..A95F; Rejang" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGyN" role="2BsfMF">
          <property role="Xl_RC" value="A960..A97F; Hangul Jamo Extended-A" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGyO" role="2BsfMF">
          <property role="Xl_RC" value="A980..A9DF; Javanese" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGyP" role="2BsfMF">
          <property role="Xl_RC" value="AA00..AA5F; Cham" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGyQ" role="2BsfMF">
          <property role="Xl_RC" value="AA60..AA7F; Myanmar Extended-A" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGyR" role="2BsfMF">
          <property role="Xl_RC" value="AA80..AADF; Tai Viet" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGyS" role="2BsfMF">
          <property role="Xl_RC" value="ABC0..ABFF; Meetei Mayek" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGyT" role="2BsfMF">
          <property role="Xl_RC" value="AC00..D7AF; Hangul Syllables" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGyU" role="2BsfMF">
          <property role="Xl_RC" value="D7B0..D7FF; Hangul Jamo Extended-B" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGyV" role="2BsfMF">
          <property role="Xl_RC" value="D800..DB7F; High Surrogates" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGyW" role="2BsfMF">
          <property role="Xl_RC" value="DB80..DBFF; High Private Use Surrogates" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGyX" role="2BsfMF">
          <property role="Xl_RC" value="DC00..DFFF; Low Surrogates" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGyY" role="2BsfMF">
          <property role="Xl_RC" value="E000..F8FF; Private Use Area" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGyZ" role="2BsfMF">
          <property role="Xl_RC" value="F900..FAFF; CJK Compatibility Ideographs" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGz0" role="2BsfMF">
          <property role="Xl_RC" value="FB00..FB4F; Alphabetic Presentation Forms" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGz1" role="2BsfMF">
          <property role="Xl_RC" value="FB50..FDFF; Arabic Presentation Forms-A" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGz2" role="2BsfMF">
          <property role="Xl_RC" value="FE00..FE0F; Variation Selectors" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGz3" role="2BsfMF">
          <property role="Xl_RC" value="FE10..FE1F; Vertical Forms" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGz4" role="2BsfMF">
          <property role="Xl_RC" value="FE20..FE2F; Combining Half Marks" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGz5" role="2BsfMF">
          <property role="Xl_RC" value="FE30..FE4F; CJK Compatibility Forms" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGz6" role="2BsfMF">
          <property role="Xl_RC" value="FE50..FE6F; Small Form Variants" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGz7" role="2BsfMF">
          <property role="Xl_RC" value="FE70..FEFF; Arabic Presentation Forms-B" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGz8" role="2BsfMF">
          <property role="Xl_RC" value="FF00..FFEF; Halfwidth and Fullwidth Forms" />
        </node>
        <node concept="Xl_RD" id="2yiuz3qhGz9" role="2BsfMF">
          <property role="Xl_RC" value="FFF0..FFFF; Specials" />
        </node>
      </node>
    </node>
    <node concept="Wx3nA" id="2yiuz3qhGzb" role="jymVt">
      <property role="TrG5h" value="blockNames" />
      <property role="3TUv4t" value="false" />
      <node concept="10Q1$e" id="2yiuz3qhGzd" role="1tU5fm">
        <node concept="17QB3L" id="2yiuz3qlytD" role="10Q1$1" />
      </node>
    </node>
    <node concept="Wx3nA" id="2yiuz3qhGze" role="jymVt">
      <property role="TrG5h" value="blockStart" />
      <property role="3TUv4t" value="false" />
      <node concept="10Q1$e" id="2yiuz3qhGzg" role="1tU5fm">
        <node concept="10Oyi0" id="2yiuz3qhGzf" role="10Q1$1" />
      </node>
    </node>
    <node concept="Wx3nA" id="2yiuz3qhGzh" role="jymVt">
      <property role="TrG5h" value="blockStop" />
      <property role="3TUv4t" value="false" />
      <node concept="10Q1$e" id="2yiuz3qhGzj" role="1tU5fm">
        <node concept="10Oyi0" id="2yiuz3qhGzi" role="10Q1$1" />
      </node>
    </node>
    <node concept="1Pe0a1" id="2yiuz3qhG$u" role="jymVt">
      <node concept="3clFbS" id="2yiuz3qhGzl" role="1Pe0a2">
        <node concept="3cpWs8" id="2yiuz3qhGzn" role="3cqZAp">
          <node concept="3cpWsn" id="2yiuz3qhGzm" role="3cpWs9">
            <property role="3TUv4t" value="false" />
            <property role="TrG5h" value="count" />
            <node concept="10Oyi0" id="2yiuz3qhGzo" role="1tU5fm" />
            <node concept="2OqwBi" id="2yiuz3qhH5_" role="33vP2m">
              <node concept="37vLTw" id="2yiuz3qhH5$" role="2Oq$k0">
                <ref role="3cqZAo" node="2yiuz3qhGwL" resolve="BLOCKS" />
              </node>
              <node concept="1Rwk04" id="2yiuz3qhXUj" role="2OqNvi" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhGzq" role="3cqZAp">
          <node concept="37vLTI" id="2yiuz3qhGzr" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qhGzs" role="37vLTJ">
              <ref role="3cqZAo" node="2yiuz3qhGzb" resolve="blockNames" />
            </node>
            <node concept="2ShNRf" id="2yiuz3qhGzx" role="37vLTx">
              <node concept="3$_iS1" id="2yiuz3qhGzv" role="2ShVmc">
                <node concept="3$GHV9" id="2yiuz3qhGzw" role="3$GQph">
                  <node concept="37vLTw" id="2yiuz3qhGzu" role="3$I4v7">
                    <ref role="3cqZAo" node="2yiuz3qhGzm" resolve="count" />
                  </node>
                </node>
                <node concept="17QB3L" id="2yiuz3qlytB" role="3$_nBY" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhGzy" role="3cqZAp">
          <node concept="37vLTI" id="2yiuz3qhGzz" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qhGz$" role="37vLTJ">
              <ref role="3cqZAo" node="2yiuz3qhGze" resolve="blockStart" />
            </node>
            <node concept="2ShNRf" id="2yiuz3qhGzD" role="37vLTx">
              <node concept="3$_iS1" id="2yiuz3qhGzB" role="2ShVmc">
                <node concept="3$GHV9" id="2yiuz3qhGzC" role="3$GQph">
                  <node concept="37vLTw" id="2yiuz3qhGzA" role="3$I4v7">
                    <ref role="3cqZAo" node="2yiuz3qhGzm" resolve="count" />
                  </node>
                </node>
                <node concept="10Oyi0" id="2yiuz3qhGz_" role="3$_nBY" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhGzE" role="3cqZAp">
          <node concept="37vLTI" id="2yiuz3qhGzF" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qhGzG" role="37vLTJ">
              <ref role="3cqZAo" node="2yiuz3qhGzh" resolve="blockStop" />
            </node>
            <node concept="2ShNRf" id="2yiuz3qhGzL" role="37vLTx">
              <node concept="3$_iS1" id="2yiuz3qhGzJ" role="2ShVmc">
                <node concept="3$GHV9" id="2yiuz3qhGzK" role="3$GQph">
                  <node concept="37vLTw" id="2yiuz3qhGzI" role="3$I4v7">
                    <ref role="3cqZAo" node="2yiuz3qhGzm" resolve="count" />
                  </node>
                </node>
                <node concept="10Oyi0" id="2yiuz3qhGzH" role="3$_nBY" />
              </node>
            </node>
          </node>
        </node>
        <node concept="1Dw8fO" id="2yiuz3qhGzM" role="3cqZAp">
          <node concept="3cpWsn" id="2yiuz3qhGzN" role="1Duv9x">
            <property role="3TUv4t" value="false" />
            <property role="TrG5h" value="i" />
            <node concept="10Oyi0" id="2yiuz3qhGzP" role="1tU5fm" />
            <node concept="3cmrfG" id="2yiuz3qhGzQ" role="33vP2m">
              <property role="3cmrfH" value="0" />
            </node>
          </node>
          <node concept="3eOVzh" id="2yiuz3qhGzR" role="1Dwp0S">
            <node concept="37vLTw" id="2yiuz3qhGzS" role="3uHU7B">
              <ref role="3cqZAo" node="2yiuz3qhGzN" resolve="i" />
            </node>
            <node concept="37vLTw" id="2yiuz3qhGzT" role="3uHU7w">
              <ref role="3cqZAo" node="2yiuz3qhGzm" resolve="count" />
            </node>
          </node>
          <node concept="3uNrnE" id="2yiuz3qhGzV" role="1Dwrff">
            <node concept="37vLTw" id="2yiuz3qhGzW" role="2$L3a6">
              <ref role="3cqZAo" node="2yiuz3qhGzN" resolve="i" />
            </node>
          </node>
          <node concept="3clFbS" id="2yiuz3qhGzY" role="2LFqv$">
            <node concept="3cpWs8" id="2yiuz3qhG$0" role="3cqZAp">
              <node concept="3cpWsn" id="2yiuz3qhGzZ" role="3cpWs9">
                <property role="3TUv4t" value="false" />
                <property role="TrG5h" value="line" />
                <node concept="17QB3L" id="2yiuz3qlytG" role="1tU5fm" />
                <node concept="AH0OO" id="2yiuz3qhG$2" role="33vP2m">
                  <node concept="37vLTw" id="2yiuz3qhG$3" role="AHHXb">
                    <ref role="3cqZAo" node="2yiuz3qhGwL" resolve="BLOCKS" />
                  </node>
                  <node concept="37vLTw" id="2yiuz3qhG$4" role="AHEQo">
                    <ref role="3cqZAo" node="2yiuz3qhGzN" resolve="i" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="2yiuz3qhG$5" role="3cqZAp">
              <node concept="37vLTI" id="2yiuz3qhG$6" role="3clFbG">
                <node concept="AH0OO" id="2yiuz3qhG$7" role="37vLTJ">
                  <node concept="37vLTw" id="2yiuz3qhG$8" role="AHHXb">
                    <ref role="3cqZAo" node="2yiuz3qhGze" resolve="blockStart" />
                  </node>
                  <node concept="37vLTw" id="2yiuz3qhG$9" role="AHEQo">
                    <ref role="3cqZAo" node="2yiuz3qhGzN" resolve="i" />
                  </node>
                </node>
                <node concept="2YIFZM" id="2yiuz3qhH5C" role="37vLTx">
                  <ref role="1Pybhc" to="r7oa:~PApplet" resolve="PApplet" />
                  <ref role="37wK5l" to="r7oa:~PApplet.unhex(java.lang.String):int" resolve="unhex" />
                  <node concept="2OqwBi" id="2yiuz3qhH5F" role="37wK5m">
                    <node concept="37vLTw" id="2yiuz3qhH5E" role="2Oq$k0">
                      <ref role="3cqZAo" node="2yiuz3qhGzZ" resolve="line" />
                    </node>
                    <node concept="liA8E" id="2yiuz3qhH5G" role="2OqNvi">
                      <ref role="37wK5l" to="wyt6:~String.substring(int,int):java.lang.String" resolve="substring" />
                      <node concept="3cmrfG" id="2yiuz3qhG$c" role="37wK5m">
                        <property role="3cmrfH" value="0" />
                      </node>
                      <node concept="3cmrfG" id="2yiuz3qhG$d" role="37wK5m">
                        <property role="3cmrfH" value="4" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="2yiuz3qhG$e" role="3cqZAp">
              <node concept="37vLTI" id="2yiuz3qhG$f" role="3clFbG">
                <node concept="AH0OO" id="2yiuz3qhG$g" role="37vLTJ">
                  <node concept="37vLTw" id="2yiuz3qhG$h" role="AHHXb">
                    <ref role="3cqZAo" node="2yiuz3qhGzh" resolve="blockStop" />
                  </node>
                  <node concept="37vLTw" id="2yiuz3qhG$i" role="AHEQo">
                    <ref role="3cqZAo" node="2yiuz3qhGzN" resolve="i" />
                  </node>
                </node>
                <node concept="2YIFZM" id="2yiuz3qhH5I" role="37vLTx">
                  <ref role="1Pybhc" to="r7oa:~PApplet" resolve="PApplet" />
                  <ref role="37wK5l" to="r7oa:~PApplet.unhex(java.lang.String):int" resolve="unhex" />
                  <node concept="2OqwBi" id="2yiuz3qhH5L" role="37wK5m">
                    <node concept="37vLTw" id="2yiuz3qhH5K" role="2Oq$k0">
                      <ref role="3cqZAo" node="2yiuz3qhGzZ" resolve="line" />
                    </node>
                    <node concept="liA8E" id="2yiuz3qhH5M" role="2OqNvi">
                      <ref role="37wK5l" to="wyt6:~String.substring(int,int):java.lang.String" resolve="substring" />
                      <node concept="3cmrfG" id="2yiuz3qhG$l" role="37wK5m">
                        <property role="3cmrfH" value="6" />
                      </node>
                      <node concept="3cmrfG" id="2yiuz3qhG$m" role="37wK5m">
                        <property role="3cmrfH" value="10" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="2yiuz3qhG$n" role="3cqZAp">
              <node concept="37vLTI" id="2yiuz3qhG$o" role="3clFbG">
                <node concept="AH0OO" id="2yiuz3qhG$p" role="37vLTJ">
                  <node concept="37vLTw" id="2yiuz3qhG$q" role="AHHXb">
                    <ref role="3cqZAo" node="2yiuz3qhGzb" resolve="blockNames" />
                  </node>
                  <node concept="37vLTw" id="2yiuz3qhG$r" role="AHEQo">
                    <ref role="3cqZAo" node="2yiuz3qhGzN" resolve="i" />
                  </node>
                </node>
                <node concept="2OqwBi" id="2yiuz3qhH5P" role="37vLTx">
                  <node concept="37vLTw" id="2yiuz3qhH5O" role="2Oq$k0">
                    <ref role="3cqZAo" node="2yiuz3qhGzZ" resolve="line" />
                  </node>
                  <node concept="liA8E" id="2yiuz3qhH5Q" role="2OqNvi">
                    <ref role="37wK5l" to="wyt6:~String.substring(int):java.lang.String" resolve="substring" />
                    <node concept="3cmrfG" id="2yiuz3qhG$t" role="37wK5m">
                      <property role="3cmrfH" value="12" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3SKdUt" id="2yiuz3qhGKp" role="3cqZAp">
          <node concept="3SKdUq" id="2yiuz3qhGKo" role="3SKWNk">
            <property role="3SKdUp" value="PApplet.println(codePointStop);" />
          </node>
        </node>
        <node concept="3SKdUt" id="2yiuz3qhGKr" role="3cqZAp">
          <node concept="3SKdUq" id="2yiuz3qhGKq" role="3SKWNk">
            <property role="3SKdUp" value="PApplet.println(codePoints);" />
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="312cEu" id="2yiuz3qhGFf">
    <property role="TrG5h" value="CheckBoxList" />
    <property role="2bfB8j" value="true" />
    <property role="1sVAO0" value="false" />
    <property role="1EXbeo" value="false" />
    <property role="3GE5qa" value="createFont" />
    <node concept="3uibUv" id="2yiuz3qhGG8" role="1zkMxy">
      <ref role="3uigEE" to="dxuu:~JList" resolve="JList" />
      <node concept="3uibUv" id="2yiuz3qhGG9" role="11_B2D">
        <ref role="3uigEE" to="dxuu:~JCheckBox" resolve="JCheckBox" />
      </node>
    </node>
    <node concept="Wx3nA" id="2yiuz3qhGGa" role="jymVt">
      <property role="TrG5h" value="noFocusBorder" />
      <property role="3TUv4t" value="false" />
      <node concept="3uibUv" id="2yiuz3qhGGb" role="1tU5fm">
        <ref role="3uigEE" to="9z78:~Border" resolve="Border" />
      </node>
      <node concept="2ShNRf" id="2yiuz3qhH5R" role="33vP2m">
        <node concept="1pGfFk" id="2yiuz3qhH5S" role="2ShVmc">
          <ref role="37wK5l" to="9z78:~EmptyBorder.&lt;init&gt;(int,int,int,int)" resolve="EmptyBorder" />
          <node concept="3cmrfG" id="2yiuz3qhGGd" role="37wK5m">
            <property role="3cmrfH" value="1" />
          </node>
          <node concept="3cmrfG" id="2yiuz3qhGGe" role="37wK5m">
            <property role="3cmrfH" value="1" />
          </node>
          <node concept="3cmrfG" id="2yiuz3qhGGf" role="37wK5m">
            <property role="3cmrfH" value="1" />
          </node>
          <node concept="3cmrfG" id="2yiuz3qhGGg" role="37wK5m">
            <property role="3cmrfH" value="1" />
          </node>
        </node>
      </node>
      <node concept="3Tmbuc" id="2yiuz3qhGGh" role="1B3o_S" />
    </node>
    <node concept="3clFbW" id="2yiuz3qhGGi" role="jymVt">
      <property role="DiZV1" value="false" />
      <property role="od$2w" value="false" />
      <node concept="3cqZAl" id="2yiuz3qhGGj" role="3clF45" />
      <node concept="3clFbS" id="2yiuz3qhGGk" role="3clF47">
        <node concept="3clFbF" id="2yiuz3qhGGl" role="3cqZAp">
          <node concept="1rXfSq" id="2yiuz3qhGGm" role="3clFbG">
            <ref role="37wK5l" to="dxuu:~JList.setCellRenderer(javax.swing.ListCellRenderer):void" resolve="setCellRenderer" />
            <node concept="2ShNRf" id="2yiuz3qhH5T" role="37wK5m">
              <node concept="HV5vD" id="2yiuz3qhH5U" role="2ShVmc">
                <ref role="HV5vE" node="2yiuz3qhGFg" resolve="CheckBoxList.CellRenderer" />
              </node>
            </node>
          </node>
          <node concept="15s5l7" id="2yiuz3qlo88" role="lGtFl" />
        </node>
        <node concept="3clFbF" id="2yiuz3qhGGo" role="3cqZAp">
          <node concept="1rXfSq" id="2yiuz3qhGGp" role="3clFbG">
            <ref role="37wK5l" to="z60i:~Component.addMouseListener(java.awt.event.MouseListener):void" resolve="addMouseListener" />
            <node concept="2ShNRf" id="2yiuz3qhGGq" role="37wK5m">
              <node concept="YeOm9" id="2yiuz3qhGGr" role="2ShVmc">
                <node concept="1Y3b0j" id="2yiuz3qhGGs" role="YeSDq">
                  <property role="1sVAO0" value="false" />
                  <property role="1EXbeo" value="false" />
                  <ref role="1Y3XeK" to="hyam:~MouseAdapter" resolve="MouseAdapter" />
                  <ref role="37wK5l" to="hyam:~MouseAdapter.&lt;init&gt;()" resolve="MouseAdapter" />
                  <node concept="3clFb_" id="2yiuz3qhGGt" role="jymVt">
                    <property role="TrG5h" value="mousePressed" />
                    <property role="DiZV1" value="false" />
                    <property role="od$2w" value="false" />
                    <node concept="37vLTG" id="2yiuz3qhGGu" role="3clF46">
                      <property role="TrG5h" value="e" />
                      <property role="3TUv4t" value="false" />
                      <node concept="3uibUv" id="2yiuz3qhGGv" role="1tU5fm">
                        <ref role="3uigEE" to="hyam:~MouseEvent" resolve="MouseEvent" />
                      </node>
                    </node>
                    <node concept="3clFbS" id="2yiuz3qhGGw" role="3clF47">
                      <node concept="3clFbJ" id="2yiuz3qhGGx" role="3cqZAp">
                        <node concept="1rXfSq" id="2yiuz3qhGGy" role="3clFbw">
                          <ref role="37wK5l" to="z60i:~Component.isEnabled():boolean" resolve="isEnabled" />
                        </node>
                        <node concept="3clFbS" id="2yiuz3qhGG$" role="3clFbx">
                          <node concept="3cpWs8" id="2yiuz3qhGGA" role="3cqZAp">
                            <node concept="3cpWsn" id="2yiuz3qhGG_" role="3cpWs9">
                              <property role="3TUv4t" value="false" />
                              <property role="TrG5h" value="index" />
                              <node concept="10Oyi0" id="2yiuz3qhGGB" role="1tU5fm" />
                              <node concept="1rXfSq" id="2yiuz3qhGGC" role="33vP2m">
                                <ref role="37wK5l" to="dxuu:~JList.locationToIndex(java.awt.Point):int" resolve="locationToIndex" />
                                <node concept="2OqwBi" id="2yiuz3qhHa6" role="37wK5m">
                                  <node concept="37vLTw" id="2yiuz3qhHa5" role="2Oq$k0">
                                    <ref role="3cqZAo" node="2yiuz3qhGGu" resolve="e" />
                                  </node>
                                  <node concept="liA8E" id="2yiuz3qhHa7" role="2OqNvi">
                                    <ref role="37wK5l" to="hyam:~MouseEvent.getPoint():java.awt.Point" resolve="getPoint" />
                                  </node>
                                </node>
                              </node>
                            </node>
                          </node>
                          <node concept="3clFbJ" id="2yiuz3qhGGE" role="3cqZAp">
                            <node concept="3y3z36" id="2yiuz3qhGGF" role="3clFbw">
                              <node concept="37vLTw" id="2yiuz3qhGGG" role="3uHU7B">
                                <ref role="3cqZAo" node="2yiuz3qhGG_" resolve="index" />
                              </node>
                              <node concept="1ZRNhn" id="2yiuz3qhGGH" role="3uHU7w">
                                <node concept="3cmrfG" id="2yiuz3qhGGI" role="2$L3a6">
                                  <property role="3cmrfH" value="1" />
                                </node>
                              </node>
                            </node>
                            <node concept="3clFbS" id="2yiuz3qhGGK" role="3clFbx">
                              <node concept="3cpWs8" id="2yiuz3qhGGM" role="3cqZAp">
                                <node concept="3cpWsn" id="2yiuz3qhGGL" role="3cpWs9">
                                  <property role="3TUv4t" value="false" />
                                  <property role="TrG5h" value="checkbox" />
                                  <node concept="3uibUv" id="2yiuz3qhGGN" role="1tU5fm">
                                    <ref role="3uigEE" to="dxuu:~JCheckBox" resolve="JCheckBox" />
                                  </node>
                                  <node concept="2OqwBi" id="2yiuz3qhGGO" role="33vP2m">
                                    <node concept="1rXfSq" id="2yiuz3qhGGP" role="2Oq$k0">
                                      <ref role="37wK5l" to="dxuu:~JList.getModel():javax.swing.ListModel" resolve="getModel" />
                                    </node>
                                    <node concept="liA8E" id="2yiuz3qhGGQ" role="2OqNvi">
                                      <ref role="37wK5l" to="dxuu:~ListModel.getElementAt(int):java.lang.Object" resolve="getElementAt" />
                                      <node concept="37vLTw" id="2yiuz3qhGGR" role="37wK5m">
                                        <ref role="3cqZAo" node="2yiuz3qhGG_" resolve="index" />
                                      </node>
                                    </node>
                                  </node>
                                </node>
                              </node>
                              <node concept="3clFbF" id="2yiuz3qhGGS" role="3cqZAp">
                                <node concept="2OqwBi" id="2yiuz3qhHej" role="3clFbG">
                                  <node concept="37vLTw" id="2yiuz3qhHei" role="2Oq$k0">
                                    <ref role="3cqZAo" node="2yiuz3qhGGL" resolve="checkbox" />
                                  </node>
                                  <node concept="liA8E" id="2yiuz3qhHek" role="2OqNvi">
                                    <ref role="37wK5l" to="dxuu:~AbstractButton.setSelected(boolean):void" resolve="setSelected" />
                                    <node concept="3fqX7Q" id="2yiuz3qhGGU" role="37wK5m">
                                      <node concept="2OqwBi" id="2yiuz3qhHiw" role="3fr31v">
                                        <node concept="37vLTw" id="2yiuz3qhHiv" role="2Oq$k0">
                                          <ref role="3cqZAo" node="2yiuz3qhGGL" resolve="checkbox" />
                                        </node>
                                        <node concept="liA8E" id="2yiuz3qhHix" role="2OqNvi">
                                          <ref role="37wK5l" to="dxuu:~AbstractButton.isSelected():boolean" resolve="isSelected" />
                                        </node>
                                      </node>
                                    </node>
                                  </node>
                                </node>
                              </node>
                              <node concept="3clFbF" id="2yiuz3qhGGW" role="3cqZAp">
                                <node concept="1rXfSq" id="2yiuz3qhGGX" role="3clFbG">
                                  <ref role="37wK5l" to="z60i:~Component.repaint():void" resolve="repaint" />
                                </node>
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                    <node concept="3Tm1VV" id="2yiuz3qhGGY" role="1B3o_S" />
                    <node concept="3cqZAl" id="2yiuz3qhGGZ" role="3clF45" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhGH0" role="3cqZAp">
          <node concept="1rXfSq" id="2yiuz3qhGH1" role="3clFbG">
            <ref role="37wK5l" to="dxuu:~JList.setSelectionMode(int):void" resolve="setSelectionMode" />
            <node concept="10M0yZ" id="2yiuz3qhXUe" role="37wK5m">
              <ref role="1PxDUh" to="dxuu:~ListSelectionModel" resolve="ListSelectionModel" />
              <ref role="3cqZAo" to="dxuu:~ListSelectionModel.SINGLE_SELECTION" resolve="SINGLE_SELECTION" />
            </node>
          </node>
        </node>
      </node>
      <node concept="3Tm1VV" id="2yiuz3qhGH3" role="1B3o_S" />
    </node>
    <node concept="312cEu" id="2yiuz3qhGFg" role="jymVt">
      <property role="TrG5h" value="CellRenderer" />
      <property role="2bfB8j" value="true" />
      <property role="1sVAO0" value="false" />
      <property role="1EXbeo" value="false" />
      <node concept="3Tmbuc" id="2yiuz3qhGFh" role="1B3o_S" />
      <node concept="3uibUv" id="2yiuz3qhGFi" role="EKbjA">
        <ref role="3uigEE" to="dxuu:~ListCellRenderer" resolve="ListCellRenderer" />
      </node>
      <node concept="3clFb_" id="2yiuz3qhGFj" role="jymVt">
        <property role="TrG5h" value="getListCellRendererComponent" />
        <property role="DiZV1" value="false" />
        <property role="od$2w" value="false" />
        <node concept="37vLTG" id="2yiuz3qhGFk" role="3clF46">
          <property role="TrG5h" value="list" />
          <property role="3TUv4t" value="false" />
          <node concept="3uibUv" id="2yiuz3qhGFl" role="1tU5fm">
            <ref role="3uigEE" to="dxuu:~JList" resolve="JList" />
          </node>
        </node>
        <node concept="37vLTG" id="2yiuz3qhGFm" role="3clF46">
          <property role="TrG5h" value="value" />
          <property role="3TUv4t" value="false" />
          <node concept="3uibUv" id="2yiuz3qhGFn" role="1tU5fm">
            <ref role="3uigEE" to="wyt6:~Object" resolve="Object" />
          </node>
        </node>
        <node concept="37vLTG" id="2yiuz3qhGFo" role="3clF46">
          <property role="TrG5h" value="index" />
          <property role="3TUv4t" value="false" />
          <node concept="10Oyi0" id="2yiuz3qhGFp" role="1tU5fm" />
        </node>
        <node concept="37vLTG" id="2yiuz3qhGFq" role="3clF46">
          <property role="TrG5h" value="isSelected" />
          <property role="3TUv4t" value="false" />
          <node concept="10P_77" id="2yiuz3qhGFr" role="1tU5fm" />
        </node>
        <node concept="37vLTG" id="2yiuz3qhGFs" role="3clF46">
          <property role="TrG5h" value="cellHasFocus" />
          <property role="3TUv4t" value="false" />
          <node concept="10P_77" id="2yiuz3qhGFt" role="1tU5fm" />
        </node>
        <node concept="3clFbS" id="2yiuz3qhGFu" role="3clF47">
          <node concept="3cpWs8" id="2yiuz3qhGFw" role="3cqZAp">
            <node concept="3cpWsn" id="2yiuz3qhGFv" role="3cpWs9">
              <property role="3TUv4t" value="false" />
              <property role="TrG5h" value="checkbox" />
              <node concept="3uibUv" id="2yiuz3qhGFx" role="1tU5fm">
                <ref role="3uigEE" to="dxuu:~JCheckBox" resolve="JCheckBox" />
              </node>
              <node concept="10QFUN" id="2yiuz3qhGFy" role="33vP2m">
                <node concept="37vLTw" id="2yiuz3qhGFz" role="10QFUP">
                  <ref role="3cqZAo" node="2yiuz3qhGFm" resolve="value" />
                </node>
                <node concept="3uibUv" id="2yiuz3qhGF$" role="10QFUM">
                  <ref role="3uigEE" to="dxuu:~JCheckBox" resolve="JCheckBox" />
                </node>
              </node>
            </node>
          </node>
          <node concept="3clFbF" id="2yiuz3qhGF_" role="3cqZAp">
            <node concept="2OqwBi" id="2yiuz3qhHqR" role="3clFbG">
              <node concept="37vLTw" id="2yiuz3qhHqQ" role="2Oq$k0">
                <ref role="3cqZAo" node="2yiuz3qhGFv" resolve="checkbox" />
              </node>
              <node concept="liA8E" id="2yiuz3qhHqS" role="2OqNvi">
                <ref role="37wK5l" to="dxuu:~JComponent.setBackground(java.awt.Color):void" resolve="setBackground" />
                <node concept="3K4zz7" id="2yiuz3qhGFE" role="37wK5m">
                  <node concept="37vLTw" id="2yiuz3qhGFB" role="3K4Cdx">
                    <ref role="3cqZAo" node="2yiuz3qhGFq" resolve="isSelected" />
                  </node>
                  <node concept="1rXfSq" id="2yiuz3qhGFC" role="3K4E3e">
                    <ref role="37wK5l" to="dxuu:~JList.getSelectionBackground():java.awt.Color" resolve="getSelectionBackground" />
                  </node>
                  <node concept="1rXfSq" id="2yiuz3qhGFD" role="3K4GZi">
                    <ref role="37wK5l" to="z60i:~Component.getBackground():java.awt.Color" resolve="getBackground" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="3clFbF" id="2yiuz3qhGFF" role="3cqZAp">
            <node concept="2OqwBi" id="2yiuz3qhHv5" role="3clFbG">
              <node concept="37vLTw" id="2yiuz3qhHv4" role="2Oq$k0">
                <ref role="3cqZAo" node="2yiuz3qhGFv" resolve="checkbox" />
              </node>
              <node concept="liA8E" id="2yiuz3qhHv6" role="2OqNvi">
                <ref role="37wK5l" to="dxuu:~JComponent.setForeground(java.awt.Color):void" resolve="setForeground" />
                <node concept="3K4zz7" id="2yiuz3qhGFK" role="37wK5m">
                  <node concept="37vLTw" id="2yiuz3qhGFH" role="3K4Cdx">
                    <ref role="3cqZAo" node="2yiuz3qhGFq" resolve="isSelected" />
                  </node>
                  <node concept="1rXfSq" id="2yiuz3qhGFI" role="3K4E3e">
                    <ref role="37wK5l" to="dxuu:~JList.getSelectionForeground():java.awt.Color" resolve="getSelectionForeground" />
                  </node>
                  <node concept="1rXfSq" id="2yiuz3qhGFJ" role="3K4GZi">
                    <ref role="37wK5l" to="z60i:~Component.getForeground():java.awt.Color" resolve="getForeground" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="3SKdUt" id="2yiuz3qhGKt" role="3cqZAp">
            <node concept="3SKdUq" id="2yiuz3qhGKs" role="3SKWNk">
              <property role="3SKdUp" value="checkbox.setEnabled(isEnabled());" />
            </node>
          </node>
          <node concept="3clFbF" id="2yiuz3qhGFL" role="3cqZAp">
            <node concept="2OqwBi" id="2yiuz3qhHzj" role="3clFbG">
              <node concept="37vLTw" id="2yiuz3qhHzi" role="2Oq$k0">
                <ref role="3cqZAo" node="2yiuz3qhGFv" resolve="checkbox" />
              </node>
              <node concept="liA8E" id="2yiuz3qhHzk" role="2OqNvi">
                <ref role="37wK5l" to="dxuu:~AbstractButton.setEnabled(boolean):void" resolve="setEnabled" />
                <node concept="2OqwBi" id="2yiuz3qhHBx" role="37wK5m">
                  <node concept="37vLTw" id="2yiuz3qhHBw" role="2Oq$k0">
                    <ref role="3cqZAo" node="2yiuz3qhGFk" resolve="list" />
                  </node>
                  <node concept="liA8E" id="2yiuz3qhHBy" role="2OqNvi">
                    <ref role="37wK5l" to="z60i:~Component.isEnabled():boolean" resolve="isEnabled" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="3clFbF" id="2yiuz3qhGFO" role="3cqZAp">
            <node concept="2OqwBi" id="2yiuz3qhHFJ" role="3clFbG">
              <node concept="37vLTw" id="2yiuz3qhHFI" role="2Oq$k0">
                <ref role="3cqZAo" node="2yiuz3qhGFv" resolve="checkbox" />
              </node>
              <node concept="liA8E" id="2yiuz3qhHFK" role="2OqNvi">
                <ref role="37wK5l" to="dxuu:~JComponent.setFont(java.awt.Font):void" resolve="setFont" />
                <node concept="1rXfSq" id="2yiuz3qhGFQ" role="37wK5m">
                  <ref role="37wK5l" to="z60i:~Component.getFont():java.awt.Font" resolve="getFont" />
                </node>
              </node>
            </node>
          </node>
          <node concept="3clFbF" id="2yiuz3qhGFR" role="3cqZAp">
            <node concept="2OqwBi" id="2yiuz3qhHJX" role="3clFbG">
              <node concept="37vLTw" id="2yiuz3qhHJW" role="2Oq$k0">
                <ref role="3cqZAo" node="2yiuz3qhGFv" resolve="checkbox" />
              </node>
              <node concept="liA8E" id="2yiuz3qhHJY" role="2OqNvi">
                <ref role="37wK5l" to="dxuu:~AbstractButton.setFocusPainted(boolean):void" resolve="setFocusPainted" />
                <node concept="3clFbT" id="2yiuz3qhGFT" role="37wK5m">
                  <property role="3clFbU" value="false" />
                </node>
              </node>
            </node>
          </node>
          <node concept="3clFbF" id="2yiuz3qhGFU" role="3cqZAp">
            <node concept="2OqwBi" id="2yiuz3qhHOb" role="3clFbG">
              <node concept="37vLTw" id="2yiuz3qhHOa" role="2Oq$k0">
                <ref role="3cqZAo" node="2yiuz3qhGFv" resolve="checkbox" />
              </node>
              <node concept="liA8E" id="2yiuz3qhHOc" role="2OqNvi">
                <ref role="37wK5l" to="dxuu:~AbstractButton.setBorderPainted(boolean):void" resolve="setBorderPainted" />
                <node concept="3clFbT" id="2yiuz3qhGFW" role="37wK5m">
                  <property role="3clFbU" value="true" />
                </node>
              </node>
            </node>
          </node>
          <node concept="3clFbF" id="2yiuz3qhGFX" role="3cqZAp">
            <node concept="2OqwBi" id="2yiuz3qhHSp" role="3clFbG">
              <node concept="37vLTw" id="2yiuz3qhHSo" role="2Oq$k0">
                <ref role="3cqZAo" node="2yiuz3qhGFv" resolve="checkbox" />
              </node>
              <node concept="liA8E" id="2yiuz3qhHSq" role="2OqNvi">
                <ref role="37wK5l" to="dxuu:~JComponent.setBorder(javax.swing.border.Border):void" resolve="setBorder" />
                <node concept="3K4zz7" id="2yiuz3qhGG3" role="37wK5m">
                  <node concept="37vLTw" id="2yiuz3qhGFZ" role="3K4Cdx">
                    <ref role="3cqZAo" node="2yiuz3qhGFq" resolve="isSelected" />
                  </node>
                  <node concept="2YIFZM" id="2yiuz3qhHWA" role="3K4E3e">
                    <ref role="1Pybhc" to="dxuu:~UIManager" resolve="UIManager" />
                    <ref role="37wK5l" to="dxuu:~UIManager.getBorder(java.lang.Object):javax.swing.border.Border" resolve="getBorder" />
                    <node concept="Xl_RD" id="2yiuz3qhGG1" role="37wK5m">
                      <property role="Xl_RC" value="List.focusCellHighlightBorder" />
                    </node>
                  </node>
                  <node concept="10M0yZ" id="2yiuz3qhXUf" role="3K4GZi">
                    <ref role="1PxDUh" node="2yiuz3qhGFf" resolve="CheckBoxList" />
                    <ref role="3cqZAo" node="2yiuz3qhGGa" resolve="noFocusBorder" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="3cpWs6" id="2yiuz3qhGG4" role="3cqZAp">
            <node concept="37vLTw" id="2yiuz3qhGG5" role="3cqZAk">
              <ref role="3cqZAo" node="2yiuz3qhGFv" resolve="checkbox" />
            </node>
          </node>
        </node>
        <node concept="3Tm1VV" id="2yiuz3qhGG6" role="1B3o_S" />
        <node concept="3uibUv" id="2yiuz3qhGG7" role="3clF45">
          <ref role="3uigEE" to="z60i:~Component" resolve="Component" />
        </node>
      </node>
    </node>
  </node>
  <node concept="312cEu" id="1LhviqSiNG4">
    <property role="TrG5h" value="IntegerManager" />
    <node concept="Wx3nA" id="1LhviqSiSIF" role="jymVt">
      <property role="2dlcS1" value="false" />
      <property role="2dld4O" value="false" />
      <property role="TrG5h" value="INSTANCE" />
      <property role="3TUv4t" value="false" />
      <node concept="3Tm6S6" id="1LhviqSiZhl" role="1B3o_S" />
      <node concept="3uibUv" id="1LhviqSiSMc" role="1tU5fm">
        <ref role="3uigEE" node="1LhviqSiNG4" resolve="IntegerManager" />
      </node>
      <node concept="2ShNRf" id="1LhviqSiSRJ" role="33vP2m">
        <node concept="HV5vD" id="1LhviqSiZfh" role="2ShVmc">
          <ref role="HV5vE" node="1LhviqSiNG4" resolve="IntegerManager" />
        </node>
      </node>
    </node>
    <node concept="312cEg" id="1LhviqSo0AJ" role="jymVt">
      <property role="34CwA1" value="false" />
      <property role="eg7rD" value="false" />
      <property role="TrG5h" value="serFile" />
      <property role="3TUv4t" value="false" />
      <node concept="3Tm6S6" id="1LhviqSo0k9" role="1B3o_S" />
      <node concept="3uibUv" id="1LhviqSo0wU" role="1tU5fm">
        <ref role="3uigEE" to="guwi:~File" resolve="File" />
      </node>
      <node concept="2ShNRf" id="1LhviqSo0QO" role="33vP2m">
        <node concept="1pGfFk" id="1LhviqSo1jg" role="2ShVmc">
          <ref role="37wK5l" to="guwi:~File.&lt;init&gt;(java.lang.String)" resolve="File" />
          <node concept="Xl_RD" id="1LhviqSo1um" role="37wK5m">
            <property role="Xl_RC" value="/Users/alexanderpann/Desktop/map.ser" />
          </node>
        </node>
      </node>
    </node>
    <node concept="312cEg" id="1LhviqSkSzZ" role="jymVt">
      <property role="34CwA1" value="false" />
      <property role="eg7rD" value="false" />
      <property role="TrG5h" value="project" />
      <property role="3TUv4t" value="false" />
      <node concept="3Tm1VV" id="1LhviqSkSBo" role="1B3o_S" />
      <node concept="3uibUv" id="1LhviqSkSu6" role="1tU5fm">
        <ref role="3uigEE" to="z1c3:~Project" resolve="Project" />
      </node>
    </node>
    <node concept="2tJIrI" id="1LhviqSiZim" role="jymVt" />
    <node concept="2YIFZL" id="1LhviqSiZsM" role="jymVt">
      <property role="TrG5h" value="getInstance" />
      <property role="od$2w" value="false" />
      <property role="DiZV1" value="false" />
      <property role="2aFKle" value="false" />
      <node concept="3clFbS" id="1LhviqSiZsP" role="3clF47">
        <node concept="3clFbF" id="1LhviqSiZ$d" role="3cqZAp">
          <node concept="37vLTw" id="1LhviqSlbfW" role="3clFbG">
            <ref role="3cqZAo" node="1LhviqSiSIF" resolve="INSTANCE" />
          </node>
        </node>
      </node>
      <node concept="3Tm1VV" id="1LhviqSiZl$" role="1B3o_S" />
      <node concept="3uibUv" id="1LhviqSiZrU" role="3clF45">
        <ref role="3uigEE" node="1LhviqSiNG4" resolve="IntegerManager" />
      </node>
    </node>
    <node concept="2tJIrI" id="1LhviqSmYNc" role="jymVt" />
    <node concept="3clFb_" id="1LhviqSmZoU" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="TrG5h" value="loadValues" />
      <property role="od$2w" value="false" />
      <property role="DiZV1" value="false" />
      <property role="2aFKle" value="false" />
      <node concept="3clFbS" id="1LhviqSmZoX" role="3clF47">
        <node concept="3cpWs8" id="1LhviqSn0Md" role="3cqZAp">
          <node concept="3cpWsn" id="1LhviqSn0Mg" role="3cpWs9">
            <property role="TrG5h" value="map" />
            <node concept="3rvAFt" id="1LhviqSn0M7" role="1tU5fm">
              <node concept="17QB3L" id="1LhviqSn0Q9" role="3rvQeY" />
              <node concept="3uibUv" id="1LhviqSn0W5" role="3rvSg0">
                <ref role="3uigEE" to="wyt6:~Integer" resolve="Integer" />
              </node>
            </node>
            <node concept="10Nm6u" id="1LhviqSn2wC" role="33vP2m" />
          </node>
        </node>
        <node concept="SfApY" id="1LhviqSmUGr" role="3cqZAp">
          <node concept="3clFbS" id="1LhviqSmUGt" role="SfCbr">
            <node concept="3cpWs8" id="1LhviqSmSyL" role="3cqZAp">
              <node concept="3cpWsn" id="1LhviqSmSyK" role="3cpWs9">
                <property role="3TUv4t" value="false" />
                <property role="TrG5h" value="fis" />
                <node concept="3uibUv" id="1LhviqSmSyM" role="1tU5fm">
                  <ref role="3uigEE" to="guwi:~FileInputStream" resolve="FileInputStream" />
                </node>
                <node concept="2ShNRf" id="1LhviqSmSz2" role="33vP2m">
                  <node concept="1pGfFk" id="1LhviqSmSzJ" role="2ShVmc">
                    <ref role="37wK5l" to="guwi:~FileInputStream.&lt;init&gt;(java.io.File)" resolve="FileInputStream" />
                    <node concept="37vLTw" id="1LhviqSo1Kv" role="37wK5m">
                      <ref role="3cqZAo" node="1LhviqSo0AJ" resolve="serFile" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3cpWs8" id="1LhviqSmSyQ" role="3cqZAp">
              <node concept="3cpWsn" id="1LhviqSmSyP" role="3cpWs9">
                <property role="3TUv4t" value="false" />
                <property role="TrG5h" value="ois" />
                <node concept="3uibUv" id="1LhviqSmSyR" role="1tU5fm">
                  <ref role="3uigEE" to="guwi:~ObjectInputStream" resolve="ObjectInputStream" />
                </node>
                <node concept="2ShNRf" id="1LhviqSmSzK" role="33vP2m">
                  <node concept="1pGfFk" id="1LhviqSmSzL" role="2ShVmc">
                    <ref role="37wK5l" to="guwi:~ObjectInputStream.&lt;init&gt;(java.io.InputStream)" resolve="ObjectInputStream" />
                    <node concept="37vLTw" id="1LhviqSmSyT" role="37wK5m">
                      <ref role="3cqZAo" node="1LhviqSmSyK" resolve="fis" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="1LhviqSn1pU" role="3cqZAp">
              <node concept="37vLTI" id="1LhviqSn1Av" role="3clFbG">
                <node concept="37vLTw" id="1LhviqSn1pS" role="37vLTJ">
                  <ref role="3cqZAo" node="1LhviqSn0Mg" resolve="map" />
                </node>
                <node concept="1eOMI4" id="1LhviqSmTUT" role="37vLTx">
                  <node concept="10QFUN" id="1LhviqSmTUQ" role="1eOMHV">
                    <node concept="3rvAFt" id="1LhviqSmTZT" role="10QFUM">
                      <node concept="17QB3L" id="1LhviqSmU6m" role="3rvQeY" />
                      <node concept="3uibUv" id="1LhviqSmUcH" role="3rvSg0">
                        <ref role="3uigEE" to="wyt6:~Integer" resolve="Integer" />
                      </node>
                    </node>
                    <node concept="2OqwBi" id="1LhviqSmSzR" role="10QFUP">
                      <node concept="37vLTw" id="1LhviqSmSzQ" role="2Oq$k0">
                        <ref role="3cqZAo" node="1LhviqSmSyP" resolve="ois" />
                      </node>
                      <node concept="liA8E" id="1LhviqSmSzS" role="2OqNvi">
                        <ref role="37wK5l" to="guwi:~ObjectInputStream.readObject():java.lang.Object" resolve="readObject" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="1LhviqSmSz0" role="3cqZAp">
              <node concept="2OqwBi" id="1LhviqSmSzY" role="3clFbG">
                <node concept="37vLTw" id="1LhviqSmSzX" role="2Oq$k0">
                  <ref role="3cqZAo" node="1LhviqSmSyP" resolve="ois" />
                </node>
                <node concept="liA8E" id="1LhviqSmSzZ" role="2OqNvi">
                  <ref role="37wK5l" to="guwi:~ObjectInputStream.close():void" resolve="close" />
                </node>
              </node>
            </node>
          </node>
          <node concept="TDmWw" id="1LhviqSmUGu" role="TEbGg">
            <node concept="3cpWsn" id="1LhviqSmUGw" role="TDEfY">
              <property role="TrG5h" value="e" />
              <node concept="3uibUv" id="1LhviqSmUNO" role="1tU5fm">
                <ref role="3uigEE" to="wyt6:~Exception" resolve="Exception" />
              </node>
            </node>
            <node concept="3clFbS" id="1LhviqSmUG$" role="TDEfX">
              <node concept="34ab3g" id="1LhviqSnnSS" role="3cqZAp">
                <property role="35gtTG" value="error" />
                <property role="34fQS0" value="true" />
                <node concept="Xl_RD" id="1LhviqSnnSU" role="34bqiv" />
                <node concept="37vLTw" id="1LhviqSnnSW" role="34bMjA">
                  <ref role="3cqZAo" node="1LhviqSmUGw" resolve="e" />
                </node>
              </node>
              <node concept="3clFbF" id="1LhviqSmUWo" role="3cqZAp">
                <node concept="2OqwBi" id="1LhviqSmUZo" role="3clFbG">
                  <node concept="10M0yZ" id="1LhviqSmUWn" role="2Oq$k0">
                    <ref role="1PxDUh" to="wyt6:~System" resolve="System" />
                    <ref role="3cqZAo" to="wyt6:~System.out" resolve="out" />
                  </node>
                  <node concept="liA8E" id="1LhviqSmV5A" role="2OqNvi">
                    <ref role="37wK5l" to="guwi:~PrintStream.println(java.lang.String):void" resolve="println" />
                    <node concept="2OqwBi" id="1LhviqSmVa$" role="37wK5m">
                      <node concept="37vLTw" id="1LhviqSmV9F" role="2Oq$k0">
                        <ref role="3cqZAo" node="1LhviqSmUGw" resolve="e" />
                      </node>
                      <node concept="liA8E" id="1LhviqSmVeH" role="2OqNvi">
                        <ref role="37wK5l" to="wyt6:~Throwable.getMessage():java.lang.String" resolve="getMessage" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbJ" id="1LhviqSocp9" role="3cqZAp">
          <node concept="3clFbS" id="1LhviqSocpb" role="3clFbx">
            <node concept="3clFbF" id="1LhviqSodue" role="3cqZAp">
              <node concept="37vLTI" id="1LhviqSodIb" role="3clFbG">
                <node concept="2ShNRf" id="1LhviqSodUj" role="37vLTx">
                  <node concept="3rGOSV" id="1LhviqSoeoz" role="2ShVmc">
                    <node concept="17QB3L" id="1LhviqSoey3" role="3rHrn6" />
                    <node concept="3uibUv" id="1LhviqSoeEW" role="3rHtpV">
                      <ref role="3uigEE" to="wyt6:~Integer" resolve="Integer" />
                    </node>
                  </node>
                </node>
                <node concept="37vLTw" id="1LhviqSoduc" role="37vLTJ">
                  <ref role="3cqZAo" node="1LhviqSn0Mg" resolve="map" />
                </node>
              </node>
            </node>
          </node>
          <node concept="3clFbC" id="1LhviqSocZ8" role="3clFbw">
            <node concept="10Nm6u" id="1LhviqSodaK" role="3uHU7w" />
            <node concept="37vLTw" id="1LhviqSocFZ" role="3uHU7B">
              <ref role="3cqZAo" node="1LhviqSn0Mg" resolve="map" />
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="1LhviqSn1Zp" role="3cqZAp">
          <node concept="37vLTw" id="1LhviqSn2cy" role="3cqZAk">
            <ref role="3cqZAo" node="1LhviqSn0Mg" resolve="map" />
          </node>
        </node>
      </node>
      <node concept="3Tm6S6" id="1LhviqSmZ8e" role="1B3o_S" />
      <node concept="3rvAFt" id="1LhviqSmZmV" role="3clF45">
        <node concept="17QB3L" id="1LhviqSmZmW" role="3rvQeY" />
        <node concept="3uibUv" id="1LhviqSmZmX" role="3rvSg0">
          <ref role="3uigEE" to="wyt6:~Integer" resolve="Integer" />
        </node>
      </node>
    </node>
    <node concept="2tJIrI" id="1LhviqSn2GL" role="jymVt" />
    <node concept="3clFb_" id="1LhviqSn3aE" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="TrG5h" value="saveValues" />
      <property role="od$2w" value="false" />
      <property role="DiZV1" value="false" />
      <property role="2aFKle" value="false" />
      <node concept="3clFbS" id="1LhviqSn3aH" role="3clF47">
        <node concept="SfApY" id="1LhviqSmXir" role="3cqZAp">
          <node concept="3clFbS" id="1LhviqSmXit" role="SfCbr">
            <node concept="3cpWs8" id="1LhviqSmWFq" role="3cqZAp">
              <node concept="3cpWsn" id="1LhviqSmWFp" role="3cpWs9">
                <property role="3TUv4t" value="false" />
                <property role="TrG5h" value="fos" />
                <node concept="3uibUv" id="1LhviqSmWFr" role="1tU5fm">
                  <ref role="3uigEE" to="guwi:~FileOutputStream" resolve="FileOutputStream" />
                </node>
                <node concept="2ShNRf" id="1LhviqSmWFC" role="33vP2m">
                  <node concept="1pGfFk" id="1LhviqSmWGl" role="2ShVmc">
                    <ref role="37wK5l" to="guwi:~FileOutputStream.&lt;init&gt;(java.io.File)" resolve="FileOutputStream" />
                    <node concept="37vLTw" id="1LhviqSo279" role="37wK5m">
                      <ref role="3cqZAo" node="1LhviqSo0AJ" resolve="serFile" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3cpWs8" id="1LhviqSmWFv" role="3cqZAp">
              <node concept="3cpWsn" id="1LhviqSmWFu" role="3cpWs9">
                <property role="3TUv4t" value="false" />
                <property role="TrG5h" value="oos" />
                <node concept="3uibUv" id="1LhviqSmWFw" role="1tU5fm">
                  <ref role="3uigEE" to="guwi:~ObjectOutputStream" resolve="ObjectOutputStream" />
                </node>
                <node concept="2ShNRf" id="1LhviqSmWGm" role="33vP2m">
                  <node concept="1pGfFk" id="1LhviqSmWGn" role="2ShVmc">
                    <ref role="37wK5l" to="guwi:~ObjectOutputStream.&lt;init&gt;(java.io.OutputStream)" resolve="ObjectOutputStream" />
                    <node concept="37vLTw" id="1LhviqSmWFy" role="37wK5m">
                      <ref role="3cqZAo" node="1LhviqSmWFp" resolve="fos" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="1LhviqSmWFz" role="3cqZAp">
              <node concept="2OqwBi" id="1LhviqSmWGt" role="3clFbG">
                <node concept="37vLTw" id="1LhviqSmWGs" role="2Oq$k0">
                  <ref role="3cqZAo" node="1LhviqSmWFu" resolve="oos" />
                </node>
                <node concept="liA8E" id="1LhviqSmWGu" role="2OqNvi">
                  <ref role="37wK5l" to="guwi:~ObjectOutputStream.writeObject(java.lang.Object):void" resolve="writeObject" />
                  <node concept="37vLTw" id="1LhviqSn4DI" role="37wK5m">
                    <ref role="3cqZAo" node="1LhviqSn3tS" resolve="map" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="1LhviqSmWFA" role="3cqZAp">
              <node concept="2OqwBi" id="1LhviqSmWG$" role="3clFbG">
                <node concept="37vLTw" id="1LhviqSmWGz" role="2Oq$k0">
                  <ref role="3cqZAo" node="1LhviqSmWFu" resolve="oos" />
                </node>
                <node concept="liA8E" id="1LhviqSmWG_" role="2OqNvi">
                  <ref role="37wK5l" to="guwi:~ObjectOutputStream.close():void" resolve="close" />
                </node>
              </node>
            </node>
          </node>
          <node concept="TDmWw" id="1LhviqSmXiu" role="TEbGg">
            <node concept="3cpWsn" id="1LhviqSmXiw" role="TDEfY">
              <property role="TrG5h" value="e" />
              <node concept="3uibUv" id="1LhviqSmXnJ" role="1tU5fm">
                <ref role="3uigEE" to="wyt6:~Exception" resolve="Exception" />
              </node>
            </node>
            <node concept="3clFbS" id="1LhviqSmXi$" role="TDEfX">
              <node concept="34ab3g" id="1LhviqSnnDa" role="3cqZAp">
                <property role="35gtTG" value="error" />
                <property role="34fQS0" value="true" />
                <node concept="Xl_RD" id="1LhviqSnnDc" role="34bqiv" />
                <node concept="37vLTw" id="1LhviqSnnDe" role="34bMjA">
                  <ref role="3cqZAo" node="1LhviqSmXiw" resolve="e" />
                </node>
              </node>
              <node concept="3clFbF" id="1LhviqSmXvP" role="3cqZAp">
                <node concept="2OqwBi" id="1LhviqSmXvQ" role="3clFbG">
                  <node concept="10M0yZ" id="1LhviqSmXvR" role="2Oq$k0">
                    <ref role="3cqZAo" to="wyt6:~System.out" resolve="out" />
                    <ref role="1PxDUh" to="wyt6:~System" resolve="System" />
                  </node>
                  <node concept="liA8E" id="1LhviqSmXvS" role="2OqNvi">
                    <ref role="37wK5l" to="guwi:~PrintStream.println(java.lang.String):void" resolve="println" />
                    <node concept="2OqwBi" id="1LhviqSmXvT" role="37wK5m">
                      <node concept="37vLTw" id="1LhviqSmXvU" role="2Oq$k0">
                        <ref role="3cqZAo" node="1LhviqSmXiw" resolve="e" />
                      </node>
                      <node concept="liA8E" id="1LhviqSmXvV" role="2OqNvi">
                        <ref role="37wK5l" to="wyt6:~Throwable.getMessage():java.lang.String" resolve="getMessage" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="3Tm6S6" id="1LhviqSn2Vj" role="1B3o_S" />
      <node concept="3cqZAl" id="1LhviqSn332" role="3clF45" />
      <node concept="37vLTG" id="1LhviqSn3tS" role="3clF46">
        <property role="TrG5h" value="map" />
        <node concept="3rvAFt" id="1LhviqSn3tP" role="1tU5fm">
          <node concept="17QB3L" id="1LhviqSn3J3" role="3rvQeY" />
          <node concept="3uibUv" id="1LhviqSn3V0" role="3rvSg0">
            <ref role="3uigEE" to="wyt6:~Integer" resolve="Integer" />
          </node>
        </node>
      </node>
    </node>
    <node concept="2tJIrI" id="1LhviqSj8nO" role="jymVt" />
    <node concept="3clFb_" id="1LhviqSmQR1" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="TrG5h" value="setValue" />
      <property role="od$2w" value="false" />
      <property role="DiZV1" value="false" />
      <property role="2aFKle" value="false" />
      <node concept="3clFbS" id="1LhviqSmQR2" role="3clF47">
        <node concept="3cpWs8" id="1LhviqSn5aq" role="3cqZAp">
          <node concept="3cpWsn" id="1LhviqSn5at" role="3cpWs9">
            <property role="TrG5h" value="values" />
            <node concept="3rvAFt" id="1LhviqSn5an" role="1tU5fm">
              <node concept="17QB3L" id="1LhviqSn5iI" role="3rvQeY" />
              <node concept="3uibUv" id="1LhviqSn5t7" role="3rvSg0">
                <ref role="3uigEE" to="wyt6:~Integer" resolve="Integer" />
              </node>
            </node>
            <node concept="1rXfSq" id="1LhviqSn5T3" role="33vP2m">
              <ref role="37wK5l" node="1LhviqSmZoU" resolve="loadValues" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="1LhviqSn6ia" role="3cqZAp">
          <node concept="37vLTI" id="1LhviqSn77C" role="3clFbG">
            <node concept="37vLTw" id="1LhviqSn7tG" role="37vLTx">
              <ref role="3cqZAo" node="1LhviqSmQXQ" resolve="value" />
            </node>
            <node concept="3EllGN" id="1LhviqSn6xR" role="37vLTJ">
              <node concept="37vLTw" id="1LhviqSn6Fa" role="3ElVtu">
                <ref role="3cqZAo" node="1LhviqSmQR5" resolve="id" />
              </node>
              <node concept="37vLTw" id="1LhviqSn6i8" role="3ElQJh">
                <ref role="3cqZAo" node="1LhviqSn5at" resolve="values" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="1LhviqSn9x0" role="3cqZAp">
          <node concept="1rXfSq" id="1LhviqSn9wY" role="3clFbG">
            <ref role="37wK5l" node="1LhviqSn3aE" resolve="saveValues" />
            <node concept="37vLTw" id="1LhviqSn9JQ" role="37wK5m">
              <ref role="3cqZAo" node="1LhviqSn5at" resolve="values" />
            </node>
          </node>
        </node>
      </node>
      <node concept="3Tm1VV" id="1LhviqSmQR3" role="1B3o_S" />
      <node concept="3cqZAl" id="1LhviqSmXP5" role="3clF45" />
      <node concept="37vLTG" id="1LhviqSmQR5" role="3clF46">
        <property role="TrG5h" value="id" />
        <node concept="17QB3L" id="1LhviqSmQR6" role="1tU5fm" />
      </node>
      <node concept="37vLTG" id="1LhviqSmQXQ" role="3clF46">
        <property role="TrG5h" value="value" />
        <node concept="10Oyi0" id="1LhviqSmQZS" role="1tU5fm" />
      </node>
    </node>
    <node concept="2tJIrI" id="1LhviqSmQPt" role="jymVt" />
    <node concept="3clFb_" id="1LhviqSj5fV" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="TrG5h" value="getValueOfDefault" />
      <property role="od$2w" value="false" />
      <property role="DiZV1" value="false" />
      <property role="2aFKle" value="false" />
      <node concept="3clFbS" id="1LhviqSj5fW" role="3clF47">
        <node concept="3cpWs8" id="1LhviqSn9Tw" role="3cqZAp">
          <node concept="3cpWsn" id="1LhviqSn9Tx" role="3cpWs9">
            <property role="TrG5h" value="values" />
            <node concept="3rvAFt" id="1LhviqSn9Ty" role="1tU5fm">
              <node concept="17QB3L" id="1LhviqSn9Tz" role="3rvQeY" />
              <node concept="3uibUv" id="1LhviqSn9T$" role="3rvSg0">
                <ref role="3uigEE" to="wyt6:~Integer" resolve="Integer" />
              </node>
            </node>
            <node concept="1rXfSq" id="1LhviqSn9T_" role="33vP2m">
              <ref role="37wK5l" node="1LhviqSmZoU" resolve="loadValues" />
            </node>
          </node>
        </node>
        <node concept="3clFbJ" id="1LhviqSnxe1" role="3cqZAp">
          <node concept="3clFbS" id="1LhviqSnxe3" role="3clFbx">
            <node concept="3clFbF" id="1LhviqSop0S" role="3cqZAp">
              <node concept="2OqwBi" id="1LhviqSopcN" role="3clFbG">
                <node concept="10M0yZ" id="1LhviqSop0R" role="2Oq$k0">
                  <ref role="1PxDUh" to="wyt6:~System" resolve="System" />
                  <ref role="3cqZAo" to="wyt6:~System.out" resolve="out" />
                </node>
                <node concept="liA8E" id="1LhviqSopj5" role="2OqNvi">
                  <ref role="37wK5l" to="guwi:~PrintStream.println(java.lang.String):void" resolve="println" />
                  <node concept="3cpWs3" id="1LhviqSopFd" role="37wK5m">
                    <node concept="3EllGN" id="1LhviqSo$vR" role="3uHU7w">
                      <node concept="37vLTw" id="1LhviqSo$CD" role="3ElVtu">
                        <ref role="3cqZAo" node="1LhviqSjJpe" resolve="id" />
                      </node>
                      <node concept="37vLTw" id="1LhviqSo$kO" role="3ElQJh">
                        <ref role="3cqZAo" node="1LhviqSn9Tx" resolve="values" />
                      </node>
                    </node>
                    <node concept="Xl_RD" id="1LhviqSopr2" role="3uHU7B">
                      <property role="Xl_RC" value="Contains key:" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3cpWs6" id="1LhviqSmW5j" role="3cqZAp">
              <node concept="3EllGN" id="1LhviqSmWlN" role="3cqZAk">
                <node concept="37vLTw" id="1LhviqSmWtz" role="3ElVtu">
                  <ref role="3cqZAo" node="1LhviqSjJpe" resolve="id" />
                </node>
                <node concept="37vLTw" id="1LhviqSmW5l" role="3ElQJh">
                  <ref role="3cqZAo" node="1LhviqSn9Tx" resolve="values" />
                </node>
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="1LhviqSnxuZ" role="3clFbw">
            <node concept="37vLTw" id="1LhviqSnxn7" role="2Oq$k0">
              <ref role="3cqZAo" node="1LhviqSn9Tx" resolve="values" />
            </node>
            <node concept="2Nt0df" id="1LhviqSnxCY" role="2OqNvi">
              <node concept="37vLTw" id="1LhviqSnxIA" role="38cxEo">
                <ref role="3cqZAo" node="1LhviqSjJpe" resolve="id" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="1LhviqSny8N" role="3cqZAp">
          <node concept="37vLTw" id="1LhviqSnytM" role="3cqZAk">
            <ref role="3cqZAo" node="1LhviqSnvZj" resolve="defaultValue" />
          </node>
        </node>
      </node>
      <node concept="3Tm1VV" id="1LhviqSj5g3" role="1B3o_S" />
      <node concept="10Oyi0" id="1LhviqSj5st" role="3clF45" />
      <node concept="37vLTG" id="1LhviqSjJpe" role="3clF46">
        <property role="TrG5h" value="id" />
        <node concept="17QB3L" id="1LhviqSjJpd" role="1tU5fm" />
      </node>
      <node concept="37vLTG" id="1LhviqSnvZj" role="3clF46">
        <property role="TrG5h" value="defaultValue" />
        <node concept="10Oyi0" id="1LhviqSnw4R" role="1tU5fm" />
      </node>
    </node>
    <node concept="3Tm1VV" id="1LhviqSiNG5" role="1B3o_S" />
  </node>
  <node concept="312cEu" id="1LhviqSsyt7">
    <property role="TrG5h" value="DocumentationButton" />
    <node concept="2YIFZL" id="1LhviqSszSF" role="jymVt">
      <property role="TrG5h" value="build" />
      <property role="DiZV1" value="false" />
      <property role="od$2w" value="false" />
      <property role="2aFKle" value="false" />
      <node concept="3clFbS" id="1LhviqSszSH" role="3clF47">
        <node concept="3cpWs8" id="1LhviqSszSI" role="3cqZAp">
          <node concept="3cpWsn" id="1LhviqSszSJ" role="3cpWs9">
            <property role="TrG5h" value="openDocumentation" />
            <node concept="3uibUv" id="1LhviqSszSK" role="1tU5fm">
              <ref role="3uigEE" to="dxuu:~JButton" resolve="JButton" />
            </node>
            <node concept="2ShNRf" id="1LhviqSszSL" role="33vP2m">
              <node concept="1pGfFk" id="1LhviqSszSM" role="2ShVmc">
                <ref role="37wK5l" to="dxuu:~JButton.&lt;init&gt;(java.lang.String)" resolve="JButton" />
                <node concept="Xl_RD" id="1LhviqSszSN" role="37wK5m">
                  <property role="Xl_RC" value="Open documentation" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="1LhviqSszSO" role="3cqZAp">
          <node concept="3cpWsn" id="1LhviqSszSP" role="3cpWs9">
            <property role="TrG5h" value="url" />
            <node concept="17QB3L" id="1LhviqSszSQ" role="1tU5fm" />
            <node concept="Xl_RD" id="1LhviqSszSR" role="33vP2m">
              <property role="Xl_RC" value="https://processing.org/reference/%s_.html" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="1LhviqSszSS" role="3cqZAp">
          <node concept="2OqwBi" id="1LhviqSszST" role="3clFbG">
            <node concept="37vLTw" id="1LhviqSszSU" role="2Oq$k0">
              <ref role="3cqZAo" node="1LhviqSszSJ" resolve="openDocumentation" />
            </node>
            <node concept="liA8E" id="1LhviqSszSV" role="2OqNvi">
              <ref role="37wK5l" to="dxuu:~AbstractButton.addActionListener(java.awt.event.ActionListener):void" resolve="addActionListener" />
              <node concept="2ShNRf" id="1LhviqSszSW" role="37wK5m">
                <node concept="YeOm9" id="1LhviqSszSX" role="2ShVmc">
                  <node concept="1Y3b0j" id="1LhviqSszSY" role="YeSDq">
                    <property role="2bfB8j" value="true" />
                    <ref role="1Y3XeK" to="hyam:~ActionListener" resolve="ActionListener" />
                    <ref role="37wK5l" to="wyt6:~Object.&lt;init&gt;()" resolve="Object" />
                    <node concept="3Tm1VV" id="1LhviqSszSZ" role="1B3o_S" />
                    <node concept="3clFb_" id="1LhviqSszT0" role="jymVt">
                      <property role="1EzhhJ" value="false" />
                      <property role="TrG5h" value="actionPerformed" />
                      <property role="DiZV1" value="false" />
                      <property role="od$2w" value="false" />
                      <node concept="3Tm1VV" id="1LhviqSszT1" role="1B3o_S" />
                      <node concept="3cqZAl" id="1LhviqSszT2" role="3clF45" />
                      <node concept="37vLTG" id="1LhviqSszT3" role="3clF46">
                        <property role="TrG5h" value="event" />
                        <node concept="3uibUv" id="1LhviqSszT4" role="1tU5fm">
                          <ref role="3uigEE" to="hyam:~ActionEvent" resolve="ActionEvent" />
                        </node>
                      </node>
                      <node concept="3clFbS" id="1LhviqSszT5" role="3clF47">
                        <node concept="1QHqEK" id="1LhviqSszT6" role="3cqZAp">
                          <node concept="1QHqEC" id="1LhviqSszT7" role="1QHqEI">
                            <node concept="3clFbS" id="1LhviqSszT8" role="1bW5cS">
                              <node concept="SfApY" id="1LhviqSszT9" role="3cqZAp">
                                <node concept="3clFbS" id="1LhviqSszTa" role="SfCbr">
                                  <node concept="3clFbF" id="1LhviqSszTb" role="3cqZAp">
                                    <node concept="2OqwBi" id="1LhviqSszTc" role="3clFbG">
                                      <node concept="2YIFZM" id="1LhviqSszTd" role="2Oq$k0">
                                        <ref role="1Pybhc" to="z60i:~Desktop" resolve="Desktop" />
                                        <ref role="37wK5l" to="z60i:~Desktop.getDesktop():java.awt.Desktop" resolve="getDesktop" />
                                      </node>
                                      <node concept="liA8E" id="1LhviqSszTe" role="2OqNvi">
                                        <ref role="37wK5l" to="z60i:~Desktop.browse(java.net.URI):void" resolve="browse" />
                                        <node concept="2YIFZM" id="1LhviqSszTf" role="37wK5m">
                                          <ref role="37wK5l" to="zf81:~URI.create(java.lang.String):java.net.URI" resolve="create" />
                                          <ref role="1Pybhc" to="zf81:~URI" resolve="URI" />
                                          <node concept="2YIFZM" id="1LhviqSszTg" role="37wK5m">
                                            <ref role="37wK5l" to="wyt6:~String.format(java.lang.String,java.lang.Object...):java.lang.String" resolve="format" />
                                            <ref role="1Pybhc" to="wyt6:~String" resolve="String" />
                                            <node concept="37vLTw" id="1LhviqSszTh" role="37wK5m">
                                              <ref role="3cqZAo" node="1LhviqSszSP" resolve="url" />
                                            </node>
                                            <node concept="37vLTw" id="1LhviqSs_2z" role="37wK5m">
                                              <ref role="3cqZAo" node="1LhviqSszTw" resolve="variableName" />
                                            </node>
                                          </node>
                                        </node>
                                      </node>
                                    </node>
                                  </node>
                                  <node concept="3clFbH" id="1LhviqSszTl" role="3cqZAp" />
                                </node>
                                <node concept="TDmWw" id="1LhviqSszTm" role="TEbGg">
                                  <node concept="3cpWsn" id="1LhviqSszTn" role="TDEfY">
                                    <property role="TrG5h" value="e" />
                                    <node concept="3uibUv" id="1LhviqSszTo" role="1tU5fm">
                                      <ref role="3uigEE" to="guwi:~IOException" resolve="IOException" />
                                    </node>
                                  </node>
                                  <node concept="3clFbS" id="1LhviqSszTp" role="TDEfX">
                                    <node concept="34ab3g" id="1LhviqSszTq" role="3cqZAp">
                                      <property role="35gtTG" value="error" />
                                      <property role="34fQS0" value="true" />
                                      <node concept="Xl_RD" id="1LhviqSszTr" role="34bqiv" />
                                      <node concept="37vLTw" id="1LhviqSszTs" role="34bMjA">
                                        <ref role="3cqZAo" node="1LhviqSszTn" resolve="e" />
                                      </node>
                                    </node>
                                  </node>
                                </node>
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="1LhviqSszTt" role="3cqZAp">
          <node concept="37vLTw" id="1LhviqSszTu" role="3clFbG">
            <ref role="3cqZAo" node="1LhviqSszSJ" resolve="openDocumentation" />
          </node>
        </node>
      </node>
      <node concept="3uibUv" id="1LhviqSs$yF" role="3clF45">
        <ref role="3uigEE" to="dxuu:~JComponent" resolve="JComponent" />
      </node>
      <node concept="37vLTG" id="1LhviqSszTw" role="3clF46">
        <property role="TrG5h" value="variableName" />
        <property role="3TUv4t" value="true" />
        <node concept="17QB3L" id="1LhviqSszTx" role="1tU5fm" />
      </node>
      <node concept="3Tm1VV" id="1LhviqSszTy" role="1B3o_S" />
    </node>
    <node concept="3Tm1VV" id="1LhviqSsyt8" role="1B3o_S" />
  </node>
</model>

