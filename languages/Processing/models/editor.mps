<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:1905dbbf-a07c-417b-a02f-d1ceba1c3763(Processing.editor)">
  <persistence version="9" />
  <languages>
    <use id="18bc6592-03a6-4e29-a83a-7ff23bde13ba" name="jetbrains.mps.lang.editor" version="3" />
    <use id="b8bb702e-43ed-4090-a902-d180d3e5f292" name="de.slisson.mps.conditionalEditor" version="0" />
    <use id="63650c59-16c8-498a-99c8-005c7ee9515d" name="jetbrains.mps.lang.access" version="0" />
    <use id="766348f7-6a67-4b85-9323-384840132299" name="de.itemis.mps.editor.math" version="0" />
    <use id="e359e0a2-368a-4c40-ae2a-e5a09f9cfd58" name="de.itemis.mps.editor.math.notations" version="0" />
    <devkit ref="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  </languages>
  <imports>
    <import index="tpee" ref="r:00000000-0000-4000-0000-011c895902ca(jetbrains.mps.baseLanguage.structure)" />
    <import index="tpek" ref="r:00000000-0000-4000-0000-011c895902c0(jetbrains.mps.baseLanguage.behavior)" />
    <import index="tpen" ref="r:00000000-0000-4000-0000-011c895902c3(jetbrains.mps.baseLanguage.editor)" />
    <import index="r7oa" ref="96f7ac91-a4c1-4f01-bffe-3b607acce953/java:processing.core(Processing/)" />
    <import index="dxuu" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:javax.swing(JDK/)" />
    <import index="hyam" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.awt.event(JDK/)" />
    <import index="z60i" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.awt(JDK/)" />
    <import index="zf81" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.net(JDK/)" />
    <import index="guwi" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.io(JDK/)" />
    <import index="cj4x" ref="1ed103c3-3aa6-49b7-9c21-6765ee11f224/java:jetbrains.mps.openapi.editor(MPS.Editor/)" />
    <import index="mhbf" ref="8865b7a8-5271-43d3-884c-6fd1d9cfdd34/java:org.jetbrains.mps.openapi.model(MPS.OpenAPI/)" />
    <import index="f4zo" ref="1ed103c3-3aa6-49b7-9c21-6765ee11f224/java:jetbrains.mps.openapi.editor.cells(MPS.Editor/)" />
    <import index="tpco" ref="r:00000000-0000-4000-0000-011c89590284(jetbrains.mps.lang.core.editor)" />
    <import index="mhfm" ref="3f233e7f-b8a6-46d2-a57f-795d56775243/java:org.jetbrains.annotations(Annotations/)" />
    <import index="zce0" ref="1ed103c3-3aa6-49b7-9c21-6765ee11f224/java:jetbrains.mps.smodel.action(MPS.Editor/)" />
    <import index="33ny" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.util(JDK/)" />
    <import index="df3e" ref="r:730c9fb4-e4ce-43d9-91d3-aba1e15e4166(Processing.runtime)" />
    <import index="wyt6" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.lang(JDK/)" />
    <import index="h7pl" ref="r:3842f4d7-6c76-41af-bee7-8a752efd1444(Processing.structure)" implicit="true" />
    <import index="tpck" ref="r:00000000-0000-4000-0000-011c89590288(jetbrains.mps.lang.core.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="b8bb702e-43ed-4090-a902-d180d3e5f292" name="de.slisson.mps.conditionalEditor">
      <concept id="2877762237606985499" name="de.slisson.mps.conditionalEditor.structure.EditorCondition" flags="ig" index="RtMap" />
      <concept id="2877762237606934069" name="de.slisson.mps.conditionalEditor.structure.ConditionalConceptEditorDeclaration" flags="ig" index="RtYIR">
        <property id="2877762237607078183" name="priority" index="Rtri_" />
        <property id="8436908933892732653" name="uniqueName" index="3NULOk" />
        <child id="2877762237607015161" name="condition" index="RtEXV" />
      </concept>
    </language>
    <language id="18bc6592-03a6-4e29-a83a-7ff23bde13ba" name="jetbrains.mps.lang.editor">
      <concept id="1071666914219" name="jetbrains.mps.lang.editor.structure.ConceptEditorDeclaration" flags="ig" index="24kQdi">
        <child id="1078153129734" name="inspectedCellModel" index="6VMZX" />
      </concept>
      <concept id="1140524381322" name="jetbrains.mps.lang.editor.structure.CellModel_ListWithRole" flags="ng" index="2czfm3">
        <property id="1140524450554" name="vertical" index="2czwfN" />
        <property id="1140524450557" name="separatorText" index="2czwfO" />
        <child id="1140524464360" name="cellLayout" index="2czzBx" />
        <child id="1140524464359" name="emptyCellModel" index="2czzBI" />
      </concept>
      <concept id="1106270549637" name="jetbrains.mps.lang.editor.structure.CellLayout_Horizontal" flags="nn" index="2iRfu4" />
      <concept id="1106270571710" name="jetbrains.mps.lang.editor.structure.CellLayout_Vertical" flags="nn" index="2iRkQZ" />
      <concept id="1237303669825" name="jetbrains.mps.lang.editor.structure.CellLayout_Indent" flags="nn" index="l2Vlx" />
      <concept id="1237307900041" name="jetbrains.mps.lang.editor.structure.IndentLayoutIndentStyleClassItem" flags="ln" index="lj46D" />
      <concept id="1237308012275" name="jetbrains.mps.lang.editor.structure.IndentLayoutNewLineStyleClassItem" flags="ln" index="ljvvj" />
      <concept id="1237375020029" name="jetbrains.mps.lang.editor.structure.IndentLayoutNewLineChildrenStyleClassItem" flags="ln" index="pj6Ft" />
      <concept id="1142886221719" name="jetbrains.mps.lang.editor.structure.QueryFunction_NodeCondition" flags="in" index="pkWqt" />
      <concept id="1142886811589" name="jetbrains.mps.lang.editor.structure.ConceptFunctionParameter_node" flags="nn" index="pncrf" />
      <concept id="1080736578640" name="jetbrains.mps.lang.editor.structure.BaseEditorComponent" flags="ig" index="2wURMF">
        <child id="1080736633877" name="cellModel" index="2wV5jI" />
      </concept>
      <concept id="7667276221847570194" name="jetbrains.mps.lang.editor.structure.ParametersInformationStyleClassItem" flags="ln" index="2$oqgb">
        <reference id="8863456892852949148" name="parametersInformation" index="Bvoe9" />
      </concept>
      <concept id="3547227755871693971" name="jetbrains.mps.lang.editor.structure.PredefinedSelector" flags="ng" index="2B6iha">
        <property id="2162403111523065396" name="cellId" index="1lyBwo" />
      </concept>
      <concept id="1216308599511" name="jetbrains.mps.lang.editor.structure.PositionStyleClassItem" flags="ln" index="LD5Jc">
        <property id="1216308761668" name="position" index="LDHlv" />
      </concept>
      <concept id="1164824717996" name="jetbrains.mps.lang.editor.structure.CellMenuDescriptor" flags="ng" index="OXEIz">
        <child id="1164824815888" name="cellMenuPart" index="OY2wv" />
      </concept>
      <concept id="1078939183254" name="jetbrains.mps.lang.editor.structure.CellModel_Component" flags="sg" stub="3162947552742194261" index="PMmxH">
        <reference id="1078939183255" name="editorComponent" index="PMmxG" />
      </concept>
      <concept id="1149850725784" name="jetbrains.mps.lang.editor.structure.CellModel_AttributedNodeCell" flags="ng" index="2SsqMj" />
      <concept id="1214317859050" name="jetbrains.mps.lang.editor.structure.LayoutConstraintStyleClassItem" flags="ln" index="2UZ17K">
        <property id="1214317859051" name="layoutConstraint" index="2UZ17L" />
      </concept>
      <concept id="1214320119173" name="jetbrains.mps.lang.editor.structure.SideTransformAnchorTagStyleClassItem" flags="ln" index="2V7CMv">
        <property id="1214320119174" name="tag" index="2V7CMs" />
      </concept>
      <concept id="1186403694788" name="jetbrains.mps.lang.editor.structure.ColorStyleClassItem" flags="ln" index="VaVBg">
        <property id="1186403713874" name="color" index="Vb096" />
        <child id="1186403803051" name="query" index="VblUZ" />
      </concept>
      <concept id="1186403751766" name="jetbrains.mps.lang.editor.structure.FontStyleStyleClassItem" flags="ln" index="Vb9p2">
        <property id="1186403771423" name="style" index="Vbekb" />
      </concept>
      <concept id="1186404549998" name="jetbrains.mps.lang.editor.structure.ForegroundColorStyleClassItem" flags="ln" index="VechU" />
      <concept id="1186414536763" name="jetbrains.mps.lang.editor.structure.BooleanStyleSheetItem" flags="ln" index="VOi$J">
        <property id="1186414551515" name="flag" index="VOm3f" />
      </concept>
      <concept id="1186414860679" name="jetbrains.mps.lang.editor.structure.EditableStyleClassItem" flags="ln" index="VPxyj" />
      <concept id="1186414928363" name="jetbrains.mps.lang.editor.structure.SelectableStyleSheetItem" flags="ln" index="VPM3Z" />
      <concept id="1164996492011" name="jetbrains.mps.lang.editor.structure.CellMenuPart_ReferentPrimary" flags="ng" index="ZcVJ$" />
      <concept id="1165004207520" name="jetbrains.mps.lang.editor.structure.CellMenuPart_ReplaceNode_Group" flags="ng" index="ZEniJ">
        <child id="1165004529293" name="createFunction" index="ZF_Y2" />
        <child id="1165004529292" name="parametersFunction" index="ZF_Y3" />
      </concept>
      <concept id="1216560327200" name="jetbrains.mps.lang.editor.structure.PositionChildrenStyleClassItem" flags="ln" index="10DmGV">
        <property id="1216560518566" name="position" index="10E5iX" />
      </concept>
      <concept id="1233758997495" name="jetbrains.mps.lang.editor.structure.PunctuationLeftStyleClassItem" flags="ln" index="11L4FC" />
      <concept id="1233759184865" name="jetbrains.mps.lang.editor.structure.PunctuationRightStyleClassItem" flags="ln" index="11LMrY" />
      <concept id="1165253627126" name="jetbrains.mps.lang.editor.structure.CellMenuPart_AbstractGroup" flags="ng" index="1exORT">
        <property id="1165254125954" name="presentation" index="1ezIyd" />
        <child id="1165253890469" name="parameterObjectType" index="1eyP2E" />
      </concept>
      <concept id="1103016434866" name="jetbrains.mps.lang.editor.structure.CellModel_JComponent" flags="sg" stub="8104358048506731196" index="3gTLQM">
        <child id="1176475119347" name="componentProvider" index="3FoqZy" />
      </concept>
      <concept id="1088013125922" name="jetbrains.mps.lang.editor.structure.CellModel_RefCell" flags="sg" stub="730538219795941030" index="1iCGBv">
        <child id="1088186146602" name="editorComponent" index="1sWHZn" />
      </concept>
      <concept id="1225456267680" name="jetbrains.mps.lang.editor.structure.RGBColor" flags="ng" index="1iSF2X">
        <property id="1225456424731" name="value" index="1iTho6" />
      </concept>
      <concept id="1381004262292414836" name="jetbrains.mps.lang.editor.structure.ICellStyle" flags="ng" index="1k5N5V">
        <reference id="1381004262292426837" name="parentStyleClass" index="1k5W1q" />
      </concept>
      <concept id="1236262245656" name="jetbrains.mps.lang.editor.structure.MatchingLabelStyleClassItem" flags="ln" index="3mYdg7">
        <property id="1238091709220" name="labelName" index="1413C4" />
      </concept>
      <concept id="1165424453110" name="jetbrains.mps.lang.editor.structure.CellMenuPart_Generic_Item" flags="ng" index="1oHujT">
        <property id="1165424453111" name="matchingText" index="1oHujS" />
        <child id="1165424453112" name="handlerFunction" index="1oHujR" />
      </concept>
      <concept id="1165424657443" name="jetbrains.mps.lang.editor.structure.CellMenuPart_Generic_Item_Handler" flags="in" index="1oIgkG" />
      <concept id="1088185857835" name="jetbrains.mps.lang.editor.structure.InlineEditorComponent" flags="ig" index="1sVBvm" />
      <concept id="1139848536355" name="jetbrains.mps.lang.editor.structure.CellModel_WithRole" flags="ng" index="1$h60E">
        <property id="1139852716018" name="noTargetText" index="1$x2rV" />
        <property id="1140017977771" name="readOnly" index="1Intyy" />
        <property id="1140114345053" name="allowEmptyText" index="1O74Pk" />
        <reference id="1140103550593" name="relationDeclaration" index="1NtTu8" />
      </concept>
      <concept id="1073389214265" name="jetbrains.mps.lang.editor.structure.EditorCellModel" flags="ng" index="3EYTF0">
        <property id="1130859485024" name="attractsFocus" index="1cu_pB" />
        <reference id="1139959269582" name="actionMap" index="1ERwB7" />
        <child id="1142887637401" name="renderingCondition" index="pqm2j" />
        <child id="1164826688380" name="menuDescriptor" index="P5bDN" />
      </concept>
      <concept id="1073389446423" name="jetbrains.mps.lang.editor.structure.CellModel_Collection" flags="sn" stub="3013115976261988961" index="3EZMnI">
        <property id="1073389446425" name="vertical" index="3EZMnw" />
        <child id="1106270802874" name="cellLayout" index="2iSdaV" />
        <child id="1073389446424" name="childCellModel" index="3EZMnx" />
      </concept>
      <concept id="1073389577006" name="jetbrains.mps.lang.editor.structure.CellModel_Constant" flags="sn" stub="3610246225209162225" index="3F0ifn">
        <property id="1082639509531" name="nullText" index="ilYzB" />
        <property id="1073389577007" name="text" index="3F0ifm" />
      </concept>
      <concept id="1073389658414" name="jetbrains.mps.lang.editor.structure.CellModel_Property" flags="sg" stub="730538219796134133" index="3F0A7n" />
      <concept id="1219418625346" name="jetbrains.mps.lang.editor.structure.IStyleContainer" flags="ng" index="3F0Thp">
        <child id="1219418656006" name="styleItem" index="3F10Kt" />
      </concept>
      <concept id="1073389882823" name="jetbrains.mps.lang.editor.structure.CellModel_RefNode" flags="sg" stub="730538219795960754" index="3F1sOY" />
      <concept id="1073390211982" name="jetbrains.mps.lang.editor.structure.CellModel_RefNodeList" flags="sg" stub="2794558372793454595" index="3F2HdR" />
      <concept id="1176474535556" name="jetbrains.mps.lang.editor.structure.QueryFunction_JComponent" flags="in" index="3Fmcul" />
      <concept id="1163613035599" name="jetbrains.mps.lang.editor.structure.CellMenuPart_AbstractGroup_Query" flags="in" index="3GJtP1" />
      <concept id="1163613131943" name="jetbrains.mps.lang.editor.structure.CellMenuPart_ReplaceNode_Group_Create" flags="in" index="3GJPmD" />
      <concept id="1163613549566" name="jetbrains.mps.lang.editor.structure.CellMenuPart_AbstractGroup_parameterObject" flags="nn" index="3GLrbK" />
      <concept id="1163613822479" name="jetbrains.mps.lang.editor.structure.CellMenuPart_Abstract_editedNode" flags="nn" index="3GMtW1" />
      <concept id="3647146066980922272" name="jetbrains.mps.lang.editor.structure.SelectInEditorOperation" flags="nn" index="1OKiuA">
        <child id="1948540814633499358" name="editorContext" index="lBI5i" />
        <child id="1948540814635895774" name="cellSelector" index="lGT1i" />
        <child id="3604384757217586546" name="selectionStart" index="3dN3m$" />
      </concept>
      <concept id="1161622981231" name="jetbrains.mps.lang.editor.structure.ConceptFunctionParameter_editorContext" flags="nn" index="1Q80Hx" />
      <concept id="1166049232041" name="jetbrains.mps.lang.editor.structure.AbstractComponent" flags="ng" index="1XWOmA">
        <reference id="1166049300910" name="conceptDeclaration" index="1XX52x" />
      </concept>
    </language>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1080223426719" name="jetbrains.mps.baseLanguage.structure.OrExpression" flags="nn" index="22lmx$" />
      <concept id="1215693861676" name="jetbrains.mps.baseLanguage.structure.BaseAssignmentExpression" flags="nn" index="d038R">
        <child id="1068498886297" name="rValue" index="37vLTx" />
        <child id="1068498886295" name="lValue" index="37vLTJ" />
      </concept>
      <concept id="1188220165133" name="jetbrains.mps.baseLanguage.structure.ArrayLiteral" flags="nn" index="2BsdOp">
        <child id="1188220173759" name="item" index="2BsfMF" />
      </concept>
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1145552977093" name="jetbrains.mps.baseLanguage.structure.GenericNewExpression" flags="nn" index="2ShNRf">
        <child id="1145553007750" name="creator" index="2ShVmc" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1070475926800" name="jetbrains.mps.baseLanguage.structure.StringLiteral" flags="nn" index="Xl_RD">
        <property id="1070475926801" name="value" index="Xl_RC" />
      </concept>
      <concept id="1081236700937" name="jetbrains.mps.baseLanguage.structure.StaticMethodCall" flags="nn" index="2YIFZM">
        <reference id="1144433194310" name="classConcept" index="1Pybhc" />
      </concept>
      <concept id="1070534760951" name="jetbrains.mps.baseLanguage.structure.ArrayType" flags="in" index="10Q1$e">
        <child id="1070534760952" name="componentType" index="10Q1$1" />
      </concept>
      <concept id="1068431474542" name="jetbrains.mps.baseLanguage.structure.VariableDeclaration" flags="ng" index="33uBYm">
        <property id="1176718929932" name="isFinal" index="3TUv4t" />
        <child id="1068431790190" name="initializer" index="33vP2m" />
      </concept>
      <concept id="1068498886296" name="jetbrains.mps.baseLanguage.structure.VariableReference" flags="nn" index="37vLTw">
        <reference id="1068581517664" name="variableDeclaration" index="3cqZAo" />
      </concept>
      <concept id="1068498886294" name="jetbrains.mps.baseLanguage.structure.AssignmentExpression" flags="nn" index="37vLTI" />
      <concept id="1225271177708" name="jetbrains.mps.baseLanguage.structure.StringType" flags="in" index="17QB3L" />
      <concept id="1225271283259" name="jetbrains.mps.baseLanguage.structure.NPEEqualsExpression" flags="nn" index="17R0WA" />
      <concept id="4972933694980447171" name="jetbrains.mps.baseLanguage.structure.BaseVariableDeclaration" flags="ng" index="19Szcq">
        <child id="5680397130376446158" name="type" index="1tU5fm" />
      </concept>
      <concept id="1068580123152" name="jetbrains.mps.baseLanguage.structure.EqualsExpression" flags="nn" index="3clFbC" />
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123157" name="jetbrains.mps.baseLanguage.structure.Statement" flags="nn" index="3clFbH" />
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068580123137" name="jetbrains.mps.baseLanguage.structure.BooleanConstant" flags="nn" index="3clFbT">
        <property id="1068580123138" name="value" index="3clFbU" />
      </concept>
      <concept id="1068580320020" name="jetbrains.mps.baseLanguage.structure.IntegerConstant" flags="nn" index="3cmrfG">
        <property id="1068580320021" name="value" index="3cmrfH" />
      </concept>
      <concept id="1068581242878" name="jetbrains.mps.baseLanguage.structure.ReturnStatement" flags="nn" index="3cpWs6">
        <child id="1068581517676" name="expression" index="3cqZAk" />
      </concept>
      <concept id="1068581242864" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclarationStatement" flags="nn" index="3cpWs8">
        <child id="1068581242865" name="localVariableDeclaration" index="3cpWs9" />
      </concept>
      <concept id="1068581242863" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclaration" flags="nr" index="3cpWsn" />
      <concept id="1081516740877" name="jetbrains.mps.baseLanguage.structure.NotExpression" flags="nn" index="3fqX7Q">
        <child id="1081516765348" name="expression" index="3fr31v" />
      </concept>
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
        <child id="1068499141038" name="actualArgument" index="37wK5m" />
      </concept>
      <concept id="1081773326031" name="jetbrains.mps.baseLanguage.structure.BinaryOperation" flags="nn" index="3uHJSO">
        <child id="1081773367579" name="rightExpression" index="3uHU7w" />
        <child id="1081773367580" name="leftExpression" index="3uHU7B" />
      </concept>
      <concept id="1080120340718" name="jetbrains.mps.baseLanguage.structure.AndExpression" flags="nn" index="1Wc70l" />
    </language>
    <language id="aee9cad2-acd4-4608-aef2-0004f6a1cdbd" name="jetbrains.mps.lang.actions">
      <concept id="5979988948250981289" name="jetbrains.mps.lang.actions.structure.SNodeCreatorAndInitializer" flags="nn" index="2fJWfE" />
    </language>
    <language id="e359e0a2-368a-4c40-ae2a-e5a09f9cfd58" name="de.itemis.mps.editor.math.notations">
      <concept id="8658283006837849794" name="de.itemis.mps.editor.math.notations.structure.SqrtEditor" flags="ng" index="jtDx7">
        <child id="8658283006838153797" name="body" index="jiWj0" />
      </concept>
      <concept id="8658283006837848169" name="de.itemis.mps.editor.math.notations.structure.DivisionEditor" flags="ng" index="jtDVG">
        <child id="8658283006838052215" name="lower" index="jiBfM" />
        <child id="8658283006838052220" name="upper" index="jiBfT" />
      </concept>
      <concept id="8658283006837840915" name="de.itemis.mps.editor.math.notations.structure.AbsEditor" flags="ng" index="jtFEm" />
      <concept id="9120555111532910673" name="de.itemis.mps.editor.math.notations.structure.AbstractBracketsEditor" flags="ng" index="1BQ63s">
        <child id="9120555111532911379" name="body" index="1BQ6eu" />
      </concept>
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1204851882688" name="jetbrains.mps.lang.smodel.structure.LinkRefQualifier" flags="ng" index="26LbJo">
        <reference id="1204851882689" name="link" index="26LbJp" />
      </concept>
      <concept id="1177026924588" name="jetbrains.mps.lang.smodel.structure.RefConcept_Reference" flags="nn" index="chp4Y">
        <reference id="1177026940964" name="conceptDeclaration" index="cht4Q" />
      </concept>
      <concept id="1138411891628" name="jetbrains.mps.lang.smodel.structure.SNodeOperation" flags="nn" index="eCIE_">
        <child id="1144104376918" name="parameter" index="1xVPHs" />
      </concept>
      <concept id="1179409122411" name="jetbrains.mps.lang.smodel.structure.Node_ConceptMethodCall" flags="nn" index="2qgKlT" />
      <concept id="7835263205327057228" name="jetbrains.mps.lang.smodel.structure.Node_GetChildrenAndChildAttributesOperation" flags="ng" index="Bykcj" />
      <concept id="1171407110247" name="jetbrains.mps.lang.smodel.structure.Node_GetAncestorOperation" flags="nn" index="2Xjw5R" />
      <concept id="5168775467716640652" name="jetbrains.mps.lang.smodel.structure.OperationParm_LinkQualifier" flags="ng" index="1aIX9F">
        <child id="5168775467716640653" name="linkQualifier" index="1aIX9E" />
      </concept>
      <concept id="1172008320231" name="jetbrains.mps.lang.smodel.structure.Node_IsNotNullOperation" flags="nn" index="3x8VRR" />
      <concept id="1144101972840" name="jetbrains.mps.lang.smodel.structure.OperationParm_Concept" flags="ng" index="1xMEDy">
        <child id="1207343664468" name="conceptArgument" index="ri$Ld" />
      </concept>
      <concept id="1180636770613" name="jetbrains.mps.lang.smodel.structure.SNodeCreator" flags="nn" index="3zrR0B">
        <child id="1180636770616" name="createdType" index="3zrR0E" />
      </concept>
      <concept id="1219352745532" name="jetbrains.mps.lang.smodel.structure.NodeRefExpression" flags="nn" index="3B5_sB">
        <reference id="1219352800908" name="referentNode" index="3B5MYn" />
      </concept>
      <concept id="1140131837776" name="jetbrains.mps.lang.smodel.structure.Node_ReplaceWithAnotherOperation" flags="nn" index="1P9Npp">
        <child id="1140131861877" name="replacementNode" index="1P9ThW" />
      </concept>
      <concept id="1138055754698" name="jetbrains.mps.lang.smodel.structure.SNodeType" flags="in" index="3Tqbb2">
        <reference id="1138405853777" name="concept" index="ehGHo" />
      </concept>
      <concept id="1138056022639" name="jetbrains.mps.lang.smodel.structure.SPropertyAccess" flags="nn" index="3TrcHB">
        <reference id="1138056395725" name="property" index="3TsBF5" />
      </concept>
      <concept id="1138056143562" name="jetbrains.mps.lang.smodel.structure.SLinkAccess" flags="nn" index="3TrEf2">
        <reference id="1138056516764" name="link" index="3Tt5mk" />
      </concept>
      <concept id="1138056282393" name="jetbrains.mps.lang.smodel.structure.SLinkListAccess" flags="nn" index="3Tsc0h">
        <reference id="1138056546658" name="link" index="3TtcxE" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
    <language id="83888646-71ce-4f1c-9c53-c54016f6ad4f" name="jetbrains.mps.baseLanguage.collections">
      <concept id="1176906603202" name="jetbrains.mps.baseLanguage.collections.structure.BinaryOperation" flags="nn" index="56pJg">
        <child id="1176906787974" name="rightExpression" index="576Qk" />
      </concept>
      <concept id="540871147943773365" name="jetbrains.mps.baseLanguage.collections.structure.SingleArgumentSequenceOperation" flags="nn" index="25WWJ4">
        <child id="540871147943773366" name="argument" index="25WWJ7" />
      </concept>
      <concept id="1176923520476" name="jetbrains.mps.baseLanguage.collections.structure.ExcludeOperation" flags="nn" index="66VNe" />
      <concept id="1240325842691" name="jetbrains.mps.baseLanguage.collections.structure.AsSequenceOperation" flags="nn" index="39bAoz" />
      <concept id="1176501494711" name="jetbrains.mps.baseLanguage.collections.structure.IsNotEmptyOperation" flags="nn" index="3GX2aA" />
      <concept id="1172254888721" name="jetbrains.mps.baseLanguage.collections.structure.ContainsOperation" flags="nn" index="3JPx81" />
    </language>
  </registry>
  <node concept="24kQdi" id="2yiuz3qbVAJ">
    <ref role="1XX52x" to="h7pl:2yiuz3qbfwd" resolve="Sketch" />
    <node concept="3EZMnI" id="2yiuz3qc4Kt" role="2wV5jI">
      <node concept="3EZMnI" id="2yiuz3qc5x2" role="3EZMnx">
        <node concept="2iRfu4" id="2yiuz3qc5x3" role="2iSdaV" />
        <node concept="3F0A7n" id="2yiuz3qc5hs" role="3EZMnx">
          <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
        </node>
        <node concept="3F0ifn" id="2yiuz3qc5JS" role="3EZMnx">
          <property role="3F0ifm" value=".pde" />
          <node concept="11L4FC" id="2yiuz3qc5Ms" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
        </node>
      </node>
      <node concept="2iRkQZ" id="2yiuz3qc4Ku" role="2iSdaV" />
      <node concept="3EZMnI" id="fCYJABC" role="3EZMnx">
        <property role="3EZMnw" value="true" />
        <node concept="3EZMnI" id="7zuBzrpzrmo" role="3EZMnx">
          <node concept="ljvvj" id="1_9L3A4Gl2h" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
          <node concept="l2Vlx" id="7zuBzrpzrmp" role="2iSdaV" />
          <node concept="PMmxH" id="3Z61ZaMckgG" role="3EZMnx">
            <ref role="PMmxG" to="tpen:3Z61ZaMckgv" resolve="ClassifierMembers_Component" />
          </node>
          <node concept="3EZMnI" id="3Z61ZaM61I6" role="3EZMnx">
            <node concept="3F0ifn" id="3Z61ZaM6c_2" role="3EZMnx">
              <node concept="ljvvj" id="3Z61ZaM6e$F" role="3F10Kt">
                <property role="VOm3f" value="true" />
              </node>
            </node>
            <node concept="3F0ifn" id="3Z61ZaM65KN" role="3EZMnx">
              <property role="3F0ifm" value="deprecated part" />
              <node concept="VPM3Z" id="3Z61ZaM65KO" role="3F10Kt" />
              <node concept="ljvvj" id="3Z61ZaM65KP" role="3F10Kt">
                <property role="VOm3f" value="true" />
              </node>
              <node concept="VechU" id="3Z61ZaM65KQ" role="3F10Kt">
                <property role="Vb096" value="red" />
              </node>
            </node>
            <node concept="3F2HdR" id="3Z61ZaM65KR" role="3EZMnx">
              <property role="2czwfN" value="false" />
              <ref role="1NtTu8" to="tpee:gr3b4el" />
              <node concept="VPxyj" id="3Z61ZaM65KS" role="3F10Kt" />
              <node concept="10DmGV" id="3Z61ZaM65KT" role="3F10Kt">
                <property role="10E5iX" value="indented" />
              </node>
              <node concept="3F0ifn" id="3Z61ZaM65KU" role="2czzBI">
                <property role="ilYzB" value="&lt;&lt;static fields&gt;&gt;" />
                <node concept="VPxyj" id="3Z61ZaM65KV" role="3F10Kt">
                  <property role="VOm3f" value="true" />
                </node>
              </node>
              <node concept="lj46D" id="3Z61ZaM65KW" role="3F10Kt">
                <property role="VOm3f" value="true" />
              </node>
              <node concept="pj6Ft" id="3Z61ZaM65KX" role="3F10Kt">
                <property role="VOm3f" value="true" />
              </node>
              <node concept="l2Vlx" id="3Z61ZaM65KY" role="2czzBx" />
              <node concept="pkWqt" id="3Z61ZaM65KZ" role="pqm2j">
                <node concept="3clFbS" id="3Z61ZaM65L0" role="2VODD2">
                  <node concept="3clFbF" id="3Z61ZaM65L1" role="3cqZAp">
                    <node concept="22lmx$" id="3Z61ZaM65L2" role="3clFbG">
                      <node concept="2OqwBi" id="3Z61ZaM65L3" role="3uHU7w">
                        <node concept="2OqwBi" id="3Z61ZaM65L4" role="2Oq$k0">
                          <node concept="2qgKlT" id="2oLu0Jc27zs" role="2OqNvi">
                            <ref role="37wK5l" to="tpek:4_LVZ3pBr7M" resolve="staticFields" />
                          </node>
                          <node concept="pncrf" id="3Z61ZaM65L5" role="2Oq$k0" />
                        </node>
                        <node concept="3GX2aA" id="3Z61ZaM65L7" role="2OqNvi" />
                      </node>
                      <node concept="22lmx$" id="3Z61ZaM65L8" role="3uHU7B">
                        <node concept="3fqX7Q" id="3Z61ZaM65L9" role="3uHU7B">
                          <node concept="2OqwBi" id="3Z61ZaM65La" role="3fr31v">
                            <node concept="pncrf" id="3Z61ZaM65Lb" role="2Oq$k0" />
                            <node concept="2qgKlT" id="3Z61ZaM65Lc" role="2OqNvi">
                              <ref role="37wK5l" to="tpek:sWroEc0xXl" resolve="isInner" />
                            </node>
                          </node>
                        </node>
                        <node concept="2OqwBi" id="3Z61ZaM65Ld" role="3uHU7w">
                          <node concept="pncrf" id="3Z61ZaM65Le" role="2Oq$k0" />
                          <node concept="2qgKlT" id="3Z61ZaM65Lf" role="2OqNvi">
                            <ref role="37wK5l" to="tpek:sWroEc0xXc" resolve="isStatic" />
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3F0ifn" id="3Z61ZaM65Lg" role="3EZMnx">
              <node concept="VPM3Z" id="3Z61ZaM65Lh" role="3F10Kt" />
              <node concept="ljvvj" id="3Z61ZaM65Li" role="3F10Kt">
                <property role="VOm3f" value="true" />
              </node>
            </node>
            <node concept="3EZMnI" id="3Z61ZaM65Lj" role="3EZMnx">
              <node concept="pkWqt" id="3Z61ZaM65Lk" role="pqm2j">
                <node concept="3clFbS" id="3Z61ZaM65Ll" role="2VODD2">
                  <node concept="3cpWs6" id="3Z61ZaM65Lm" role="3cqZAp">
                    <node concept="2OqwBi" id="2_1mL0eog4p" role="3cqZAk">
                      <node concept="2OqwBi" id="3Z61ZaM65Lo" role="2Oq$k0">
                        <node concept="pncrf" id="3Z61ZaM65Lp" role="2Oq$k0" />
                        <node concept="Bykcj" id="2_1mL0eog4m" role="2OqNvi">
                          <node concept="1aIX9F" id="2_1mL0eog4n" role="1xVPHs">
                            <node concept="26LbJo" id="2_1mL0eog4o" role="1aIX9E">
                              <ref role="26LbJp" to="tpee:h3anRLq" />
                            </node>
                          </node>
                        </node>
                      </node>
                      <node concept="3GX2aA" id="2_1mL0eog4q" role="2OqNvi" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="VPM3Z" id="3Z61ZaM65Ls" role="3F10Kt">
                <property role="VOm3f" value="false" />
              </node>
              <node concept="LD5Jc" id="3Z61ZaM65Lt" role="3F10Kt">
                <property role="LDHlv" value="indented" />
              </node>
              <node concept="3EZMnI" id="3Z61ZaM65Lu" role="3EZMnx">
                <property role="3EZMnw" value="true" />
                <node concept="VPM3Z" id="3Z61ZaM65Lv" role="3F10Kt">
                  <property role="VOm3f" value="false" />
                </node>
                <node concept="lj46D" id="3Z61ZaM65Lw" role="3F10Kt">
                  <property role="VOm3f" value="true" />
                </node>
                <node concept="3F0ifn" id="3Z61ZaM65Lx" role="3EZMnx">
                  <property role="3F0ifm" value="deprecated" />
                  <node concept="VechU" id="3Z61ZaM65Ly" role="3F10Kt">
                    <property role="Vb096" value="red" />
                  </node>
                </node>
                <node concept="3F0ifn" id="3Z61ZaM65Lz" role="3EZMnx">
                  <property role="3F0ifm" value="static" />
                  <ref role="1k5W1q" to="tpen:hgVS8CF" resolve="KeyWord" />
                </node>
                <node concept="3F0ifn" id="3Z61ZaM65L$" role="3EZMnx">
                  <property role="3F0ifm" value="{" />
                  <ref role="1k5W1q" to="tpen:hFD5onb" resolve="LeftBrace" />
                  <node concept="ljvvj" id="3Z61ZaM65L_" role="3F10Kt">
                    <property role="VOm3f" value="true" />
                  </node>
                </node>
                <node concept="3F1sOY" id="3Z61ZaM65LA" role="3EZMnx">
                  <ref role="1NtTu8" to="tpee:h3anRLq" />
                  <node concept="10DmGV" id="3Z61ZaM65LB" role="3F10Kt">
                    <property role="10E5iX" value="next-line" />
                  </node>
                  <node concept="lj46D" id="3Z61ZaM65LC" role="3F10Kt">
                    <property role="VOm3f" value="true" />
                  </node>
                  <node concept="ljvvj" id="3Z61ZaM65LD" role="3F10Kt">
                    <property role="VOm3f" value="true" />
                  </node>
                </node>
                <node concept="3F0ifn" id="3Z61ZaM65LE" role="3EZMnx">
                  <property role="3F0ifm" value="}" />
                  <ref role="1k5W1q" to="tpen:hFD5_7H" resolve="RightBrace" />
                  <node concept="LD5Jc" id="3Z61ZaM65LF" role="3F10Kt">
                    <property role="LDHlv" value="next-line" />
                  </node>
                  <node concept="ljvvj" id="3Z61ZaM65LG" role="3F10Kt">
                    <property role="VOm3f" value="true" />
                  </node>
                </node>
                <node concept="3F0ifn" id="3Z61ZaM65LH" role="3EZMnx">
                  <node concept="VPM3Z" id="3Z61ZaM65LI" role="3F10Kt">
                    <property role="VOm3f" value="false" />
                  </node>
                  <node concept="ljvvj" id="3Z61ZaM65LJ" role="3F10Kt">
                    <property role="VOm3f" value="true" />
                  </node>
                </node>
                <node concept="l2Vlx" id="3Z61ZaM65LK" role="2iSdaV" />
              </node>
              <node concept="l2Vlx" id="3Z61ZaM65LL" role="2iSdaV" />
              <node concept="ljvvj" id="3Z61ZaM65LM" role="3F10Kt">
                <property role="VOm3f" value="true" />
              </node>
            </node>
            <node concept="3F1sOY" id="3Z61ZaM65LN" role="3EZMnx">
              <property role="1$x2rV" value="&lt;&lt;static initializer&gt;&gt;" />
              <ref role="1NtTu8" to="tpee:hLPgbgU" />
              <node concept="lj46D" id="3Z61ZaM65LO" role="3F10Kt">
                <property role="VOm3f" value="true" />
              </node>
              <node concept="ljvvj" id="3Z61ZaM65LP" role="3F10Kt">
                <property role="VOm3f" value="true" />
              </node>
              <node concept="pkWqt" id="3Z61ZaM65LQ" role="pqm2j">
                <node concept="3clFbS" id="3Z61ZaM65LR" role="2VODD2">
                  <node concept="3clFbF" id="3Z61ZaM65LS" role="3cqZAp">
                    <node concept="22lmx$" id="3Z61ZaM65LT" role="3clFbG">
                      <node concept="2OqwBi" id="2_1mL0eog4k" role="3uHU7w">
                        <node concept="2OqwBi" id="3Z61ZaM65LV" role="2Oq$k0">
                          <node concept="pncrf" id="3Z61ZaM65LW" role="2Oq$k0" />
                          <node concept="Bykcj" id="2_1mL0eog4h" role="2OqNvi">
                            <node concept="1aIX9F" id="2_1mL0eog4i" role="1xVPHs">
                              <node concept="26LbJo" id="2_1mL0eog4j" role="1aIX9E">
                                <ref role="26LbJp" to="tpee:hLPgbgU" />
                              </node>
                            </node>
                          </node>
                        </node>
                        <node concept="3GX2aA" id="2_1mL0eog4l" role="2OqNvi" />
                      </node>
                      <node concept="22lmx$" id="3Z61ZaM65LZ" role="3uHU7B">
                        <node concept="3fqX7Q" id="3Z61ZaM65M0" role="3uHU7B">
                          <node concept="2OqwBi" id="3Z61ZaM65M1" role="3fr31v">
                            <node concept="pncrf" id="3Z61ZaM65M2" role="2Oq$k0" />
                            <node concept="2qgKlT" id="3Z61ZaM65M3" role="2OqNvi">
                              <ref role="37wK5l" to="tpek:sWroEc0xXl" resolve="isInner" />
                            </node>
                          </node>
                        </node>
                        <node concept="2OqwBi" id="3Z61ZaM65M4" role="3uHU7w">
                          <node concept="pncrf" id="3Z61ZaM65M5" role="2Oq$k0" />
                          <node concept="2qgKlT" id="3Z61ZaM65M6" role="2OqNvi">
                            <ref role="37wK5l" to="tpek:sWroEc0xXc" resolve="isStatic" />
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3F2HdR" id="3Z61ZaM65M7" role="3EZMnx">
              <property role="2czwfN" value="false" />
              <ref role="1NtTu8" to="tpee:fz12cDB" />
              <node concept="3F0ifn" id="3Z61ZaM65M8" role="2czzBI">
                <property role="ilYzB" value="&lt;&lt;fields&gt;&gt;" />
                <node concept="VPxyj" id="3Z61ZaM65M9" role="3F10Kt">
                  <property role="VOm3f" value="true" />
                </node>
              </node>
              <node concept="VPM3Z" id="3Z61ZaM65Ma" role="3F10Kt">
                <property role="VOm3f" value="false" />
              </node>
              <node concept="10DmGV" id="3Z61ZaM65Mb" role="3F10Kt">
                <property role="10E5iX" value="indented" />
              </node>
              <node concept="lj46D" id="3Z61ZaM65Mc" role="3F10Kt">
                <property role="VOm3f" value="true" />
              </node>
              <node concept="ljvvj" id="3Z61ZaM65Md" role="3F10Kt">
                <property role="VOm3f" value="true" />
              </node>
              <node concept="pj6Ft" id="3Z61ZaM65Me" role="3F10Kt">
                <property role="VOm3f" value="true" />
              </node>
              <node concept="l2Vlx" id="3Z61ZaM65Mf" role="2czzBx" />
            </node>
            <node concept="3F2HdR" id="3Z61ZaM65Mg" role="3EZMnx">
              <property role="2czwfN" value="false" />
              <ref role="1NtTu8" to="tpee:huRv3Ah" />
              <node concept="10DmGV" id="3Z61ZaM65Mh" role="3F10Kt">
                <property role="10E5iX" value="indented" />
              </node>
              <node concept="3F0ifn" id="3Z61ZaM65Mi" role="2czzBI">
                <property role="ilYzB" value="&lt;&lt;properties&gt;&gt;" />
                <node concept="VPxyj" id="3Z61ZaM65Mj" role="3F10Kt">
                  <property role="VOm3f" value="true" />
                </node>
              </node>
              <node concept="lj46D" id="3Z61ZaM65Mk" role="3F10Kt">
                <property role="VOm3f" value="true" />
              </node>
              <node concept="ljvvj" id="3Z61ZaM65Ml" role="3F10Kt">
                <property role="VOm3f" value="true" />
              </node>
              <node concept="pj6Ft" id="3Z61ZaM65Mm" role="3F10Kt">
                <property role="VOm3f" value="true" />
              </node>
              <node concept="l2Vlx" id="3Z61ZaM65Mn" role="2czzBx" />
            </node>
            <node concept="3F1sOY" id="3Z61ZaM65Mo" role="3EZMnx">
              <property role="1$x2rV" value="&lt;&lt;initializer&gt;&gt;" />
              <ref role="1NtTu8" to="tpee:hzKIQWS" />
              <node concept="lj46D" id="3Z61ZaM65Mp" role="3F10Kt">
                <property role="VOm3f" value="true" />
              </node>
              <node concept="ljvvj" id="3Z61ZaM65Mq" role="3F10Kt">
                <property role="VOm3f" value="true" />
              </node>
            </node>
            <node concept="3F2HdR" id="3Z61ZaM65Mr" role="3EZMnx">
              <property role="2czwfN" value="false" />
              <ref role="1NtTu8" to="tpee:fz12cDD" />
              <node concept="3F0ifn" id="3Z61ZaM65Ms" role="2czzBI">
                <property role="ilYzB" value="&lt;&lt;constructor&gt;&gt;" />
                <node concept="VPxyj" id="3Z61ZaM65Mt" role="3F10Kt">
                  <property role="VOm3f" value="true" />
                </node>
              </node>
              <node concept="VPM3Z" id="3Z61ZaM65Mu" role="3F10Kt">
                <property role="VOm3f" value="false" />
              </node>
              <node concept="10DmGV" id="3Z61ZaM65Mv" role="3F10Kt">
                <property role="10E5iX" value="indented" />
              </node>
              <node concept="lj46D" id="3Z61ZaM65Mw" role="3F10Kt">
                <property role="VOm3f" value="true" />
              </node>
              <node concept="ljvvj" id="3Z61ZaM65Mx" role="3F10Kt">
                <property role="VOm3f" value="true" />
              </node>
              <node concept="pj6Ft" id="3Z61ZaM65My" role="3F10Kt">
                <property role="VOm3f" value="true" />
              </node>
              <node concept="l2Vlx" id="3Z61ZaM65Mz" role="2czzBx" />
            </node>
            <node concept="3F2HdR" id="3Z61ZaM65M$" role="3EZMnx">
              <property role="2czwfN" value="false" />
              <ref role="1NtTu8" to="tpee:g7MN44b" />
              <node concept="3F0ifn" id="3Z61ZaM65M_" role="2czzBI">
                <property role="ilYzB" value="&lt;&lt;methods&gt;&gt;" />
                <node concept="VPxyj" id="3Z61ZaM65MA" role="3F10Kt">
                  <property role="VOm3f" value="true" />
                </node>
              </node>
              <node concept="VPM3Z" id="3Z61ZaM65MB" role="3F10Kt">
                <property role="VOm3f" value="false" />
              </node>
              <node concept="10DmGV" id="3Z61ZaM65MC" role="3F10Kt">
                <property role="10E5iX" value="indented" />
              </node>
              <node concept="lj46D" id="3Z61ZaM65MD" role="3F10Kt">
                <property role="VOm3f" value="true" />
              </node>
              <node concept="ljvvj" id="3Z61ZaM65ME" role="3F10Kt">
                <property role="VOm3f" value="true" />
              </node>
              <node concept="pj6Ft" id="3Z61ZaM65MF" role="3F10Kt">
                <property role="VOm3f" value="true" />
              </node>
              <node concept="l2Vlx" id="3Z61ZaM65MG" role="2czzBx" />
            </node>
            <node concept="3F2HdR" id="3Z61ZaM65MH" role="3EZMnx">
              <property role="2czwfN" value="false" />
              <ref role="1NtTu8" to="tpee:f$Wxw_K" />
              <node concept="3F0ifn" id="3Z61ZaM65MI" role="2czzBI">
                <property role="ilYzB" value="&lt;&lt;static methods&gt;&gt;" />
                <node concept="VPxyj" id="3Z61ZaM65MJ" role="3F10Kt">
                  <property role="VOm3f" value="true" />
                </node>
              </node>
              <node concept="VPM3Z" id="3Z61ZaM65MK" role="3F10Kt">
                <property role="VOm3f" value="false" />
              </node>
              <node concept="10DmGV" id="3Z61ZaM65ML" role="3F10Kt">
                <property role="10E5iX" value="indented" />
              </node>
              <node concept="lj46D" id="3Z61ZaM65MM" role="3F10Kt">
                <property role="VOm3f" value="true" />
              </node>
              <node concept="ljvvj" id="3Z61ZaM65MN" role="3F10Kt">
                <property role="VOm3f" value="true" />
              </node>
              <node concept="pj6Ft" id="3Z61ZaM65MO" role="3F10Kt">
                <property role="VOm3f" value="true" />
              </node>
              <node concept="l2Vlx" id="3Z61ZaM65MP" role="2czzBx" />
              <node concept="pkWqt" id="3Z61ZaM65MQ" role="pqm2j">
                <node concept="3clFbS" id="3Z61ZaM65MR" role="2VODD2">
                  <node concept="3clFbF" id="3Z61ZaM65MS" role="3cqZAp">
                    <node concept="22lmx$" id="3Z61ZaM65MT" role="3clFbG">
                      <node concept="22lmx$" id="3Z61ZaM65MU" role="3uHU7B">
                        <node concept="3fqX7Q" id="3Z61ZaM65MV" role="3uHU7B">
                          <node concept="2OqwBi" id="3Z61ZaM65MW" role="3fr31v">
                            <node concept="pncrf" id="3Z61ZaM65MX" role="2Oq$k0" />
                            <node concept="2qgKlT" id="3Z61ZaM65MY" role="2OqNvi">
                              <ref role="37wK5l" to="tpek:sWroEc0xXl" resolve="isInner" />
                            </node>
                          </node>
                        </node>
                        <node concept="2OqwBi" id="3Z61ZaM65MZ" role="3uHU7w">
                          <node concept="pncrf" id="3Z61ZaM65N0" role="2Oq$k0" />
                          <node concept="2qgKlT" id="3Z61ZaM65N1" role="2OqNvi">
                            <ref role="37wK5l" to="tpek:sWroEc0xXc" resolve="isStatic" />
                          </node>
                        </node>
                      </node>
                      <node concept="2OqwBi" id="3Z61ZaM65N2" role="3uHU7w">
                        <node concept="2OqwBi" id="3Z61ZaM65N3" role="2Oq$k0">
                          <node concept="2qgKlT" id="2oLu0Jc2aNk" role="2OqNvi">
                            <ref role="37wK5l" to="tpek:4_LVZ3pCeXr" resolve="staticMethods" />
                          </node>
                          <node concept="pncrf" id="3Z61ZaM65N4" role="2Oq$k0" />
                        </node>
                        <node concept="3GX2aA" id="3Z61ZaM65N6" role="2OqNvi" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3F0ifn" id="3Z61ZaM65N7" role="3EZMnx">
              <node concept="VPM3Z" id="3Z61ZaM65N8" role="3F10Kt">
                <property role="VOm3f" value="false" />
              </node>
              <node concept="ljvvj" id="3Z61ZaM65N9" role="3F10Kt">
                <property role="VOm3f" value="true" />
              </node>
            </node>
            <node concept="3F2HdR" id="3Z61ZaM65Na" role="3EZMnx">
              <property role="2czwfN" value="false" />
              <property role="2czwfO" value=" " />
              <ref role="1NtTu8" to="tpee:h9F2oqR" />
              <node concept="3F0ifn" id="3Z61ZaM65Nb" role="2czzBI">
                <property role="ilYzB" value="&lt;&lt;nested classifiers&gt;&gt;" />
                <node concept="VPxyj" id="3Z61ZaM65Nc" role="3F10Kt">
                  <property role="VOm3f" value="true" />
                </node>
              </node>
              <node concept="VPM3Z" id="3Z61ZaM65Nd" role="3F10Kt">
                <property role="VOm3f" value="false" />
              </node>
              <node concept="10DmGV" id="3Z61ZaM65Ne" role="3F10Kt">
                <property role="10E5iX" value="indented" />
              </node>
              <node concept="lj46D" id="3Z61ZaM65Nf" role="3F10Kt">
                <property role="VOm3f" value="true" />
              </node>
              <node concept="ljvvj" id="3Z61ZaM65Ng" role="3F10Kt">
                <property role="VOm3f" value="true" />
              </node>
              <node concept="pj6Ft" id="3Z61ZaM65Nh" role="3F10Kt">
                <property role="VOm3f" value="true" />
              </node>
              <node concept="l2Vlx" id="3Z61ZaM65Ni" role="2czzBx" />
            </node>
            <node concept="VPM3Z" id="3Z61ZaM61I8" role="3F10Kt">
              <property role="VOm3f" value="false" />
            </node>
            <node concept="l2Vlx" id="3Z61ZaM61Ib" role="2iSdaV" />
            <node concept="pkWqt" id="3Z61ZaM6ilI" role="pqm2j">
              <node concept="3clFbS" id="3Z61ZaM6ilJ" role="2VODD2">
                <node concept="3clFbF" id="3Z61ZaMcWwO" role="3cqZAp">
                  <node concept="2OqwBi" id="3Z61ZaMcWwP" role="3clFbG">
                    <node concept="2OqwBi" id="3Z61ZaMcWwQ" role="2Oq$k0">
                      <node concept="66VNe" id="3Z61ZaMcWwR" role="2OqNvi">
                        <node concept="2OqwBi" id="3Z61ZaMcWwS" role="576Qk">
                          <node concept="3Tsc0h" id="3Z61ZaMcWwT" role="2OqNvi">
                            <ref role="3TtcxE" to="tpee:4EqhHTp4Mw3" />
                          </node>
                          <node concept="pncrf" id="3Z61ZaMcWwU" role="2Oq$k0" />
                        </node>
                      </node>
                      <node concept="2OqwBi" id="3Z61ZaMcWwV" role="2Oq$k0">
                        <node concept="2qgKlT" id="3Z61ZaMcWwW" role="2OqNvi">
                          <ref role="37wK5l" to="tpek:1hodSy8nQmC" resolve="members" />
                        </node>
                        <node concept="pncrf" id="3Z61ZaMcWwX" role="2Oq$k0" />
                      </node>
                    </node>
                    <node concept="3GX2aA" id="3Z61ZaMcWwY" role="2OqNvi" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="l2Vlx" id="i0I0pLK" role="2iSdaV" />
      </node>
    </node>
    <node concept="3F1sOY" id="2yiuz3qh0Ul" role="6VMZX">
      <ref role="1NtTu8" to="h7pl:2yiuz3qgZd0" />
    </node>
  </node>
  <node concept="RtYIR" id="2yiuz3qd6jz">
    <property role="Rtri_" value="100" />
    <property role="3NULOk" value="HideInSketch" />
    <ref role="1XX52x" to="tpee:fIYIFWa" resolve="StaticMethodDeclaration" />
    <node concept="3F0ifn" id="2yiuz3qd6j_" role="2wV5jI">
      <property role="3F0ifm" value="" />
      <node concept="pkWqt" id="2yiuz3qd6jC" role="pqm2j">
        <node concept="3clFbS" id="2yiuz3qd6jD" role="2VODD2">
          <node concept="3clFbF" id="2yiuz3qd6kM" role="3cqZAp">
            <node concept="3clFbT" id="2yiuz3qd6kL" role="3clFbG">
              <property role="3clFbU" value="false" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="RtMap" id="2yiuz3qd6n9" role="RtEXV">
      <node concept="3clFbS" id="2yiuz3qd6na" role="2VODD2">
        <node concept="3clFbF" id="2yiuz3qd6oq" role="3cqZAp">
          <node concept="1Wc70l" id="2yiuz3qd7cf" role="3clFbG">
            <node concept="2OqwBi" id="2yiuz3qd8mA" role="3uHU7w">
              <node concept="2OqwBi" id="2yiuz3qd7r0" role="2Oq$k0">
                <node concept="pncrf" id="2yiuz3qd7dq" role="2Oq$k0" />
                <node concept="2Xjw5R" id="2yiuz3qd7Zc" role="2OqNvi">
                  <node concept="1xMEDy" id="2yiuz3qd7Ze" role="1xVPHs">
                    <node concept="chp4Y" id="2yiuz3qd86l" role="ri$Ld">
                      <ref role="cht4Q" to="h7pl:2yiuz3qbfwd" resolve="Sketch" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3x8VRR" id="2yiuz3qd8O$" role="2OqNvi" />
            </node>
            <node concept="2OqwBi" id="2yiuz3qd6_H" role="3uHU7B">
              <node concept="pncrf" id="2yiuz3qd6op" role="2Oq$k0" />
              <node concept="2qgKlT" id="2yiuz3qd74t" role="2OqNvi">
                <ref role="37wK5l" to="tpek:hEwJkuu" resolve="isMainMethod" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="RtYIR" id="2yiuz3qdi9I">
    <property role="Rtri_" value="100" />
    <property role="3NULOk" value="HideInSketch" />
    <ref role="1XX52x" to="tpck:4H9z7u7GMNF" resolve="ExportScope" />
    <node concept="RtMap" id="2yiuz3qdi9O" role="RtEXV">
      <node concept="3clFbS" id="2yiuz3qdi9P" role="2VODD2">
        <node concept="3clFbF" id="2yiuz3qdi9Q" role="3cqZAp">
          <node concept="2OqwBi" id="2yiuz3qdi9S" role="3clFbG">
            <node concept="2OqwBi" id="2yiuz3qdi9T" role="2Oq$k0">
              <node concept="pncrf" id="2yiuz3qdi9U" role="2Oq$k0" />
              <node concept="2Xjw5R" id="2yiuz3qdi9V" role="2OqNvi">
                <node concept="1xMEDy" id="2yiuz3qdi9W" role="1xVPHs">
                  <node concept="chp4Y" id="2yiuz3qdi9X" role="ri$Ld">
                    <ref role="cht4Q" to="h7pl:2yiuz3qbfwd" resolve="Sketch" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3x8VRR" id="2yiuz3qdi9Y" role="2OqNvi" />
          </node>
        </node>
      </node>
    </node>
    <node concept="2SsqMj" id="2yiuz3qdl9C" role="2wV5jI" />
  </node>
  <node concept="RtYIR" id="2yiuz3qg3UB">
    <property role="Rtri_" value="100" />
    <property role="3NULOk" value="HideClass" />
    <ref role="1XX52x" to="tpee:fIYIFW9" resolve="StaticMethodCall" />
    <node concept="RtMap" id="2yiuz3qg3UD" role="RtEXV">
      <node concept="3clFbS" id="2yiuz3qg3UE" role="2VODD2">
        <node concept="3clFbF" id="2yiuz3qg3VN" role="3cqZAp">
          <node concept="1Wc70l" id="1LhviqSwjNC" role="3clFbG">
            <node concept="2OqwBi" id="1LhviqSwk_c" role="3uHU7w">
              <node concept="2OqwBi" id="1LhviqSwjYl" role="2Oq$k0">
                <node concept="pncrf" id="1LhviqSwjS9" role="2Oq$k0" />
                <node concept="3TrEf2" id="1LhviqSwke5" role="2OqNvi">
                  <ref role="3Tt5mk" to="tpee:fIYIWN3" />
                </node>
              </node>
              <node concept="3x8VRR" id="1LhviqSwl8A" role="2OqNvi" />
            </node>
            <node concept="17R0WA" id="2yiuz3qg6U7" role="3uHU7B">
              <node concept="2OqwBi" id="2yiuz3qg40a" role="3uHU7B">
                <node concept="pncrf" id="2yiuz3qg3VM" role="2Oq$k0" />
                <node concept="3TrEf2" id="2yiuz3qg49u" role="2OqNvi">
                  <ref role="3Tt5mk" to="tpee:gDPybl6" />
                </node>
              </node>
              <node concept="3B5_sB" id="2yiuz3qg74E" role="3uHU7w">
                <ref role="3B5MYn" to="r7oa:~PApplet" resolve="PApplet" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="3EZMnI" id="fITxkhc" role="2wV5jI">
      <property role="3EZMnw" value="false" />
      <node concept="PMmxH" id="4k0WLUKaRxk" role="3EZMnx">
        <ref role="PMmxG" to="tpen:4k0WLUKaCd7" resolve="IMethodCall_typeArguments" />
        <node concept="VPM3Z" id="48lPkMUHlsR" role="3F10Kt">
          <property role="VOm3f" value="false" />
        </node>
      </node>
      <node concept="3EZMnI" id="i0EShm6" role="3EZMnx">
        <node concept="VPM3Z" id="i0EShm7" role="3F10Kt">
          <property role="VOm3f" value="false" />
        </node>
        <node concept="2V7CMv" id="4k0WLUKaRxo" role="3F10Kt">
          <property role="2V7CMs" value="ext_2_RTransform" />
        </node>
        <node concept="l2Vlx" id="i0EShm9" role="2iSdaV" />
        <node concept="1iCGBv" id="fPFmMTQ" role="3EZMnx">
          <property role="1$x2rV" value="&lt;no method&gt;" />
          <ref role="1NtTu8" to="tpee:fIYIWN3" />
          <node concept="1sVBvm" id="fPFmMTR" role="1sWHZn">
            <node concept="3F0A7n" id="fPFmMTS" role="2wV5jI">
              <property role="1Intyy" value="true" />
              <ref role="1k5W1q" to="tpen:6H7j4iMM5Cm" resolve="MPSMethodCall" />
              <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
              <node concept="Vb9p2" id="hEUNQY6" role="3F10Kt">
                <property role="Vbekb" value="ITALIC" />
              </node>
            </node>
          </node>
          <node concept="OXEIz" id="gX0y5AU" role="P5bDN">
            <node concept="ZEniJ" id="gX0y7Ez" role="OY2wv">
              <property role="1ezIyd" value="default_referent" />
              <node concept="3Tqbb2" id="gX0y8GN" role="1eyP2E" />
              <node concept="3GJtP1" id="gX0y7E_" role="ZF_Y3">
                <node concept="3clFbS" id="gX0y7EA" role="2VODD2">
                  <node concept="3cpWs6" id="hOuFOpO" role="3cqZAp">
                    <node concept="2YIFZM" id="hOuFQoO" role="3cqZAk">
                      <ref role="37wK5l" to="tpen:hOuD9yG" resolve="replaceNodeMenu_parameterObjects" />
                      <ref role="1Pybhc" to="tpen:gVMbuAU" resolve="QueriesUtil" />
                      <node concept="2OqwBi" id="hOuFRSN" role="37wK5m">
                        <node concept="3GMtW1" id="hOuFRBs" role="2Oq$k0" />
                        <node concept="3TrEf2" id="hOuFS9V" role="2OqNvi">
                          <ref role="3Tt5mk" to="tpee:gDPybl6" />
                        </node>
                      </node>
                      <node concept="3GMtW1" id="hOuFSYW" role="37wK5m" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3GJPmD" id="gX0y7EB" role="ZF_Y2">
                <node concept="3clFbS" id="gX0y7EC" role="2VODD2">
                  <node concept="3cpWs6" id="gX0yeGa" role="3cqZAp">
                    <node concept="2YIFZM" id="gX0yeGb" role="3cqZAk">
                      <ref role="37wK5l" to="tpen:hOuGc0s" resolve="replaceNodeMenu_createNewNode" />
                      <ref role="1Pybhc" to="tpen:gVMbuAU" resolve="QueriesUtil" />
                      <node concept="2OqwBi" id="hOuHae_" role="37wK5m">
                        <node concept="3GMtW1" id="gX0yeGc" role="2Oq$k0" />
                        <node concept="3TrEf2" id="hOuHaGE" role="2OqNvi">
                          <ref role="3Tt5mk" to="tpee:gDPybl6" />
                        </node>
                      </node>
                      <node concept="3GLrbK" id="gX0yeGd" role="37wK5m" />
                      <node concept="3GMtW1" id="hP7LVt3" role="37wK5m" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="2V7CMv" id="2OAzs3a2wkt" role="3F10Kt">
            <property role="2V7CMs" value="ext_2_RTransform" />
          </node>
          <node concept="VechU" id="1LhviqSsEUD" role="3F10Kt">
            <property role="Vb096" value="DARK_GREEN" />
          </node>
        </node>
        <node concept="PMmxH" id="h5nm_Cn" role="3EZMnx">
          <ref role="PMmxG" to="tpen:h5njIub" resolve="IMethodCall_actualArguments" />
          <node concept="11L4FC" id="hX7x$vr" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
        </node>
      </node>
      <node concept="l2Vlx" id="i0DaPca" role="2iSdaV" />
      <node concept="2$oqgb" id="1PnDPd6XSDX" role="3F10Kt">
        <ref role="Bvoe9" to="tpen:47XGxT8xUGh" resolve="BaseMethodParameterInformationQuery" />
      </node>
    </node>
    <node concept="3gTLQM" id="1LhviqSswXC" role="6VMZX">
      <node concept="3Fmcul" id="1LhviqSswXD" role="3FoqZy">
        <node concept="3clFbS" id="1LhviqSswXE" role="2VODD2">
          <node concept="3clFbF" id="1LhviqSsA3s" role="3cqZAp">
            <node concept="2YIFZM" id="1LhviqSsA3t" role="3clFbG">
              <ref role="37wK5l" to="df3e:1LhviqSszSF" resolve="build" />
              <ref role="1Pybhc" to="df3e:1LhviqSsyt7" resolve="DocumentationButton" />
              <node concept="2OqwBi" id="1LhviqSsA3u" role="37wK5m">
                <node concept="pncrf" id="1LhviqSsA3v" role="2Oq$k0" />
                <node concept="2qgKlT" id="1LhviqSsA3w" role="2OqNvi">
                  <ref role="37wK5l" to="tpek:hEwJgm_" resolve="getVariableExpectedName" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="2yiuz3qgPUr">
    <ref role="1XX52x" to="h7pl:2yiuz3qgPTS" resolve="DataFolder" />
    <node concept="3EZMnI" id="2yiuz3qh4eW" role="2wV5jI">
      <node concept="3F0ifn" id="2yiuz3qh4f5" role="3EZMnx">
        <property role="3F0ifm" value="data folder" />
      </node>
      <node concept="2iRfu4" id="2yiuz3qh4eX" role="2iSdaV" />
      <node concept="3F1sOY" id="2yiuz3qgTDl" role="3EZMnx">
        <ref role="1NtTu8" to="h7pl:2yiuz3qgTBd" />
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="2yiuz3qhm4T">
    <ref role="1XX52x" to="h7pl:2yiuz3qh7HE" resolve="FileReference" />
    <node concept="3F0A7n" id="2yiuz3qhm4V" role="2wV5jI">
      <ref role="1NtTu8" to="h7pl:2yiuz3qhfLN" resolve="path" />
      <node concept="VechU" id="2yiuz3qhm6A" role="3F10Kt">
        <property role="Vb096" value="LIGHT_BLUE" />
      </node>
      <node concept="VPxyj" id="2yiuz3qhCud" role="3F10Kt">
        <property role="VOm3f" value="false" />
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="1LhviqSpdRI">
    <ref role="1XX52x" to="h7pl:1LhviqSpdFO" resolve="ProcessingHexIntegerLiteral" />
    <node concept="3EZMnI" id="hanoELk" role="2wV5jI">
      <property role="3EZMnw" value="false" />
      <node concept="3F0ifn" id="hanoF8y" role="3EZMnx">
        <property role="3F0ifm" value="#" />
        <node concept="Vb9p2" id="hEUNR1H" role="3F10Kt">
          <property role="Vbekb" value="PLAIN" />
        </node>
        <node concept="11LMrY" id="5PQ7FHx$bfK" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="VechU" id="6QrovImaOxY" role="3F10Kt">
          <property role="Vb096" value="blue" />
        </node>
      </node>
      <node concept="3F0A7n" id="hanoGgA" role="3EZMnx">
        <property role="1O74Pk" value="true" />
        <ref role="1NtTu8" to="tpee:hanoCGW" resolve="value" />
        <node concept="2UZ17K" id="hEV2aH3" role="3F10Kt">
          <property role="2UZ17L" value="punctuation" />
        </node>
        <node concept="VechU" id="6QrovImbics" role="3F10Kt">
          <property role="Vb096" value="blue" />
        </node>
      </node>
      <node concept="l2Vlx" id="i0v2YyQ" role="2iSdaV" />
    </node>
  </node>
  <node concept="RtYIR" id="1LhviqSpr8n">
    <property role="Rtri_" value="100" />
    <property role="3NULOk" value="Help" />
    <ref role="1XX52x" to="tpee:6LFqxSRBTg4" resolve="LocalMethodCall" />
    <node concept="RtMap" id="1LhviqSprgS" role="RtEXV">
      <node concept="3clFbS" id="1LhviqSprgT" role="2VODD2">
        <node concept="3cpWs8" id="1LhviqSpx0W" role="3cqZAp">
          <node concept="3cpWsn" id="1LhviqSpx0Z" role="3cpWs9">
            <property role="TrG5h" value="cls" />
            <node concept="3Tqbb2" id="1LhviqSpx0U" role="1tU5fm">
              <ref role="ehGHo" to="tpee:fz12cDA" resolve="ClassConcept" />
            </node>
            <node concept="2OqwBi" id="1LhviqSpt2U" role="33vP2m">
              <node concept="2OqwBi" id="1LhviqSpsze" role="2Oq$k0">
                <node concept="pncrf" id="1LhviqSpsuL" role="2Oq$k0" />
                <node concept="3TrEf2" id="1LhviqSpsIa" role="2OqNvi">
                  <ref role="3Tt5mk" to="tpee:6LFqxSRBTg7" />
                </node>
              </node>
              <node concept="2Xjw5R" id="1LhviqSpvyD" role="2OqNvi">
                <node concept="1xMEDy" id="1LhviqSpvyF" role="1xVPHs">
                  <node concept="chp4Y" id="1LhviqSpvA0" role="ri$Ld">
                    <ref role="cht4Q" to="tpee:fz12cDA" resolve="ClassConcept" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="1LhviqSpxoc" role="3cqZAp">
          <node concept="3clFbC" id="1LhviqSpyBc" role="3clFbG">
            <node concept="3B5_sB" id="1LhviqSpyN9" role="3uHU7w">
              <ref role="3B5MYn" to="r7oa:~PApplet" resolve="PApplet" />
            </node>
            <node concept="37vLTw" id="1LhviqSpyp9" role="3uHU7B">
              <ref role="3cqZAo" node="1LhviqSpx0Z" resolve="cls" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="3gTLQM" id="1LhviqSpAP2" role="6VMZX">
      <node concept="3Fmcul" id="1LhviqSpAP4" role="3FoqZy">
        <node concept="3clFbS" id="1LhviqSpAP6" role="2VODD2">
          <node concept="3clFbF" id="1LhviqSpCPh" role="3cqZAp">
            <node concept="2YIFZM" id="1LhviqSs_tQ" role="3clFbG">
              <ref role="37wK5l" to="df3e:1LhviqSszSF" resolve="build" />
              <ref role="1Pybhc" to="df3e:1LhviqSsyt7" resolve="DocumentationButton" />
              <node concept="2OqwBi" id="1LhviqSs_$P" role="37wK5m">
                <node concept="pncrf" id="1LhviqSs_wh" role="2Oq$k0" />
                <node concept="2qgKlT" id="1LhviqSs_KN" role="2OqNvi">
                  <ref role="37wK5l" to="tpek:hEwJgm_" resolve="getVariableExpectedName" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="3EZMnI" id="6LFqxSRCOeE" role="2wV5jI">
      <node concept="2$oqgb" id="6LFqxSRCOeF" role="3F10Kt">
        <ref role="Bvoe9" to="tpen:47XGxT8xUGh" resolve="BaseMethodParameterInformationQuery" />
      </node>
      <node concept="PMmxH" id="6LFqxSRCOeG" role="3EZMnx">
        <ref role="PMmxG" to="tpen:4k0WLUKaCd7" resolve="IMethodCall_typeArguments" />
        <node concept="VPM3Z" id="6LFqxSRCOeH" role="3F10Kt">
          <property role="VOm3f" value="false" />
        </node>
      </node>
      <node concept="1iCGBv" id="6LFqxSRCOeI" role="3EZMnx">
        <ref role="1NtTu8" to="tpee:6LFqxSRBTg7" />
        <node concept="1sVBvm" id="6LFqxSRCOeJ" role="1sWHZn">
          <node concept="3F0A7n" id="6LFqxSRCOeK" role="2wV5jI">
            <property role="1Intyy" value="true" />
            <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
            <ref role="1k5W1q" to="tpen:6H7j4iMM5Cm" resolve="MPSMethodCall" />
          </node>
        </node>
        <node concept="VechU" id="1LhviqSpVvK" role="3F10Kt">
          <property role="Vb096" value="DARK_GREEN" />
        </node>
      </node>
      <node concept="PMmxH" id="6LFqxSRCOeL" role="3EZMnx">
        <ref role="PMmxG" to="tpen:h5njIub" resolve="IMethodCall_actualArguments" />
      </node>
      <node concept="l2Vlx" id="6LFqxSRCOeM" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="1LhviqSq4xW">
    <property role="3GE5qa" value="begin_end" />
    <ref role="1XX52x" to="h7pl:1LhviqSq44v" resolve="PushStyle" />
    <node concept="3EZMnI" id="1LhviqSq4Gb" role="2wV5jI">
      <node concept="3F0ifn" id="1LhviqSq4DA" role="3EZMnx">
        <property role="3F0ifm" value="pushStyle()" />
        <node concept="3mYdg7" id="1LhviqSq4Xk" role="3F10Kt">
          <property role="1413C4" value="pushStyle" />
        </node>
      </node>
      <node concept="3F1sOY" id="1LhviqSq55U" role="3EZMnx">
        <ref role="1NtTu8" to="h7pl:1LhviqSq4zE" />
      </node>
      <node concept="3F0ifn" id="1LhviqSq4KC" role="3EZMnx">
        <property role="3F0ifm" value="popStyle()" />
        <node concept="3mYdg7" id="1LhviqSq54b" role="3F10Kt">
          <property role="1413C4" value="pushStyle" />
        </node>
      </node>
      <node concept="2iRkQZ" id="1LhviqSq4JH" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="1LhviqSqiQ5">
    <property role="3GE5qa" value="begin_end" />
    <ref role="1XX52x" to="h7pl:1LhviqSqiyB" resolve="PushMatrix" />
    <node concept="3EZMnI" id="1LhviqSqiQ6" role="2wV5jI">
      <node concept="3F0ifn" id="1LhviqSqiQ7" role="3EZMnx">
        <property role="3F0ifm" value="pushMatrix()" />
        <node concept="3mYdg7" id="1LhviqSqiQ8" role="3F10Kt">
          <property role="1413C4" value="pushStyle" />
        </node>
      </node>
      <node concept="3F1sOY" id="1LhviqSqiQ9" role="3EZMnx">
        <ref role="1NtTu8" to="h7pl:1LhviqSqjkC" />
      </node>
      <node concept="3F0ifn" id="1LhviqSqiQa" role="3EZMnx">
        <property role="3F0ifm" value="popMatrix()" />
        <node concept="3mYdg7" id="1LhviqSqiQb" role="3F10Kt">
          <property role="1413C4" value="pushStyle" />
        </node>
      </node>
      <node concept="2iRkQZ" id="1LhviqSqiQc" role="2iSdaV" />
    </node>
  </node>
  <node concept="RtYIR" id="1LhviqSrfmB">
    <property role="Rtri_" value="100" />
    <property role="3NULOk" value="TWO_PI" />
    <ref role="1XX52x" to="tpee:fz7vLUo" resolve="VariableReference" />
    <node concept="RtMap" id="1LhviqSrfmC" role="RtEXV">
      <node concept="3clFbS" id="1LhviqSrfmD" role="2VODD2">
        <node concept="3cpWs8" id="1LhviqSrfmE" role="3cqZAp">
          <node concept="3cpWsn" id="1LhviqSrfmF" role="3cpWs9">
            <property role="TrG5h" value="intf" />
            <node concept="3Tqbb2" id="1LhviqSrfmG" role="1tU5fm">
              <ref role="ehGHo" to="tpee:g7HP654" resolve="Interface" />
            </node>
            <node concept="2OqwBi" id="1LhviqSrfmH" role="33vP2m">
              <node concept="2OqwBi" id="1LhviqSrfmI" role="2Oq$k0">
                <node concept="pncrf" id="1LhviqSrfmJ" role="2Oq$k0" />
                <node concept="3TrEf2" id="1LhviqSrfmK" role="2OqNvi">
                  <ref role="3Tt5mk" to="tpee:fzcqZ_w" />
                </node>
              </node>
              <node concept="2Xjw5R" id="1LhviqSrfmL" role="2OqNvi">
                <node concept="1xMEDy" id="1LhviqSrfmM" role="1xVPHs">
                  <node concept="chp4Y" id="1LhviqSrfmN" role="ri$Ld">
                    <ref role="cht4Q" to="tpee:g7HP654" resolve="Interface" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="1LhviqSrfmS" role="3cqZAp">
          <node concept="1Wc70l" id="1LhviqSrfmT" role="3clFbG">
            <node concept="3clFbC" id="1LhviqSrfmU" role="3uHU7B">
              <node concept="37vLTw" id="1LhviqSrfmV" role="3uHU7B">
                <ref role="3cqZAo" node="1LhviqSrfmF" resolve="intf" />
              </node>
              <node concept="3B5_sB" id="1LhviqSrfmW" role="3uHU7w">
                <ref role="3B5MYn" to="r7oa:~PConstants" resolve="PConstants" />
              </node>
            </node>
            <node concept="17R0WA" id="1LhviqSrfmX" role="3uHU7w">
              <node concept="Xl_RD" id="1LhviqSrfmY" role="3uHU7w">
                <property role="Xl_RC" value="TWO_PI" />
              </node>
              <node concept="2OqwBi" id="1LhviqSrVua" role="3uHU7B">
                <node concept="2OqwBi" id="1LhviqSrVub" role="2Oq$k0">
                  <node concept="pncrf" id="1LhviqSrVuc" role="2Oq$k0" />
                  <node concept="3TrEf2" id="1LhviqSrVud" role="2OqNvi">
                    <ref role="3Tt5mk" to="tpee:fzcqZ_w" />
                  </node>
                </node>
                <node concept="3TrcHB" id="1LhviqSrVue" role="2OqNvi">
                  <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="3F0ifn" id="1LhviqSrfn2" role="2wV5jI">
      <property role="3F0ifm" value="2π" />
    </node>
    <node concept="3gTLQM" id="1LhviqSuWKA" role="6VMZX">
      <node concept="3Fmcul" id="1LhviqSuWKB" role="3FoqZy">
        <node concept="3clFbS" id="1LhviqSuWKC" role="2VODD2">
          <node concept="3clFbF" id="1LhviqSuWQw" role="3cqZAp">
            <node concept="2YIFZM" id="1LhviqSuWQx" role="3clFbG">
              <ref role="1Pybhc" to="df3e:1LhviqSsyt7" resolve="DocumentationButton" />
              <ref role="37wK5l" to="df3e:1LhviqSszSF" resolve="build" />
              <node concept="2OqwBi" id="1LhviqSuWQy" role="37wK5m">
                <node concept="pncrf" id="1LhviqSuWQz" role="2Oq$k0" />
                <node concept="2qgKlT" id="1LhviqSuWQ$" role="2OqNvi">
                  <ref role="37wK5l" to="tpek:hEwJgm_" resolve="getVariableExpectedName" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="RtYIR" id="1LhviqSrfCc">
    <property role="Rtri_" value="100" />
    <property role="3NULOk" value="HALF_PI" />
    <ref role="1XX52x" to="tpee:fz7vLUo" resolve="VariableReference" />
    <node concept="RtMap" id="1LhviqSrfCd" role="RtEXV">
      <node concept="3clFbS" id="1LhviqSrfCe" role="2VODD2">
        <node concept="3cpWs8" id="1LhviqSrfCf" role="3cqZAp">
          <node concept="3cpWsn" id="1LhviqSrfCg" role="3cpWs9">
            <property role="TrG5h" value="intf" />
            <node concept="3Tqbb2" id="1LhviqSrfCh" role="1tU5fm">
              <ref role="ehGHo" to="tpee:g7HP654" resolve="Interface" />
            </node>
            <node concept="2OqwBi" id="1LhviqSrfCi" role="33vP2m">
              <node concept="2OqwBi" id="1LhviqSrfCj" role="2Oq$k0">
                <node concept="pncrf" id="1LhviqSrfCk" role="2Oq$k0" />
                <node concept="3TrEf2" id="1LhviqSrfCl" role="2OqNvi">
                  <ref role="3Tt5mk" to="tpee:fzcqZ_w" />
                </node>
              </node>
              <node concept="2Xjw5R" id="1LhviqSrfCm" role="2OqNvi">
                <node concept="1xMEDy" id="1LhviqSrfCn" role="1xVPHs">
                  <node concept="chp4Y" id="1LhviqSrfCo" role="ri$Ld">
                    <ref role="cht4Q" to="tpee:g7HP654" resolve="Interface" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="1LhviqSrfCt" role="3cqZAp">
          <node concept="1Wc70l" id="1LhviqSrfCu" role="3clFbG">
            <node concept="3clFbC" id="1LhviqSrfCv" role="3uHU7B">
              <node concept="37vLTw" id="1LhviqSrfCw" role="3uHU7B">
                <ref role="3cqZAo" node="1LhviqSrfCg" resolve="intf" />
              </node>
              <node concept="3B5_sB" id="1LhviqSrfCx" role="3uHU7w">
                <ref role="3B5MYn" to="r7oa:~PConstants" resolve="PConstants" />
              </node>
            </node>
            <node concept="17R0WA" id="1LhviqSrfCy" role="3uHU7w">
              <node concept="Xl_RD" id="1LhviqSrfCz" role="3uHU7w">
                <property role="Xl_RC" value="HALF_PI" />
              </node>
              <node concept="2OqwBi" id="1LhviqSrV5r" role="3uHU7B">
                <node concept="2OqwBi" id="1LhviqSrfC$" role="2Oq$k0">
                  <node concept="pncrf" id="1LhviqSrfC_" role="2Oq$k0" />
                  <node concept="3TrEf2" id="1LhviqSrURG" role="2OqNvi">
                    <ref role="3Tt5mk" to="tpee:fzcqZ_w" />
                  </node>
                </node>
                <node concept="3TrcHB" id="1LhviqSrVk6" role="2OqNvi">
                  <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="jtDVG" id="1LhviqSrhDA" role="2wV5jI">
      <node concept="3F0ifn" id="1LhviqSrhQ1" role="jiBfM">
        <property role="3F0ifm" value="2" />
        <node concept="VPxyj" id="1LhviqSs2br" role="3F10Kt">
          <property role="VOm3f" value="false" />
        </node>
      </node>
      <node concept="3F0ifn" id="1LhviqSrhP8" role="jiBfT">
        <property role="3F0ifm" value="π" />
      </node>
    </node>
    <node concept="3gTLQM" id="1LhviqSuVS_" role="6VMZX">
      <node concept="3Fmcul" id="1LhviqSuVSA" role="3FoqZy">
        <node concept="3clFbS" id="1LhviqSuVSB" role="2VODD2">
          <node concept="3clFbF" id="1LhviqSuVYv" role="3cqZAp">
            <node concept="2YIFZM" id="1LhviqSuVYw" role="3clFbG">
              <ref role="1Pybhc" to="df3e:1LhviqSsyt7" resolve="DocumentationButton" />
              <ref role="37wK5l" to="df3e:1LhviqSszSF" resolve="build" />
              <node concept="2OqwBi" id="1LhviqSuVYx" role="37wK5m">
                <node concept="pncrf" id="1LhviqSuVYy" role="2Oq$k0" />
                <node concept="2qgKlT" id="1LhviqSuVYz" role="2OqNvi">
                  <ref role="37wK5l" to="tpek:hEwJgm_" resolve="getVariableExpectedName" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="RtYIR" id="1LhviqSqEhe">
    <property role="Rtri_" value="100" />
    <property role="3NULOk" value="PI" />
    <ref role="1XX52x" to="tpee:fz7vLUo" resolve="VariableReference" />
    <node concept="RtMap" id="1LhviqSqEhf" role="RtEXV">
      <node concept="3clFbS" id="1LhviqSqEhg" role="2VODD2">
        <node concept="3cpWs8" id="1LhviqSqEhh" role="3cqZAp">
          <node concept="3cpWsn" id="1LhviqSqEhi" role="3cpWs9">
            <property role="TrG5h" value="intf" />
            <node concept="3Tqbb2" id="1LhviqSqEhj" role="1tU5fm">
              <ref role="ehGHo" to="tpee:g7HP654" resolve="Interface" />
            </node>
            <node concept="2OqwBi" id="1LhviqSqEhk" role="33vP2m">
              <node concept="2OqwBi" id="1LhviqSqEhl" role="2Oq$k0">
                <node concept="pncrf" id="1LhviqSqEhm" role="2Oq$k0" />
                <node concept="3TrEf2" id="1LhviqSqH7z" role="2OqNvi">
                  <ref role="3Tt5mk" to="tpee:fzcqZ_w" />
                </node>
              </node>
              <node concept="2Xjw5R" id="1LhviqSqEho" role="2OqNvi">
                <node concept="1xMEDy" id="1LhviqSqEhp" role="1xVPHs">
                  <node concept="chp4Y" id="1LhviqSra5J" role="ri$Ld">
                    <ref role="cht4Q" to="tpee:g7HP654" resolve="Interface" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="1LhviqSqEhr" role="3cqZAp">
          <node concept="1Wc70l" id="1LhviqSrf6J" role="3clFbG">
            <node concept="3clFbC" id="1LhviqSqEhs" role="3uHU7B">
              <node concept="37vLTw" id="1LhviqSqEhu" role="3uHU7B">
                <ref role="3cqZAo" node="1LhviqSqEhi" resolve="intf" />
              </node>
              <node concept="3B5_sB" id="1LhviqSqEht" role="3uHU7w">
                <ref role="3B5MYn" to="r7oa:~PConstants" resolve="PConstants" />
              </node>
            </node>
            <node concept="17R0WA" id="1LhviqSqLgC" role="3uHU7w">
              <node concept="Xl_RD" id="1LhviqSqLlh" role="3uHU7w">
                <property role="Xl_RC" value="PI" />
              </node>
              <node concept="2OqwBi" id="1LhviqSrU4Q" role="3uHU7B">
                <node concept="2OqwBi" id="1LhviqSqKKo" role="2Oq$k0">
                  <node concept="pncrf" id="1LhviqSqKFZ" role="2Oq$k0" />
                  <node concept="3TrEf2" id="1LhviqSrTR7" role="2OqNvi">
                    <ref role="3Tt5mk" to="tpee:fzcqZ_w" />
                  </node>
                </node>
                <node concept="3TrcHB" id="1LhviqSrUkn" role="2OqNvi">
                  <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="3F0ifn" id="1LhviqSqLF9" role="2wV5jI">
      <property role="3F0ifm" value="π" />
    </node>
    <node concept="3gTLQM" id="1LhviqSuWbf" role="6VMZX">
      <node concept="3Fmcul" id="1LhviqSuWbg" role="3FoqZy">
        <node concept="3clFbS" id="1LhviqSuWbh" role="2VODD2">
          <node concept="3clFbF" id="1LhviqSuWh9" role="3cqZAp">
            <node concept="2YIFZM" id="1LhviqSuWha" role="3clFbG">
              <ref role="1Pybhc" to="df3e:1LhviqSsyt7" resolve="DocumentationButton" />
              <ref role="37wK5l" to="df3e:1LhviqSszSF" resolve="build" />
              <node concept="2OqwBi" id="1LhviqSuWhb" role="37wK5m">
                <node concept="pncrf" id="1LhviqSuWhc" role="2Oq$k0" />
                <node concept="2qgKlT" id="1LhviqSuWhd" role="2OqNvi">
                  <ref role="37wK5l" to="tpek:hEwJgm_" resolve="getVariableExpectedName" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="RtYIR" id="1LhviqSs6Rr">
    <property role="Rtri_" value="100" />
    <property role="3NULOk" value="TAU" />
    <ref role="1XX52x" to="tpee:fz7vLUo" resolve="VariableReference" />
    <node concept="RtMap" id="1LhviqSs6Rs" role="RtEXV">
      <node concept="3clFbS" id="1LhviqSs6Rt" role="2VODD2">
        <node concept="3cpWs8" id="1LhviqSs6Ru" role="3cqZAp">
          <node concept="3cpWsn" id="1LhviqSs6Rv" role="3cpWs9">
            <property role="TrG5h" value="intf" />
            <node concept="3Tqbb2" id="1LhviqSs6Rw" role="1tU5fm">
              <ref role="ehGHo" to="tpee:g7HP654" resolve="Interface" />
            </node>
            <node concept="2OqwBi" id="1LhviqSs6Rx" role="33vP2m">
              <node concept="2OqwBi" id="1LhviqSs6Ry" role="2Oq$k0">
                <node concept="pncrf" id="1LhviqSs6Rz" role="2Oq$k0" />
                <node concept="3TrEf2" id="1LhviqSs6R$" role="2OqNvi">
                  <ref role="3Tt5mk" to="tpee:fzcqZ_w" />
                </node>
              </node>
              <node concept="2Xjw5R" id="1LhviqSs6R_" role="2OqNvi">
                <node concept="1xMEDy" id="1LhviqSs6RA" role="1xVPHs">
                  <node concept="chp4Y" id="1LhviqSs6RB" role="ri$Ld">
                    <ref role="cht4Q" to="tpee:g7HP654" resolve="Interface" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="1LhviqSs6RC" role="3cqZAp">
          <node concept="1Wc70l" id="1LhviqSs6RD" role="3clFbG">
            <node concept="3clFbC" id="1LhviqSs6RE" role="3uHU7B">
              <node concept="37vLTw" id="1LhviqSs6RF" role="3uHU7B">
                <ref role="3cqZAo" node="1LhviqSs6Rv" resolve="intf" />
              </node>
              <node concept="3B5_sB" id="1LhviqSs6RG" role="3uHU7w">
                <ref role="3B5MYn" to="r7oa:~PConstants" resolve="PConstants" />
              </node>
            </node>
            <node concept="17R0WA" id="1LhviqSs6RH" role="3uHU7w">
              <node concept="Xl_RD" id="1LhviqSs6RI" role="3uHU7w">
                <property role="Xl_RC" value="TAU" />
              </node>
              <node concept="2OqwBi" id="1LhviqSs6RJ" role="3uHU7B">
                <node concept="2OqwBi" id="1LhviqSs6RK" role="2Oq$k0">
                  <node concept="pncrf" id="1LhviqSs6RL" role="2Oq$k0" />
                  <node concept="3TrEf2" id="1LhviqSs6RM" role="2OqNvi">
                    <ref role="3Tt5mk" to="tpee:fzcqZ_w" />
                  </node>
                </node>
                <node concept="3TrcHB" id="1LhviqSs6RN" role="2OqNvi">
                  <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="3F0ifn" id="1LhviqSs6RO" role="2wV5jI">
      <property role="3F0ifm" value="Τ" />
    </node>
    <node concept="3gTLQM" id="1LhviqSuWtW" role="6VMZX">
      <node concept="3Fmcul" id="1LhviqSuWtX" role="3FoqZy">
        <node concept="3clFbS" id="1LhviqSuWtY" role="2VODD2">
          <node concept="3clFbF" id="1LhviqSuWzQ" role="3cqZAp">
            <node concept="2YIFZM" id="1LhviqSuWzR" role="3clFbG">
              <ref role="1Pybhc" to="df3e:1LhviqSsyt7" resolve="DocumentationButton" />
              <ref role="37wK5l" to="df3e:1LhviqSszSF" resolve="build" />
              <node concept="2OqwBi" id="1LhviqSuWzS" role="37wK5m">
                <node concept="pncrf" id="1LhviqSuWzT" role="2Oq$k0" />
                <node concept="2qgKlT" id="1LhviqSuWzU" role="2OqNvi">
                  <ref role="37wK5l" to="tpek:hEwJgm_" resolve="getVariableExpectedName" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="RtYIR" id="1LhviqSt19f">
    <property role="Rtri_" value="110" />
    <property role="3NULOk" value="Abs" />
    <ref role="1XX52x" to="tpee:fIYIFW9" resolve="StaticMethodCall" />
    <node concept="RtMap" id="1LhviqSt19g" role="RtEXV">
      <node concept="3clFbS" id="1LhviqSt19h" role="2VODD2">
        <node concept="3clFbF" id="1LhviqSt19i" role="3cqZAp">
          <node concept="1Wc70l" id="1LhviqSt29E" role="3clFbG">
            <node concept="17R0WA" id="1LhviqSt2Ll" role="3uHU7w">
              <node concept="Xl_RD" id="1LhviqSt2Sk" role="3uHU7w">
                <property role="Xl_RC" value="abs" />
              </node>
              <node concept="2OqwBi" id="1LhviqStnid" role="3uHU7B">
                <node concept="2OqwBi" id="1LhviqStnie" role="2Oq$k0">
                  <node concept="pncrf" id="1LhviqStnif" role="2Oq$k0" />
                  <node concept="3TrEf2" id="1LhviqStnig" role="2OqNvi">
                    <ref role="3Tt5mk" to="tpee:fIYIWN3" />
                  </node>
                </node>
                <node concept="3TrcHB" id="1LhviqStnih" role="2OqNvi">
                  <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
                </node>
              </node>
            </node>
            <node concept="17R0WA" id="1LhviqSt19j" role="3uHU7B">
              <node concept="2OqwBi" id="1LhviqSt19l" role="3uHU7B">
                <node concept="pncrf" id="1LhviqSt19m" role="2Oq$k0" />
                <node concept="3TrEf2" id="1LhviqSt19n" role="2OqNvi">
                  <ref role="3Tt5mk" to="tpee:gDPybl6" />
                </node>
              </node>
              <node concept="3B5_sB" id="1LhviqSt19k" role="3uHU7w">
                <ref role="3B5MYn" to="r7oa:~PApplet" resolve="PApplet" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="3gTLQM" id="1LhviqSt19X" role="6VMZX">
      <node concept="3Fmcul" id="1LhviqSt19Y" role="3FoqZy">
        <node concept="3clFbS" id="1LhviqSt19Z" role="2VODD2">
          <node concept="3clFbF" id="1LhviqSt1a0" role="3cqZAp">
            <node concept="2YIFZM" id="1LhviqSt1a1" role="3clFbG">
              <ref role="37wK5l" to="df3e:1LhviqSszSF" resolve="build" />
              <ref role="1Pybhc" to="df3e:1LhviqSsyt7" resolve="DocumentationButton" />
              <node concept="2OqwBi" id="1LhviqSt1a2" role="37wK5m">
                <node concept="pncrf" id="1LhviqSt1a3" role="2Oq$k0" />
                <node concept="2qgKlT" id="1LhviqSt1a4" role="2OqNvi">
                  <ref role="37wK5l" to="tpek:hEwJgm_" resolve="getVariableExpectedName" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="jtFEm" id="1LhviqSt3e_" role="2wV5jI">
      <node concept="PMmxH" id="1LhviqStarD" role="1BQ6eu">
        <ref role="PMmxG" to="tpen:h5njIub" resolve="IMethodCall_actualArguments" />
      </node>
    </node>
  </node>
  <node concept="RtYIR" id="1LhviqSt9DY">
    <property role="Rtri_" value="110" />
    <property role="3NULOk" value="Sqrt" />
    <ref role="1XX52x" to="tpee:fIYIFW9" resolve="StaticMethodCall" />
    <node concept="RtMap" id="1LhviqSt9DZ" role="RtEXV">
      <node concept="3clFbS" id="1LhviqSt9E0" role="2VODD2">
        <node concept="3clFbF" id="1LhviqSt9E1" role="3cqZAp">
          <node concept="1Wc70l" id="1LhviqSt9E2" role="3clFbG">
            <node concept="17R0WA" id="1LhviqSt9E3" role="3uHU7w">
              <node concept="Xl_RD" id="1LhviqSt9E4" role="3uHU7w">
                <property role="Xl_RC" value="sqrt" />
              </node>
              <node concept="2OqwBi" id="1LhviqStmyb" role="3uHU7B">
                <node concept="2OqwBi" id="1LhviqSt9E5" role="2Oq$k0">
                  <node concept="pncrf" id="1LhviqSt9E6" role="2Oq$k0" />
                  <node concept="3TrEf2" id="1LhviqStmaw" role="2OqNvi">
                    <ref role="3Tt5mk" to="tpee:fIYIWN3" />
                  </node>
                </node>
                <node concept="3TrcHB" id="1LhviqStn5E" role="2OqNvi">
                  <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
                </node>
              </node>
            </node>
            <node concept="17R0WA" id="1LhviqSt9E8" role="3uHU7B">
              <node concept="2OqwBi" id="1LhviqSt9E9" role="3uHU7B">
                <node concept="pncrf" id="1LhviqSt9Ea" role="2Oq$k0" />
                <node concept="3TrEf2" id="1LhviqSt9Eb" role="2OqNvi">
                  <ref role="3Tt5mk" to="tpee:gDPybl6" />
                </node>
              </node>
              <node concept="3B5_sB" id="1LhviqSt9Ec" role="3uHU7w">
                <ref role="3B5MYn" to="r7oa:~PApplet" resolve="PApplet" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="3gTLQM" id="1LhviqSt9Ed" role="6VMZX">
      <node concept="3Fmcul" id="1LhviqSt9Ee" role="3FoqZy">
        <node concept="3clFbS" id="1LhviqSt9Ef" role="2VODD2">
          <node concept="3clFbF" id="1LhviqSt9Eg" role="3cqZAp">
            <node concept="2YIFZM" id="1LhviqSt9Eh" role="3clFbG">
              <ref role="1Pybhc" to="df3e:1LhviqSsyt7" resolve="DocumentationButton" />
              <ref role="37wK5l" to="df3e:1LhviqSszSF" resolve="build" />
              <node concept="2OqwBi" id="1LhviqSt9Ei" role="37wK5m">
                <node concept="pncrf" id="1LhviqSt9Ej" role="2Oq$k0" />
                <node concept="2qgKlT" id="1LhviqSt9Ek" role="2OqNvi">
                  <ref role="37wK5l" to="tpek:hEwJgm_" resolve="getVariableExpectedName" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="jtDx7" id="1LhviqStacF" role="2wV5jI">
      <node concept="PMmxH" id="1LhviqStao9" role="jiWj0">
        <ref role="PMmxG" to="tpen:h5njIub" resolve="IMethodCall_actualArguments" />
      </node>
    </node>
  </node>
  <node concept="RtYIR" id="1LhviqStOme">
    <property role="Rtri_" value="100" />
    <property role="3NULOk" value="BuiltinVariables" />
    <ref role="1XX52x" to="tpee:fz7vLUo" resolve="VariableReference" />
    <node concept="RtMap" id="1LhviqStOmf" role="RtEXV">
      <node concept="3clFbS" id="1LhviqStOmg" role="2VODD2">
        <node concept="3cpWs8" id="1LhviqStOmh" role="3cqZAp">
          <node concept="3cpWsn" id="1LhviqStOmi" role="3cpWs9">
            <property role="TrG5h" value="cls" />
            <node concept="3Tqbb2" id="1LhviqStOmj" role="1tU5fm">
              <ref role="ehGHo" to="tpee:fz12cDA" resolve="ClassConcept" />
            </node>
            <node concept="2OqwBi" id="1LhviqStOmk" role="33vP2m">
              <node concept="2OqwBi" id="1LhviqStOml" role="2Oq$k0">
                <node concept="pncrf" id="1LhviqStOmm" role="2Oq$k0" />
                <node concept="3TrEf2" id="1LhviqStOmn" role="2OqNvi">
                  <ref role="3Tt5mk" to="tpee:fzcqZ_w" />
                </node>
              </node>
              <node concept="2Xjw5R" id="1LhviqStOmo" role="2OqNvi">
                <node concept="1xMEDy" id="1LhviqStOmp" role="1xVPHs">
                  <node concept="chp4Y" id="1LhviqStSZ3" role="ri$Ld">
                    <ref role="cht4Q" to="tpee:fz12cDA" resolve="ClassConcept" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="1LhviqStQUY" role="3cqZAp">
          <node concept="3cpWsn" id="1LhviqStQUX" role="3cpWs9">
            <property role="3TUv4t" value="false" />
            <property role="TrG5h" value="p5Builtins" />
            <node concept="10Q1$e" id="1LhviqStQV0" role="1tU5fm">
              <node concept="17QB3L" id="1LhviqStR6n" role="10Q1$1" />
            </node>
            <node concept="2BsdOp" id="1LhviqStQVf" role="33vP2m">
              <node concept="Xl_RD" id="1LhviqStQV1" role="2BsfMF">
                <property role="Xl_RC" value="focused" />
              </node>
              <node concept="Xl_RD" id="1LhviqStQV2" role="2BsfMF">
                <property role="Xl_RC" value="frameCount" />
              </node>
              <node concept="Xl_RD" id="1LhviqStQV3" role="2BsfMF">
                <property role="Xl_RC" value="frameRate" />
              </node>
              <node concept="Xl_RD" id="1LhviqStQV4" role="2BsfMF">
                <property role="Xl_RC" value="height" />
              </node>
              <node concept="Xl_RD" id="1LhviqStQV5" role="2BsfMF">
                <property role="Xl_RC" value="online" />
              </node>
              <node concept="Xl_RD" id="1LhviqStQV6" role="2BsfMF">
                <property role="Xl_RC" value="screen" />
              </node>
              <node concept="Xl_RD" id="1LhviqStQV7" role="2BsfMF">
                <property role="Xl_RC" value="width" />
              </node>
              <node concept="Xl_RD" id="1LhviqStQV8" role="2BsfMF">
                <property role="Xl_RC" value="mouseX" />
              </node>
              <node concept="Xl_RD" id="1LhviqStQV9" role="2BsfMF">
                <property role="Xl_RC" value="mouseY" />
              </node>
              <node concept="Xl_RD" id="1LhviqStQVa" role="2BsfMF">
                <property role="Xl_RC" value="pmouseX" />
              </node>
              <node concept="Xl_RD" id="1LhviqStQVb" role="2BsfMF">
                <property role="Xl_RC" value="pmouseY" />
              </node>
              <node concept="Xl_RD" id="1LhviqStQVc" role="2BsfMF">
                <property role="Xl_RC" value="key" />
              </node>
              <node concept="Xl_RD" id="1LhviqStQVd" role="2BsfMF">
                <property role="Xl_RC" value="keyCode" />
              </node>
              <node concept="Xl_RD" id="1LhviqStQVe" role="2BsfMF">
                <property role="Xl_RC" value="keyPressed" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="1LhviqStQMH" role="3cqZAp" />
        <node concept="3clFbF" id="1LhviqStOmr" role="3cqZAp">
          <node concept="1Wc70l" id="1LhviqStOms" role="3clFbG">
            <node concept="3clFbC" id="1LhviqStOmt" role="3uHU7B">
              <node concept="37vLTw" id="1LhviqStOmu" role="3uHU7B">
                <ref role="3cqZAo" node="1LhviqStOmi" resolve="cls" />
              </node>
              <node concept="3B5_sB" id="1LhviqStOmv" role="3uHU7w">
                <ref role="3B5MYn" to="r7oa:~PApplet" resolve="PApplet" />
              </node>
            </node>
            <node concept="2OqwBi" id="1LhviqStSng" role="3uHU7w">
              <node concept="2OqwBi" id="1LhviqStRU8" role="2Oq$k0">
                <node concept="37vLTw" id="1LhviqStRNP" role="2Oq$k0">
                  <ref role="3cqZAo" node="1LhviqStQUX" resolve="p5Builtins" />
                </node>
                <node concept="39bAoz" id="1LhviqStS90" role="2OqNvi" />
              </node>
              <node concept="3JPx81" id="1LhviqStSyY" role="2OqNvi">
                <node concept="2OqwBi" id="1LhviqStOmy" role="25WWJ7">
                  <node concept="2OqwBi" id="1LhviqStOmz" role="2Oq$k0">
                    <node concept="pncrf" id="1LhviqStOm$" role="2Oq$k0" />
                    <node concept="3TrEf2" id="1LhviqStOm_" role="2OqNvi">
                      <ref role="3Tt5mk" to="tpee:fzcqZ_w" />
                    </node>
                  </node>
                  <node concept="3TrcHB" id="1LhviqStOmA" role="2OqNvi">
                    <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="1iCGBv" id="gwDC3mr" role="2wV5jI">
      <ref role="1NtTu8" to="tpee:fzcqZ_w" />
      <node concept="1sVBvm" id="gwDC3ms" role="1sWHZn">
        <node concept="3F0A7n" id="gwDC3mt" role="2wV5jI">
          <property role="1Intyy" value="true" />
          <property role="1$x2rV" value="&lt;no name&gt;" />
          <ref role="1k5W1q" to="tpen:hFD0yD_" resolve="VariableName" />
          <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
        </node>
      </node>
      <node concept="VechU" id="1LhviqStTvM" role="3F10Kt">
        <node concept="1iSF2X" id="1LhviqStTxx" role="VblUZ">
          <property role="1iTho6" value="CC5D82" />
        </node>
      </node>
    </node>
    <node concept="3gTLQM" id="1LhviqSuV5V" role="6VMZX">
      <node concept="3Fmcul" id="1LhviqSuV5W" role="3FoqZy">
        <node concept="3clFbS" id="1LhviqSuV5X" role="2VODD2">
          <node concept="3clFbF" id="1LhviqSuVgM" role="3cqZAp">
            <node concept="2YIFZM" id="1LhviqSuVod" role="3clFbG">
              <ref role="37wK5l" to="df3e:1LhviqSszSF" resolve="build" />
              <ref role="1Pybhc" to="df3e:1LhviqSsyt7" resolve="DocumentationButton" />
              <node concept="2OqwBi" id="1LhviqSuVtX" role="37wK5m">
                <node concept="pncrf" id="1LhviqSuVpP" role="2Oq$k0" />
                <node concept="2qgKlT" id="1LhviqSuVE$" role="2OqNvi">
                  <ref role="37wK5l" to="tpek:hEwJgm_" resolve="getVariableExpectedName" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="RtYIR" id="1LhviqSvZGG">
    <property role="Rtri_" value="100" />
    <property role="3NULOk" value="HideClass" />
    <ref role="1XX52x" to="tpee:f_0M0x6" resolve="StaticFieldReference" />
    <node concept="RtMap" id="1LhviqSvZGH" role="RtEXV">
      <node concept="3clFbS" id="1LhviqSvZGI" role="2VODD2">
        <node concept="3clFbF" id="1LhviqSvZGJ" role="3cqZAp">
          <node concept="1Wc70l" id="1LhviqSwipX" role="3clFbG">
            <node concept="2OqwBi" id="1LhviqSwj6L" role="3uHU7w">
              <node concept="2OqwBi" id="1LhviqSwi$E" role="2Oq$k0">
                <node concept="pncrf" id="1LhviqSwiuu" role="2Oq$k0" />
                <node concept="3TrEf2" id="1LhviqSwiLS" role="2OqNvi">
                  <ref role="3Tt5mk" to="tpee:f_2Pw7K" />
                </node>
              </node>
              <node concept="3x8VRR" id="1LhviqSwjxT" role="2OqNvi" />
            </node>
            <node concept="17R0WA" id="1LhviqSvZGK" role="3uHU7B">
              <node concept="2OqwBi" id="1LhviqSvZGM" role="3uHU7B">
                <node concept="pncrf" id="1LhviqSvZGN" role="2Oq$k0" />
                <node concept="3TrEf2" id="1LhviqSw0md" role="2OqNvi">
                  <ref role="3Tt5mk" to="tpee:gDPxDYr" />
                </node>
              </node>
              <node concept="3B5_sB" id="1LhviqSvZGL" role="3uHU7w">
                <ref role="3B5MYn" to="r7oa:~PApplet" resolve="PApplet" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="3gTLQM" id="1LhviqSvZHq" role="6VMZX">
      <node concept="3Fmcul" id="1LhviqSvZHr" role="3FoqZy">
        <node concept="3clFbS" id="1LhviqSvZHs" role="2VODD2">
          <node concept="3clFbF" id="1LhviqSvZHt" role="3cqZAp">
            <node concept="2YIFZM" id="1LhviqSvZHu" role="3clFbG">
              <ref role="1Pybhc" to="df3e:1LhviqSsyt7" resolve="DocumentationButton" />
              <ref role="37wK5l" to="df3e:1LhviqSszSF" resolve="build" />
              <node concept="2OqwBi" id="1LhviqSvZHv" role="37wK5m">
                <node concept="pncrf" id="1LhviqSvZHw" role="2Oq$k0" />
                <node concept="2qgKlT" id="1LhviqSvZHx" role="2OqNvi">
                  <ref role="37wK5l" to="tpek:hEwJgm_" resolve="getVariableExpectedName" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="3EZMnI" id="fKolU0o" role="2wV5jI">
      <property role="3EZMnw" value="false" />
      <node concept="1iCGBv" id="fPFlXsb" role="3EZMnx">
        <property role="1$x2rV" value="&lt;no static member&gt;" />
        <ref role="1NtTu8" to="tpee:f_2Pw7K" />
        <ref role="1ERwB7" to="tpen:6LG$uYA2eFu" resolve="StaticFieldReference_DeleteDot" />
        <node concept="1sVBvm" id="fPFlXsc" role="1sWHZn">
          <node concept="3F0A7n" id="fPFlXsd" role="2wV5jI">
            <property role="1Intyy" value="true" />
            <property role="1cu_pB" value="1" />
            <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
            <ref role="1k5W1q" to="tpen:hrRWGGt" resolve="StaticField" />
          </node>
        </node>
        <node concept="OXEIz" id="gX0xOjq" role="P5bDN">
          <node concept="1oHujT" id="4d8Vyfy8$Fc" role="OY2wv">
            <property role="1oHujS" value="class" />
            <node concept="1oIgkG" id="4d8Vyfy8$Fd" role="1oHujR">
              <node concept="3clFbS" id="4d8Vyfy8$Fe" role="2VODD2">
                <node concept="3cpWs8" id="4d8Vyfy8Umc" role="3cqZAp">
                  <node concept="3cpWsn" id="4d8Vyfy8Umd" role="3cpWs9">
                    <property role="TrG5h" value="expr" />
                    <node concept="3Tqbb2" id="4d8Vyfy8Ume" role="1tU5fm">
                      <ref role="ehGHo" to="tpee:gfVsKKk" resolve="ClassifierClassExpression" />
                    </node>
                    <node concept="2ShNRf" id="4d8Vyfy8Umj" role="33vP2m">
                      <node concept="2fJWfE" id="3nElHYn1gtj" role="2ShVmc">
                        <node concept="3Tqbb2" id="3nElHYn1gtk" role="3zrR0E">
                          <ref role="ehGHo" to="tpee:gfVsKKk" resolve="ClassifierClassExpression" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbF" id="4d8Vyfy8Umz" role="3cqZAp">
                  <node concept="37vLTI" id="4d8Vyfy8Um_" role="3clFbG">
                    <node concept="2OqwBi" id="4d8Vyfy8UmD" role="37vLTx">
                      <node concept="3GMtW1" id="4d8Vyfy8UmC" role="2Oq$k0" />
                      <node concept="3TrEf2" id="4d8Vyfy8UmH" role="2OqNvi">
                        <ref role="3Tt5mk" to="tpee:gDPxDYr" />
                      </node>
                    </node>
                    <node concept="2OqwBi" id="4d8Vyfy8UmI" role="37vLTJ">
                      <node concept="37vLTw" id="3GM_nagTAyk" role="2Oq$k0">
                        <ref role="3cqZAo" node="4d8Vyfy8Umd" resolve="expr" />
                      </node>
                      <node concept="3TrEf2" id="4d8Vyfy8UmM" role="2OqNvi">
                        <ref role="3Tt5mk" to="tpee:gfVsUgY" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbF" id="4d8Vyfy8Ump" role="3cqZAp">
                  <node concept="2OqwBi" id="4d8Vyfy8Umr" role="3clFbG">
                    <node concept="3GMtW1" id="4d8Vyfy8Umq" role="2Oq$k0" />
                    <node concept="1P9Npp" id="4d8Vyfy8Umv" role="2OqNvi">
                      <node concept="37vLTw" id="3GM_nagTBfn" role="1P9ThW">
                        <ref role="3cqZAo" node="4d8Vyfy8Umd" resolve="expr" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbF" id="4a7y6gVgeYz" role="3cqZAp">
                  <node concept="2OqwBi" id="4a7y6gVgf2B" role="3clFbG">
                    <node concept="37vLTw" id="4a7y6gVgeYy" role="2Oq$k0">
                      <ref role="3cqZAo" node="4d8Vyfy8Umd" resolve="expr" />
                    </node>
                    <node concept="1OKiuA" id="4a7y6gVgpG9" role="2OqNvi">
                      <node concept="1Q80Hx" id="4a7y6gVgpGU" role="lBI5i" />
                      <node concept="2B6iha" id="4a7y6gVgpIq" role="lGT1i">
                        <property role="1lyBwo" value="last" />
                      </node>
                      <node concept="3cmrfG" id="4a7y6gVgpJP" role="3dN3m$">
                        <property role="3cmrfH" value="-1" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="ZcVJ$" id="gX0xOKI" role="OY2wv" />
          <node concept="ZEniJ" id="gX0xQ8D" role="OY2wv">
            <property role="1ezIyd" value="default_referent" />
            <node concept="3Tqbb2" id="gX0xRbS" role="1eyP2E" />
            <node concept="3GJtP1" id="gX0xQ8F" role="ZF_Y3">
              <node concept="3clFbS" id="gX0xQ8G" role="2VODD2">
                <node concept="3cpWs6" id="hOuDUw8" role="3cqZAp">
                  <node concept="2YIFZM" id="hOuDUw9" role="3cqZAk">
                    <ref role="1Pybhc" to="tpen:gVMbuAU" resolve="QueriesUtil" />
                    <ref role="37wK5l" to="tpen:hOuD9yG" resolve="replaceNodeMenu_parameterObjects" />
                    <node concept="2OqwBi" id="hOuDWBc" role="37wK5m">
                      <node concept="3GMtW1" id="hOuDUwa" role="2Oq$k0" />
                      <node concept="3TrEf2" id="hOuDWMH" role="2OqNvi">
                        <ref role="3Tt5mk" to="tpee:gDPxDYr" />
                      </node>
                    </node>
                    <node concept="3GMtW1" id="hOuDYgJ" role="37wK5m" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3GJPmD" id="gX0xQ8H" role="ZF_Y2">
              <node concept="3clFbS" id="gX0xQ8I" role="2VODD2">
                <node concept="3cpWs6" id="hOuGXH8" role="3cqZAp">
                  <node concept="2YIFZM" id="hOuGZK2" role="3cqZAk">
                    <ref role="1Pybhc" to="tpen:gVMbuAU" resolve="QueriesUtil" />
                    <ref role="37wK5l" to="tpen:hOuGc0s" resolve="replaceNodeMenu_createNewNode" />
                    <node concept="2OqwBi" id="hOuH0o7" role="37wK5m">
                      <node concept="3GMtW1" id="hOuH0mp" role="2Oq$k0" />
                      <node concept="3TrEf2" id="hOuH0ML" role="2OqNvi">
                        <ref role="3Tt5mk" to="tpee:gDPxDYr" />
                      </node>
                    </node>
                    <node concept="3GLrbK" id="hOuH18i" role="37wK5m" />
                    <node concept="3GMtW1" id="hP7LSz2" role="37wK5m" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="2V7CMv" id="hEVbLli" role="3F10Kt">
          <property role="2V7CMs" value="default_RTransform" />
        </node>
      </node>
      <node concept="l2Vlx" id="i0v3bx0" role="2iSdaV" />
    </node>
  </node>
  <node concept="RtYIR" id="1LhviqSwFzS">
    <property role="Rtri_" value="100" />
    <property role="3NULOk" value="VectorType" />
    <ref role="1XX52x" to="tpee:g7uibYu" resolve="ClassifierType" />
    <node concept="3F0ifn" id="1LhviqSwFEZ" role="2wV5jI">
      <property role="3F0ifm" value="vec" />
    </node>
    <node concept="RtMap" id="1LhviqSwF$K" role="RtEXV">
      <node concept="3clFbS" id="1LhviqSwF$L" role="2VODD2">
        <node concept="3clFbF" id="1LhviqSwFJl" role="3cqZAp">
          <node concept="3clFbC" id="1LhviqSwGgV" role="3clFbG">
            <node concept="3B5_sB" id="1LhviqSwGkx" role="3uHU7w">
              <ref role="3B5MYn" to="r7oa:~PVector" resolve="PVector" />
            </node>
            <node concept="2OqwBi" id="1LhviqSwFNG" role="3uHU7B">
              <node concept="pncrf" id="1LhviqSwFJk" role="2Oq$k0" />
              <node concept="3TrEf2" id="1LhviqSwG1e" role="2OqNvi">
                <ref role="3Tt5mk" to="tpee:g7uigIF" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
</model>

