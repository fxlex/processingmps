<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:35924531-de91-475e-be95-e399470f2764(Processing.operator)">
  <persistence version="9" />
  <languages>
    <use id="fc8d557e-5de6-4dd8-b749-aab2fb23aefc" name="jetbrains.mps.baseLanguage.overloadedOperators" version="0" />
    <use id="0ae47ad3-5abd-486c-ac0f-298884f39393" name="jetbrains.mps.baseLanguage.constructors" version="0" />
    <devkit ref="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  </languages>
  <imports>
    <import index="r7oa" ref="96f7ac91-a4c1-4f01-bffe-3b607acce953/java:processing.core(Processing/)" />
    <import index="tpee" ref="r:00000000-0000-4000-0000-011c895902ca(jetbrains.mps.baseLanguage.structure)" />
  </imports>
  <registry>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1202948039474" name="jetbrains.mps.baseLanguage.structure.InstanceMethodCallOperation" flags="nn" index="liA8E" />
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1070534436861" name="jetbrains.mps.baseLanguage.structure.FloatType" flags="in" index="10OMs4" />
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
        <child id="1068499141038" name="actualArgument" index="37wK5m" />
      </concept>
      <concept id="1107535904670" name="jetbrains.mps.baseLanguage.structure.ClassifierType" flags="in" index="3uibUv">
        <reference id="1107535924139" name="classifier" index="3uigEE" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
    <language id="fc8d557e-5de6-4dd8-b749-aab2fb23aefc" name="jetbrains.mps.baseLanguage.overloadedOperators">
      <concept id="2838654975957402167" name="jetbrains.mps.baseLanguage.overloadedOperators.structure.CustomOperator" flags="ng" index="eGIYu">
        <reference id="2838654975957402169" name="declaration" index="eGIYg" />
      </concept>
      <concept id="2838654975957155508" name="jetbrains.mps.baseLanguage.overloadedOperators.structure.BinaryOperationReference" flags="ng" index="eHHct">
        <reference id="2838654975957155509" name="binaryOperation" index="eHHcs" />
      </concept>
      <concept id="483844232470132813" name="jetbrains.mps.baseLanguage.overloadedOperators.structure.OverloadedBinaryOperator" flags="in" index="lHwQ9">
        <property id="2673276898228709090" name="commutative" index="34dOne" />
        <child id="2838654975957155510" name="operator" index="eHHcv" />
        <child id="6677452554237917709" name="returnType" index="1RiMAP" />
        <child id="6677452554239170993" name="leftType" index="1Rm4C9" />
        <child id="6677452554239170994" name="rightType" index="1Rm4Ca" />
      </concept>
      <concept id="483844232470139399" name="jetbrains.mps.baseLanguage.overloadedOperators.structure.OverloadedOperatorContainer" flags="ng" index="lHYv3">
        <child id="2838654975956759196" name="customOperators" index="eEdWP" />
        <child id="483844232470139400" name="operators" index="lHYvc" />
      </concept>
      <concept id="483844232470668960" name="jetbrains.mps.baseLanguage.overloadedOperators.structure.LeftOperand" flags="nn" index="lJXd$" />
      <concept id="7789383629180756961" name="jetbrains.mps.baseLanguage.overloadedOperators.structure.RightOperand" flags="nn" index="1w_D35" />
      <concept id="1569627462442419521" name="jetbrains.mps.baseLanguage.overloadedOperators.structure.CustomOperatorDeclaration" flags="ng" index="1Pn6Et" />
    </language>
  </registry>
  <node concept="lHYv3" id="1LhviqSuoRL">
    <property role="TrG5h" value="PVectorOperators" />
    <node concept="1Pn6Et" id="1LhviqSuwoz" role="eEdWP">
      <property role="TrG5h" value="x" />
    </node>
    <node concept="lHwQ9" id="1LhviqSuoXI" role="lHYvc">
      <property role="34dOne" value="true" />
      <node concept="3uibUv" id="1LhviqSupcK" role="1RiMAP">
        <ref role="3uigEE" to="r7oa:~PVector" resolve="PVector" />
      </node>
      <node concept="3uibUv" id="1LhviqSup6z" role="1Rm4C9">
        <ref role="3uigEE" to="r7oa:~PVector" resolve="PVector" />
      </node>
      <node concept="3uibUv" id="1LhviqSupa4" role="1Rm4Ca">
        <ref role="3uigEE" to="r7oa:~PVector" resolve="PVector" />
      </node>
      <node concept="eHHct" id="1LhviqSup3T" role="eHHcv">
        <ref role="eHHcs" to="tpee:fzcpWvV" resolve="PlusExpression" />
      </node>
      <node concept="3clFbS" id="1LhviqSuoXN" role="2VODD2">
        <node concept="3clFbF" id="1LhviqSupeQ" role="3cqZAp">
          <node concept="2OqwBi" id="1LhviqSupfF" role="3clFbG">
            <node concept="lJXd$" id="1LhviqSupeP" role="2Oq$k0" />
            <node concept="liA8E" id="1LhviqSupjo" role="2OqNvi">
              <ref role="37wK5l" to="r7oa:~PVector.add(processing.core.PVector):processing.core.PVector" resolve="add" />
              <node concept="1w_D35" id="1LhviqSupkV" role="37wK5m" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="lHwQ9" id="1LhviqSusAe" role="lHYvc">
      <node concept="3uibUv" id="1LhviqSusAf" role="1RiMAP">
        <ref role="3uigEE" to="r7oa:~PVector" resolve="PVector" />
      </node>
      <node concept="3uibUv" id="1LhviqSusAg" role="1Rm4C9">
        <ref role="3uigEE" to="r7oa:~PVector" resolve="PVector" />
      </node>
      <node concept="3uibUv" id="1LhviqSusAh" role="1Rm4Ca">
        <ref role="3uigEE" to="r7oa:~PVector" resolve="PVector" />
      </node>
      <node concept="eHHct" id="1LhviqSusE5" role="eHHcv">
        <ref role="eHHcs" to="tpee:fzcpWvP" resolve="MinusExpression" />
      </node>
      <node concept="3clFbS" id="1LhviqSusAj" role="2VODD2">
        <node concept="3clFbF" id="1LhviqSusAk" role="3cqZAp">
          <node concept="2OqwBi" id="1LhviqSusAl" role="3clFbG">
            <node concept="1w_D35" id="1LhviqSvmGu" role="2Oq$k0" />
            <node concept="liA8E" id="1LhviqSusAn" role="2OqNvi">
              <ref role="37wK5l" to="r7oa:~PVector.sub(processing.core.PVector):processing.core.PVector" resolve="sub" />
              <node concept="lJXd$" id="1LhviqSvmI7" role="37wK5m" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="lHwQ9" id="1LhviqSut1W" role="lHYvc">
      <property role="34dOne" value="true" />
      <node concept="3uibUv" id="1LhviqSut1X" role="1RiMAP">
        <ref role="3uigEE" to="r7oa:~PVector" resolve="PVector" />
      </node>
      <node concept="3uibUv" id="1LhviqSut1Y" role="1Rm4C9">
        <ref role="3uigEE" to="r7oa:~PVector" resolve="PVector" />
      </node>
      <node concept="10OMs4" id="1LhviqSutdF" role="1Rm4Ca" />
      <node concept="eHHct" id="1LhviqSut4l" role="eHHcv">
        <ref role="eHHcs" to="tpee:fT7qRmf" resolve="MulExpression" />
      </node>
      <node concept="3clFbS" id="1LhviqSut21" role="2VODD2">
        <node concept="3clFbF" id="1LhviqSut22" role="3cqZAp">
          <node concept="2OqwBi" id="1LhviqSut23" role="3clFbG">
            <node concept="lJXd$" id="1LhviqSut24" role="2Oq$k0" />
            <node concept="liA8E" id="1LhviqSut25" role="2OqNvi">
              <ref role="37wK5l" to="r7oa:~PVector.mult(float):processing.core.PVector" resolve="mult" />
              <node concept="1w_D35" id="1LhviqSut26" role="37wK5m" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="lHwQ9" id="1LhviqSutge" role="lHYvc">
      <node concept="3uibUv" id="1LhviqSutgf" role="1RiMAP">
        <ref role="3uigEE" to="r7oa:~PVector" resolve="PVector" />
      </node>
      <node concept="3uibUv" id="1LhviqSutgg" role="1Rm4C9">
        <ref role="3uigEE" to="r7oa:~PVector" resolve="PVector" />
      </node>
      <node concept="10OMs4" id="1LhviqSutgh" role="1Rm4Ca" />
      <node concept="eHHct" id="1LhviqSutjC" role="eHHcv">
        <ref role="eHHcs" to="tpee:fWFJ1fq" resolve="DivExpression" />
      </node>
      <node concept="3clFbS" id="1LhviqSutgj" role="2VODD2">
        <node concept="3clFbF" id="1LhviqSutgk" role="3cqZAp">
          <node concept="2OqwBi" id="1LhviqSutgl" role="3clFbG">
            <node concept="lJXd$" id="1LhviqSutgm" role="2Oq$k0" />
            <node concept="liA8E" id="1LhviqSutgn" role="2OqNvi">
              <ref role="37wK5l" to="r7oa:~PVector.div(float):processing.core.PVector" resolve="div" />
              <node concept="1w_D35" id="1LhviqSutgo" role="37wK5m" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="lHwQ9" id="1LhviqSuwr7" role="lHYvc">
      <property role="34dOne" value="true" />
      <node concept="3uibUv" id="1LhviqSuwr8" role="1RiMAP">
        <ref role="3uigEE" to="r7oa:~PVector" resolve="PVector" />
      </node>
      <node concept="3uibUv" id="1LhviqSuwr9" role="1Rm4C9">
        <ref role="3uigEE" to="r7oa:~PVector" resolve="PVector" />
      </node>
      <node concept="3uibUv" id="1LhviqSuwx9" role="1Rm4Ca">
        <ref role="3uigEE" to="r7oa:~PVector" resolve="PVector" />
      </node>
      <node concept="eGIYu" id="1LhviqSuwtS" role="eHHcv">
        <ref role="eGIYg" node="1LhviqSuwoz" resolve="x" />
      </node>
      <node concept="3clFbS" id="1LhviqSuwrc" role="2VODD2">
        <node concept="3clFbF" id="1LhviqSuwrd" role="3cqZAp">
          <node concept="2OqwBi" id="1LhviqSuwre" role="3clFbG">
            <node concept="lJXd$" id="1LhviqSuwrf" role="2Oq$k0" />
            <node concept="liA8E" id="1LhviqSuwrg" role="2OqNvi">
              <ref role="37wK5l" to="r7oa:~PVector.cross(processing.core.PVector):processing.core.PVector" resolve="cross" />
              <node concept="1w_D35" id="1LhviqSuwrh" role="37wK5m" />
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
</model>

