<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:72733906-0945-4cb8-993c-3e0b9084bcdd(Processing.actions)">
  <persistence version="9" />
  <languages>
    <use id="aee9cad2-acd4-4608-aef2-0004f6a1cdbd" name="jetbrains.mps.lang.actions" version="0" />
    <devkit ref="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  </languages>
  <imports>
    <import index="guwi" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.io(JDK/)" />
    <import index="tpee" ref="r:00000000-0000-4000-0000-011c895902ca(jetbrains.mps.baseLanguage.structure)" implicit="true" />
    <import index="h7pl" ref="r:3842f4d7-6c76-41af-bee7-8a752efd1444(Processing.structure)" implicit="true" />
    <import index="hwgx" ref="r:fd2980c8-676c-4b19-b524-18c70e02f8b7(com.mbeddr.core.base.behavior)" implicit="true" />
  </imports>
  <registry>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1215693861676" name="jetbrains.mps.baseLanguage.structure.BaseAssignmentExpression" flags="nn" index="d038R">
        <child id="1068498886297" name="rValue" index="37vLTx" />
        <child id="1068498886295" name="lValue" index="37vLTJ" />
      </concept>
      <concept id="1202948039474" name="jetbrains.mps.baseLanguage.structure.InstanceMethodCallOperation" flags="nn" index="liA8E" />
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1145552977093" name="jetbrains.mps.baseLanguage.structure.GenericNewExpression" flags="nn" index="2ShNRf">
        <child id="1145553007750" name="creator" index="2ShVmc" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1068431474542" name="jetbrains.mps.baseLanguage.structure.VariableDeclaration" flags="ng" index="33uBYm">
        <child id="1068431790190" name="initializer" index="33vP2m" />
      </concept>
      <concept id="1068498886296" name="jetbrains.mps.baseLanguage.structure.VariableReference" flags="nn" index="37vLTw">
        <reference id="1068581517664" name="variableDeclaration" index="3cqZAo" />
      </concept>
      <concept id="1068498886294" name="jetbrains.mps.baseLanguage.structure.AssignmentExpression" flags="nn" index="37vLTI" />
      <concept id="1225271177708" name="jetbrains.mps.baseLanguage.structure.StringType" flags="in" index="17QB3L" />
      <concept id="4972933694980447171" name="jetbrains.mps.baseLanguage.structure.BaseVariableDeclaration" flags="ng" index="19Szcq">
        <child id="5680397130376446158" name="type" index="1tU5fm" />
      </concept>
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068581242864" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclarationStatement" flags="nn" index="3cpWs8">
        <child id="1068581242865" name="localVariableDeclaration" index="3cpWs9" />
      </concept>
      <concept id="1068581242863" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclaration" flags="nr" index="3cpWsn" />
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
        <child id="1068499141038" name="actualArgument" index="37wK5m" />
      </concept>
      <concept id="1212685548494" name="jetbrains.mps.baseLanguage.structure.ClassCreator" flags="nn" index="1pGfFk" />
      <concept id="1107535904670" name="jetbrains.mps.baseLanguage.structure.ClassifierType" flags="in" index="3uibUv">
        <reference id="1107535924139" name="classifier" index="3uigEE" />
      </concept>
      <concept id="1081773326031" name="jetbrains.mps.baseLanguage.structure.BinaryOperation" flags="nn" index="3uHJSO">
        <child id="1081773367579" name="rightExpression" index="3uHU7w" />
        <child id="1081773367580" name="leftExpression" index="3uHU7B" />
      </concept>
      <concept id="1080120340718" name="jetbrains.mps.baseLanguage.structure.AndExpression" flags="nn" index="1Wc70l" />
    </language>
    <language id="aee9cad2-acd4-4608-aef2-0004f6a1cdbd" name="jetbrains.mps.lang.actions">
      <concept id="1177323996388" name="jetbrains.mps.lang.actions.structure.AddMenuPart" flags="ng" index="tYCnQ" />
      <concept id="1177333529597" name="jetbrains.mps.lang.actions.structure.ConceptPart" flags="ng" index="uyZFJ">
        <reference id="1177333551023" name="concept" index="uz4UX" />
        <child id="1177333559040" name="part" index="uz6Si" />
      </concept>
      <concept id="1177337641126" name="jetbrains.mps.lang.actions.structure.ParameterizedSubstituteMenuPart" flags="ng" index="uMFAO">
        <child id="1177337679534" name="type" index="uMOYW" />
        <child id="1177338017561" name="query" index="uO7ob" />
        <child id="1177339421668" name="handler" index="uTubQ" />
      </concept>
      <concept id="1177337833147" name="jetbrains.mps.lang.actions.structure.ConceptFunctionParameter_parameterObject" flags="nn" index="uNquD" />
      <concept id="1177337890340" name="jetbrains.mps.lang.actions.structure.QueryFunction_ParameterizedSubstitute_Query" flags="in" index="uNCsQ" />
      <concept id="1177339225103" name="jetbrains.mps.lang.actions.structure.QueryFunction_ParameterizedSubstitute_Handler" flags="in" index="uSIkt" />
      <concept id="1154465102724" name="jetbrains.mps.lang.actions.structure.NodeSubstitutePreconditionFunction" flags="in" index="3buRE8" />
      <concept id="1154465273778" name="jetbrains.mps.lang.actions.structure.ConceptFunctionParameter_parentNode" flags="nn" index="3bvxqY" />
      <concept id="1112056943463" name="jetbrains.mps.lang.actions.structure.NodeSubstituteActions" flags="ng" index="3FK_9_">
        <child id="1112058057696" name="actionsBuilder" index="3FOPby" />
      </concept>
      <concept id="1112058030570" name="jetbrains.mps.lang.actions.structure.NodeSubstituteActionsBuilder" flags="ig" index="3FOIzC">
        <reference id="1112058088712" name="applicableConcept" index="3FOWKa" />
        <child id="1177324142645" name="part" index="tZc4B" />
        <child id="1154465386371" name="precondition" index="3bvWUf" />
      </concept>
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1177026924588" name="jetbrains.mps.lang.smodel.structure.RefConcept_Reference" flags="nn" index="chp4Y">
        <reference id="1177026940964" name="conceptDeclaration" index="cht4Q" />
      </concept>
      <concept id="1138411891628" name="jetbrains.mps.lang.smodel.structure.SNodeOperation" flags="nn" index="eCIE_">
        <child id="1144104376918" name="parameter" index="1xVPHs" />
      </concept>
      <concept id="1179409122411" name="jetbrains.mps.lang.smodel.structure.Node_ConceptMethodCall" flags="nn" index="2qgKlT" />
      <concept id="1171407110247" name="jetbrains.mps.lang.smodel.structure.Node_GetAncestorOperation" flags="nn" index="2Xjw5R" />
      <concept id="1172008320231" name="jetbrains.mps.lang.smodel.structure.Node_IsNotNullOperation" flags="nn" index="3x8VRR" />
      <concept id="1144100932627" name="jetbrains.mps.lang.smodel.structure.OperationParm_Inclusion" flags="ng" index="1xIGOp" />
      <concept id="1144101972840" name="jetbrains.mps.lang.smodel.structure.OperationParm_Concept" flags="ng" index="1xMEDy">
        <child id="1207343664468" name="conceptArgument" index="ri$Ld" />
      </concept>
      <concept id="1180636770613" name="jetbrains.mps.lang.smodel.structure.SNodeCreator" flags="nn" index="3zrR0B">
        <child id="1180636770616" name="createdType" index="3zrR0E" />
      </concept>
      <concept id="1138055754698" name="jetbrains.mps.lang.smodel.structure.SNodeType" flags="in" index="3Tqbb2">
        <reference id="1138405853777" name="concept" index="ehGHo" />
      </concept>
      <concept id="1138056022639" name="jetbrains.mps.lang.smodel.structure.SPropertyAccess" flags="nn" index="3TrcHB">
        <reference id="1138056395725" name="property" index="3TsBF5" />
      </concept>
      <concept id="1138056143562" name="jetbrains.mps.lang.smodel.structure.SLinkAccess" flags="nn" index="3TrEf2">
        <reference id="1138056516764" name="link" index="3Tt5mk" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
    <language id="83888646-71ce-4f1c-9c53-c54016f6ad4f" name="jetbrains.mps.baseLanguage.collections">
      <concept id="1240325842691" name="jetbrains.mps.baseLanguage.collections.structure.AsSequenceOperation" flags="nn" index="39bAoz" />
    </language>
  </registry>
  <node concept="3FK_9_" id="2yiuz3qh7Sp">
    <property role="TrG5h" value="AddFilesInDataFolder" />
    <node concept="3FOIzC" id="2yiuz3qh7Sq" role="3FOPby">
      <ref role="3FOWKa" to="tpee:fz3vP1J" resolve="Expression" />
      <node concept="tYCnQ" id="2yiuz3qh7Uz" role="tZc4B">
        <ref role="uz4UX" to="h7pl:2yiuz3qh7HE" resolve="FileReference" />
        <node concept="uMFAO" id="2yiuz3qh7YC" role="uz6Si">
          <node concept="17QB3L" id="2yiuz3qh812" role="uMOYW" />
          <node concept="uNCsQ" id="2yiuz3qh7YE" role="uO7ob">
            <node concept="3clFbS" id="2yiuz3qh7YF" role="2VODD2">
              <node concept="3cpWs8" id="2yiuz3qh96x" role="3cqZAp">
                <node concept="3cpWsn" id="2yiuz3qh96$" role="3cpWs9">
                  <property role="TrG5h" value="sketch" />
                  <node concept="3Tqbb2" id="2yiuz3qh96w" role="1tU5fm">
                    <ref role="ehGHo" to="h7pl:2yiuz3qbfwd" resolve="Sketch" />
                  </node>
                  <node concept="2OqwBi" id="2yiuz3qh9bQ" role="33vP2m">
                    <node concept="3bvxqY" id="2yiuz3qh9bR" role="2Oq$k0" />
                    <node concept="2Xjw5R" id="2yiuz3qh9bS" role="2OqNvi">
                      <node concept="1xMEDy" id="2yiuz3qh9bT" role="1xVPHs">
                        <node concept="chp4Y" id="2yiuz3qh9bU" role="ri$Ld">
                          <ref role="cht4Q" to="h7pl:2yiuz3qbfwd" resolve="Sketch" />
                        </node>
                      </node>
                      <node concept="1xIGOp" id="2yiuz3qh9bV" role="1xVPHs" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3cpWs8" id="2yiuz3qheTo" role="3cqZAp">
                <node concept="3cpWsn" id="2yiuz3qheTp" role="3cpWs9">
                  <property role="TrG5h" value="path" />
                  <node concept="3uibUv" id="2yiuz3qheTq" role="1tU5fm">
                    <ref role="3uigEE" to="guwi:~File" resolve="File" />
                  </node>
                  <node concept="2ShNRf" id="2yiuz3qhf2S" role="33vP2m">
                    <node concept="1pGfFk" id="2yiuz3qhf8m" role="2ShVmc">
                      <ref role="37wK5l" to="guwi:~File.&lt;init&gt;(java.lang.String)" resolve="File" />
                      <node concept="2OqwBi" id="2yiuz3qhepu" role="37wK5m">
                        <node concept="2OqwBi" id="2yiuz3qhedH" role="2Oq$k0">
                          <node concept="2OqwBi" id="2yiuz3qhdwo" role="2Oq$k0">
                            <node concept="37vLTw" id="2yiuz3qhdlB" role="2Oq$k0">
                              <ref role="3cqZAo" node="2yiuz3qh96$" resolve="sketch" />
                            </node>
                            <node concept="3TrEf2" id="2yiuz3qhdQy" role="2OqNvi">
                              <ref role="3Tt5mk" to="h7pl:2yiuz3qgZd0" />
                            </node>
                          </node>
                          <node concept="3TrEf2" id="2yiuz3qhejD" role="2OqNvi">
                            <ref role="3Tt5mk" to="h7pl:2yiuz3qgTBd" />
                          </node>
                        </node>
                        <node concept="2qgKlT" id="2yiuz3qheFy" role="2OqNvi">
                          <ref role="37wK5l" to="hwgx:5lKnBeAuKov" resolve="getCanonicalPath" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3clFbF" id="2yiuz3qhfgX" role="3cqZAp">
                <node concept="2OqwBi" id="2yiuz3qhfwp" role="3clFbG">
                  <node concept="2OqwBi" id="2yiuz3qhfku" role="2Oq$k0">
                    <node concept="37vLTw" id="2yiuz3qhfgV" role="2Oq$k0">
                      <ref role="3cqZAo" node="2yiuz3qheTp" resolve="path" />
                    </node>
                    <node concept="liA8E" id="2yiuz3qhfrb" role="2OqNvi">
                      <ref role="37wK5l" to="guwi:~File.list():java.lang.String[]" resolve="list" />
                    </node>
                  </node>
                  <node concept="39bAoz" id="2yiuz3qhfF7" role="2OqNvi" />
                </node>
              </node>
            </node>
          </node>
          <node concept="uSIkt" id="2yiuz3qh7YG" role="uTubQ">
            <node concept="3clFbS" id="2yiuz3qh7YH" role="2VODD2">
              <node concept="3cpWs8" id="2yiuz3qhfJ9" role="3cqZAp">
                <node concept="3cpWsn" id="2yiuz3qhfJc" role="3cpWs9">
                  <property role="TrG5h" value="node" />
                  <node concept="3Tqbb2" id="2yiuz3qhfJ8" role="1tU5fm">
                    <ref role="ehGHo" to="h7pl:2yiuz3qh7HE" resolve="FileReference" />
                  </node>
                  <node concept="2ShNRf" id="2yiuz3qhgcS" role="33vP2m">
                    <node concept="3zrR0B" id="2yiuz3qhgql" role="2ShVmc">
                      <node concept="3Tqbb2" id="2yiuz3qhgqn" role="3zrR0E">
                        <ref role="ehGHo" to="h7pl:2yiuz3qh7HE" resolve="FileReference" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3clFbF" id="2yiuz3qhgtw" role="3cqZAp">
                <node concept="37vLTI" id="2yiuz3qhgLF" role="3clFbG">
                  <node concept="uNquD" id="2yiuz3qhgOW" role="37vLTx" />
                  <node concept="2OqwBi" id="2yiuz3qhgw8" role="37vLTJ">
                    <node concept="37vLTw" id="2yiuz3qhgtu" role="2Oq$k0">
                      <ref role="3cqZAo" node="2yiuz3qhfJc" resolve="node" />
                    </node>
                    <node concept="3TrcHB" id="2yiuz3qhg_Z" role="2OqNvi">
                      <ref role="3TsBF5" to="h7pl:2yiuz3qhfLN" resolve="path" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3clFbF" id="2yiuz3qhgSY" role="3cqZAp">
                <node concept="37vLTw" id="2yiuz3qhgSW" role="3clFbG">
                  <ref role="3cqZAo" node="2yiuz3qhfJc" resolve="node" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="3buRE8" id="2yiuz3qh8u1" role="3bvWUf">
        <node concept="3clFbS" id="2yiuz3qh8u2" role="2VODD2">
          <node concept="3cpWs8" id="2yiuz3qhaWu" role="3cqZAp">
            <node concept="3cpWsn" id="2yiuz3qhaWv" role="3cpWs9">
              <property role="TrG5h" value="sketch" />
              <node concept="3Tqbb2" id="2yiuz3qhaWw" role="1tU5fm">
                <ref role="ehGHo" to="h7pl:2yiuz3qbfwd" resolve="Sketch" />
              </node>
              <node concept="2OqwBi" id="2yiuz3qhaWx" role="33vP2m">
                <node concept="3bvxqY" id="2yiuz3qhaWy" role="2Oq$k0" />
                <node concept="2Xjw5R" id="2yiuz3qhaWz" role="2OqNvi">
                  <node concept="1xMEDy" id="2yiuz3qhaW$" role="1xVPHs">
                    <node concept="chp4Y" id="2yiuz3qhaW_" role="ri$Ld">
                      <ref role="cht4Q" to="h7pl:2yiuz3qbfwd" resolve="Sketch" />
                    </node>
                  </node>
                  <node concept="1xIGOp" id="2yiuz3qhaWA" role="1xVPHs" />
                </node>
              </node>
            </node>
          </node>
          <node concept="3clFbF" id="2yiuz3qhb2_" role="3cqZAp">
            <node concept="1Wc70l" id="2yiuz3qhbXn" role="3clFbG">
              <node concept="2OqwBi" id="2yiuz3qhd59" role="3uHU7w">
                <node concept="2OqwBi" id="2yiuz3qhcSA" role="2Oq$k0">
                  <node concept="2OqwBi" id="2yiuz3qhc97" role="2Oq$k0">
                    <node concept="37vLTw" id="2yiuz3qhbYv" role="2Oq$k0">
                      <ref role="3cqZAo" node="2yiuz3qhaWv" resolve="sketch" />
                    </node>
                    <node concept="3TrEf2" id="2yiuz3qhcxo" role="2OqNvi">
                      <ref role="3Tt5mk" to="h7pl:2yiuz3qgZd0" />
                    </node>
                  </node>
                  <node concept="3TrEf2" id="2yiuz3qhcZh" role="2OqNvi">
                    <ref role="3Tt5mk" to="h7pl:2yiuz3qgTBd" />
                  </node>
                </node>
                <node concept="2qgKlT" id="2yiuz3qhdem" role="2OqNvi">
                  <ref role="37wK5l" to="hwgx:5lKnBeAuiv7" resolve="isValidDirectory" />
                </node>
              </node>
              <node concept="2OqwBi" id="2yiuz3qhbda" role="3uHU7B">
                <node concept="37vLTw" id="2yiuz3qhb2z" role="2Oq$k0">
                  <ref role="3cqZAo" node="2yiuz3qhaWv" resolve="sketch" />
                </node>
                <node concept="3x8VRR" id="2yiuz3qhbzh" role="2OqNvi" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
</model>

