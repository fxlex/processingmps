<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:62518ff5-2c9f-4a33-a04a-b09d4bac168e(Processing.behavior)">
  <persistence version="9" />
  <languages>
    <use id="af65afd8-f0dd-4942-87d9-63a55f2a9db1" name="jetbrains.mps.lang.behavior" version="0" />
    <use id="3f4bc5f5-c6c1-4a28-8b10-c83066ffa4a1" name="jetbrains.mps.lang.constraints" version="0" />
    <devkit ref="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  </languages>
  <imports>
    <import index="33ny" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.util(JDK/)" />
    <import index="r7oa" ref="96f7ac91-a4c1-4f01-bffe-3b607acce953/java:processing.core(Processing/)" />
    <import index="df3e" ref="r:730c9fb4-e4ce-43d9-91d3-aba1e15e4166(Processing.runtime)" />
    <import index="eoo2" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.nio.file(JDK/)" />
    <import index="h7pl" ref="r:3842f4d7-6c76-41af-bee7-8a752efd1444(Processing.structure)" implicit="true" />
    <import index="tpck" ref="r:00000000-0000-4000-0000-011c89590288(jetbrains.mps.lang.core.structure)" implicit="true" />
    <import index="tpee" ref="r:00000000-0000-4000-0000-011c895902ca(jetbrains.mps.baseLanguage.structure)" implicit="true" />
    <import index="vs0r" ref="r:f7764ca4-8c75-4049-922b-08516400a727(com.mbeddr.core.base.structure)" implicit="true" />
    <import index="hwgx" ref="r:fd2980c8-676c-4b19-b524-18c70e02f8b7(com.mbeddr.core.base.behavior)" implicit="true" />
  </imports>
  <registry>
    <language id="af65afd8-f0dd-4942-87d9-63a55f2a9db1" name="jetbrains.mps.lang.behavior">
      <concept id="1225194240794" name="jetbrains.mps.lang.behavior.structure.ConceptBehavior" flags="ng" index="13h7C7">
        <reference id="1225194240799" name="concept" index="13h7C2" />
        <child id="1225194240805" name="method" index="13h7CS" />
        <child id="1225194240801" name="constructor" index="13h7CW" />
      </concept>
      <concept id="1225194413805" name="jetbrains.mps.lang.behavior.structure.ConceptConstructorDeclaration" flags="in" index="13hLZK" />
      <concept id="1225194472830" name="jetbrains.mps.lang.behavior.structure.ConceptMethodDeclaration" flags="ng" index="13i0hz" />
      <concept id="1225194691553" name="jetbrains.mps.lang.behavior.structure.ThisNodeExpression" flags="nn" index="13iPFW" />
    </language>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1215693861676" name="jetbrains.mps.baseLanguage.structure.BaseAssignmentExpression" flags="nn" index="d038R">
        <child id="1068498886297" name="rValue" index="37vLTx" />
        <child id="1068498886295" name="lValue" index="37vLTJ" />
      </concept>
      <concept id="1202948039474" name="jetbrains.mps.baseLanguage.structure.InstanceMethodCallOperation" flags="nn" index="liA8E" />
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1145552977093" name="jetbrains.mps.baseLanguage.structure.GenericNewExpression" flags="nn" index="2ShNRf">
        <child id="1145553007750" name="creator" index="2ShVmc" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1070475926800" name="jetbrains.mps.baseLanguage.structure.StringLiteral" flags="nn" index="Xl_RD">
        <property id="1070475926801" name="value" index="Xl_RC" />
      </concept>
      <concept id="1081236700937" name="jetbrains.mps.baseLanguage.structure.StaticMethodCall" flags="nn" index="2YIFZM">
        <reference id="1144433194310" name="classConcept" index="1Pybhc" />
      </concept>
      <concept id="1068431474542" name="jetbrains.mps.baseLanguage.structure.VariableDeclaration" flags="ng" index="33uBYm">
        <child id="1068431790190" name="initializer" index="33vP2m" />
      </concept>
      <concept id="1068498886296" name="jetbrains.mps.baseLanguage.structure.VariableReference" flags="nn" index="37vLTw">
        <reference id="1068581517664" name="variableDeclaration" index="3cqZAo" />
      </concept>
      <concept id="1068498886294" name="jetbrains.mps.baseLanguage.structure.AssignmentExpression" flags="nn" index="37vLTI" />
      <concept id="1225271177708" name="jetbrains.mps.baseLanguage.structure.StringType" flags="in" index="17QB3L" />
      <concept id="4972933694980447171" name="jetbrains.mps.baseLanguage.structure.BaseVariableDeclaration" flags="ng" index="19Szcq">
        <child id="5680397130376446158" name="type" index="1tU5fm" />
      </concept>
      <concept id="1068580123132" name="jetbrains.mps.baseLanguage.structure.BaseMethodDeclaration" flags="ng" index="3clF44">
        <child id="1068580123133" name="returnType" index="3clF45" />
        <child id="1068580123135" name="body" index="3clF47" />
      </concept>
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068580320020" name="jetbrains.mps.baseLanguage.structure.IntegerConstant" flags="nn" index="3cmrfG">
        <property id="1068580320021" name="value" index="3cmrfH" />
      </concept>
      <concept id="1068581242875" name="jetbrains.mps.baseLanguage.structure.PlusExpression" flags="nn" index="3cpWs3" />
      <concept id="1068581242864" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclarationStatement" flags="nn" index="3cpWs8">
        <child id="1068581242865" name="localVariableDeclaration" index="3cpWs9" />
      </concept>
      <concept id="1068581242863" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclaration" flags="nr" index="3cpWsn" />
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
        <child id="1068499141038" name="actualArgument" index="37wK5m" />
      </concept>
      <concept id="1212685548494" name="jetbrains.mps.baseLanguage.structure.ClassCreator" flags="nn" index="1pGfFk" />
      <concept id="1107535904670" name="jetbrains.mps.baseLanguage.structure.ClassifierType" flags="in" index="3uibUv">
        <reference id="1107535924139" name="classifier" index="3uigEE" />
      </concept>
      <concept id="1081773326031" name="jetbrains.mps.baseLanguage.structure.BinaryOperation" flags="nn" index="3uHJSO">
        <child id="1081773367579" name="rightExpression" index="3uHU7w" />
        <child id="1081773367580" name="leftExpression" index="3uHU7B" />
      </concept>
      <concept id="1178549954367" name="jetbrains.mps.baseLanguage.structure.IVisible" flags="ng" index="1B3ioH">
        <child id="1178549979242" name="visibility" index="1B3o_S" />
      </concept>
      <concept id="1146644602865" name="jetbrains.mps.baseLanguage.structure.PublicVisibility" flags="nn" index="3Tm1VV" />
    </language>
    <language id="3a13115c-633c-4c5c-bbcc-75c4219e9555" name="jetbrains.mps.lang.quotation">
      <concept id="1196350785113" name="jetbrains.mps.lang.quotation.structure.Quotation" flags="nn" index="2c44tf">
        <child id="1196350785114" name="quotedNode" index="2c44tc" />
      </concept>
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1177026924588" name="jetbrains.mps.lang.smodel.structure.RefConcept_Reference" flags="nn" index="chp4Y">
        <reference id="1177026940964" name="conceptDeclaration" index="cht4Q" />
      </concept>
      <concept id="1138411891628" name="jetbrains.mps.lang.smodel.structure.SNodeOperation" flags="nn" index="eCIE_">
        <child id="1144104376918" name="parameter" index="1xVPHs" />
      </concept>
      <concept id="1179409122411" name="jetbrains.mps.lang.smodel.structure.Node_ConceptMethodCall" flags="nn" index="2qgKlT" />
      <concept id="1171407110247" name="jetbrains.mps.lang.smodel.structure.Node_GetAncestorOperation" flags="nn" index="2Xjw5R" />
      <concept id="1144101972840" name="jetbrains.mps.lang.smodel.structure.OperationParm_Concept" flags="ng" index="1xMEDy">
        <child id="1207343664468" name="conceptArgument" index="ri$Ld" />
      </concept>
      <concept id="1180636770613" name="jetbrains.mps.lang.smodel.structure.SNodeCreator" flags="nn" index="3zrR0B">
        <child id="1180636770616" name="createdType" index="3zrR0E" />
      </concept>
      <concept id="6407023681583036853" name="jetbrains.mps.lang.smodel.structure.NodeAttributeQualifier" flags="ng" index="3CFYIy">
        <reference id="6407023681583036854" name="attributeConcept" index="3CFYIx" />
      </concept>
      <concept id="6407023681583031218" name="jetbrains.mps.lang.smodel.structure.AttributeAccess" flags="nn" index="3CFZ6_">
        <child id="6407023681583036852" name="qualifier" index="3CFYIz" />
      </concept>
      <concept id="1138055754698" name="jetbrains.mps.lang.smodel.structure.SNodeType" flags="in" index="3Tqbb2">
        <reference id="1138405853777" name="concept" index="ehGHo" />
      </concept>
      <concept id="1138056022639" name="jetbrains.mps.lang.smodel.structure.SPropertyAccess" flags="nn" index="3TrcHB">
        <reference id="1138056395725" name="property" index="3TsBF5" />
      </concept>
      <concept id="1138056143562" name="jetbrains.mps.lang.smodel.structure.SLinkAccess" flags="nn" index="3TrEf2">
        <reference id="1138056516764" name="link" index="3Tt5mk" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="13h7C7" id="2yiuz3qc3BN">
    <ref role="13h7C2" to="h7pl:2yiuz3qbfwd" resolve="Sketch" />
    <node concept="13hLZK" id="2yiuz3qc3BO" role="13h7CW">
      <node concept="3clFbS" id="2yiuz3qc3BP" role="2VODD2">
        <node concept="3clFbF" id="2yiuz3qc3CH" role="3cqZAp">
          <node concept="37vLTI" id="2yiuz3qc6F6" role="3clFbG">
            <node concept="3cpWs3" id="2yiuz3qc6YX" role="37vLTx">
              <node concept="2OqwBi" id="2yiuz3qc8qc" role="3uHU7w">
                <node concept="2ShNRf" id="2yiuz3qc77x" role="2Oq$k0">
                  <node concept="1pGfFk" id="2yiuz3qc8oF" role="2ShVmc">
                    <ref role="37wK5l" to="33ny:~Random.&lt;init&gt;()" resolve="Random" />
                  </node>
                </node>
                <node concept="liA8E" id="2yiuz3qc8y2" role="2OqNvi">
                  <ref role="37wK5l" to="33ny:~Random.nextInt(int):int" resolve="nextInt" />
                  <node concept="3cmrfG" id="2yiuz3qc8Gm" role="37wK5m">
                    <property role="3cmrfH" value="1000000" />
                  </node>
                </node>
              </node>
              <node concept="Xl_RD" id="2yiuz3qc6MP" role="3uHU7B">
                <property role="Xl_RC" value="sketch_" />
              </node>
            </node>
            <node concept="2OqwBi" id="2yiuz3qc3KI" role="37vLTJ">
              <node concept="13iPFW" id="2yiuz3qc3CG" role="2Oq$k0" />
              <node concept="3TrcHB" id="2yiuz3qc6aI" role="2OqNvi">
                <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qc8ZU" role="3cqZAp">
          <node concept="37vLTI" id="2yiuz3qcaIn" role="3clFbG">
            <node concept="2c44tf" id="2yiuz3qcaNz" role="37vLTx">
              <node concept="3uibUv" id="2yiuz3qgP8o" role="2c44tc">
                <ref role="3uigEE" to="r7oa:~PApplet" resolve="PApplet" />
              </node>
            </node>
            <node concept="2OqwBi" id="2yiuz3qc9a7" role="37vLTJ">
              <node concept="13iPFW" id="2yiuz3qc8ZS" role="2Oq$k0" />
              <node concept="3TrEf2" id="2yiuz3qcahw" role="2OqNvi">
                <ref role="3Tt5mk" to="tpee:gXzkM_H" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="2yiuz3qdg5Y" role="3cqZAp">
          <node concept="3cpWsn" id="2yiuz3qdg61" role="3cpWs9">
            <property role="TrG5h" value="export" />
            <node concept="3Tqbb2" id="2yiuz3qdg5W" role="1tU5fm">
              <ref role="ehGHo" to="tpck:2erkSmBSEUR" resolve="ExportScopeNamespace" />
            </node>
            <node concept="2ShNRf" id="2yiuz3qdelX" role="33vP2m">
              <node concept="3zrR0B" id="2yiuz3qdei9" role="2ShVmc">
                <node concept="3Tqbb2" id="2yiuz3qdeia" role="3zrR0E">
                  <ref role="ehGHo" to="tpck:2erkSmBSEUR" resolve="ExportScopeNamespace" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qdggD" role="3cqZAp">
          <node concept="37vLTI" id="2yiuz3qdgGw" role="3clFbG">
            <node concept="2OqwBi" id="2yiuz3qdgSE" role="37vLTx">
              <node concept="13iPFW" id="2yiuz3qdgIJ" role="2Oq$k0" />
              <node concept="3TrcHB" id="2yiuz3qdhgo" role="2OqNvi">
                <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
              </node>
            </node>
            <node concept="2OqwBi" id="2yiuz3qdglM" role="37vLTJ">
              <node concept="37vLTw" id="2yiuz3qdggB" role="2Oq$k0">
                <ref role="3cqZAo" node="2yiuz3qdg61" resolve="export" />
              </node>
              <node concept="3TrcHB" id="2yiuz3qdgvp" role="2OqNvi">
                <ref role="3TsBF5" to="tpck:2erkSmBSEUT" resolve="namespace" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qdd$C" role="3cqZAp">
          <node concept="37vLTI" id="2yiuz3qdegl" role="3clFbG">
            <node concept="2OqwBi" id="2yiuz3qddIr" role="37vLTJ">
              <node concept="13iPFW" id="2yiuz3qdd$A" role="2Oq$k0" />
              <node concept="3CFZ6_" id="2yiuz3qde6x" role="2OqNvi">
                <node concept="3CFYIy" id="2yiuz3qde9F" role="3CFYIz">
                  <ref role="3CFYIx" to="tpck:4H9z7u7GMNF" resolve="ExportScope" />
                </node>
              </node>
            </node>
            <node concept="37vLTw" id="2yiuz3qdgbg" role="37vLTx">
              <ref role="3cqZAo" node="2yiuz3qdg61" resolve="export" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qgZg9" role="3cqZAp">
          <node concept="37vLTI" id="2yiuz3qh09D" role="3clFbG">
            <node concept="2ShNRf" id="2yiuz3qh0eW" role="37vLTx">
              <node concept="3zrR0B" id="2yiuz3qh0br" role="2ShVmc">
                <node concept="3Tqbb2" id="2yiuz3qh0bs" role="3zrR0E">
                  <ref role="ehGHo" to="h7pl:2yiuz3qgPTS" resolve="DataFolder" />
                </node>
              </node>
            </node>
            <node concept="2OqwBi" id="2yiuz3qgZr0" role="37vLTJ">
              <node concept="13iPFW" id="2yiuz3qgZg7" role="2Oq$k0" />
              <node concept="3TrEf2" id="2yiuz3qgZLe" role="2OqNvi">
                <ref role="3Tt5mk" to="h7pl:2yiuz3qgZd0" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="13h7C7" id="2yiuz3qgQKc">
    <ref role="13h7C2" to="h7pl:2yiuz3qgPTS" resolve="DataFolder" />
    <node concept="13hLZK" id="2yiuz3qgQKd" role="13h7CW">
      <node concept="3clFbS" id="2yiuz3qgQKe" role="2VODD2">
        <node concept="3clFbF" id="2yiuz3qgTEr" role="3cqZAp">
          <node concept="37vLTI" id="2yiuz3qgTQr" role="3clFbG">
            <node concept="2ShNRf" id="2yiuz3qgTUY" role="37vLTx">
              <node concept="3zrR0B" id="2yiuz3qgTUW" role="2ShVmc">
                <node concept="3Tqbb2" id="2yiuz3qgTUX" role="3zrR0E">
                  <ref role="ehGHo" to="vs0r:4eXJ6EOwIAn" resolve="FileSystemDirPicker" />
                </node>
              </node>
            </node>
            <node concept="2OqwBi" id="2yiuz3qgTGg" role="37vLTJ">
              <node concept="13iPFW" id="2yiuz3qgTEq" role="2Oq$k0" />
              <node concept="3TrEf2" id="2yiuz3qgTIW" role="2OqNvi">
                <ref role="3Tt5mk" to="h7pl:2yiuz3qgTBd" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="13h7C7" id="2yiuz3qhp5U">
    <ref role="13h7C2" to="h7pl:2yiuz3qh7HE" resolve="FileReference" />
    <node concept="13i0hz" id="2yiuz3qhp5X" role="13h7CS">
      <property role="TrG5h" value="fullPath" />
      <node concept="3Tm1VV" id="2yiuz3qhp5Y" role="1B3o_S" />
      <node concept="17QB3L" id="2yiuz3qhp65" role="3clF45" />
      <node concept="3clFbS" id="2yiuz3qhp60" role="3clF47">
        <node concept="3cpWs8" id="2yiuz3qhq4b" role="3cqZAp">
          <node concept="3cpWsn" id="2yiuz3qhq4e" role="3cpWs9">
            <property role="TrG5h" value="sketch" />
            <node concept="3Tqbb2" id="2yiuz3qhq4a" role="1tU5fm">
              <ref role="ehGHo" to="h7pl:2yiuz3qbfwd" resolve="Sketch" />
            </node>
            <node concept="2OqwBi" id="2yiuz3qhpq_" role="33vP2m">
              <node concept="13iPFW" id="2yiuz3qhqba" role="2Oq$k0" />
              <node concept="2Xjw5R" id="2yiuz3qhptn" role="2OqNvi">
                <node concept="1xMEDy" id="2yiuz3qhptp" role="1xVPHs">
                  <node concept="chp4Y" id="2yiuz3qhpv1" role="ri$Ld">
                    <ref role="cht4Q" to="h7pl:2yiuz3qbfwd" resolve="Sketch" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qhr3L" role="3cqZAp">
          <node concept="2OqwBi" id="2yiuz3qhsS$" role="3clFbG">
            <node concept="2YIFZM" id="2yiuz3qhr4j" role="2Oq$k0">
              <ref role="37wK5l" to="eoo2:~Paths.get(java.lang.String,java.lang.String...):java.nio.file.Path" resolve="get" />
              <ref role="1Pybhc" to="eoo2:~Paths" resolve="Paths" />
              <node concept="2OqwBi" id="2yiuz3qhsg3" role="37wK5m">
                <node concept="2OqwBi" id="2yiuz3qhs3T" role="2Oq$k0">
                  <node concept="2OqwBi" id="2yiuz3qhrml" role="2Oq$k0">
                    <node concept="37vLTw" id="2yiuz3qhr5g" role="2Oq$k0">
                      <ref role="3cqZAo" node="2yiuz3qhq4e" resolve="sketch" />
                    </node>
                    <node concept="3TrEf2" id="2yiuz3qhrGD" role="2OqNvi">
                      <ref role="3Tt5mk" to="h7pl:2yiuz3qgZd0" />
                    </node>
                  </node>
                  <node concept="3TrEf2" id="2yiuz3qhsa9" role="2OqNvi">
                    <ref role="3Tt5mk" to="h7pl:2yiuz3qgTBd" />
                  </node>
                </node>
                <node concept="2qgKlT" id="2yiuz3qhsoP" role="2OqNvi">
                  <ref role="37wK5l" to="hwgx:5lKnBeAuKov" resolve="getCanonicalPath" />
                </node>
              </node>
              <node concept="2OqwBi" id="2yiuz3qhsAr" role="37wK5m">
                <node concept="13iPFW" id="2yiuz3qhsxU" role="2Oq$k0" />
                <node concept="3TrcHB" id="2yiuz3qhsJI" role="2OqNvi">
                  <ref role="3TsBF5" to="h7pl:2yiuz3qhfLN" resolve="path" />
                </node>
              </node>
            </node>
            <node concept="liA8E" id="2yiuz3qht6q" role="2OqNvi">
              <ref role="37wK5l" to="eoo2:~Path.toString():java.lang.String" resolve="toString" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="13hLZK" id="2yiuz3qhp5V" role="13h7CW">
      <node concept="3clFbS" id="2yiuz3qhp5W" role="2VODD2" />
    </node>
  </node>
</model>

