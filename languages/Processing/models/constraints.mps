<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:436b0861-e354-4ea2-a6a8-c23b35242afc(Processing.constraints)">
  <persistence version="9" />
  <languages>
    <use id="3f4bc5f5-c6c1-4a28-8b10-c83066ffa4a1" name="jetbrains.mps.lang.constraints" version="0" />
    <use id="7a5dda62-9140-4668-ab76-d5ed1746f2b2" name="jetbrains.mps.lang.typesystem" version="0" />
    <use id="309e0004-4976-4416-b947-ec02ae4ecef2" name="com.mbeddr.mpsutil.modellisteners" version="0" />
    <devkit ref="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  </languages>
  <imports>
    <import index="eoo2" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.nio.file(JDK/)" />
    <import index="fnmy" ref="r:89c0fb70-0977-4113-a076-5906f9d8630f(jetbrains.mps.baseLanguage.scopes)" />
    <import index="tpee" ref="r:00000000-0000-4000-0000-011c895902ca(jetbrains.mps.baseLanguage.structure)" />
    <import index="o8zo" ref="r:314576fc-3aee-4386-a0a5-a38348ac317d(jetbrains.mps.scope)" />
    <import index="df3e" ref="r:730c9fb4-e4ce-43d9-91d3-aba1e15e4166(Processing.runtime)" />
    <import index="h7pl" ref="r:3842f4d7-6c76-41af-bee7-8a752efd1444(Processing.structure)" implicit="true" />
    <import index="mhbf" ref="8865b7a8-5271-43d3-884c-6fd1d9cfdd34/java:org.jetbrains.mps.openapi.model(MPS.OpenAPI/)" implicit="true" />
    <import index="wyt6" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.lang(JDK/)" implicit="true" />
  </imports>
  <registry>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1202948039474" name="jetbrains.mps.baseLanguage.structure.InstanceMethodCallOperation" flags="nn" index="liA8E" />
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1070475926800" name="jetbrains.mps.baseLanguage.structure.StringLiteral" flags="nn" index="Xl_RD">
        <property id="1070475926801" name="value" index="Xl_RC" />
      </concept>
      <concept id="1081236700937" name="jetbrains.mps.baseLanguage.structure.StaticMethodCall" flags="nn" index="2YIFZM">
        <reference id="1144433194310" name="classConcept" index="1Pybhc" />
      </concept>
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068581242875" name="jetbrains.mps.baseLanguage.structure.PlusExpression" flags="nn" index="3cpWs3" />
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
        <child id="1068499141038" name="actualArgument" index="37wK5m" />
      </concept>
      <concept id="1081773326031" name="jetbrains.mps.baseLanguage.structure.BinaryOperation" flags="nn" index="3uHJSO">
        <child id="1081773367579" name="rightExpression" index="3uHU7w" />
        <child id="1081773367580" name="leftExpression" index="3uHU7B" />
      </concept>
    </language>
    <language id="3f4bc5f5-c6c1-4a28-8b10-c83066ffa4a1" name="jetbrains.mps.lang.constraints">
      <concept id="1202989531578" name="jetbrains.mps.lang.constraints.structure.ConstraintFunction_CanBeAChild" flags="in" index="nKS2y" />
      <concept id="1202989658459" name="jetbrains.mps.lang.constraints.structure.ConstraintFunctionParameter_parentNode" flags="nn" index="nLn13" />
      <concept id="1213093968558" name="jetbrains.mps.lang.constraints.structure.ConceptConstraints" flags="ng" index="1M2fIO">
        <reference id="1213093996982" name="concept" index="1M2myG" />
        <child id="1213106463729" name="canBeChild" index="1MLUbF" />
      </concept>
    </language>
    <language id="760a0a8c-eabb-4521-8bfd-65db761a9ba3" name="jetbrains.mps.baseLanguage.logging">
      <concept id="1167227138527" name="jetbrains.mps.baseLanguage.logging.structure.LogStatement" flags="nn" index="34ab3g">
        <property id="1167245565795" name="severity" index="35gtTG" />
        <child id="1167227463056" name="logExpression" index="34bqiv" />
      </concept>
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1177026924588" name="jetbrains.mps.lang.smodel.structure.RefConcept_Reference" flags="nn" index="chp4Y">
        <reference id="1177026940964" name="conceptDeclaration" index="cht4Q" />
      </concept>
      <concept id="1138411891628" name="jetbrains.mps.lang.smodel.structure.SNodeOperation" flags="nn" index="eCIE_">
        <child id="1144104376918" name="parameter" index="1xVPHs" />
      </concept>
      <concept id="1145404486709" name="jetbrains.mps.lang.smodel.structure.SemanticDowncastExpression" flags="nn" index="2JrnkZ">
        <child id="1145404616321" name="leftExpression" index="2JrQYb" />
      </concept>
      <concept id="1171407110247" name="jetbrains.mps.lang.smodel.structure.Node_GetAncestorOperation" flags="nn" index="2Xjw5R" />
      <concept id="1172008320231" name="jetbrains.mps.lang.smodel.structure.Node_IsNotNullOperation" flags="nn" index="3x8VRR" />
      <concept id="1144101972840" name="jetbrains.mps.lang.smodel.structure.OperationParm_Concept" flags="ng" index="1xMEDy">
        <child id="1207343664468" name="conceptArgument" index="ri$Ld" />
      </concept>
    </language>
    <language id="309e0004-4976-4416-b947-ec02ae4ecef2" name="com.mbeddr.mpsutil.modellisteners">
      <concept id="5818559022137760597" name="com.mbeddr.mpsutil.modellisteners.structure.Parameter_instance" flags="ng" index="j_vvf" />
      <concept id="5818559022137597839" name="com.mbeddr.mpsutil.modellisteners.structure.ConceptModelListeners" flags="ng" index="jA7cl">
        <reference id="1213093996982" name="concept" index="1M2myH" />
        <child id="5818559022137986141" name="listeners" index="j$A37" />
      </concept>
      <concept id="6105788070833315443" name="com.mbeddr.mpsutil.modellisteners.structure.PropertyListener" flags="ig" index="3vq$el">
        <reference id="6105788070833315720" name="property" index="3vq$9I" />
      </concept>
      <concept id="6105788070833320481" name="com.mbeddr.mpsutil.modellisteners.structure.Parameter_newPropertyValue" flags="ng" index="3vqAZ7" />
    </language>
  </registry>
  <node concept="1M2fIO" id="2yiuz3qhp68">
    <ref role="1M2myG" to="h7pl:2yiuz3qh7HE" resolve="FileReference" />
    <node concept="nKS2y" id="2yiuz3qhpnI" role="1MLUbF">
      <node concept="3clFbS" id="2yiuz3qhpnJ" role="2VODD2">
        <node concept="3clFbF" id="2yiuz3qhpoO" role="3cqZAp">
          <node concept="2OqwBi" id="2yiuz3qhpDN" role="3clFbG">
            <node concept="2OqwBi" id="2yiuz3qhpq_" role="2Oq$k0">
              <node concept="nLn13" id="2yiuz3qhpoN" role="2Oq$k0" />
              <node concept="2Xjw5R" id="2yiuz3qhptn" role="2OqNvi">
                <node concept="1xMEDy" id="2yiuz3qhptp" role="1xVPHs">
                  <node concept="chp4Y" id="2yiuz3qhpv1" role="ri$Ld">
                    <ref role="cht4Q" to="h7pl:2yiuz3qbfwd" resolve="Sketch" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3x8VRR" id="2yiuz3qhq0y" role="2OqNvi" />
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="jA7cl" id="1LhviqSj2gL">
    <ref role="1M2myH" to="tpee:fzcmrck" resolve="IntegerConstant" />
    <node concept="3vq$el" id="1LhviqSj2qr" role="j$A37">
      <ref role="3vq$9I" to="tpee:fzcmrcl" resolve="value" />
      <node concept="3clFbS" id="1LhviqSj2qs" role="2VODD2">
        <node concept="34ab3g" id="1LhviqSk7iM" role="3cqZAp">
          <property role="35gtTG" value="warn" />
          <node concept="3cpWs3" id="1LhviqSoyNv" role="34bqiv">
            <node concept="3vqAZ7" id="1LhviqSoyQc" role="3uHU7w" />
            <node concept="Xl_RD" id="1LhviqSk7iO" role="3uHU7B">
              <property role="Xl_RC" value="Integer changed:" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="1LhviqSj5ST" role="3cqZAp">
          <node concept="2OqwBi" id="1LhviqSj5WP" role="3clFbG">
            <node concept="2YIFZM" id="1LhviqSj5Wk" role="2Oq$k0">
              <ref role="37wK5l" to="df3e:1LhviqSiZsM" resolve="getInstance" />
              <ref role="1Pybhc" to="df3e:1LhviqSiNG4" resolve="IntegerManager" />
            </node>
            <node concept="liA8E" id="1LhviqSj5Z0" role="2OqNvi">
              <ref role="37wK5l" to="df3e:1LhviqSmQR1" resolve="setValue" />
              <node concept="2OqwBi" id="1LhviqSncI8" role="37wK5m">
                <node concept="2OqwBi" id="1LhviqSncCJ" role="2Oq$k0">
                  <node concept="2JrnkZ" id="1LhviqSncB5" role="2Oq$k0">
                    <node concept="j_vvf" id="1LhviqSj64t" role="2JrQYb" />
                  </node>
                  <node concept="liA8E" id="1LhviqSncGN" role="2OqNvi">
                    <ref role="37wK5l" to="mhbf:~SNode.getNodeId():org.jetbrains.mps.openapi.model.SNodeId" resolve="getNodeId" />
                  </node>
                </node>
                <node concept="liA8E" id="1LhviqSncLl" role="2OqNvi">
                  <ref role="37wK5l" to="wyt6:~Object.toString():java.lang.String" resolve="toString" />
                </node>
              </node>
              <node concept="2YIFZM" id="1LhviqSj8DL" role="37wK5m">
                <ref role="37wK5l" to="wyt6:~Integer.parseInt(java.lang.String):int" resolve="parseInt" />
                <ref role="1Pybhc" to="wyt6:~Integer" resolve="Integer" />
                <node concept="3vqAZ7" id="1LhviqSj8Ff" role="37wK5m" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
</model>

