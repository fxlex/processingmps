<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:ad337301-81f8-4339-a646-076e6387f0d7(Processing.typesystem)">
  <persistence version="9" />
  <languages>
    <use id="7a5dda62-9140-4668-ab76-d5ed1746f2b2" name="jetbrains.mps.lang.typesystem" version="0" />
    <devkit ref="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  </languages>
  <imports>
    <import index="r7oa" ref="96f7ac91-a4c1-4f01-bffe-3b607acce953/java:processing.core(Processing/)" />
    <import index="tpee" ref="r:00000000-0000-4000-0000-011c895902ca(jetbrains.mps.baseLanguage.structure)" />
    <import index="guwi" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.io(JDK/)" />
    <import index="tpek" ref="r:00000000-0000-4000-0000-011c895902c0(jetbrains.mps.baseLanguage.behavior)" implicit="true" />
    <import index="h7pl" ref="r:3842f4d7-6c76-41af-bee7-8a752efd1444(Processing.structure)" implicit="true" />
    <import index="wyt6" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.lang(JDK/)" implicit="true" />
  </imports>
  <registry>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1080223426719" name="jetbrains.mps.baseLanguage.structure.OrExpression" flags="nn" index="22lmx$" />
      <concept id="1215693861676" name="jetbrains.mps.baseLanguage.structure.BaseAssignmentExpression" flags="nn" index="d038R">
        <child id="1068498886297" name="rValue" index="37vLTx" />
        <child id="1068498886295" name="lValue" index="37vLTJ" />
      </concept>
      <concept id="4836112446988635817" name="jetbrains.mps.baseLanguage.structure.UndefinedType" flags="in" index="2jxLKc" />
      <concept id="1202948039474" name="jetbrains.mps.baseLanguage.structure.InstanceMethodCallOperation" flags="nn" index="liA8E" />
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1070475926800" name="jetbrains.mps.baseLanguage.structure.StringLiteral" flags="nn" index="Xl_RD">
        <property id="1070475926801" name="value" index="Xl_RC" />
      </concept>
      <concept id="1081236700938" name="jetbrains.mps.baseLanguage.structure.StaticMethodDeclaration" flags="ig" index="2YIFZL" />
      <concept id="1081236700937" name="jetbrains.mps.baseLanguage.structure.StaticMethodCall" flags="nn" index="2YIFZM">
        <reference id="1144433194310" name="classConcept" index="1Pybhc" />
      </concept>
      <concept id="1070534760951" name="jetbrains.mps.baseLanguage.structure.ArrayType" flags="in" index="10Q1$e">
        <child id="1070534760952" name="componentType" index="10Q1$1" />
      </concept>
      <concept id="1068431474542" name="jetbrains.mps.baseLanguage.structure.VariableDeclaration" flags="ng" index="33uBYm">
        <child id="1068431790190" name="initializer" index="33vP2m" />
      </concept>
      <concept id="1068431790191" name="jetbrains.mps.baseLanguage.structure.Expression" flags="nn" index="33vP2n" />
      <concept id="1068498886296" name="jetbrains.mps.baseLanguage.structure.VariableReference" flags="nn" index="37vLTw">
        <reference id="1068581517664" name="variableDeclaration" index="3cqZAo" />
      </concept>
      <concept id="1068498886292" name="jetbrains.mps.baseLanguage.structure.ParameterDeclaration" flags="ir" index="37vLTG" />
      <concept id="1068498886294" name="jetbrains.mps.baseLanguage.structure.AssignmentExpression" flags="nn" index="37vLTI" />
      <concept id="1225271177708" name="jetbrains.mps.baseLanguage.structure.StringType" flags="in" index="17QB3L" />
      <concept id="4972933694980447171" name="jetbrains.mps.baseLanguage.structure.BaseVariableDeclaration" flags="ng" index="19Szcq">
        <child id="5680397130376446158" name="type" index="1tU5fm" />
      </concept>
      <concept id="1068580123132" name="jetbrains.mps.baseLanguage.structure.BaseMethodDeclaration" flags="ng" index="3clF44">
        <child id="1068580123133" name="returnType" index="3clF45" />
        <child id="1068580123134" name="parameter" index="3clF46" />
        <child id="1068580123135" name="body" index="3clF47" />
      </concept>
      <concept id="1068580123165" name="jetbrains.mps.baseLanguage.structure.InstanceMethodDeclaration" flags="ig" index="3clFb_" />
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123157" name="jetbrains.mps.baseLanguage.structure.Statement" flags="nn" index="3clFbH" />
      <concept id="1068580123159" name="jetbrains.mps.baseLanguage.structure.IfStatement" flags="nn" index="3clFbJ">
        <child id="1068580123160" name="condition" index="3clFbw" />
        <child id="1068580123161" name="ifTrue" index="3clFbx" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068580123137" name="jetbrains.mps.baseLanguage.structure.BooleanConstant" flags="nn" index="3clFbT">
        <property id="1068580123138" name="value" index="3clFbU" />
      </concept>
      <concept id="1068580320020" name="jetbrains.mps.baseLanguage.structure.IntegerConstant" flags="nn" index="3cmrfG">
        <property id="1068580320021" name="value" index="3cmrfH" />
      </concept>
      <concept id="1068581242878" name="jetbrains.mps.baseLanguage.structure.ReturnStatement" flags="nn" index="3cpWs6">
        <child id="1068581517676" name="expression" index="3cqZAk" />
      </concept>
      <concept id="1068581242864" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclarationStatement" flags="nn" index="3cpWs8">
        <child id="1068581242865" name="localVariableDeclaration" index="3cpWs9" />
      </concept>
      <concept id="1068581242863" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclaration" flags="nr" index="3cpWsn" />
      <concept id="1068581517677" name="jetbrains.mps.baseLanguage.structure.VoidType" flags="in" index="3cqZAl" />
      <concept id="1081516740877" name="jetbrains.mps.baseLanguage.structure.NotExpression" flags="nn" index="3fqX7Q">
        <child id="1081516765348" name="expression" index="3fr31v" />
      </concept>
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
        <child id="1068499141038" name="actualArgument" index="37wK5m" />
      </concept>
      <concept id="1081773326031" name="jetbrains.mps.baseLanguage.structure.BinaryOperation" flags="nn" index="3uHJSO">
        <child id="1081773367579" name="rightExpression" index="3uHU7w" />
        <child id="1081773367580" name="leftExpression" index="3uHU7B" />
      </concept>
      <concept id="1178549954367" name="jetbrains.mps.baseLanguage.structure.IVisible" flags="ng" index="1B3ioH">
        <child id="1178549979242" name="visibility" index="1B3o_S" />
      </concept>
      <concept id="5497648299878491908" name="jetbrains.mps.baseLanguage.structure.BaseVariableReference" flags="nn" index="1M0zk4">
        <reference id="5497648299878491909" name="baseVariableDeclaration" index="1M0zk5" />
      </concept>
      <concept id="1146644602865" name="jetbrains.mps.baseLanguage.structure.PublicVisibility" flags="nn" index="3Tm1VV" />
      <concept id="1116615150612" name="jetbrains.mps.baseLanguage.structure.ClassifierClassExpression" flags="nn" index="3VsKOn">
        <reference id="1116615189566" name="classifier" index="3VsUkX" />
      </concept>
    </language>
    <language id="fd392034-7849-419d-9071-12563d152375" name="jetbrains.mps.baseLanguage.closures">
      <concept id="1199569711397" name="jetbrains.mps.baseLanguage.closures.structure.ClosureLiteral" flags="nn" index="1bVj0M">
        <child id="1199569906740" name="parameter" index="1bW2Oz" />
        <child id="1199569916463" name="body" index="1bW5cS" />
      </concept>
    </language>
    <language id="3a13115c-633c-4c5c-bbcc-75c4219e9555" name="jetbrains.mps.lang.quotation">
      <concept id="1196350785110" name="jetbrains.mps.lang.quotation.structure.AbstractAntiquotation" flags="ng" index="2c44t0">
        <child id="1196350785111" name="expression" index="2c44t1" />
      </concept>
      <concept id="1196350785117" name="jetbrains.mps.lang.quotation.structure.ReferenceAntiquotation" flags="ng" index="2c44tb" />
      <concept id="1196350785112" name="jetbrains.mps.lang.quotation.structure.Antiquotation" flags="ng" index="2c44te" />
      <concept id="1196350785113" name="jetbrains.mps.lang.quotation.structure.Quotation" flags="nn" index="2c44tf">
        <child id="1196350785114" name="quotedNode" index="2c44tc" />
      </concept>
    </language>
    <language id="7a5dda62-9140-4668-ab76-d5ed1746f2b2" name="jetbrains.mps.lang.typesystem">
      <concept id="1185788614172" name="jetbrains.mps.lang.typesystem.structure.NormalTypeClause" flags="ng" index="mw_s8">
        <child id="1185788644032" name="normalType" index="mwGJk" />
      </concept>
      <concept id="1175517767210" name="jetbrains.mps.lang.typesystem.structure.ReportErrorStatement" flags="nn" index="2MkqsV">
        <child id="1175517851849" name="errorString" index="2MkJ7o" />
      </concept>
      <concept id="1227096774658" name="jetbrains.mps.lang.typesystem.structure.MessageStatement" flags="ng" index="2OEH$v">
        <child id="1227096802791" name="helginsIntention" index="2OEOjU" />
        <child id="1227096802790" name="nodeToReport" index="2OEOjV" />
      </concept>
      <concept id="1216383170661" name="jetbrains.mps.lang.typesystem.structure.TypesystemQuickFix" flags="ng" index="Q5z_Y">
        <child id="1216383424566" name="executeBlock" index="Q6x$H" />
        <child id="1216383476350" name="quickFixArgument" index="Q6Id_" />
        <child id="1216391046856" name="descriptionBlock" index="QzAvj" />
      </concept>
      <concept id="1216383287005" name="jetbrains.mps.lang.typesystem.structure.QuickFixExecuteBlock" flags="in" index="Q5ZZ6" />
      <concept id="1216383482742" name="jetbrains.mps.lang.typesystem.structure.QuickFixArgument" flags="ng" index="Q6JDH">
        <child id="1216383511839" name="argumentType" index="Q6QK4" />
      </concept>
      <concept id="1216390348809" name="jetbrains.mps.lang.typesystem.structure.QuickFixArgumentReference" flags="nn" index="QwW4i">
        <reference id="1216390348810" name="quickFixArgument" index="QwW4h" />
      </concept>
      <concept id="1216390987552" name="jetbrains.mps.lang.typesystem.structure.QuickFixDescriptionBlock" flags="in" index="QznSV" />
      <concept id="1195213580585" name="jetbrains.mps.lang.typesystem.structure.AbstractCheckingRule" flags="ig" index="18hYwZ">
        <child id="1195213635060" name="body" index="18ibNy" />
      </concept>
      <concept id="1195214364922" name="jetbrains.mps.lang.typesystem.structure.NonTypesystemRule" flags="ig" index="18kY7G" />
      <concept id="1210784285454" name="jetbrains.mps.lang.typesystem.structure.TypesystemIntention" flags="ng" index="3Cnw8n">
        <property id="1216127910019" name="applyImmediately" index="ARO6o" />
        <reference id="1216388525179" name="quickFix" index="QpYPw" />
        <child id="1210784493590" name="actualArgument" index="3Coj4f" />
      </concept>
      <concept id="1210784384552" name="jetbrains.mps.lang.typesystem.structure.TypesystemIntentionArgument" flags="ng" index="3CnSsL">
        <reference id="1216386999476" name="quickFixArgument" index="QkamJ" />
        <child id="1210784642750" name="value" index="3CoRuB" />
      </concept>
      <concept id="1174642788531" name="jetbrains.mps.lang.typesystem.structure.ConceptReference" flags="ig" index="1YaCAy">
        <reference id="1174642800329" name="concept" index="1YaFvo" />
      </concept>
      <concept id="1174643105530" name="jetbrains.mps.lang.typesystem.structure.InferenceRule" flags="ig" index="1YbPZF" />
      <concept id="1174648085619" name="jetbrains.mps.lang.typesystem.structure.AbstractRule" flags="ng" index="1YuPPy">
        <child id="1174648101952" name="applicableNode" index="1YuTPh" />
      </concept>
      <concept id="1174650418652" name="jetbrains.mps.lang.typesystem.structure.ApplicableNodeReference" flags="nn" index="1YBJjd">
        <reference id="1174650432090" name="applicableNode" index="1YBMHb" />
      </concept>
      <concept id="1174657487114" name="jetbrains.mps.lang.typesystem.structure.TypeOfExpression" flags="nn" index="1Z2H0r">
        <child id="1174657509053" name="term" index="1Z2MuG" />
      </concept>
      <concept id="1174658326157" name="jetbrains.mps.lang.typesystem.structure.CreateEquationStatement" flags="nn" index="1Z5TYs" />
      <concept id="1174660718586" name="jetbrains.mps.lang.typesystem.structure.AbstractEquationStatement" flags="nn" index="1Zf1VF">
        <child id="1174660783413" name="leftExpression" index="1ZfhK$" />
        <child id="1174660783414" name="rightExpression" index="1ZfhKB" />
      </concept>
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1177026924588" name="jetbrains.mps.lang.smodel.structure.RefConcept_Reference" flags="nn" index="chp4Y">
        <reference id="1177026940964" name="conceptDeclaration" index="cht4Q" />
      </concept>
      <concept id="1138411891628" name="jetbrains.mps.lang.smodel.structure.SNodeOperation" flags="nn" index="eCIE_">
        <child id="1144104376918" name="parameter" index="1xVPHs" />
      </concept>
      <concept id="1179409122411" name="jetbrains.mps.lang.smodel.structure.Node_ConceptMethodCall" flags="nn" index="2qgKlT" />
      <concept id="1883223317721008708" name="jetbrains.mps.lang.smodel.structure.IfInstanceOfStatement" flags="nn" index="Jncv_">
        <reference id="1883223317721008712" name="nodeConcept" index="JncvD" />
        <child id="1883223317721008709" name="body" index="Jncv$" />
        <child id="1883223317721008711" name="variable" index="JncvA" />
        <child id="1883223317721008710" name="nodeExpression" index="JncvB" />
      </concept>
      <concept id="1883223317721008713" name="jetbrains.mps.lang.smodel.structure.IfInstanceOfVariable" flags="ng" index="JncvC" />
      <concept id="1883223317721107059" name="jetbrains.mps.lang.smodel.structure.IfInstanceOfVarReference" flags="nn" index="Jnkvi" />
      <concept id="1171407110247" name="jetbrains.mps.lang.smodel.structure.Node_GetAncestorOperation" flags="nn" index="2Xjw5R" />
      <concept id="1139613262185" name="jetbrains.mps.lang.smodel.structure.Node_GetParentOperation" flags="nn" index="1mfA1w" />
      <concept id="1171999116870" name="jetbrains.mps.lang.smodel.structure.Node_IsNullOperation" flags="nn" index="3w_OXm" />
      <concept id="1144101972840" name="jetbrains.mps.lang.smodel.structure.OperationParm_Concept" flags="ng" index="1xMEDy">
        <child id="1207343664468" name="conceptArgument" index="ri$Ld" />
      </concept>
      <concept id="1144146199828" name="jetbrains.mps.lang.smodel.structure.Node_CopyOperation" flags="nn" index="1$rogu" />
      <concept id="1140133623887" name="jetbrains.mps.lang.smodel.structure.Node_DeleteOperation" flags="nn" index="1PgB_6" />
      <concept id="1140137987495" name="jetbrains.mps.lang.smodel.structure.SNodeTypeCastExpression" flags="nn" index="1PxgMI">
        <reference id="1140138128738" name="concept" index="1PxNhF" />
        <child id="1140138123956" name="leftExpression" index="1PxMeX" />
      </concept>
      <concept id="1138055754698" name="jetbrains.mps.lang.smodel.structure.SNodeType" flags="in" index="3Tqbb2">
        <reference id="1138405853777" name="concept" index="ehGHo" />
      </concept>
      <concept id="1138056143562" name="jetbrains.mps.lang.smodel.structure.SLinkAccess" flags="nn" index="3TrEf2">
        <reference id="1138056516764" name="link" index="3Tt5mk" />
      </concept>
      <concept id="1138056282393" name="jetbrains.mps.lang.smodel.structure.SLinkListAccess" flags="nn" index="3Tsc0h">
        <reference id="1138056546658" name="link" index="3TtcxE" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <child id="5169995583184591170" name="smodelAttribute" index="lGtFl" />
      </concept>
      <concept id="3364660638048049745" name="jetbrains.mps.lang.core.structure.LinkAttribute" flags="ng" index="A9Btn">
        <property id="1757699476691236116" name="linkRole" index="2qtEX8" />
        <property id="1341860900488019036" name="linkId" index="P3scX" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
    <language id="83888646-71ce-4f1c-9c53-c54016f6ad4f" name="jetbrains.mps.baseLanguage.collections">
      <concept id="1204796164442" name="jetbrains.mps.baseLanguage.collections.structure.InternalSequenceOperation" flags="nn" index="23sCx2">
        <child id="1204796294226" name="closure" index="23t8la" />
      </concept>
      <concept id="540871147943773365" name="jetbrains.mps.baseLanguage.collections.structure.SingleArgumentSequenceOperation" flags="nn" index="25WWJ4">
        <child id="540871147943773366" name="argument" index="25WWJ7" />
      </concept>
      <concept id="1203518072036" name="jetbrains.mps.baseLanguage.collections.structure.SmartClosureParameterDeclaration" flags="ig" index="Rh6nW" />
      <concept id="1160612413312" name="jetbrains.mps.baseLanguage.collections.structure.AddElementOperation" flags="nn" index="TSZUe" />
      <concept id="1225621920911" name="jetbrains.mps.baseLanguage.collections.structure.InsertElementOperation" flags="nn" index="1sK_Qi">
        <child id="1225621943565" name="element" index="1sKFgg" />
        <child id="1225621960341" name="index" index="1sKJu8" />
      </concept>
      <concept id="1225727723840" name="jetbrains.mps.baseLanguage.collections.structure.FindFirstOperation" flags="nn" index="1z4cxt" />
    </language>
  </registry>
  <node concept="18kY7G" id="2yiuz3qcQVS">
    <property role="TrG5h" value="MainMethodMissing" />
    <node concept="3clFbS" id="2yiuz3qcQVT" role="18ibNy">
      <node concept="3clFbJ" id="2yiuz3qcQW4" role="3cqZAp">
        <node concept="2OqwBi" id="2yiuz3qcRBT" role="3clFbw">
          <node concept="2OqwBi" id="2yiuz3qcR61" role="2Oq$k0">
            <node concept="1YBJjd" id="2yiuz3qcQWg" role="2Oq$k0">
              <ref role="1YBMHb" node="2yiuz3qcQVV" resolve="sketch" />
            </node>
            <node concept="2qgKlT" id="2yiuz3qcRrb" role="2OqNvi">
              <ref role="37wK5l" to="tpek:hEwIClG" resolve="getMainMethod" />
            </node>
          </node>
          <node concept="3w_OXm" id="2yiuz3qcS7b" role="2OqNvi" />
        </node>
        <node concept="3clFbS" id="2yiuz3qcQW6" role="3clFbx">
          <node concept="2MkqsV" id="2yiuz3qcSdr" role="3cqZAp">
            <node concept="3Cnw8n" id="2yiuz3qcSGS" role="2OEOjU">
              <property role="ARO6o" value="true" />
              <ref role="QpYPw" node="2yiuz3qcSgD" resolve="AddMainMethod" />
              <node concept="3CnSsL" id="2yiuz3qcSNq" role="3Coj4f">
                <ref role="QkamJ" node="2yiuz3qcSgQ" resolve="sketch" />
                <node concept="1YBJjd" id="2yiuz3qcSOJ" role="3CoRuB">
                  <ref role="1YBMHb" node="2yiuz3qcQVV" resolve="sketch" />
                </node>
              </node>
            </node>
            <node concept="Xl_RD" id="2yiuz3qcSdB" role="2MkJ7o">
              <property role="Xl_RC" value="Main method missing" />
            </node>
            <node concept="1YBJjd" id="2yiuz3qcSeE" role="2OEOjV">
              <ref role="1YBMHb" node="2yiuz3qcQVV" resolve="sketch" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="1YaCAy" id="2yiuz3qcQVV" role="1YuTPh">
      <property role="TrG5h" value="sketch" />
      <ref role="1YaFvo" to="h7pl:2yiuz3qbfwd" resolve="Sketch" />
    </node>
  </node>
  <node concept="Q5z_Y" id="2yiuz3qcSgD">
    <property role="TrG5h" value="AddMainMethod" />
    <node concept="Q6JDH" id="2yiuz3qcSgQ" role="Q6Id_">
      <property role="TrG5h" value="sketch" />
      <node concept="3Tqbb2" id="2yiuz3qcSgW" role="Q6QK4">
        <ref role="ehGHo" to="h7pl:2yiuz3qbfwd" resolve="Sketch" />
      </node>
    </node>
    <node concept="Q5ZZ6" id="2yiuz3qcSgE" role="Q6x$H">
      <node concept="3clFbS" id="2yiuz3qcSgF" role="2VODD2">
        <node concept="3cpWs8" id="2yiuz3qcShc" role="3cqZAp">
          <node concept="3cpWsn" id="2yiuz3qcShf" role="3cpWs9">
            <property role="TrG5h" value="mainMethod" />
            <node concept="3Tqbb2" id="2yiuz3qcShb" role="1tU5fm">
              <ref role="ehGHo" to="tpee:fIYIFWa" resolve="StaticMethodDeclaration" />
            </node>
            <node concept="2c44tf" id="2yiuz3qcSi1" role="33vP2m">
              <node concept="2YIFZL" id="2yiuz3qcSi_" role="2c44tc">
                <property role="TrG5h" value="main" />
                <node concept="3cqZAl" id="2yiuz3qcSiB" role="3clF45" />
                <node concept="3Tm1VV" id="2yiuz3qcSiC" role="1B3o_S" />
                <node concept="3clFbS" id="2yiuz3qcSiD" role="3clF47">
                  <node concept="3clFbF" id="2yiuz3qcSpp" role="3cqZAp">
                    <node concept="2YIFZM" id="2yiuz3qcSpC" role="3clFbG">
                      <ref role="37wK5l" to="r7oa:~PApplet.main(java.lang.Class,java.lang.String...):void" resolve="main" />
                      <ref role="1Pybhc" to="r7oa:~PApplet" resolve="PApplet" />
                      <node concept="3VsKOn" id="2yiuz3qcSr8" role="37wK5m">
                        <ref role="3VsUkX" to="r7oa:~PApplet" resolve="PApplet" />
                        <node concept="2c44tb" id="2yiuz3qcStG" role="lGtFl">
                          <property role="P3scX" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1116615150612/1116615189566" />
                          <property role="2qtEX8" value="classifier" />
                          <node concept="QwW4i" id="2yiuz3qcSv2" role="2c44t1">
                            <ref role="QwW4h" node="2yiuz3qcSgQ" resolve="sketch" />
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="37vLTG" id="2yiuz3qcSjd" role="3clF46">
                  <property role="TrG5h" value="args" />
                  <node concept="10Q1$e" id="2yiuz3qcSkq" role="1tU5fm">
                    <node concept="17QB3L" id="2yiuz3qcSjc" role="10Q1$1" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qcUA7" role="3cqZAp">
          <node concept="2OqwBi" id="2yiuz3qcVLN" role="3clFbG">
            <node concept="2OqwBi" id="2yiuz3qcUIA" role="2Oq$k0">
              <node concept="QwW4i" id="2yiuz3qcUA5" role="2Oq$k0">
                <ref role="QwW4h" node="2yiuz3qcSgQ" resolve="sketch" />
              </node>
              <node concept="3Tsc0h" id="2yiuz3qcV4t" role="2OqNvi">
                <ref role="3TtcxE" to="tpee:4EqhHTp4Mw3" />
              </node>
            </node>
            <node concept="TSZUe" id="2yiuz3qcWCh" role="2OqNvi">
              <node concept="37vLTw" id="2yiuz3qcWI$" role="25WWJ7">
                <ref role="3cqZAo" node="2yiuz3qcShf" resolve="mainMethod" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="18kY7G" id="2yiuz3qdtGS">
    <property role="TrG5h" value="SizeCallIncorrectPlace" />
    <node concept="3clFbS" id="2yiuz3qdtGT" role="18ibNy">
      <node concept="3clFbJ" id="2yiuz3qe4T_" role="3cqZAp">
        <node concept="3clFbS" id="2yiuz3qe4TB" role="3clFbx">
          <node concept="3cpWs6" id="2yiuz3qe5Nv" role="3cqZAp" />
        </node>
        <node concept="22lmx$" id="2yiuz3qezwz" role="3clFbw">
          <node concept="2OqwBi" id="2yiuz3qe$Lv" role="3uHU7w">
            <node concept="2OqwBi" id="2yiuz3qezId" role="2Oq$k0">
              <node concept="1YBJjd" id="2yiuz3qezDI" role="2Oq$k0">
                <ref role="1YBMHb" node="2yiuz3qdtKQ" resolve="localMethodCall" />
              </node>
              <node concept="2Xjw5R" id="2yiuz3qe$lX" role="2OqNvi">
                <node concept="1xMEDy" id="2yiuz3qe$lZ" role="1xVPHs">
                  <node concept="chp4Y" id="2yiuz3qe$uY" role="ri$Ld">
                    <ref role="cht4Q" to="h7pl:2yiuz3qbfwd" resolve="Sketch" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3w_OXm" id="2yiuz3qfC_l" role="2OqNvi" />
          </node>
          <node concept="3fqX7Q" id="2yiuz3qe5FJ" role="3uHU7B">
            <node concept="2OqwBi" id="2yiuz3qe5FL" role="3fr31v">
              <node concept="2OqwBi" id="2yiuz3qe5FM" role="2Oq$k0">
                <node concept="1YBJjd" id="2yiuz3qe5FN" role="2Oq$k0">
                  <ref role="1YBMHb" node="2yiuz3qdtKQ" resolve="localMethodCall" />
                </node>
                <node concept="2qgKlT" id="2yiuz3qe5FO" role="2OqNvi">
                  <ref role="37wK5l" to="tpek:hEwJgm_" resolve="getVariableExpectedName" />
                </node>
              </node>
              <node concept="liA8E" id="2yiuz3qe5FP" role="2OqNvi">
                <ref role="37wK5l" to="wyt6:~String.equals(java.lang.Object):boolean" resolve="equals" />
                <node concept="Xl_RD" id="2yiuz3qe5FQ" role="37wK5m">
                  <property role="Xl_RC" value="size" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="3cpWs8" id="2yiuz3qew3C" role="3cqZAp">
        <node concept="3cpWsn" id="2yiuz3qew3F" role="3cpWs9">
          <property role="TrG5h" value="method" />
          <node concept="3Tqbb2" id="2yiuz3qew3A" role="1tU5fm">
            <ref role="ehGHo" to="tpee:fzclF8t" resolve="InstanceMethodDeclaration" />
          </node>
          <node concept="2OqwBi" id="2yiuz3qewhO" role="33vP2m">
            <node concept="1YBJjd" id="2yiuz3qewe3" role="2Oq$k0">
              <ref role="1YBMHb" node="2yiuz3qdtKQ" resolve="localMethodCall" />
            </node>
            <node concept="2Xjw5R" id="2yiuz3qewyc" role="2OqNvi">
              <node concept="1xMEDy" id="2yiuz3qewye" role="1xVPHs">
                <node concept="chp4Y" id="2yiuz3qewyC" role="ri$Ld">
                  <ref role="cht4Q" to="tpee:fzclF8t" resolve="InstanceMethodDeclaration" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="3clFbJ" id="2yiuz3qewB2" role="3cqZAp">
        <node concept="3clFbS" id="2yiuz3qewB4" role="3clFbx">
          <node concept="3cpWs6" id="2yiuz3qexqn" role="3cqZAp" />
        </node>
        <node concept="2OqwBi" id="2yiuz3qewRb" role="3clFbw">
          <node concept="37vLTw" id="2yiuz3qewBX" role="2Oq$k0">
            <ref role="3cqZAo" node="2yiuz3qew3F" resolve="method" />
          </node>
          <node concept="3w_OXm" id="2yiuz3qexnR" role="2OqNvi" />
        </node>
      </node>
      <node concept="3cpWs8" id="2yiuz3qe1p2" role="3cqZAp">
        <node concept="3cpWsn" id="2yiuz3qe1p5" role="3cpWs9">
          <property role="TrG5h" value="settings" />
          <node concept="3Tqbb2" id="2yiuz3qe1p0" role="1tU5fm">
            <ref role="ehGHo" to="tpee:fzclF8t" resolve="InstanceMethodDeclaration" />
          </node>
          <node concept="2c44tf" id="2yiuz3qe1Bp" role="33vP2m">
            <node concept="3clFb_" id="2yiuz3qe1BQ" role="2c44tc">
              <property role="TrG5h" value="settings" />
              <node concept="3cqZAl" id="2yiuz3qe1BS" role="3clF45" />
              <node concept="3Tm1VV" id="2yiuz3qe1BT" role="1B3o_S" />
              <node concept="3clFbS" id="2yiuz3qe1BU" role="3clF47" />
            </node>
          </node>
        </node>
      </node>
      <node concept="3clFbJ" id="2yiuz3qe0aD" role="3cqZAp">
        <node concept="3fqX7Q" id="2yiuz3qe3GP" role="3clFbw">
          <node concept="2OqwBi" id="2yiuz3qe3GR" role="3fr31v">
            <node concept="2qgKlT" id="2yiuz3qe3GV" role="2OqNvi">
              <ref role="37wK5l" to="tpek:hEwIB0z" resolve="hasSameSignature" />
              <node concept="37vLTw" id="2yiuz3qe3GW" role="37wK5m">
                <ref role="3cqZAo" node="2yiuz3qe1p5" resolve="settings" />
              </node>
            </node>
            <node concept="37vLTw" id="2yiuz3qexzk" role="2Oq$k0">
              <ref role="3cqZAo" node="2yiuz3qew3F" resolve="method" />
            </node>
          </node>
        </node>
        <node concept="3clFbS" id="2yiuz3qe0aF" role="3clFbx">
          <node concept="2MkqsV" id="2yiuz3qe3N0" role="3cqZAp">
            <node concept="Xl_RD" id="2yiuz3qe3Nc" role="2MkJ7o">
              <property role="Xl_RC" value="Size can only be used in settings method" />
            </node>
            <node concept="1YBJjd" id="2yiuz3qe3OG" role="2OEOjV">
              <ref role="1YBMHb" node="2yiuz3qdtKQ" resolve="localMethodCall" />
            </node>
            <node concept="3Cnw8n" id="2yiuz3qeyY_" role="2OEOjU">
              <property role="ARO6o" value="true" />
              <ref role="QpYPw" node="2yiuz3qeyQ7" resolve="MoveSizeCall" />
              <node concept="3CnSsL" id="2yiuz3qez7i" role="3Coj4f">
                <ref role="QkamJ" node="2yiuz3qez6V" resolve="sizeCall" />
                <node concept="1YBJjd" id="2yiuz3qez83" role="3CoRuB">
                  <ref role="1YBMHb" node="2yiuz3qdtKQ" resolve="localMethodCall" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="3clFbH" id="2yiuz3qfXzW" role="3cqZAp" />
    </node>
    <node concept="1YaCAy" id="2yiuz3qdtKQ" role="1YuTPh">
      <property role="TrG5h" value="localMethodCall" />
      <ref role="1YaFvo" to="tpee:6LFqxSRBTg4" resolve="LocalMethodCall" />
    </node>
  </node>
  <node concept="Q5z_Y" id="2yiuz3qeyQ7">
    <property role="TrG5h" value="MoveSizeCall" />
    <node concept="Q6JDH" id="2yiuz3qez6V" role="Q6Id_">
      <property role="TrG5h" value="sizeCall" />
      <node concept="3Tqbb2" id="2yiuz3qez71" role="Q6QK4">
        <ref role="ehGHo" to="tpee:6LFqxSRBTg4" resolve="LocalMethodCall" />
      </node>
    </node>
    <node concept="Q5ZZ6" id="2yiuz3qeyQ8" role="Q6x$H">
      <node concept="3clFbS" id="2yiuz3qeyQ9" role="2VODD2">
        <node concept="3cpWs8" id="2yiuz3qezjK" role="3cqZAp">
          <node concept="3cpWsn" id="2yiuz3qezjN" role="3cpWs9">
            <property role="TrG5h" value="sketch" />
            <node concept="3Tqbb2" id="2yiuz3qezjJ" role="1tU5fm">
              <ref role="ehGHo" to="h7pl:2yiuz3qbfwd" resolve="Sketch" />
            </node>
            <node concept="2OqwBi" id="2yiuz3qe_uU" role="33vP2m">
              <node concept="QwW4i" id="2yiuz3qe_r3" role="2Oq$k0">
                <ref role="QwW4h" node="2yiuz3qez6V" resolve="sizeCall" />
              </node>
              <node concept="2Xjw5R" id="2yiuz3qe_AD" role="2OqNvi">
                <node concept="1xMEDy" id="2yiuz3qe_AF" role="1xVPHs">
                  <node concept="chp4Y" id="2yiuz3qe_Bc" role="ri$Ld">
                    <ref role="cht4Q" to="h7pl:2yiuz3qbfwd" resolve="Sketch" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbJ" id="2yiuz3qe_C3" role="3cqZAp">
          <node concept="3clFbS" id="2yiuz3qe_C5" role="3clFbx">
            <node concept="3cpWs6" id="2yiuz3qeA9D" role="3cqZAp" />
          </node>
          <node concept="2OqwBi" id="2yiuz3qe_Mk" role="3clFbw">
            <node concept="37vLTw" id="2yiuz3qe_Ct" role="2Oq$k0">
              <ref role="3cqZAo" node="2yiuz3qezjN" resolve="sketch" />
            </node>
            <node concept="3w_OXm" id="2yiuz3qeA7y" role="2OqNvi" />
          </node>
        </node>
        <node concept="3cpWs8" id="2yiuz3qeAfw" role="3cqZAp">
          <node concept="3cpWsn" id="2yiuz3qeAfz" role="3cpWs9">
            <property role="TrG5h" value="settings" />
            <node concept="3Tqbb2" id="2yiuz3qeAf$" role="1tU5fm">
              <ref role="ehGHo" to="tpee:fzclF8t" resolve="InstanceMethodDeclaration" />
            </node>
            <node concept="2c44tf" id="2yiuz3qeAf_" role="33vP2m">
              <node concept="3clFb_" id="2yiuz3qeAfA" role="2c44tc">
                <property role="TrG5h" value="settings" />
                <node concept="3cqZAl" id="2yiuz3qeAfB" role="3clF45" />
                <node concept="3Tm1VV" id="2yiuz3qeAfC" role="1B3o_S" />
                <node concept="3clFbS" id="2yiuz3qeAfD" role="3clF47" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="2yiuz3qeGT4" role="3cqZAp">
          <node concept="3cpWsn" id="2yiuz3qeGT7" role="3cpWs9">
            <property role="TrG5h" value="declaredSettingsMethod" />
            <node concept="3Tqbb2" id="2yiuz3qeGT2" role="1tU5fm">
              <ref role="ehGHo" to="tpee:fzclF8t" resolve="InstanceMethodDeclaration" />
            </node>
            <node concept="1PxgMI" id="2yiuz3qf4C9" role="33vP2m">
              <ref role="1PxNhF" to="tpee:fzclF8t" resolve="InstanceMethodDeclaration" />
              <node concept="2OqwBi" id="2yiuz3qeIcP" role="1PxMeX">
                <node concept="2OqwBi" id="2yiuz3qeH4$" role="2Oq$k0">
                  <node concept="37vLTw" id="2yiuz3qeGUD" role="2Oq$k0">
                    <ref role="3cqZAo" node="2yiuz3qezjN" resolve="sketch" />
                  </node>
                  <node concept="3Tsc0h" id="2yiuz3qeHpN" role="2OqNvi">
                    <ref role="3TtcxE" to="tpee:4EqhHTp4Mw3" />
                  </node>
                </node>
                <node concept="1z4cxt" id="2yiuz3qeJ4h" role="2OqNvi">
                  <node concept="1bVj0M" id="2yiuz3qeJ4j" role="23t8la">
                    <node concept="3clFbS" id="2yiuz3qeJ4k" role="1bW5cS">
                      <node concept="Jncv_" id="2yiuz3qeJwf" role="3cqZAp">
                        <ref role="JncvD" to="tpee:fzclF8t" resolve="InstanceMethodDeclaration" />
                        <node concept="37vLTw" id="2yiuz3qeJ$V" role="JncvB">
                          <ref role="3cqZAo" node="2yiuz3qeJ4l" resolve="it" />
                        </node>
                        <node concept="3clFbS" id="2yiuz3qeJwh" role="Jncv$">
                          <node concept="3cpWs6" id="2yiuz3qf3Pq" role="3cqZAp">
                            <node concept="2OqwBi" id="2yiuz3qf2va" role="3cqZAk">
                              <node concept="Jnkvi" id="2yiuz3qf2gH" role="2Oq$k0">
                                <ref role="1M0zk5" node="2yiuz3qeJwi" resolve="method" />
                              </node>
                              <node concept="2qgKlT" id="2yiuz3qf3oV" role="2OqNvi">
                                <ref role="37wK5l" to="tpek:hEwIB0z" resolve="hasSameSignature" />
                                <node concept="37vLTw" id="2yiuz3qf3yd" role="37wK5m">
                                  <ref role="3cqZAo" node="2yiuz3qeAfz" resolve="settings" />
                                </node>
                              </node>
                            </node>
                          </node>
                        </node>
                        <node concept="JncvC" id="2yiuz3qeJwi" role="JncvA">
                          <property role="TrG5h" value="method" />
                          <node concept="2jxLKc" id="2yiuz3qeJwj" role="1tU5fm" />
                        </node>
                      </node>
                      <node concept="3cpWs6" id="2yiuz3qf50m" role="3cqZAp">
                        <node concept="3clFbT" id="2yiuz3qf58a" role="3cqZAk">
                          <property role="3clFbU" value="false" />
                        </node>
                      </node>
                    </node>
                    <node concept="Rh6nW" id="2yiuz3qeJ4l" role="1bW2Oz">
                      <property role="TrG5h" value="it" />
                      <node concept="2jxLKc" id="2yiuz3qeJ4m" role="1tU5fm" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbJ" id="2yiuz3qf5j0" role="3cqZAp">
          <node concept="3clFbS" id="2yiuz3qf5j2" role="3clFbx">
            <node concept="3clFbF" id="2yiuz3qf5Yu" role="3cqZAp">
              <node concept="37vLTI" id="2yiuz3qf86r" role="3clFbG">
                <node concept="37vLTw" id="2yiuz3qf5Ys" role="37vLTJ">
                  <ref role="3cqZAo" node="2yiuz3qeGT7" resolve="declaredSettingsMethod" />
                </node>
                <node concept="37vLTw" id="2yiuz3qfJlO" role="37vLTx">
                  <ref role="3cqZAo" node="2yiuz3qeAfz" resolve="settings" />
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="2yiuz3qf8dw" role="3cqZAp">
              <node concept="2OqwBi" id="2yiuz3qfaRV" role="3clFbG">
                <node concept="2OqwBi" id="2yiuz3qf8lG" role="2Oq$k0">
                  <node concept="37vLTw" id="2yiuz3qf8du" role="2Oq$k0">
                    <ref role="3cqZAo" node="2yiuz3qezjN" resolve="sketch" />
                  </node>
                  <node concept="3Tsc0h" id="2yiuz3qfGdY" role="2OqNvi">
                    <ref role="3TtcxE" to="tpee:4EqhHTp4Mw3" />
                  </node>
                </node>
                <node concept="1sK_Qi" id="2yiuz3qfNd_" role="2OqNvi">
                  <node concept="3cmrfG" id="2yiuz3qfNgN" role="1sKJu8">
                    <property role="3cmrfH" value="0" />
                  </node>
                  <node concept="37vLTw" id="2yiuz3qfNku" role="1sKFgg">
                    <ref role="3cqZAo" node="2yiuz3qeGT7" resolve="declaredSettingsMethod" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="2yiuz3qf5wu" role="3clFbw">
            <node concept="37vLTw" id="2yiuz3qf5k1" role="2Oq$k0">
              <ref role="3cqZAo" node="2yiuz3qeGT7" resolve="declaredSettingsMethod" />
            </node>
            <node concept="3w_OXm" id="2yiuz3qf5Vh" role="2OqNvi" />
          </node>
        </node>
        <node concept="3cpWs8" id="2yiuz3qfmjJ" role="3cqZAp">
          <node concept="3cpWsn" id="2yiuz3qfmjM" role="3cpWs9">
            <property role="TrG5h" value="stmt" />
            <node concept="3Tqbb2" id="2yiuz3qfmjH" role="1tU5fm">
              <ref role="ehGHo" to="tpee:fzclF8j" resolve="ExpressionStatement" />
            </node>
            <node concept="2c44tf" id="2yiuz3qfmmo" role="33vP2m">
              <node concept="3clFbF" id="2yiuz3qfmmY" role="2c44tc">
                <node concept="33vP2n" id="2yiuz3qfmn0" role="3clFbG">
                  <node concept="2c44te" id="2yiuz3qfmoz" role="lGtFl">
                    <node concept="2OqwBi" id="2yiuz3qfqE2" role="2c44t1">
                      <node concept="QwW4i" id="2yiuz3qfmoM" role="2Oq$k0">
                        <ref role="QwW4h" node="2yiuz3qez6V" resolve="sizeCall" />
                      </node>
                      <node concept="1$rogu" id="2yiuz3qfuHi" role="2OqNvi" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qfuK3" role="3cqZAp">
          <node concept="2OqwBi" id="2yiuz3qfvyU" role="3clFbG">
            <node concept="2OqwBi" id="2yiuz3qfuOA" role="2Oq$k0">
              <node concept="QwW4i" id="2yiuz3qfuK1" role="2Oq$k0">
                <ref role="QwW4h" node="2yiuz3qez6V" resolve="sizeCall" />
              </node>
              <node concept="1mfA1w" id="2yiuz3qfvqG" role="2OqNvi" />
            </node>
            <node concept="1PgB_6" id="2yiuz3qfvUZ" role="2OqNvi" />
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qfiCm" role="3cqZAp">
          <node concept="2OqwBi" id="2yiuz3qfkzL" role="3clFbG">
            <node concept="2OqwBi" id="2yiuz3qfjGA" role="2Oq$k0">
              <node concept="2OqwBi" id="2yiuz3qfiNy" role="2Oq$k0">
                <node concept="37vLTw" id="2yiuz3qfiCk" role="2Oq$k0">
                  <ref role="3cqZAo" node="2yiuz3qeGT7" resolve="declaredSettingsMethod" />
                </node>
                <node concept="3TrEf2" id="2yiuz3qfjek" role="2OqNvi">
                  <ref role="3Tt5mk" to="tpee:fzclF7Z" />
                </node>
              </node>
              <node concept="3Tsc0h" id="2yiuz3qfjTd" role="2OqNvi">
                <ref role="3TtcxE" to="tpee:fzcqZ_x" />
              </node>
            </node>
            <node concept="1sK_Qi" id="2yiuz3qfRqY" role="2OqNvi">
              <node concept="3cmrfG" id="2yiuz3qfRx0" role="1sKJu8">
                <property role="3cmrfH" value="0" />
              </node>
              <node concept="37vLTw" id="2yiuz3qfRKl" role="1sKFgg">
                <ref role="3cqZAo" node="2yiuz3qfmjM" resolve="stmt" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="QznSV" id="2yiuz3qez8e" role="QzAvj">
      <node concept="3clFbS" id="2yiuz3qez8f" role="2VODD2">
        <node concept="3clFbF" id="2yiuz3qez8S" role="3cqZAp">
          <node concept="Xl_RD" id="2yiuz3qez8R" role="3clFbG">
            <property role="Xl_RC" value="Move size call" />
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="1YbPZF" id="2yiuz3qhfXx">
    <property role="TrG5h" value="typeof_FileReference" />
    <node concept="3clFbS" id="2yiuz3qhfXy" role="18ibNy">
      <node concept="1Z5TYs" id="2yiuz3qhg3g" role="3cqZAp">
        <node concept="mw_s8" id="2yiuz3qhg48" role="1ZfhKB">
          <node concept="2c44tf" id="2yiuz3qhg44" role="mwGJk">
            <node concept="17QB3L" id="2yiuz3qhg4r" role="2c44tc" />
          </node>
        </node>
        <node concept="mw_s8" id="2yiuz3qhg3j" role="1ZfhK$">
          <node concept="1Z2H0r" id="2yiuz3qhfXC" role="mwGJk">
            <node concept="1YBJjd" id="2yiuz3qhfYC" role="1Z2MuG">
              <ref role="1YBMHb" node="2yiuz3qhfX$" resolve="fileReference" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="1YaCAy" id="2yiuz3qhfX$" role="1YuTPh">
      <property role="TrG5h" value="fileReference" />
      <ref role="1YaFvo" to="h7pl:2yiuz3qh7HE" resolve="FileReference" />
    </node>
  </node>
</model>

