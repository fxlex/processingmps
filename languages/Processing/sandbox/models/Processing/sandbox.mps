<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:4f066dc3-a22d-4735-80b0-aa2cd83961a8(Processing.sandbox)">
  <persistence version="9" />
  <languages>
    <use id="96f7ac91-a4c1-4f01-bffe-3b607acce953" name="Processing" version="-1" />
    <use id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core" version="1" />
    <use id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage" version="4" />
    <use id="d4280a54-f6df-4383-aa41-d1b2bffa7eb1" name="com.mbeddr.core.base" version="3" />
    <use id="f2801650-65d5-424e-bb1b-463a8781b786" name="jetbrains.mps.baseLanguage.javadoc" version="2" />
    <use id="2693fc71-9b0e-4b05-ab13-f57227d675f2" name="com.mbeddr.core.util" version="0" />
    <use id="fc8d557e-5de6-4dd8-b749-aab2fb23aefc" name="jetbrains.mps.baseLanguage.overloadedOperators" version="0" />
    <use id="83888646-71ce-4f1c-9c53-c54016f6ad4f" name="jetbrains.mps.baseLanguage.collections" version="0" />
    <use id="0ae47ad3-5abd-486c-ac0f-298884f39393" name="jetbrains.mps.baseLanguage.constructors" version="0" />
  </languages>
  <imports>
    <import index="r7oa" ref="96f7ac91-a4c1-4f01-bffe-3b607acce953/java:processing.core(Processing/)" />
    <import index="33ny" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.util(JDK/)" />
    <import index="tpee" ref="r:00000000-0000-4000-0000-011c895902ca(jetbrains.mps.baseLanguage.structure)" />
    <import index="sp02" ref="r:35924531-de91-475e-be95-e399470f2764(Processing.operator)" implicit="true" />
  </imports>
  <registry>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1215693861676" name="jetbrains.mps.baseLanguage.structure.BaseAssignmentExpression" flags="nn" index="d038R">
        <child id="1068498886297" name="rValue" index="37vLTx" />
        <child id="1068498886295" name="lValue" index="37vLTJ" />
      </concept>
      <concept id="4836112446988635817" name="jetbrains.mps.baseLanguage.structure.UndefinedType" flags="in" index="2jxLKc" />
      <concept id="1202948039474" name="jetbrains.mps.baseLanguage.structure.InstanceMethodCallOperation" flags="nn" index="liA8E" />
      <concept id="1179360813171" name="jetbrains.mps.baseLanguage.structure.HexIntegerLiteral" flags="nn" index="2nou5x">
        <property id="1179360856892" name="value" index="2noCCI" />
      </concept>
      <concept id="1239714755177" name="jetbrains.mps.baseLanguage.structure.AbstractUnaryNumberOperation" flags="nn" index="2$Kvd9">
        <child id="1239714902950" name="expression" index="2$L3a6" />
      </concept>
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1197029447546" name="jetbrains.mps.baseLanguage.structure.FieldReferenceOperation" flags="nn" index="2OwXpG">
        <reference id="1197029500499" name="fieldDeclaration" index="2Oxat5" />
      </concept>
      <concept id="1145552977093" name="jetbrains.mps.baseLanguage.structure.GenericNewExpression" flags="nn" index="2ShNRf">
        <child id="1145553007750" name="creator" index="2ShVmc" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1070475354124" name="jetbrains.mps.baseLanguage.structure.ThisExpression" flags="nn" index="Xjq3P" />
      <concept id="1070475926800" name="jetbrains.mps.baseLanguage.structure.StringLiteral" flags="nn" index="Xl_RD">
        <property id="1070475926801" name="value" index="Xl_RC" />
      </concept>
      <concept id="1081236700938" name="jetbrains.mps.baseLanguage.structure.StaticMethodDeclaration" flags="ig" index="2YIFZL" />
      <concept id="1081236700937" name="jetbrains.mps.baseLanguage.structure.StaticMethodCall" flags="nn" index="2YIFZM">
        <reference id="1144433194310" name="classConcept" index="1Pybhc" />
      </concept>
      <concept id="1070533707846" name="jetbrains.mps.baseLanguage.structure.StaticFieldReference" flags="nn" index="10M0yZ">
        <reference id="1144433057691" name="classifier" index="1PxDUh" />
      </concept>
      <concept id="1070534370425" name="jetbrains.mps.baseLanguage.structure.IntegerType" flags="in" index="10Oyi0" />
      <concept id="1070534436861" name="jetbrains.mps.baseLanguage.structure.FloatType" flags="in" index="10OMs4" />
      <concept id="1070534513062" name="jetbrains.mps.baseLanguage.structure.DoubleType" flags="in" index="10P55v" />
      <concept id="1070534644030" name="jetbrains.mps.baseLanguage.structure.BooleanType" flags="in" index="10P_77" />
      <concept id="1070534760951" name="jetbrains.mps.baseLanguage.structure.ArrayType" flags="in" index="10Q1$e">
        <child id="1070534760952" name="componentType" index="10Q1$1" />
      </concept>
      <concept id="1068390468200" name="jetbrains.mps.baseLanguage.structure.FieldDeclaration" flags="ig" index="312cEg">
        <property id="8606350594693632173" name="isTransient" index="eg7rD" />
        <property id="1240249534625" name="isVolatile" index="34CwA1" />
      </concept>
      <concept id="1068390468198" name="jetbrains.mps.baseLanguage.structure.ClassConcept" flags="ig" index="312cEu">
        <property id="1075300953594" name="abstractClass" index="1sVAO0" />
        <property id="1221565133444" name="isFinal" index="1EXbeo" />
        <child id="1165602531693" name="superclass" index="1zkMxy" />
      </concept>
      <concept id="1068431474542" name="jetbrains.mps.baseLanguage.structure.VariableDeclaration" flags="ng" index="33uBYm">
        <property id="1176718929932" name="isFinal" index="3TUv4t" />
        <child id="1068431790190" name="initializer" index="33vP2m" />
      </concept>
      <concept id="1068498886296" name="jetbrains.mps.baseLanguage.structure.VariableReference" flags="nn" index="37vLTw">
        <reference id="1068581517664" name="variableDeclaration" index="3cqZAo" />
      </concept>
      <concept id="1068498886292" name="jetbrains.mps.baseLanguage.structure.ParameterDeclaration" flags="ir" index="37vLTG" />
      <concept id="1068498886294" name="jetbrains.mps.baseLanguage.structure.AssignmentExpression" flags="nn" index="37vLTI" />
      <concept id="1225271177708" name="jetbrains.mps.baseLanguage.structure.StringType" flags="in" index="17QB3L" />
      <concept id="4972933694980447171" name="jetbrains.mps.baseLanguage.structure.BaseVariableDeclaration" flags="ng" index="19Szcq">
        <child id="5680397130376446158" name="type" index="1tU5fm" />
      </concept>
      <concept id="1068580123132" name="jetbrains.mps.baseLanguage.structure.BaseMethodDeclaration" flags="ng" index="3clF44">
        <property id="4276006055363816570" name="isSynchronized" index="od$2w" />
        <property id="1181808852946" name="isFinal" index="DiZV1" />
        <child id="1068580123133" name="returnType" index="3clF45" />
        <child id="1068580123134" name="parameter" index="3clF46" />
        <child id="1068580123135" name="body" index="3clF47" />
      </concept>
      <concept id="1068580123165" name="jetbrains.mps.baseLanguage.structure.InstanceMethodDeclaration" flags="ig" index="3clFb_">
        <property id="1178608670077" name="isAbstract" index="1EzhhJ" />
      </concept>
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123157" name="jetbrains.mps.baseLanguage.structure.Statement" flags="nn" index="3clFbH" />
      <concept id="1068580123159" name="jetbrains.mps.baseLanguage.structure.IfStatement" flags="nn" index="3clFbJ">
        <child id="1068580123160" name="condition" index="3clFbw" />
        <child id="1068580123161" name="ifTrue" index="3clFbx" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068580123140" name="jetbrains.mps.baseLanguage.structure.ConstructorDeclaration" flags="ig" index="3clFbW" />
      <concept id="1068580320020" name="jetbrains.mps.baseLanguage.structure.IntegerConstant" flags="nn" index="3cmrfG">
        <property id="1068580320021" name="value" index="3cmrfH" />
      </concept>
      <concept id="1068581242875" name="jetbrains.mps.baseLanguage.structure.PlusExpression" flags="nn" index="3cpWs3" />
      <concept id="1068581242878" name="jetbrains.mps.baseLanguage.structure.ReturnStatement" flags="nn" index="3cpWs6">
        <child id="1068581517676" name="expression" index="3cqZAk" />
      </concept>
      <concept id="1068581242864" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclarationStatement" flags="nn" index="3cpWs8">
        <child id="1068581242865" name="localVariableDeclaration" index="3cpWs9" />
      </concept>
      <concept id="1068581242869" name="jetbrains.mps.baseLanguage.structure.MinusExpression" flags="nn" index="3cpWsd" />
      <concept id="1068581242863" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclaration" flags="nr" index="3cpWsn" />
      <concept id="1068581517677" name="jetbrains.mps.baseLanguage.structure.VoidType" flags="in" index="3cqZAl" />
      <concept id="1081506762703" name="jetbrains.mps.baseLanguage.structure.GreaterThanExpression" flags="nn" index="3eOSWO" />
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
        <child id="1068499141038" name="actualArgument" index="37wK5m" />
      </concept>
      <concept id="1212685548494" name="jetbrains.mps.baseLanguage.structure.ClassCreator" flags="nn" index="1pGfFk">
        <child id="1212687122400" name="typeParameter" index="1pMfVU" />
      </concept>
      <concept id="1107461130800" name="jetbrains.mps.baseLanguage.structure.Classifier" flags="ng" index="3pOWGL">
        <property id="521412098689998745" name="nonStatic" index="2bfB8j" />
        <child id="5375687026011219971" name="member" index="jymVt" unordered="true" />
      </concept>
      <concept id="7812454656619025416" name="jetbrains.mps.baseLanguage.structure.MethodDeclaration" flags="ng" index="1rXfSm">
        <property id="8355037393041754995" name="isNative" index="2aFKle" />
      </concept>
      <concept id="7812454656619025412" name="jetbrains.mps.baseLanguage.structure.LocalMethodCall" flags="nn" index="1rXfSq" />
      <concept id="1107535904670" name="jetbrains.mps.baseLanguage.structure.ClassifierType" flags="in" index="3uibUv">
        <reference id="1107535924139" name="classifier" index="3uigEE" />
      </concept>
      <concept id="1081773326031" name="jetbrains.mps.baseLanguage.structure.BinaryOperation" flags="nn" index="3uHJSO">
        <child id="1081773367579" name="rightExpression" index="3uHU7w" />
        <child id="1081773367580" name="leftExpression" index="3uHU7B" />
      </concept>
      <concept id="1214918800624" name="jetbrains.mps.baseLanguage.structure.PostfixIncrementExpression" flags="nn" index="3uNrnE" />
      <concept id="1178549954367" name="jetbrains.mps.baseLanguage.structure.IVisible" flags="ng" index="1B3ioH">
        <child id="1178549979242" name="visibility" index="1B3o_S" />
      </concept>
      <concept id="6329021646629104957" name="jetbrains.mps.baseLanguage.structure.TextCommentPart" flags="nn" index="3SKdUq">
        <property id="6329021646629104958" name="text" index="3SKdUp" />
      </concept>
      <concept id="6329021646629104954" name="jetbrains.mps.baseLanguage.structure.SingleLineComment" flags="nn" index="3SKdUt">
        <child id="6329021646629175155" name="commentPart" index="3SKWNk" />
      </concept>
      <concept id="1146644602865" name="jetbrains.mps.baseLanguage.structure.PublicVisibility" flags="nn" index="3Tm1VV" />
      <concept id="1116615150612" name="jetbrains.mps.baseLanguage.structure.ClassifierClassExpression" flags="nn" index="3VsKOn">
        <reference id="1116615189566" name="classifier" index="3VsUkX" />
      </concept>
      <concept id="8064396509828172209" name="jetbrains.mps.baseLanguage.structure.UnaryMinus" flags="nn" index="1ZRNhn" />
    </language>
    <language id="fd392034-7849-419d-9071-12563d152375" name="jetbrains.mps.baseLanguage.closures">
      <concept id="1199569711397" name="jetbrains.mps.baseLanguage.closures.structure.ClosureLiteral" flags="nn" index="1bVj0M">
        <child id="1199569906740" name="parameter" index="1bW2Oz" />
        <child id="1199569916463" name="body" index="1bW5cS" />
      </concept>
    </language>
    <language id="f2801650-65d5-424e-bb1b-463a8781b786" name="jetbrains.mps.baseLanguage.javadoc">
      <concept id="6832197706140896242" name="jetbrains.mps.baseLanguage.javadoc.structure.FieldDocComment" flags="ng" index="z59LJ" />
      <concept id="5349172909345501395" name="jetbrains.mps.baseLanguage.javadoc.structure.BaseDocComment" flags="ng" index="P$AiS">
        <child id="8465538089690331502" name="body" index="TZ5H$" />
      </concept>
      <concept id="8465538089690331500" name="jetbrains.mps.baseLanguage.javadoc.structure.CommentLine" flags="ng" index="TZ5HA">
        <child id="8970989240999019149" name="part" index="1dT_Ay" />
      </concept>
      <concept id="8970989240999019143" name="jetbrains.mps.baseLanguage.javadoc.structure.TextCommentLinePart" flags="ng" index="1dT_AC">
        <property id="8970989240999019144" name="text" index="1dT_AB" />
      </concept>
    </language>
    <language id="d4280a54-f6df-4383-aa41-d1b2bffa7eb1" name="com.mbeddr.core.base">
      <concept id="6156524541422549000" name="com.mbeddr.core.base.structure.AbstractPicker" flags="ng" index="3N1QpV">
        <property id="9294901202237533" name="mayBeEmpty" index="3kgbRO" />
        <property id="6156524541422553710" name="path" index="3N1Lgt" />
        <property id="2711621784026951428" name="pointOnlyToExistingFile" index="1RwFax" />
      </concept>
      <concept id="4881264737620519319" name="com.mbeddr.core.base.structure.FileSystemDirPicker" flags="ng" index="3RfPnX" />
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1156234966388" name="shortDescription" index="OYnhT" />
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
        <child id="5169995583184591170" name="smodelAttribute" index="lGtFl" />
      </concept>
      <concept id="2565736246230036151" name="jetbrains.mps.lang.core.structure.ExportScopeNamespace" flags="ig" index="24uvOM">
        <property id="2565736246230036153" name="namespace" index="24uvOW" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
    <language id="fc8d557e-5de6-4dd8-b749-aab2fb23aefc" name="jetbrains.mps.baseLanguage.overloadedOperators">
      <concept id="1569627462441399919" name="jetbrains.mps.baseLanguage.overloadedOperators.structure.CustomOperatorUsage" flags="nn" index="1Pj1AN">
        <reference id="1569627462441399920" name="operator" index="1Pj1AG" />
      </concept>
    </language>
    <language id="96f7ac91-a4c1-4f01-bffe-3b607acce953" name="Processing">
      <concept id="2040549711431391988" name="Processing.structure.ProcessingHexIntegerLiteral" flags="ng" index="2$dqvi" />
      <concept id="2040549711431677007" name="Processing.structure.IContainsStatementList" flags="ng" index="2$e4_D">
        <child id="2040549711431677224" name="statements" index="2$e4we" />
      </concept>
      <concept id="2040549711431674023" name="Processing.structure.PushMatrix" flags="ng" index="2$e5m1" />
      <concept id="2040549711431614751" name="Processing.structure.PushStyle" flags="ng" index="2$ejKT">
        <child id="2040549711431616746" name="statements" index="2$ejnc" />
      </concept>
      <concept id="2923533458354272269" name="Processing.structure.Sketch" flags="ig" index="3MJC8X">
        <child id="2923533458355778368" name="dataFolder" index="3MOo_K" />
      </concept>
      <concept id="2923533458355740280" name="Processing.structure.DataFolder" flags="ng" index="3MOih8">
        <child id="2923533458355755469" name="dir" index="3MOufX" />
      </concept>
      <concept id="2923533458355813226" name="Processing.structure.FileReference" flags="ng" index="3MPw5q">
        <property id="2923533458355846259" name="path" index="3MPCp3" />
      </concept>
    </language>
    <language id="0ae47ad3-5abd-486c-ac0f-298884f39393" name="jetbrains.mps.baseLanguage.constructors">
      <concept id="6820702584719416486" name="jetbrains.mps.baseLanguage.constructors.structure.CustomConstructorUsage" flags="nn" index="2AoxLU">
        <reference id="6820702584719569331" name="customConstructor" index="2Ap75J" />
        <child id="6820702584719569344" name="element" index="2Ap74s" />
      </concept>
      <concept id="4739047193854255783" name="jetbrains.mps.baseLanguage.constructors.structure.ListParameterReference" flags="nn" index="MIHzR" />
      <concept id="526936149311701954" name="jetbrains.mps.baseLanguage.constructors.structure.CustomConstructor" flags="ig" index="38mkTw">
        <property id="3330196687714050064" name="leftParenthesis" index="1_lXKI" />
        <property id="3330196687714050065" name="rightParenthesis" index="1_lXKJ" />
        <property id="3330196687714050063" name="separator" index="1_lXKL" />
        <child id="3330196687714050067" name="returnType" index="1_lXKH" />
        <child id="5379647004618201111" name="arguments" index="3XUO1_" />
      </concept>
      <concept id="526936149311701953" name="jetbrains.mps.baseLanguage.constructors.structure.CustomConstructorContainer" flags="ng" index="38mkTz">
        <child id="3041831561922340678" name="constructors" index="1oLirQ" />
      </concept>
      <concept id="5379647004618176186" name="jetbrains.mps.baseLanguage.constructors.structure.CustomArgumentClause" flags="ng" index="3XUIV8">
        <child id="5379647004618201121" name="parameter" index="3XUO1j" />
      </concept>
      <concept id="5379647004618201113" name="jetbrains.mps.baseLanguage.constructors.structure.CustomConstructorParameter" flags="ng" index="3XUO1F">
        <child id="5379647004618207272" name="type" index="3XUQxq" />
      </concept>
      <concept id="5379647004618378852" name="jetbrains.mps.baseLanguage.constructors.structure.CustomConstructorParameterReference" flags="nn" index="3XVpom">
        <reference id="5379647004618378853" name="parameter" index="3XVpon" />
      </concept>
    </language>
    <language id="83888646-71ce-4f1c-9c53-c54016f6ad4f" name="jetbrains.mps.baseLanguage.collections">
      <concept id="1204796164442" name="jetbrains.mps.baseLanguage.collections.structure.InternalSequenceOperation" flags="nn" index="23sCx2">
        <child id="1204796294226" name="closure" index="23t8la" />
      </concept>
      <concept id="540871147943773365" name="jetbrains.mps.baseLanguage.collections.structure.SingleArgumentSequenceOperation" flags="nn" index="25WWJ4">
        <child id="540871147943773366" name="argument" index="25WWJ7" />
      </concept>
      <concept id="1204980550705" name="jetbrains.mps.baseLanguage.collections.structure.VisitAllOperation" flags="nn" index="2es0OD" />
      <concept id="1151688443754" name="jetbrains.mps.baseLanguage.collections.structure.ListType" flags="in" index="_YKpA">
        <child id="1151688676805" name="elementType" index="_ZDj9" />
      </concept>
      <concept id="1237721394592" name="jetbrains.mps.baseLanguage.collections.structure.AbstractContainerCreator" flags="nn" index="HWqM0">
        <child id="1237721435807" name="elementType" index="HW$YZ" />
      </concept>
      <concept id="1227022210526" name="jetbrains.mps.baseLanguage.collections.structure.ClearAllElementsOperation" flags="nn" index="2Kehj3" />
      <concept id="1203518072036" name="jetbrains.mps.baseLanguage.collections.structure.SmartClosureParameterDeclaration" flags="ig" index="Rh6nW" />
      <concept id="1160600644654" name="jetbrains.mps.baseLanguage.collections.structure.ListCreatorWithInit" flags="nn" index="Tc6Ow" />
      <concept id="1160612413312" name="jetbrains.mps.baseLanguage.collections.structure.AddElementOperation" flags="nn" index="TSZUe" />
    </language>
  </registry>
  <node concept="3MJC8X" id="2yiuz3qgPBz">
    <property role="TrG5h" value="sketch_154223" />
    <node concept="3Tm1VV" id="2yiuz3qgPB$" role="1B3o_S" />
    <node concept="3uibUv" id="2yiuz3qgPB_" role="1zkMxy">
      <ref role="3uigEE" to="r7oa:~PApplet" resolve="PApplet" />
    </node>
    <node concept="24uvOM" id="2yiuz3qgPBA" role="lGtFl">
      <property role="24uvOW" value="sketch_154223" />
    </node>
    <node concept="2YIFZL" id="2yiuz3qgPBE" role="jymVt">
      <property role="TrG5h" value="main" />
      <node concept="3cqZAl" id="2yiuz3qgPBF" role="3clF45" />
      <node concept="3Tm1VV" id="2yiuz3qgPBG" role="1B3o_S" />
      <node concept="3clFbS" id="2yiuz3qgPBH" role="3clF47">
        <node concept="3clFbF" id="2yiuz3qgPBI" role="3cqZAp">
          <node concept="2YIFZM" id="2yiuz3qgPBJ" role="3clFbG">
            <ref role="37wK5l" to="r7oa:~PApplet.main(java.lang.Class,java.lang.String...):void" resolve="main" />
            <ref role="1Pybhc" to="r7oa:~PApplet" resolve="PApplet" />
            <node concept="3VsKOn" id="2yiuz3qgPBK" role="37wK5m">
              <ref role="3VsUkX" node="2yiuz3qgPBz" resolve="sketch_154223" />
            </node>
          </node>
        </node>
      </node>
      <node concept="37vLTG" id="2yiuz3qgPBL" role="3clF46">
        <property role="TrG5h" value="args" />
        <node concept="10Q1$e" id="2yiuz3qgPBM" role="1tU5fm">
          <node concept="17QB3L" id="2yiuz3qgPBN" role="10Q1$1" />
        </node>
      </node>
    </node>
    <node concept="3clFb_" id="2yiuz3qgPOB" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="TrG5h" value="setup" />
      <property role="od$2w" value="false" />
      <property role="DiZV1" value="false" />
      <property role="2aFKle" value="false" />
      <node concept="3clFbS" id="2yiuz3qgPOE" role="3clF47">
        <node concept="3SKdUt" id="1LhviqSvXUH" role="3cqZAp">
          <node concept="3SKdUq" id="1LhviqSvXUJ" role="3SKWNk">
            <property role="3SKdUp" value="autocompletion for files in data folder" />
          </node>
        </node>
        <node concept="3clFbF" id="1LhviqSwC1w" role="3cqZAp">
          <node concept="1rXfSq" id="2yiuz3qh7$p" role="3clFbG">
            <ref role="37wK5l" to="r7oa:~PApplet.loadFont(java.lang.String):processing.core.PFont" resolve="loadFont" />
            <node concept="3MPw5q" id="2yiuz3qhFJH" role="37wK5m">
              <property role="3MPCp3" value="Serif-48.vlw" />
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="1LhviqSqDNK" role="3cqZAp">
          <node concept="3cpWsn" id="1LhviqSqDNN" role="3cpWs9">
            <property role="TrG5h" value="pi" />
            <node concept="10P55v" id="1LhviqSqDNI" role="1tU5fm" />
            <node concept="37vLTw" id="1LhviqSrTyE" role="33vP2m">
              <ref role="3cqZAo" to="r7oa:~PConstants.PI" resolve="PI" />
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="1LhviqSwx1y" role="3cqZAp">
          <node concept="3cpWsn" id="1LhviqSwx1z" role="3cpWs9">
            <property role="TrG5h" value="two_pi" />
            <node concept="10P55v" id="1LhviqSwx1$" role="1tU5fm" />
            <node concept="37vLTw" id="1LhviqSwxT8" role="33vP2m">
              <ref role="3cqZAo" to="r7oa:~PConstants.TWO_PI" resolve="TWO_PI" />
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="1LhviqSwxcj" role="3cqZAp">
          <node concept="3cpWsn" id="1LhviqSwxck" role="3cpWs9">
            <property role="TrG5h" value="half_pi" />
            <node concept="10P55v" id="1LhviqSwxcl" role="1tU5fm" />
            <node concept="37vLTw" id="1LhviqSwy6a" role="33vP2m">
              <ref role="3cqZAo" to="r7oa:~PConstants.HALF_PI" resolve="HALF_PI" />
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="1LhviqSwxn8" role="3cqZAp">
          <node concept="3cpWsn" id="1LhviqSwxn9" role="3cpWs9">
            <property role="TrG5h" value="tau" />
            <node concept="10P55v" id="1LhviqSwxna" role="1tU5fm" />
            <node concept="37vLTw" id="1LhviqSwynJ" role="33vP2m">
              <ref role="3cqZAo" to="r7oa:~PConstants.TAU" resolve="TAU" />
            </node>
          </node>
        </node>
        <node concept="3SKdUt" id="1LhviqSvYwN" role="3cqZAp">
          <node concept="3SKdUq" id="1LhviqSvYwP" role="3SKWNk">
            <property role="3SKdUp" value="custom constructor + vector addition and cross product" />
          </node>
        </node>
        <node concept="3SKdUt" id="1LhviqSx1QV" role="3cqZAp">
          <node concept="3SKdUq" id="1LhviqSx1QX" role="3SKWNk">
            <property role="3SKdUp" value="with custom operators" />
          </node>
        </node>
        <node concept="3cpWs8" id="1LhviqSur53" role="3cqZAp">
          <node concept="3cpWsn" id="1LhviqSur54" role="3cpWs9">
            <property role="TrG5h" value="p1" />
            <node concept="3uibUv" id="1LhviqSur55" role="1tU5fm">
              <ref role="3uigEE" to="r7oa:~PVector" resolve="PVector" />
            </node>
            <node concept="2AoxLU" id="1LhviqSwVul" role="33vP2m">
              <ref role="2Ap75J" node="1LhviqSwRhV" resolve="PVectorConstructorXYZ" />
              <node concept="3cmrfG" id="1LhviqSwVws" role="2Ap74s">
                <property role="3cmrfH" value="1" />
              </node>
              <node concept="3cmrfG" id="1LhviqSwV_r" role="2Ap74s">
                <property role="3cmrfH" value="1" />
              </node>
              <node concept="3cmrfG" id="1LhviqSwVEV" role="2Ap74s">
                <property role="3cmrfH" value="1" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="1LhviqSurxO" role="3cqZAp">
          <node concept="3cpWsn" id="1LhviqSurxP" role="3cpWs9">
            <property role="TrG5h" value="p2" />
            <node concept="3uibUv" id="1LhviqSurxQ" role="1tU5fm">
              <ref role="3uigEE" to="r7oa:~PVector" resolve="PVector" />
            </node>
            <node concept="2AoxLU" id="1LhviqSwWom" role="33vP2m">
              <ref role="2Ap75J" node="1LhviqSwRhV" resolve="PVectorConstructorXYZ" />
              <node concept="3cmrfG" id="1LhviqSwWri" role="2Ap74s">
                <property role="3cmrfH" value="2" />
              </node>
              <node concept="3cmrfG" id="1LhviqSwWw1" role="2Ap74s">
                <property role="3cmrfH" value="2" />
              </node>
              <node concept="3cmrfG" id="1LhviqSwW_M" role="2Ap74s">
                <property role="3cmrfH" value="2" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="1LhviqSurZG" role="3cqZAp">
          <node concept="3cpWsn" id="1LhviqSurZH" role="3cpWs9">
            <property role="TrG5h" value="p3" />
            <node concept="3uibUv" id="1LhviqSurZI" role="1tU5fm">
              <ref role="3uigEE" to="r7oa:~PVector" resolve="PVector" />
            </node>
            <node concept="1Pj1AN" id="1LhviqSuTbT" role="33vP2m">
              <ref role="1Pj1AG" to="sp02:1LhviqSuwoz" resolve="x" />
              <node concept="3cpWs3" id="1LhviqSusiV" role="3uHU7B">
                <node concept="37vLTw" id="1LhviqSus8U" role="3uHU7B">
                  <ref role="3cqZAo" node="1LhviqSur54" resolve="p1" />
                </node>
                <node concept="37vLTw" id="1LhviqSuslQ" role="3uHU7w">
                  <ref role="3cqZAo" node="1LhviqSurxP" resolve="p2" />
                </node>
              </node>
              <node concept="37vLTw" id="1LhviqSuTp3" role="3uHU7w">
                <ref role="3cqZAo" node="1LhviqSurxP" resolve="p2" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3SKdUt" id="1LhviqSvYOQ" role="3cqZAp">
          <node concept="3SKdUq" id="1LhviqSvYOS" role="3SKWNk">
            <property role="3SKdUp" value="hex numbers" />
          </node>
        </node>
        <node concept="3cpWs8" id="1LhviqSvWKp" role="3cqZAp">
          <node concept="3cpWsn" id="1LhviqSvWKs" role="3cpWs9">
            <property role="TrG5h" value="c" />
            <node concept="10Oyi0" id="1LhviqSvWKn" role="1tU5fm" />
            <node concept="2$dqvi" id="1LhviqSvWQP" role="33vP2m">
              <property role="2noCCI" value="123456" />
            </node>
          </node>
        </node>
        <node concept="3SKdUt" id="1LhviqSvXGL" role="3cqZAp">
          <node concept="3SKdUq" id="1LhviqSvXGN" role="3SKWNk">
            <property role="3SKdUp" value="abs()" />
          </node>
        </node>
        <node concept="3cpWs8" id="1LhviqSvX5u" role="3cqZAp">
          <node concept="3cpWsn" id="1LhviqSvX5x" role="3cpWs9">
            <property role="TrG5h" value="absolute" />
            <node concept="10Oyi0" id="1LhviqSvX5s" role="1tU5fm" />
            <node concept="2YIFZM" id="1LhviqSvXfp" role="33vP2m">
              <ref role="1Pybhc" to="r7oa:~PApplet" resolve="PApplet" />
              <ref role="37wK5l" to="r7oa:~PApplet.abs(int):int" resolve="abs" />
              <node concept="3cmrfG" id="1LhviqSvXjy" role="37wK5m">
                <property role="3cmrfH" value="-1" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3SKdUt" id="1LhviqSwvVv" role="3cqZAp">
          <node concept="3SKdUq" id="1LhviqSwvVw" role="3SKWNk">
            <property role="3SKdUp" value="sqrt()" />
          </node>
        </node>
        <node concept="3cpWs8" id="1LhviqSwvVx" role="3cqZAp">
          <node concept="3cpWsn" id="1LhviqSwvVy" role="3cpWs9">
            <property role="TrG5h" value="root" />
            <node concept="10OMs4" id="1LhviqSwwtA" role="1tU5fm" />
            <node concept="2YIFZM" id="1LhviqSwwhY" role="33vP2m">
              <ref role="1Pybhc" to="r7oa:~PApplet" resolve="PApplet" />
              <ref role="37wK5l" to="r7oa:~PApplet.sqrt(float):float" resolve="sqrt" />
              <node concept="3cmrfG" id="1LhviqSwwpZ" role="37wK5m">
                <property role="3cmrfH" value="-1" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3SKdUt" id="1LhviqSvZ03" role="3cqZAp">
          <node concept="3SKdUq" id="1LhviqSvZ04" role="3SKWNk">
            <property role="3SKdUp" value="PApplet class is automatically hidden" />
          </node>
        </node>
        <node concept="3cpWs8" id="1LhviqSvZ05" role="3cqZAp">
          <node concept="3cpWsn" id="1LhviqSvZ06" role="3cpWs9">
            <property role="TrG5h" value="t" />
            <node concept="10OMs4" id="1LhviqSwvy8" role="1tU5fm" />
            <node concept="3cpWs3" id="1LhviqSwtin" role="33vP2m">
              <node concept="10M0yZ" id="1LhviqSvZa2" role="3uHU7B">
                <ref role="1PxDUh" to="r7oa:~PApplet" resolve="PApplet" />
                <ref role="3cqZAo" to="r7oa:~PConstants.ADD" resolve="ADD" />
              </node>
              <node concept="1rXfSq" id="1LhviqSwuGg" role="3uHU7w">
                <ref role="37wK5l" to="r7oa:~PApplet.textWidth(java.lang.String):float" resolve="textWidth" />
                <node concept="Xl_RD" id="1LhviqSwuWN" role="37wK5m">
                  <property role="Xl_RC" value="hello" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="2$e5m1" id="1LhviqSwzfp" role="3cqZAp">
          <node concept="3clFbS" id="1LhviqSwzuu" role="2$e4we">
            <node concept="3cpWs8" id="1LhviqSwzup" role="3cqZAp">
              <node concept="3cpWsn" id="1LhviqSwzus" role="3cpWs9">
                <property role="TrG5h" value="a" />
                <node concept="10Oyi0" id="1LhviqSwzuo" role="1tU5fm" />
                <node concept="3cmrfG" id="1LhviqSwzCc" role="33vP2m">
                  <property role="3cmrfH" value="1" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="1LhviqSwzEs" role="3cqZAp" />
        <node concept="2$ejKT" id="1LhviqSw$_z" role="3cqZAp">
          <node concept="3clFbS" id="1LhviqSwXs0" role="2$ejnc">
            <node concept="3cpWs8" id="1LhviqSwXrV" role="3cqZAp">
              <node concept="3cpWsn" id="1LhviqSwXrY" role="3cpWs9">
                <property role="TrG5h" value="aa" />
                <node concept="10Oyi0" id="1LhviqSwXrU" role="1tU5fm" />
                <node concept="3cmrfG" id="1LhviqSwXBB" role="33vP2m">
                  <property role="3cmrfH" value="2" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="1LhviqSwzOD" role="3cqZAp" />
        <node concept="3SKdUt" id="1LhviqSwASY" role="3cqZAp">
          <node concept="3SKdUq" id="1LhviqSwAT0" role="3SKWNk">
            <property role="3SKdUp" value="highlighting of builtin variables" />
          </node>
        </node>
        <node concept="3cpWs8" id="1LhviqSwEvH" role="3cqZAp">
          <node concept="3cpWsn" id="1LhviqSwEvK" role="3cpWs9">
            <property role="TrG5h" value="cc" />
            <node concept="10P_77" id="1LhviqSwELT" role="1tU5fm" />
            <node concept="3eOSWO" id="1LhviqSwAsA" role="33vP2m">
              <node concept="37vLTw" id="1LhviqSwA6x" role="3uHU7B">
                <ref role="3cqZAo" to="r7oa:~PApplet.pmouseX" resolve="pmouseX" />
              </node>
              <node concept="3cmrfG" id="1LhviqSwABW" role="3uHU7w">
                <property role="3cmrfH" value="0" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="3cqZAl" id="2yiuz3qgPMe" role="3clF45" />
      <node concept="3Tm1VV" id="2yiuz3qgPRB" role="1B3o_S" />
    </node>
    <node concept="3MOih8" id="2yiuz3qhFrR" role="3MOo_K">
      <node concept="3RfPnX" id="2yiuz3qhFrS" role="3MOufX">
        <property role="1RwFax" value="true" />
        <property role="3kgbRO" value="false" />
        <property role="3N1Lgt" value="/Users/alexanderpann/Desktop/data" />
      </node>
    </node>
  </node>
  <node concept="3MJC8X" id="2yiuz3qrGLT">
    <property role="TrG5h" value="koch" />
    <property role="3GE5qa" value="koch" />
    <node concept="3clFb_" id="2yiuz3qrHFY" role="jymVt">
      <property role="TrG5h" value="settings" />
      <node concept="3cqZAl" id="2yiuz3qrHFZ" role="3clF45" />
      <node concept="3Tm1VV" id="2yiuz3qrHG0" role="1B3o_S" />
      <node concept="3clFbS" id="2yiuz3qrHG1" role="3clF47">
        <node concept="3clFbF" id="2yiuz3qrHG5" role="3cqZAp">
          <node concept="1rXfSq" id="2yiuz3qrHG2" role="3clFbG">
            <ref role="37wK5l" to="r7oa:~PApplet.size(int,int):void" resolve="size" />
            <node concept="3cmrfG" id="2yiuz3qrHG3" role="37wK5m">
              <property role="3cmrfH" value="640" />
            </node>
            <node concept="3cmrfG" id="2yiuz3qrHG4" role="37wK5m">
              <property role="3cmrfH" value="360" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="312cEg" id="2yiuz3qrHxT" role="jymVt">
      <property role="34CwA1" value="false" />
      <property role="eg7rD" value="false" />
      <property role="TrG5h" value="k" />
      <property role="3TUv4t" value="false" />
      <node concept="3uibUv" id="2yiuz3qrI7k" role="1tU5fm">
        <ref role="3uigEE" node="2yiuz3qrHYc" resolve="KochFractal" />
      </node>
      <node concept="z59LJ" id="2yiuz3qrHxW" role="lGtFl">
        <node concept="TZ5HA" id="2yiuz3qrHyt" role="TZ5H$">
          <node concept="1dT_AC" id="2yiuz3qrHyu" role="1dT_Ay">
            <property role="1dT_AB" value="Koch Curve" />
          </node>
        </node>
        <node concept="TZ5HA" id="2yiuz3qrHyv" role="TZ5H$">
          <node concept="1dT_AC" id="2yiuz3qrHyw" role="1dT_Ay">
            <property role="1dT_AB" value="by Daniel Shiffman." />
          </node>
        </node>
        <node concept="TZ5HA" id="2yiuz3qrHyx" role="TZ5H$">
          <node concept="1dT_AC" id="2yiuz3qrHyy" role="1dT_Ay">
            <property role="1dT_AB" value="" />
          </node>
        </node>
        <node concept="TZ5HA" id="2yiuz3qrHyz" role="TZ5H$">
          <node concept="1dT_AC" id="2yiuz3qrHy$" role="1dT_Ay">
            <property role="1dT_AB" value="Renders a simple fractal, the Koch snowflake. " />
          </node>
        </node>
        <node concept="TZ5HA" id="2yiuz3qrHy_" role="TZ5H$">
          <node concept="1dT_AC" id="2yiuz3qrHyA" role="1dT_Ay">
            <property role="1dT_AB" value="Each recursive level is drawn in sequence. " />
          </node>
        </node>
      </node>
    </node>
    <node concept="3clFb_" id="2yiuz3qrHxX" role="jymVt">
      <property role="TrG5h" value="setup" />
      <property role="DiZV1" value="false" />
      <property role="od$2w" value="false" />
      <node concept="3clFbS" id="2yiuz3qrHxY" role="3clF47">
        <node concept="3clFbF" id="2yiuz3qrHy3" role="3cqZAp">
          <node concept="1rXfSq" id="2yiuz3qrHy4" role="3clFbG">
            <ref role="37wK5l" to="r7oa:~PApplet.frameRate(float):void" resolve="frameRate" />
            <node concept="3cmrfG" id="2yiuz3qrHy5" role="37wK5m">
              <property role="3cmrfH" value="1" />
            </node>
          </node>
        </node>
        <node concept="3SKdUt" id="2yiuz3qrHyC" role="3cqZAp">
          <node concept="3SKdUq" id="2yiuz3qrHyB" role="3SKWNk">
            <property role="3SKdUp" value="Animate slowly" />
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qrHy6" role="3cqZAp">
          <node concept="37vLTI" id="2yiuz3qrHy7" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qrHy8" role="37vLTJ">
              <ref role="3cqZAo" node="2yiuz3qrHxT" resolve="k" />
            </node>
            <node concept="2ShNRf" id="2yiuz3qrUgp" role="37vLTx">
              <node concept="1pGfFk" id="2yiuz3qrUgq" role="2ShVmc">
                <ref role="37wK5l" node="2yiuz3qrHYq" resolve="KochFractal" />
                <node concept="Xjq3P" id="2yiuz3qseua" role="37wK5m" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="3cqZAl" id="2yiuz3qrHya" role="3clF45" />
      <node concept="3Tm1VV" id="2yiuz3qrUUL" role="1B3o_S" />
    </node>
    <node concept="3clFb_" id="2yiuz3qrHyb" role="jymVt">
      <property role="TrG5h" value="draw" />
      <property role="DiZV1" value="false" />
      <property role="od$2w" value="false" />
      <node concept="3clFbS" id="2yiuz3qrHyc" role="3clF47">
        <node concept="3clFbF" id="1LhviqSpd2I" role="3cqZAp">
          <node concept="1rXfSq" id="1LhviqSpd2G" role="3clFbG">
            <ref role="37wK5l" to="r7oa:~PApplet.background(int):void" resolve="background" />
            <node concept="3cmrfG" id="1LhviqSx2oJ" role="37wK5m">
              <property role="3cmrfH" value="200" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qrHyg" role="3cqZAp">
          <node concept="2OqwBi" id="2yiuz3qrHyM" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qrHyL" role="2Oq$k0">
              <ref role="3cqZAo" node="2yiuz3qrHxT" resolve="k" />
            </node>
            <node concept="liA8E" id="2yiuz3qrHyN" role="2OqNvi">
              <ref role="37wK5l" node="2yiuz3qrHZi" resolve="render" />
            </node>
          </node>
        </node>
        <node concept="3SKdUt" id="2yiuz3qrHyG" role="3cqZAp">
          <node concept="3SKdUq" id="2yiuz3qrHyF" role="3SKWNk">
            <property role="3SKdUp" value="Iterate" />
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qrHyi" role="3cqZAp">
          <node concept="2OqwBi" id="2yiuz3qrHyR" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qrHyQ" role="2Oq$k0">
              <ref role="3cqZAo" node="2yiuz3qrHxT" resolve="k" />
            </node>
            <node concept="liA8E" id="2yiuz3qrHyS" role="2OqNvi">
              <ref role="37wK5l" node="2yiuz3qrHYO" resolve="nextLevel" />
            </node>
          </node>
        </node>
        <node concept="3SKdUt" id="2yiuz3qrHyI" role="3cqZAp">
          <node concept="3SKdUq" id="2yiuz3qrHyH" role="3SKWNk">
            <property role="3SKdUp" value="Let's not do it more than 5 times. . ." />
          </node>
        </node>
        <node concept="3clFbJ" id="2yiuz3qrHyk" role="3cqZAp">
          <node concept="3eOSWO" id="2yiuz3qrHyl" role="3clFbw">
            <node concept="2OqwBi" id="2yiuz3qrHyW" role="3uHU7B">
              <node concept="37vLTw" id="2yiuz3qrHyV" role="2Oq$k0">
                <ref role="3cqZAo" node="2yiuz3qrHxT" resolve="k" />
              </node>
              <node concept="liA8E" id="2yiuz3qrHyX" role="2OqNvi">
                <ref role="37wK5l" node="2yiuz3qrHZd" resolve="getCount" />
              </node>
            </node>
            <node concept="3cmrfG" id="2yiuz3qrHyn" role="3uHU7w">
              <property role="3cmrfH" value="5" />
            </node>
          </node>
          <node concept="3clFbS" id="2yiuz3qrHyp" role="3clFbx">
            <node concept="3clFbF" id="2yiuz3qrHyq" role="3cqZAp">
              <node concept="2OqwBi" id="2yiuz3qrHz1" role="3clFbG">
                <node concept="37vLTw" id="2yiuz3qrHz0" role="2Oq$k0">
                  <ref role="3cqZAo" node="2yiuz3qrHxT" resolve="k" />
                </node>
                <node concept="liA8E" id="2yiuz3qrHz2" role="2OqNvi">
                  <ref role="37wK5l" node="2yiuz3qrHYZ" resolve="restart" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="3cqZAl" id="2yiuz3qrHys" role="3clF45" />
      <node concept="3Tm1VV" id="2yiuz3qrULJ" role="1B3o_S" />
    </node>
    <node concept="3Tm1VV" id="2yiuz3qrGLU" role="1B3o_S" />
    <node concept="3uibUv" id="2yiuz3qrGLV" role="1zkMxy">
      <ref role="3uigEE" to="r7oa:~PApplet" resolve="PApplet" />
    </node>
    <node concept="24uvOM" id="2yiuz3qrGLW" role="lGtFl">
      <property role="24uvOW" value="sketch_901352" />
    </node>
    <node concept="3MOih8" id="2yiuz3qrGLX" role="3MOo_K">
      <node concept="3RfPnX" id="2yiuz3qrGLY" role="3MOufX">
        <property role="1RwFax" value="true" />
        <property role="3kgbRO" value="false" />
      </node>
    </node>
    <node concept="2YIFZL" id="2yiuz3qrGM2" role="jymVt">
      <property role="TrG5h" value="main" />
      <node concept="3cqZAl" id="2yiuz3qrGM3" role="3clF45" />
      <node concept="3Tm1VV" id="2yiuz3qrGM4" role="1B3o_S" />
      <node concept="3clFbS" id="2yiuz3qrGM5" role="3clF47">
        <node concept="3clFbF" id="2yiuz3qrGM6" role="3cqZAp">
          <node concept="2YIFZM" id="2yiuz3qrGM7" role="3clFbG">
            <ref role="37wK5l" to="r7oa:~PApplet.main(java.lang.Class,java.lang.String...):void" resolve="main" />
            <ref role="1Pybhc" to="r7oa:~PApplet" resolve="PApplet" />
            <node concept="3VsKOn" id="2yiuz3qrGM8" role="37wK5m">
              <ref role="3VsUkX" node="2yiuz3qrGLT" resolve="koch" />
            </node>
          </node>
        </node>
      </node>
      <node concept="37vLTG" id="2yiuz3qrGM9" role="3clF46">
        <property role="TrG5h" value="args" />
        <node concept="10Q1$e" id="2yiuz3qrGMa" role="1tU5fm">
          <node concept="17QB3L" id="2yiuz3qrGMb" role="10Q1$1" />
        </node>
      </node>
    </node>
  </node>
  <node concept="312cEu" id="2yiuz3qrHYc">
    <property role="TrG5h" value="KochFractal" />
    <property role="2bfB8j" value="true" />
    <property role="1sVAO0" value="false" />
    <property role="1EXbeo" value="false" />
    <property role="3GE5qa" value="koch" />
    <node concept="312cEg" id="2yiuz3qrHYd" role="jymVt">
      <property role="34CwA1" value="false" />
      <property role="eg7rD" value="false" />
      <property role="TrG5h" value="start" />
      <property role="3TUv4t" value="false" />
      <node concept="3uibUv" id="2yiuz3qrHYf" role="1tU5fm">
        <ref role="3uigEE" to="r7oa:~PVector" resolve="PVector" />
      </node>
    </node>
    <node concept="312cEg" id="2yiuz3qrHYg" role="jymVt">
      <property role="34CwA1" value="false" />
      <property role="eg7rD" value="false" />
      <property role="TrG5h" value="end" />
      <property role="3TUv4t" value="false" />
      <node concept="3uibUv" id="2yiuz3qrHYi" role="1tU5fm">
        <ref role="3uigEE" to="r7oa:~PVector" resolve="PVector" />
      </node>
    </node>
    <node concept="312cEg" id="2yiuz3qrHYj" role="jymVt">
      <property role="34CwA1" value="false" />
      <property role="eg7rD" value="false" />
      <property role="TrG5h" value="lines" />
      <property role="3TUv4t" value="false" />
      <node concept="_YKpA" id="1LhviqSvvEB" role="1tU5fm">
        <node concept="3uibUv" id="1LhviqSvvED" role="_ZDj9">
          <ref role="3uigEE" node="2yiuz3qrJFf" resolve="KochLine" />
        </node>
      </node>
    </node>
    <node concept="312cEg" id="2yiuz3qrHYn" role="jymVt">
      <property role="34CwA1" value="false" />
      <property role="eg7rD" value="false" />
      <property role="TrG5h" value="count" />
      <property role="3TUv4t" value="false" />
      <node concept="10Oyi0" id="2yiuz3qrHYp" role="1tU5fm" />
    </node>
    <node concept="312cEg" id="2yiuz3qs9MZ" role="jymVt">
      <property role="34CwA1" value="false" />
      <property role="eg7rD" value="false" />
      <property role="TrG5h" value="p" />
      <property role="3TUv4t" value="false" />
      <node concept="3uibUv" id="2yiuz3qs9kj" role="1tU5fm">
        <ref role="3uigEE" to="r7oa:~PApplet" resolve="PApplet" />
      </node>
    </node>
    <node concept="3clFbW" id="2yiuz3qrHYq" role="jymVt">
      <property role="DiZV1" value="false" />
      <property role="od$2w" value="false" />
      <node concept="3cqZAl" id="2yiuz3qrHYr" role="3clF45" />
      <node concept="3clFbS" id="2yiuz3qrHYs" role="3clF47">
        <node concept="3clFbF" id="2yiuz3qsbcQ" role="3cqZAp">
          <node concept="37vLTI" id="2yiuz3qsbr_" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qsbsl" role="37vLTx">
              <ref role="3cqZAo" node="2yiuz3qs1gY" resolve="p" />
            </node>
            <node concept="2OqwBi" id="2yiuz3qsbjq" role="37vLTJ">
              <node concept="Xjq3P" id="2yiuz3qsbcO" role="2Oq$k0" />
              <node concept="2OwXpG" id="2yiuz3qsblz" role="2OqNvi">
                <ref role="2Oxat5" node="2yiuz3qs9MZ" resolve="p" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qrHYt" role="3cqZAp">
          <node concept="37vLTI" id="2yiuz3qrHYu" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qrHYv" role="37vLTJ">
              <ref role="3cqZAo" node="2yiuz3qrHYd" resolve="start" />
            </node>
            <node concept="2ShNRf" id="2yiuz3qrI0F" role="37vLTx">
              <node concept="1pGfFk" id="2yiuz3qrI0G" role="2ShVmc">
                <ref role="37wK5l" to="r7oa:~PVector.&lt;init&gt;(float,float)" resolve="PVector" />
                <node concept="3cmrfG" id="2yiuz3qrHYx" role="37wK5m">
                  <property role="3cmrfH" value="0" />
                </node>
                <node concept="3cpWsd" id="2yiuz3qrHYy" role="37wK5m">
                  <node concept="2OqwBi" id="2yiuz3qs1Rz" role="3uHU7B">
                    <node concept="37vLTw" id="2yiuz3qs1PN" role="2Oq$k0">
                      <ref role="3cqZAo" node="2yiuz3qs1gY" resolve="p" />
                    </node>
                    <node concept="2OwXpG" id="2yiuz3qs20u" role="2OqNvi">
                      <ref role="2Oxat5" to="r7oa:~PApplet.height" resolve="height" />
                    </node>
                  </node>
                  <node concept="3cmrfG" id="2yiuz3qrHY$" role="3uHU7w">
                    <property role="3cmrfH" value="20" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qrHY_" role="3cqZAp">
          <node concept="37vLTI" id="2yiuz3qrHYA" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qrHYB" role="37vLTJ">
              <ref role="3cqZAo" node="2yiuz3qrHYg" resolve="end" />
            </node>
            <node concept="2ShNRf" id="2yiuz3qrI0H" role="37vLTx">
              <node concept="1pGfFk" id="2yiuz3qrI0I" role="2ShVmc">
                <ref role="37wK5l" to="r7oa:~PVector.&lt;init&gt;(float,float)" resolve="PVector" />
                <node concept="2OqwBi" id="2yiuz3qs28j" role="37wK5m">
                  <node concept="37vLTw" id="2yiuz3qs272" role="2Oq$k0">
                    <ref role="3cqZAo" node="2yiuz3qs1gY" resolve="p" />
                  </node>
                  <node concept="2OwXpG" id="2yiuz3qs2iB" role="2OqNvi">
                    <ref role="2Oxat5" to="r7oa:~PApplet.width" resolve="width" />
                  </node>
                </node>
                <node concept="3cpWsd" id="2yiuz3qrHYE" role="37wK5m">
                  <node concept="2OqwBi" id="2yiuz3qs2rX" role="3uHU7B">
                    <node concept="37vLTw" id="2yiuz3qs2py" role="2Oq$k0">
                      <ref role="3cqZAo" node="2yiuz3qs1gY" resolve="p" />
                    </node>
                    <node concept="2OwXpG" id="2yiuz3qs2$1" role="2OqNvi">
                      <ref role="2Oxat5" to="r7oa:~PApplet.height" resolve="height" />
                    </node>
                  </node>
                  <node concept="3cmrfG" id="2yiuz3qrHYG" role="3uHU7w">
                    <property role="3cmrfH" value="20" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qrHYH" role="3cqZAp">
          <node concept="37vLTI" id="2yiuz3qrHYI" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qrHYJ" role="37vLTJ">
              <ref role="3cqZAo" node="2yiuz3qrHYj" resolve="lines" />
            </node>
            <node concept="2ShNRf" id="2yiuz3qrR4e" role="37vLTx">
              <node concept="1pGfFk" id="2yiuz3qrR4f" role="2ShVmc">
                <ref role="37wK5l" to="33ny:~ArrayList.&lt;init&gt;()" resolve="ArrayList" />
                <node concept="3uibUv" id="2yiuz3qrHYL" role="1pMfVU">
                  <ref role="3uigEE" node="2yiuz3qrJFf" resolve="KochLine" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qrHYM" role="3cqZAp">
          <node concept="1rXfSq" id="2yiuz3qrHYN" role="3clFbG">
            <ref role="37wK5l" node="2yiuz3qrHYZ" resolve="restart" />
          </node>
        </node>
      </node>
      <node concept="37vLTG" id="2yiuz3qs1gY" role="3clF46">
        <property role="TrG5h" value="p" />
        <node concept="3uibUv" id="2yiuz3qs1gX" role="1tU5fm">
          <ref role="3uigEE" to="r7oa:~PApplet" resolve="PApplet" />
        </node>
      </node>
    </node>
    <node concept="3clFb_" id="2yiuz3qrHYO" role="jymVt">
      <property role="TrG5h" value="nextLevel" />
      <property role="DiZV1" value="false" />
      <property role="od$2w" value="false" />
      <node concept="3clFbS" id="2yiuz3qrHYP" role="3clF47">
        <node concept="3SKdUt" id="2yiuz3qrI0s" role="3cqZAp">
          <node concept="3SKdUq" id="2yiuz3qrI0r" role="3SKWNk">
            <property role="3SKdUp" value="For every line that is in the arraylist" />
          </node>
        </node>
        <node concept="3SKdUt" id="2yiuz3qrI0u" role="3cqZAp">
          <node concept="3SKdUq" id="2yiuz3qrI0t" role="3SKWNk">
            <property role="3SKdUp" value="create 4 more lines in a new arraylist" />
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qrHYQ" role="3cqZAp">
          <node concept="37vLTI" id="2yiuz3qrHYR" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qrHYS" role="37vLTJ">
              <ref role="3cqZAo" node="2yiuz3qrHYj" resolve="lines" />
            </node>
            <node concept="1rXfSq" id="2yiuz3qrHYT" role="37vLTx">
              <ref role="37wK5l" node="2yiuz3qrHZu" resolve="iterate" />
              <node concept="37vLTw" id="2yiuz3qrHYU" role="37wK5m">
                <ref role="3cqZAo" node="2yiuz3qrHYj" resolve="lines" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qrHYV" role="3cqZAp">
          <node concept="3uNrnE" id="2yiuz3qrHYW" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qrHYX" role="2$L3a6">
              <ref role="3cqZAo" node="2yiuz3qrHYn" resolve="count" />
            </node>
          </node>
        </node>
      </node>
      <node concept="3cqZAl" id="2yiuz3qrHYY" role="3clF45" />
    </node>
    <node concept="3clFb_" id="2yiuz3qrHYZ" role="jymVt">
      <property role="TrG5h" value="restart" />
      <property role="DiZV1" value="false" />
      <property role="od$2w" value="false" />
      <node concept="3clFbS" id="2yiuz3qrHZ0" role="3clF47">
        <node concept="3clFbF" id="2yiuz3qrHZ1" role="3cqZAp">
          <node concept="37vLTI" id="2yiuz3qrHZ2" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qrHZ3" role="37vLTJ">
              <ref role="3cqZAo" node="2yiuz3qrHYn" resolve="count" />
            </node>
            <node concept="3cmrfG" id="2yiuz3qrHZ4" role="37vLTx">
              <property role="3cmrfH" value="0" />
            </node>
          </node>
        </node>
        <node concept="3SKdUt" id="2yiuz3qrI0w" role="3cqZAp">
          <node concept="3SKdUq" id="2yiuz3qrI0v" role="3SKWNk">
            <property role="3SKdUp" value="Reset count" />
          </node>
        </node>
        <node concept="3clFbF" id="1LhviqSvLJe" role="3cqZAp">
          <node concept="2OqwBi" id="1LhviqSvLUU" role="3clFbG">
            <node concept="37vLTw" id="1LhviqSvLJc" role="2Oq$k0">
              <ref role="3cqZAo" node="2yiuz3qrHYj" resolve="lines" />
            </node>
            <node concept="2Kehj3" id="1LhviqSvMi1" role="2OqNvi" />
          </node>
        </node>
        <node concept="3SKdUt" id="2yiuz3qrI0y" role="3cqZAp">
          <node concept="3SKdUq" id="2yiuz3qrI0x" role="3SKWNk">
            <property role="3SKdUp" value="Empty the array list" />
          </node>
        </node>
        <node concept="3clFbF" id="1LhviqSvMzM" role="3cqZAp">
          <node concept="2OqwBi" id="1LhviqSvMKl" role="3clFbG">
            <node concept="37vLTw" id="1LhviqSvMzK" role="2Oq$k0">
              <ref role="3cqZAo" node="2yiuz3qrHYj" resolve="lines" />
            </node>
            <node concept="TSZUe" id="1LhviqSvN6K" role="2OqNvi">
              <node concept="2ShNRf" id="2yiuz3qrRwm" role="25WWJ7">
                <node concept="1pGfFk" id="2yiuz3qrRwn" role="2ShVmc">
                  <ref role="37wK5l" node="2yiuz3qrJFm" resolve="KochLine" />
                  <node concept="37vLTw" id="2yiuz3qsbMp" role="37wK5m">
                    <ref role="3cqZAo" node="2yiuz3qs9MZ" resolve="p" />
                  </node>
                  <node concept="37vLTw" id="2yiuz3qrHZa" role="37wK5m">
                    <ref role="3cqZAo" node="2yiuz3qrHYd" resolve="start" />
                  </node>
                  <node concept="37vLTw" id="2yiuz3qrHZb" role="37wK5m">
                    <ref role="3cqZAo" node="2yiuz3qrHYg" resolve="end" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3SKdUt" id="2yiuz3qrI0$" role="3cqZAp">
          <node concept="3SKdUq" id="2yiuz3qrI0z" role="3SKWNk">
            <property role="3SKdUp" value="Add the initial line (from one end PVector to the other)" />
          </node>
        </node>
      </node>
      <node concept="3cqZAl" id="2yiuz3qrHZc" role="3clF45" />
    </node>
    <node concept="3clFb_" id="2yiuz3qrHZd" role="jymVt">
      <property role="TrG5h" value="getCount" />
      <property role="DiZV1" value="false" />
      <property role="od$2w" value="false" />
      <node concept="3clFbS" id="2yiuz3qrHZe" role="3clF47">
        <node concept="3cpWs6" id="2yiuz3qrHZf" role="3cqZAp">
          <node concept="37vLTw" id="2yiuz3qrHZg" role="3cqZAk">
            <ref role="3cqZAo" node="2yiuz3qrHYn" resolve="count" />
          </node>
        </node>
      </node>
      <node concept="10Oyi0" id="2yiuz3qrHZh" role="3clF45" />
    </node>
    <node concept="3clFb_" id="2yiuz3qrHZi" role="jymVt">
      <property role="TrG5h" value="render" />
      <property role="DiZV1" value="false" />
      <property role="od$2w" value="false" />
      <node concept="3clFbS" id="2yiuz3qrHZj" role="3clF47">
        <node concept="3clFbF" id="1LhviqSvJ52" role="3cqZAp">
          <node concept="2OqwBi" id="1LhviqSvJme" role="3clFbG">
            <node concept="37vLTw" id="1LhviqSvJ50" role="2Oq$k0">
              <ref role="3cqZAo" node="2yiuz3qrHYj" resolve="lines" />
            </node>
            <node concept="2es0OD" id="1LhviqSvJFY" role="2OqNvi">
              <node concept="1bVj0M" id="1LhviqSvJG0" role="23t8la">
                <node concept="3clFbS" id="1LhviqSvJG1" role="1bW5cS">
                  <node concept="3clFbF" id="1LhviqSvJLg" role="3cqZAp">
                    <node concept="2OqwBi" id="1LhviqSvJOM" role="3clFbG">
                      <node concept="37vLTw" id="1LhviqSvJLf" role="2Oq$k0">
                        <ref role="3cqZAo" node="1LhviqSvJG2" resolve="it" />
                      </node>
                      <node concept="liA8E" id="1LhviqSvLil" role="2OqNvi">
                        <ref role="37wK5l" node="2yiuz3qrJF_" resolve="display" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="Rh6nW" id="1LhviqSvJG2" role="1bW2Oz">
                  <property role="TrG5h" value="it" />
                  <node concept="2jxLKc" id="1LhviqSvJG3" role="1tU5fm" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="3cqZAl" id="2yiuz3qrHZt" role="3clF45" />
    </node>
    <node concept="3clFb_" id="2yiuz3qrHZu" role="jymVt">
      <property role="TrG5h" value="iterate" />
      <property role="DiZV1" value="false" />
      <property role="od$2w" value="false" />
      <node concept="37vLTG" id="2yiuz3qrHZv" role="3clF46">
        <property role="TrG5h" value="before" />
        <property role="3TUv4t" value="false" />
        <node concept="_YKpA" id="1LhviqSvECk" role="1tU5fm">
          <node concept="3uibUv" id="1LhviqSvECm" role="_ZDj9">
            <ref role="3uigEE" node="2yiuz3qrJFf" resolve="KochLine" />
          </node>
        </node>
      </node>
      <node concept="3clFbS" id="2yiuz3qrHZy" role="3clF47">
        <node concept="3cpWs8" id="2yiuz3qrHZ$" role="3cqZAp">
          <node concept="3cpWsn" id="2yiuz3qrHZz" role="3cpWs9">
            <property role="3TUv4t" value="false" />
            <property role="TrG5h" value="now" />
            <node concept="_YKpA" id="1LhviqSvx8N" role="1tU5fm">
              <node concept="3uibUv" id="1LhviqSvxIf" role="_ZDj9">
                <ref role="3uigEE" node="2yiuz3qrJFf" resolve="KochLine" />
              </node>
            </node>
            <node concept="2ShNRf" id="2yiuz3qrRwh" role="33vP2m">
              <node concept="Tc6Ow" id="1LhviqSvyOn" role="2ShVmc">
                <node concept="3uibUv" id="1LhviqSvzTj" role="HW$YZ">
                  <ref role="3uigEE" node="2yiuz3qrJFf" resolve="KochLine" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3SKdUt" id="2yiuz3qrI0A" role="3cqZAp">
          <node concept="3SKdUq" id="2yiuz3qrI0_" role="3SKWNk">
            <property role="3SKdUp" value="Create emtpy list" />
          </node>
        </node>
        <node concept="3clFbF" id="1LhviqSvFmu" role="3cqZAp">
          <node concept="2OqwBi" id="1LhviqSvFVl" role="3clFbG">
            <node concept="37vLTw" id="1LhviqSvFms" role="2Oq$k0">
              <ref role="3cqZAo" node="2yiuz3qrHZv" resolve="before" />
            </node>
            <node concept="2es0OD" id="1LhviqSvGg9" role="2OqNvi">
              <node concept="1bVj0M" id="1LhviqSvGgb" role="23t8la">
                <node concept="3clFbS" id="1LhviqSvGgc" role="1bW5cS">
                  <node concept="3SKdUt" id="2yiuz3qrI0C" role="3cqZAp">
                    <node concept="3SKdUq" id="2yiuz3qrI0B" role="3SKWNk">
                      <property role="3SKdUp" value="Calculate 5 koch PVectors (done for us by the line object)" />
                    </node>
                  </node>
                  <node concept="3cpWs8" id="2yiuz3qrHZG" role="3cqZAp">
                    <node concept="3cpWsn" id="2yiuz3qrHZF" role="3cpWs9">
                      <property role="3TUv4t" value="false" />
                      <property role="TrG5h" value="a" />
                      <node concept="3uibUv" id="2yiuz3qrHZH" role="1tU5fm">
                        <ref role="3uigEE" to="r7oa:~PVector" resolve="PVector" />
                      </node>
                      <node concept="2OqwBi" id="2yiuz3qrI11" role="33vP2m">
                        <node concept="37vLTw" id="2yiuz3qrI10" role="2Oq$k0">
                          <ref role="3cqZAo" node="1LhviqSvGgd" resolve="it" />
                        </node>
                        <node concept="liA8E" id="2yiuz3qrI12" role="2OqNvi">
                          <ref role="37wK5l" node="2yiuz3qrJFL" resolve="start" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="3cpWs8" id="2yiuz3qrHZK" role="3cqZAp">
                    <node concept="3cpWsn" id="2yiuz3qrHZJ" role="3cpWs9">
                      <property role="3TUv4t" value="false" />
                      <property role="TrG5h" value="b" />
                      <node concept="3uibUv" id="2yiuz3qrHZL" role="1tU5fm">
                        <ref role="3uigEE" to="r7oa:~PVector" resolve="PVector" />
                      </node>
                      <node concept="2OqwBi" id="2yiuz3qrI16" role="33vP2m">
                        <node concept="37vLTw" id="2yiuz3qrI15" role="2Oq$k0">
                          <ref role="3cqZAo" node="1LhviqSvGgd" resolve="it" />
                        </node>
                        <node concept="liA8E" id="2yiuz3qrI17" role="2OqNvi">
                          <ref role="37wK5l" node="2yiuz3qrJFV" resolve="kochleft" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="3cpWs8" id="2yiuz3qrHZO" role="3cqZAp">
                    <node concept="3cpWsn" id="2yiuz3qrHZN" role="3cpWs9">
                      <property role="3TUv4t" value="false" />
                      <property role="TrG5h" value="c" />
                      <node concept="3uibUv" id="2yiuz3qrHZP" role="1tU5fm">
                        <ref role="3uigEE" to="r7oa:~PVector" resolve="PVector" />
                      </node>
                      <node concept="2OqwBi" id="2yiuz3qrI1b" role="33vP2m">
                        <node concept="37vLTw" id="2yiuz3qrI1a" role="2Oq$k0">
                          <ref role="3cqZAo" node="1LhviqSvGgd" resolve="it" />
                        </node>
                        <node concept="liA8E" id="2yiuz3qrI1c" role="2OqNvi">
                          <ref role="37wK5l" node="2yiuz3qrJGc" resolve="kochmiddle" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="3cpWs8" id="2yiuz3qrHZS" role="3cqZAp">
                    <node concept="3cpWsn" id="2yiuz3qrHZR" role="3cpWs9">
                      <property role="3TUv4t" value="false" />
                      <property role="TrG5h" value="d" />
                      <node concept="3uibUv" id="2yiuz3qrHZT" role="1tU5fm">
                        <ref role="3uigEE" to="r7oa:~PVector" resolve="PVector" />
                      </node>
                      <node concept="2OqwBi" id="2yiuz3qrI1g" role="33vP2m">
                        <node concept="37vLTw" id="2yiuz3qrI1f" role="2Oq$k0">
                          <ref role="3cqZAo" node="1LhviqSvGgd" resolve="it" />
                        </node>
                        <node concept="liA8E" id="2yiuz3qrI1h" role="2OqNvi">
                          <ref role="37wK5l" node="2yiuz3qrJGD" resolve="kochright" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="3cpWs8" id="2yiuz3qrHZW" role="3cqZAp">
                    <node concept="3cpWsn" id="2yiuz3qrHZV" role="3cpWs9">
                      <property role="3TUv4t" value="false" />
                      <property role="TrG5h" value="e" />
                      <node concept="3uibUv" id="2yiuz3qrHZX" role="1tU5fm">
                        <ref role="3uigEE" to="r7oa:~PVector" resolve="PVector" />
                      </node>
                      <node concept="2OqwBi" id="2yiuz3qrI1l" role="33vP2m">
                        <node concept="37vLTw" id="2yiuz3qrI1k" role="2Oq$k0">
                          <ref role="3cqZAo" node="1LhviqSvGgd" resolve="it" />
                        </node>
                        <node concept="liA8E" id="2yiuz3qrI1m" role="2OqNvi">
                          <ref role="37wK5l" node="2yiuz3qrJFQ" resolve="end" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="3SKdUt" id="2yiuz3qrI0E" role="3cqZAp">
                    <node concept="3SKdUq" id="2yiuz3qrI0D" role="3SKWNk">
                      <property role="3SKdUp" value="Make line segments between all the PVectors and add them" />
                    </node>
                  </node>
                  <node concept="3clFbF" id="1LhviqSv$xN" role="3cqZAp">
                    <node concept="2OqwBi" id="1LhviqSv$Ig" role="3clFbG">
                      <node concept="37vLTw" id="1LhviqSv$xL" role="2Oq$k0">
                        <ref role="3cqZAo" node="2yiuz3qrHZz" resolve="now" />
                      </node>
                      <node concept="TSZUe" id="1LhviqSv_k8" role="2OqNvi">
                        <node concept="2ShNRf" id="1LhviqSv_nl" role="25WWJ7">
                          <node concept="1pGfFk" id="1LhviqSv_nm" role="2ShVmc">
                            <ref role="37wK5l" node="2yiuz3qrJFm" resolve="KochLine" />
                            <node concept="37vLTw" id="1LhviqSv_nn" role="37wK5m">
                              <ref role="3cqZAo" node="2yiuz3qs9MZ" resolve="p" />
                            </node>
                            <node concept="37vLTw" id="1LhviqSv_no" role="37wK5m">
                              <ref role="3cqZAo" node="2yiuz3qrHZF" resolve="a" />
                            </node>
                            <node concept="37vLTw" id="1LhviqSv_np" role="37wK5m">
                              <ref role="3cqZAo" node="2yiuz3qrHZJ" resolve="b" />
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="3clFbF" id="1LhviqSv_zx" role="3cqZAp">
                    <node concept="2OqwBi" id="1LhviqSv_Lg" role="3clFbG">
                      <node concept="37vLTw" id="1LhviqSv_zv" role="2Oq$k0">
                        <ref role="3cqZAo" node="2yiuz3qrHZz" resolve="now" />
                      </node>
                      <node concept="TSZUe" id="1LhviqSvA4t" role="2OqNvi">
                        <node concept="2ShNRf" id="2yiuz3qrR4j" role="25WWJ7">
                          <node concept="1pGfFk" id="2yiuz3qrR4k" role="2ShVmc">
                            <ref role="37wK5l" node="2yiuz3qrJFm" resolve="KochLine" />
                            <node concept="37vLTw" id="2yiuz3qsc3l" role="37wK5m">
                              <ref role="3cqZAo" node="2yiuz3qs9MZ" resolve="p" />
                            </node>
                            <node concept="37vLTw" id="2yiuz3qrI02" role="37wK5m">
                              <ref role="3cqZAo" node="2yiuz3qrHZF" resolve="a" />
                            </node>
                            <node concept="37vLTw" id="2yiuz3qrI03" role="37wK5m">
                              <ref role="3cqZAo" node="2yiuz3qrHZJ" resolve="b" />
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="3clFbF" id="1LhviqSvA6w" role="3cqZAp">
                    <node concept="2OqwBi" id="1LhviqSvA6y" role="3clFbG">
                      <node concept="37vLTw" id="1LhviqSvA6z" role="2Oq$k0">
                        <ref role="3cqZAo" node="2yiuz3qrHZz" resolve="now" />
                      </node>
                      <node concept="TSZUe" id="1LhviqSvA6$" role="2OqNvi">
                        <node concept="2ShNRf" id="2yiuz3qrPtC" role="25WWJ7">
                          <node concept="1pGfFk" id="2yiuz3qrPtD" role="2ShVmc">
                            <ref role="37wK5l" node="2yiuz3qrJFm" resolve="KochLine" />
                            <node concept="37vLTw" id="2yiuz3qschF" role="37wK5m">
                              <ref role="3cqZAo" node="2yiuz3qs9MZ" resolve="p" />
                            </node>
                            <node concept="37vLTw" id="2yiuz3qrI07" role="37wK5m">
                              <ref role="3cqZAo" node="2yiuz3qrHZJ" resolve="b" />
                            </node>
                            <node concept="37vLTw" id="2yiuz3qrI08" role="37wK5m">
                              <ref role="3cqZAo" node="2yiuz3qrHZN" resolve="c" />
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="3clFbF" id="1LhviqSvAfX" role="3cqZAp">
                    <node concept="2OqwBi" id="1LhviqSvAfZ" role="3clFbG">
                      <node concept="37vLTw" id="1LhviqSvAg0" role="2Oq$k0">
                        <ref role="3cqZAo" node="2yiuz3qrHZz" resolve="now" />
                      </node>
                      <node concept="TSZUe" id="1LhviqSvAg1" role="2OqNvi">
                        <node concept="2ShNRf" id="2yiuz3qrOXc" role="25WWJ7">
                          <node concept="1pGfFk" id="2yiuz3qrOXd" role="2ShVmc">
                            <ref role="37wK5l" node="2yiuz3qrJFm" resolve="KochLine" />
                            <node concept="37vLTw" id="2yiuz3qscqN" role="37wK5m">
                              <ref role="3cqZAo" node="2yiuz3qs9MZ" resolve="p" />
                            </node>
                            <node concept="37vLTw" id="2yiuz3qrI0c" role="37wK5m">
                              <ref role="3cqZAo" node="2yiuz3qrHZN" resolve="c" />
                            </node>
                            <node concept="37vLTw" id="2yiuz3qrI0d" role="37wK5m">
                              <ref role="3cqZAo" node="2yiuz3qrHZR" resolve="d" />
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="3clFbF" id="1LhviqSvAoD" role="3cqZAp">
                    <node concept="2OqwBi" id="1LhviqSvAoF" role="3clFbG">
                      <node concept="37vLTw" id="1LhviqSvAoG" role="2Oq$k0">
                        <ref role="3cqZAo" node="2yiuz3qrHZz" resolve="now" />
                      </node>
                      <node concept="TSZUe" id="1LhviqSvAoH" role="2OqNvi">
                        <node concept="2ShNRf" id="2yiuz3qrR4p" role="25WWJ7">
                          <node concept="1pGfFk" id="2yiuz3qrR4q" role="2ShVmc">
                            <ref role="37wK5l" node="2yiuz3qrJFm" resolve="KochLine" />
                            <node concept="37vLTw" id="2yiuz3qscAf" role="37wK5m">
                              <ref role="3cqZAo" node="2yiuz3qs9MZ" resolve="p" />
                            </node>
                            <node concept="37vLTw" id="2yiuz3qrI0h" role="37wK5m">
                              <ref role="3cqZAo" node="2yiuz3qrHZR" resolve="d" />
                            </node>
                            <node concept="37vLTw" id="2yiuz3qrI0i" role="37wK5m">
                              <ref role="3cqZAo" node="2yiuz3qrHZV" resolve="e" />
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="Rh6nW" id="1LhviqSvGgd" role="1bW2Oz">
                  <property role="TrG5h" value="it" />
                  <node concept="2jxLKc" id="1LhviqSvGge" role="1tU5fm" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="1LhviqSvIdV" role="3cqZAp">
          <node concept="37vLTw" id="1LhviqSvIdT" role="3clFbG">
            <ref role="3cqZAo" node="2yiuz3qrHZz" resolve="now" />
          </node>
        </node>
      </node>
      <node concept="_YKpA" id="1LhviqSvw3G" role="3clF45">
        <node concept="3uibUv" id="1LhviqSvwAo" role="_ZDj9">
          <ref role="3uigEE" node="2yiuz3qrJFf" resolve="KochLine" />
        </node>
      </node>
    </node>
  </node>
  <node concept="312cEu" id="2yiuz3qrJFf">
    <property role="TrG5h" value="KochLine" />
    <property role="2bfB8j" value="true" />
    <property role="1sVAO0" value="false" />
    <property role="1EXbeo" value="false" />
    <property role="3GE5qa" value="koch" />
    <node concept="312cEg" id="2yiuz3qrJFg" role="jymVt">
      <property role="34CwA1" value="false" />
      <property role="eg7rD" value="false" />
      <property role="TrG5h" value="a" />
      <property role="3TUv4t" value="false" />
      <node concept="3uibUv" id="2yiuz3qrJFi" role="1tU5fm">
        <ref role="3uigEE" to="r7oa:~PVector" resolve="PVector" />
      </node>
    </node>
    <node concept="312cEg" id="2yiuz3qrJFj" role="jymVt">
      <property role="34CwA1" value="false" />
      <property role="eg7rD" value="false" />
      <property role="TrG5h" value="b" />
      <property role="3TUv4t" value="false" />
      <node concept="3uibUv" id="2yiuz3qrJFl" role="1tU5fm">
        <ref role="3uigEE" to="r7oa:~PVector" resolve="PVector" />
      </node>
    </node>
    <node concept="312cEg" id="2yiuz3qs3DO" role="jymVt">
      <property role="34CwA1" value="false" />
      <property role="eg7rD" value="false" />
      <property role="TrG5h" value="p" />
      <property role="3TUv4t" value="false" />
      <node concept="3uibUv" id="2yiuz3qs3sO" role="1tU5fm">
        <ref role="3uigEE" to="r7oa:~PApplet" resolve="PApplet" />
      </node>
    </node>
    <node concept="3clFbW" id="2yiuz3qrJFm" role="jymVt">
      <property role="DiZV1" value="false" />
      <property role="od$2w" value="false" />
      <node concept="3cqZAl" id="2yiuz3qrJFn" role="3clF45" />
      <node concept="37vLTG" id="2yiuz3qs30m" role="3clF46">
        <property role="TrG5h" value="p" />
        <node concept="3uibUv" id="2yiuz3qs34t" role="1tU5fm">
          <ref role="3uigEE" to="r7oa:~PApplet" resolve="PApplet" />
        </node>
      </node>
      <node concept="37vLTG" id="2yiuz3qrJFo" role="3clF46">
        <property role="TrG5h" value="start" />
        <property role="3TUv4t" value="false" />
        <node concept="3uibUv" id="2yiuz3qrJFp" role="1tU5fm">
          <ref role="3uigEE" to="r7oa:~PVector" resolve="PVector" />
        </node>
      </node>
      <node concept="37vLTG" id="2yiuz3qrJFq" role="3clF46">
        <property role="TrG5h" value="end" />
        <property role="3TUv4t" value="false" />
        <node concept="3uibUv" id="2yiuz3qrJFr" role="1tU5fm">
          <ref role="3uigEE" to="r7oa:~PVector" resolve="PVector" />
        </node>
      </node>
      <node concept="3clFbS" id="2yiuz3qrJFs" role="3clF47">
        <node concept="3clFbF" id="2yiuz3qs44T" role="3cqZAp">
          <node concept="37vLTI" id="2yiuz3qs4ct" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qs4jc" role="37vLTx">
              <ref role="3cqZAo" node="2yiuz3qs30m" resolve="p" />
            </node>
            <node concept="2OqwBi" id="2yiuz3qs464" role="37vLTJ">
              <node concept="Xjq3P" id="2yiuz3qs44R" role="2Oq$k0" />
              <node concept="2OwXpG" id="2yiuz3qs48Y" role="2OqNvi">
                <ref role="2Oxat5" node="2yiuz3qs3DO" resolve="p" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qrJFt" role="3cqZAp">
          <node concept="37vLTI" id="2yiuz3qrJFu" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qrJFv" role="37vLTJ">
              <ref role="3cqZAo" node="2yiuz3qrJFg" resolve="a" />
            </node>
            <node concept="2OqwBi" id="2yiuz3qrJGY" role="37vLTx">
              <node concept="37vLTw" id="2yiuz3qrJGX" role="2Oq$k0">
                <ref role="3cqZAo" node="2yiuz3qrJFo" resolve="start" />
              </node>
              <node concept="liA8E" id="2yiuz3qrJGZ" role="2OqNvi">
                <ref role="37wK5l" to="r7oa:~PVector.copy():processing.core.PVector" resolve="copy" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qrJFx" role="3cqZAp">
          <node concept="37vLTI" id="2yiuz3qrJFy" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qrJFz" role="37vLTJ">
              <ref role="3cqZAo" node="2yiuz3qrJFj" resolve="b" />
            </node>
            <node concept="2OqwBi" id="2yiuz3qrJH3" role="37vLTx">
              <node concept="37vLTw" id="2yiuz3qrJH2" role="2Oq$k0">
                <ref role="3cqZAo" node="2yiuz3qrJFq" resolve="end" />
              </node>
              <node concept="liA8E" id="2yiuz3qrJH4" role="2OqNvi">
                <ref role="37wK5l" to="r7oa:~PVector.copy():processing.core.PVector" resolve="copy" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="3clFb_" id="2yiuz3qrJF_" role="jymVt">
      <property role="TrG5h" value="display" />
      <property role="DiZV1" value="false" />
      <property role="od$2w" value="false" />
      <node concept="3clFbS" id="2yiuz3qrJFA" role="3clF47">
        <node concept="3clFbF" id="2yiuz3qs4ry" role="3cqZAp">
          <node concept="2OqwBi" id="2yiuz3qs4u5" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qs4rw" role="2Oq$k0">
              <ref role="3cqZAo" node="2yiuz3qs3DO" resolve="p" />
            </node>
            <node concept="liA8E" id="2yiuz3qs4VS" role="2OqNvi">
              <ref role="37wK5l" to="r7oa:~PApplet.stroke(int):void" resolve="stroke" />
              <node concept="3cmrfG" id="2yiuz3qs4Ww" role="37wK5m">
                <property role="3cmrfH" value="255" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qs56f" role="3cqZAp">
          <node concept="2OqwBi" id="2yiuz3qs599" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qs56d" role="2Oq$k0">
              <ref role="3cqZAo" node="2yiuz3qs3DO" resolve="p" />
            </node>
            <node concept="liA8E" id="2yiuz3qs5vU" role="2OqNvi">
              <ref role="37wK5l" to="r7oa:~PApplet.line(float,float,float,float):void" resolve="line" />
              <node concept="2OqwBi" id="2yiuz3qs5_U" role="37wK5m">
                <node concept="37vLTw" id="2yiuz3qs5$z" role="2Oq$k0">
                  <ref role="3cqZAo" node="2yiuz3qrJFg" resolve="a" />
                </node>
                <node concept="2OwXpG" id="2yiuz3qs5Do" role="2OqNvi">
                  <ref role="2Oxat5" to="r7oa:~PVector.x" resolve="x" />
                </node>
              </node>
              <node concept="2OqwBi" id="2yiuz3qs5KF" role="37wK5m">
                <node concept="37vLTw" id="2yiuz3qs5H1" role="2Oq$k0">
                  <ref role="3cqZAo" node="2yiuz3qrJFg" resolve="a" />
                </node>
                <node concept="2OwXpG" id="2yiuz3qs5NT" role="2OqNvi">
                  <ref role="2Oxat5" to="r7oa:~PVector.y" resolve="y" />
                </node>
              </node>
              <node concept="2OqwBi" id="2yiuz3qs5W8" role="37wK5m">
                <node concept="37vLTw" id="2yiuz3qs5TU" role="2Oq$k0">
                  <ref role="3cqZAo" node="2yiuz3qrJFj" resolve="b" />
                </node>
                <node concept="2OwXpG" id="2yiuz3qs61c" role="2OqNvi">
                  <ref role="2Oxat5" to="r7oa:~PVector.x" resolve="x" />
                </node>
              </node>
              <node concept="2OqwBi" id="2yiuz3qs6aQ" role="37wK5m">
                <node concept="37vLTw" id="2yiuz3qs68o" role="2Oq$k0">
                  <ref role="3cqZAo" node="2yiuz3qrJFj" resolve="b" />
                </node>
                <node concept="2OwXpG" id="2yiuz3qs6eE" role="2OqNvi">
                  <ref role="2Oxat5" to="r7oa:~PVector.y" resolve="y" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="3cqZAl" id="2yiuz3qrJFK" role="3clF45" />
    </node>
    <node concept="3clFb_" id="2yiuz3qrJFL" role="jymVt">
      <property role="TrG5h" value="start" />
      <property role="DiZV1" value="false" />
      <property role="od$2w" value="false" />
      <node concept="3clFbS" id="2yiuz3qrJFM" role="3clF47">
        <node concept="3cpWs6" id="2yiuz3qrJFN" role="3cqZAp">
          <node concept="2OqwBi" id="2yiuz3qrJHs" role="3cqZAk">
            <node concept="37vLTw" id="2yiuz3qrJHr" role="2Oq$k0">
              <ref role="3cqZAo" node="2yiuz3qrJFg" resolve="a" />
            </node>
            <node concept="liA8E" id="2yiuz3qrJHt" role="2OqNvi">
              <ref role="37wK5l" to="r7oa:~PVector.copy():processing.core.PVector" resolve="copy" />
            </node>
          </node>
        </node>
      </node>
      <node concept="3uibUv" id="2yiuz3qrJFP" role="3clF45">
        <ref role="3uigEE" to="r7oa:~PVector" resolve="PVector" />
      </node>
    </node>
    <node concept="3clFb_" id="2yiuz3qrJFQ" role="jymVt">
      <property role="TrG5h" value="end" />
      <property role="DiZV1" value="false" />
      <property role="od$2w" value="false" />
      <node concept="3clFbS" id="2yiuz3qrJFR" role="3clF47">
        <node concept="3cpWs6" id="2yiuz3qrJFS" role="3cqZAp">
          <node concept="2OqwBi" id="2yiuz3qrJHx" role="3cqZAk">
            <node concept="37vLTw" id="2yiuz3qrJHw" role="2Oq$k0">
              <ref role="3cqZAo" node="2yiuz3qrJFj" resolve="b" />
            </node>
            <node concept="liA8E" id="2yiuz3qrJHy" role="2OqNvi">
              <ref role="37wK5l" to="r7oa:~PVector.copy():processing.core.PVector" resolve="copy" />
            </node>
          </node>
        </node>
      </node>
      <node concept="3uibUv" id="2yiuz3qrJFU" role="3clF45">
        <ref role="3uigEE" to="r7oa:~PVector" resolve="PVector" />
      </node>
    </node>
    <node concept="3clFb_" id="2yiuz3qrJFV" role="jymVt">
      <property role="TrG5h" value="kochleft" />
      <property role="DiZV1" value="false" />
      <property role="od$2w" value="false" />
      <node concept="3clFbS" id="2yiuz3qrJFW" role="3clF47">
        <node concept="3cpWs8" id="2yiuz3qrJFY" role="3cqZAp">
          <node concept="3cpWsn" id="2yiuz3qrJFX" role="3cpWs9">
            <property role="3TUv4t" value="false" />
            <property role="TrG5h" value="v" />
            <node concept="3uibUv" id="2yiuz3qrJFZ" role="1tU5fm">
              <ref role="3uigEE" to="r7oa:~PVector" resolve="PVector" />
            </node>
            <node concept="2YIFZM" id="2yiuz3qrJH_" role="33vP2m">
              <ref role="1Pybhc" to="r7oa:~PVector" resolve="PVector" />
              <ref role="37wK5l" to="r7oa:~PVector.sub(processing.core.PVector,processing.core.PVector):processing.core.PVector" resolve="sub" />
              <node concept="37vLTw" id="2yiuz3qrJG1" role="37wK5m">
                <ref role="3cqZAo" node="2yiuz3qrJFj" resolve="b" />
              </node>
              <node concept="37vLTw" id="2yiuz3qrJG2" role="37wK5m">
                <ref role="3cqZAo" node="2yiuz3qrJFg" resolve="a" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qrJG3" role="3cqZAp">
          <node concept="2OqwBi" id="2yiuz3qrJHD" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qrJHC" role="2Oq$k0">
              <ref role="3cqZAo" node="2yiuz3qrJFX" resolve="v" />
            </node>
            <node concept="liA8E" id="2yiuz3qrJHE" role="2OqNvi">
              <ref role="37wK5l" to="r7oa:~PVector.div(float):processing.core.PVector" resolve="div" />
              <node concept="3cmrfG" id="2yiuz3qrJG5" role="37wK5m">
                <property role="3cmrfH" value="3" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qrJG6" role="3cqZAp">
          <node concept="2OqwBi" id="2yiuz3qrJHI" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qrJHH" role="2Oq$k0">
              <ref role="3cqZAo" node="2yiuz3qrJFX" resolve="v" />
            </node>
            <node concept="liA8E" id="2yiuz3qrJHJ" role="2OqNvi">
              <ref role="37wK5l" to="r7oa:~PVector.add(processing.core.PVector):processing.core.PVector" resolve="add" />
              <node concept="37vLTw" id="2yiuz3qrJG8" role="37wK5m">
                <ref role="3cqZAo" node="2yiuz3qrJFg" resolve="a" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="2yiuz3qrJG9" role="3cqZAp">
          <node concept="37vLTw" id="2yiuz3qrJGa" role="3cqZAk">
            <ref role="3cqZAo" node="2yiuz3qrJFX" resolve="v" />
          </node>
        </node>
      </node>
      <node concept="3uibUv" id="2yiuz3qrJGb" role="3clF45">
        <ref role="3uigEE" to="r7oa:~PVector" resolve="PVector" />
      </node>
    </node>
    <node concept="3clFb_" id="2yiuz3qrJGc" role="jymVt">
      <property role="TrG5h" value="kochmiddle" />
      <property role="DiZV1" value="false" />
      <property role="od$2w" value="false" />
      <node concept="3clFbS" id="2yiuz3qrJGd" role="3clF47">
        <node concept="3cpWs8" id="2yiuz3qrJGf" role="3cqZAp">
          <node concept="3cpWsn" id="2yiuz3qrJGe" role="3cpWs9">
            <property role="3TUv4t" value="false" />
            <property role="TrG5h" value="v" />
            <node concept="3uibUv" id="2yiuz3qrJGg" role="1tU5fm">
              <ref role="3uigEE" to="r7oa:~PVector" resolve="PVector" />
            </node>
            <node concept="2YIFZM" id="2yiuz3qrJHM" role="33vP2m">
              <ref role="1Pybhc" to="r7oa:~PVector" resolve="PVector" />
              <ref role="37wK5l" to="r7oa:~PVector.sub(processing.core.PVector,processing.core.PVector):processing.core.PVector" resolve="sub" />
              <node concept="37vLTw" id="2yiuz3qrJGi" role="37wK5m">
                <ref role="3cqZAo" node="2yiuz3qrJFj" resolve="b" />
              </node>
              <node concept="37vLTw" id="2yiuz3qrJGj" role="37wK5m">
                <ref role="3cqZAo" node="2yiuz3qrJFg" resolve="a" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qrJGk" role="3cqZAp">
          <node concept="2OqwBi" id="2yiuz3qrJHQ" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qrJHP" role="2Oq$k0">
              <ref role="3cqZAo" node="2yiuz3qrJGe" resolve="v" />
            </node>
            <node concept="liA8E" id="2yiuz3qrJHR" role="2OqNvi">
              <ref role="37wK5l" to="r7oa:~PVector.div(float):processing.core.PVector" resolve="div" />
              <node concept="3cmrfG" id="2yiuz3qrJGm" role="37wK5m">
                <property role="3cmrfH" value="3" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="2yiuz3qrJGo" role="3cqZAp">
          <node concept="3cpWsn" id="2yiuz3qrJGn" role="3cpWs9">
            <property role="3TUv4t" value="false" />
            <property role="TrG5h" value="p" />
            <node concept="3uibUv" id="2yiuz3qrJGp" role="1tU5fm">
              <ref role="3uigEE" to="r7oa:~PVector" resolve="PVector" />
            </node>
            <node concept="2OqwBi" id="2yiuz3qrJHV" role="33vP2m">
              <node concept="37vLTw" id="2yiuz3qrJHU" role="2Oq$k0">
                <ref role="3cqZAo" node="2yiuz3qrJFg" resolve="a" />
              </node>
              <node concept="liA8E" id="2yiuz3qrJHW" role="2OqNvi">
                <ref role="37wK5l" to="r7oa:~PVector.copy():processing.core.PVector" resolve="copy" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qrJGr" role="3cqZAp">
          <node concept="2OqwBi" id="2yiuz3qrJI0" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qrJHZ" role="2Oq$k0">
              <ref role="3cqZAo" node="2yiuz3qrJGn" resolve="p" />
            </node>
            <node concept="liA8E" id="2yiuz3qrJI1" role="2OqNvi">
              <ref role="37wK5l" to="r7oa:~PVector.add(processing.core.PVector):processing.core.PVector" resolve="add" />
              <node concept="37vLTw" id="2yiuz3qrJGt" role="37wK5m">
                <ref role="3cqZAo" node="2yiuz3qrJGe" resolve="v" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qrJGu" role="3cqZAp">
          <node concept="2OqwBi" id="2yiuz3qrJI5" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qrJI4" role="2Oq$k0">
              <ref role="3cqZAo" node="2yiuz3qrJGe" resolve="v" />
            </node>
            <node concept="liA8E" id="2yiuz3qrJI6" role="2OqNvi">
              <ref role="37wK5l" to="r7oa:~PVector.rotate(float):processing.core.PVector" resolve="rotate" />
              <node concept="1ZRNhn" id="2yiuz3qrJGw" role="37wK5m">
                <node concept="2YIFZM" id="2yiuz3qs784" role="2$L3a6">
                  <ref role="37wK5l" to="r7oa:~PApplet.radians(float):float" resolve="radians" />
                  <ref role="1Pybhc" to="r7oa:~PApplet" resolve="PApplet" />
                  <node concept="3cmrfG" id="2yiuz3qs7cB" role="37wK5m">
                    <property role="3cmrfH" value="60" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qrJGz" role="3cqZAp">
          <node concept="2OqwBi" id="2yiuz3qrJIa" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qrJI9" role="2Oq$k0">
              <ref role="3cqZAo" node="2yiuz3qrJGn" resolve="p" />
            </node>
            <node concept="liA8E" id="2yiuz3qrJIb" role="2OqNvi">
              <ref role="37wK5l" to="r7oa:~PVector.add(processing.core.PVector):processing.core.PVector" resolve="add" />
              <node concept="37vLTw" id="2yiuz3qrJG_" role="37wK5m">
                <ref role="3cqZAo" node="2yiuz3qrJGe" resolve="v" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="2yiuz3qrJGA" role="3cqZAp">
          <node concept="37vLTw" id="2yiuz3qrJGB" role="3cqZAk">
            <ref role="3cqZAo" node="2yiuz3qrJGn" resolve="p" />
          </node>
        </node>
      </node>
      <node concept="3uibUv" id="2yiuz3qrJGC" role="3clF45">
        <ref role="3uigEE" to="r7oa:~PVector" resolve="PVector" />
      </node>
    </node>
    <node concept="3clFb_" id="2yiuz3qrJGD" role="jymVt">
      <property role="TrG5h" value="kochright" />
      <property role="DiZV1" value="false" />
      <property role="od$2w" value="false" />
      <node concept="3clFbS" id="2yiuz3qrJGE" role="3clF47">
        <node concept="3cpWs8" id="2yiuz3qrJGG" role="3cqZAp">
          <node concept="3cpWsn" id="2yiuz3qrJGF" role="3cpWs9">
            <property role="3TUv4t" value="false" />
            <property role="TrG5h" value="v" />
            <node concept="3uibUv" id="2yiuz3qrJGH" role="1tU5fm">
              <ref role="3uigEE" to="r7oa:~PVector" resolve="PVector" />
            </node>
            <node concept="2YIFZM" id="2yiuz3qrJIe" role="33vP2m">
              <ref role="1Pybhc" to="r7oa:~PVector" resolve="PVector" />
              <ref role="37wK5l" to="r7oa:~PVector.sub(processing.core.PVector,processing.core.PVector):processing.core.PVector" resolve="sub" />
              <node concept="37vLTw" id="2yiuz3qrJGJ" role="37wK5m">
                <ref role="3cqZAo" node="2yiuz3qrJFg" resolve="a" />
              </node>
              <node concept="37vLTw" id="2yiuz3qrJGK" role="37wK5m">
                <ref role="3cqZAo" node="2yiuz3qrJFj" resolve="b" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qrJGL" role="3cqZAp">
          <node concept="2OqwBi" id="2yiuz3qrJIi" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qrJIh" role="2Oq$k0">
              <ref role="3cqZAo" node="2yiuz3qrJGF" resolve="v" />
            </node>
            <node concept="liA8E" id="2yiuz3qrJIj" role="2OqNvi">
              <ref role="37wK5l" to="r7oa:~PVector.div(float):processing.core.PVector" resolve="div" />
              <node concept="3cmrfG" id="2yiuz3qrJGN" role="37wK5m">
                <property role="3cmrfH" value="3" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2yiuz3qrJGO" role="3cqZAp">
          <node concept="2OqwBi" id="2yiuz3qrJIn" role="3clFbG">
            <node concept="37vLTw" id="2yiuz3qrJIm" role="2Oq$k0">
              <ref role="3cqZAo" node="2yiuz3qrJGF" resolve="v" />
            </node>
            <node concept="liA8E" id="2yiuz3qrJIo" role="2OqNvi">
              <ref role="37wK5l" to="r7oa:~PVector.add(processing.core.PVector):processing.core.PVector" resolve="add" />
              <node concept="37vLTw" id="2yiuz3qrJGQ" role="37wK5m">
                <ref role="3cqZAo" node="2yiuz3qrJFj" resolve="b" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="2yiuz3qrJGR" role="3cqZAp">
          <node concept="37vLTw" id="2yiuz3qrJGS" role="3cqZAk">
            <ref role="3cqZAo" node="2yiuz3qrJGF" resolve="v" />
          </node>
        </node>
      </node>
      <node concept="3uibUv" id="2yiuz3qrJGT" role="3clF45">
        <ref role="3uigEE" to="r7oa:~PVector" resolve="PVector" />
      </node>
    </node>
  </node>
  <node concept="38mkTz" id="1LhviqSwQiw">
    <property role="TrG5h" value="PVectorConstructor" />
    <node concept="38mkTw" id="1LhviqSwQlT" role="1oLirQ">
      <property role="TrG5h" value="PVectorConstructorXY" />
      <property role="1_lXKI" value="[" />
      <property role="1_lXKJ" value="]" />
      <property role="1_lXKL" value="," />
      <property role="OYnhT" value="Create a new PVector(x,y)" />
      <node concept="3uibUv" id="1LhviqSwQVS" role="1_lXKH">
        <ref role="3uigEE" to="r7oa:~PVector" resolve="PVector" />
      </node>
      <node concept="3XUIV8" id="1LhviqSwQL9" role="3XUO1_">
        <node concept="3XUO1F" id="1LhviqSwQLa" role="3XUO1j">
          <property role="TrG5h" value="x" />
          <node concept="10OMs4" id="1LhviqSwQL7" role="3XUQxq" />
        </node>
        <node concept="3XUO1F" id="1LhviqSwQN6" role="3XUO1j">
          <property role="TrG5h" value="y" />
          <node concept="10OMs4" id="1LhviqSwQO4" role="3XUQxq" />
        </node>
      </node>
      <node concept="3clFbS" id="1LhviqSwQlW" role="2VODD2">
        <node concept="3clFbF" id="1LhviqSwR5J" role="3cqZAp">
          <node concept="2ShNRf" id="1LhviqSwR5H" role="3clFbG">
            <node concept="1pGfFk" id="1LhviqSwRbn" role="2ShVmc">
              <ref role="37wK5l" to="r7oa:~PVector.&lt;init&gt;(float,float)" resolve="PVector" />
              <node concept="MIHzR" id="1LhviqSwRd6" role="37wK5m">
                <ref role="3XVpon" node="1LhviqSwQLa" resolve="x" />
              </node>
              <node concept="MIHzR" id="1LhviqSwRfw" role="37wK5m">
                <ref role="3XVpon" node="1LhviqSwQN6" resolve="y" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="38mkTw" id="1LhviqSwRhV" role="1oLirQ">
      <property role="TrG5h" value="PVectorConstructorXYZ" />
      <property role="1_lXKI" value="[" />
      <property role="1_lXKJ" value="]" />
      <property role="1_lXKL" value="," />
      <property role="OYnhT" value="Create a new PVector(x,y,z)" />
      <node concept="3uibUv" id="1LhviqSwRhW" role="1_lXKH">
        <ref role="3uigEE" to="r7oa:~PVector" resolve="PVector" />
      </node>
      <node concept="3XUIV8" id="1LhviqSwRhX" role="3XUO1_">
        <node concept="3XUO1F" id="1LhviqSwRhY" role="3XUO1j">
          <property role="TrG5h" value="x" />
          <node concept="10OMs4" id="1LhviqSwRhZ" role="3XUQxq" />
        </node>
        <node concept="3XUO1F" id="1LhviqSwRi0" role="3XUO1j">
          <property role="TrG5h" value="y" />
          <node concept="10OMs4" id="1LhviqSwRi1" role="3XUQxq" />
        </node>
        <node concept="3XUO1F" id="1LhviqSwRtl" role="3XUO1j">
          <property role="TrG5h" value="z" />
          <node concept="10OMs4" id="1LhviqSwRtv" role="3XUQxq" />
        </node>
      </node>
      <node concept="3clFbS" id="1LhviqSwRi2" role="2VODD2">
        <node concept="3clFbF" id="1LhviqSwRi3" role="3cqZAp">
          <node concept="2ShNRf" id="1LhviqSwRi4" role="3clFbG">
            <node concept="1pGfFk" id="1LhviqSwRi5" role="2ShVmc">
              <ref role="37wK5l" to="r7oa:~PVector.&lt;init&gt;(float,float,float)" resolve="PVector" />
              <node concept="MIHzR" id="1LhviqSwRi6" role="37wK5m">
                <ref role="3XVpon" node="1LhviqSwRhY" resolve="x" />
              </node>
              <node concept="MIHzR" id="1LhviqSwRi7" role="37wK5m">
                <ref role="3XVpon" node="1LhviqSwRi0" resolve="y" />
              </node>
              <node concept="MIHzR" id="1LhviqSwRxt" role="37wK5m">
                <ref role="3XVpon" node="1LhviqSwRtl" resolve="z" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="38mkTw" id="1LhviqSwR$W" role="1oLirQ">
      <property role="TrG5h" value="PVectorConstructor" />
      <property role="1_lXKI" value="[" />
      <property role="1_lXKJ" value="]" />
      <property role="OYnhT" value="Create a new PVector()" />
      <node concept="3uibUv" id="1LhviqSwR$X" role="1_lXKH">
        <ref role="3uigEE" to="r7oa:~PVector" resolve="PVector" />
      </node>
      <node concept="3XUIV8" id="1LhviqSwR$Y" role="3XUO1_" />
      <node concept="3clFbS" id="1LhviqSwR_5" role="2VODD2">
        <node concept="3clFbF" id="1LhviqSwR_6" role="3cqZAp">
          <node concept="2ShNRf" id="1LhviqSwR_7" role="3clFbG">
            <node concept="1pGfFk" id="1LhviqSwROp" role="2ShVmc">
              <ref role="37wK5l" to="r7oa:~PVector.&lt;init&gt;()" resolve="PVector" />
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
</model>

